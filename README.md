# miniCode()

[![GPL enforced badge](https://img.shields.io/badge/GPL-enforced-blue.svg "This project enforces the GPL.")](https://gplenforced.org)

Developmemnt is temporarily paused, since I'm working on improving LineageOS's SnapdragonCamera
(I'm not a maintainer, but I'm doing some work)

## miniCode() is a C++/C/Python 3 IDE for Android.


miniCode() features syntax highlighting, type (and operator) autocomplete (C++/C only), git integration,
the LLVM/Clang compiler infrastructure, an integrated manual pager, makefile support
and a feature-packed editor and file manager.

miniCode() is fully free software (as in freedom and beer), and is licensed
under the terms of version 3 (or later) of the GNU General Public License.

# Cons

* miniCode() lacks variable autocomplete and error highlighting, which will
  be added in a future release.


# Licenses

  See COPYING

# The mysterious tarballs

  the "mysterious tarballs" contain the compiler, and several other toolchains, such as make and file.
  the manuals (source will be made avaliable) are part of the linux man-pages project. simply format them and pipe through col -b.
  build the compiler with termux-packages and then extract the resulting debian packages.

--------

## Find me elsewhere: 

* Reddit: [u/oldosfan](https://old.reddit.com/u/oldosfan). **To avoid running non-free Javascript, use a free Reddit client such as Slide.**
* GitHub (abandoned): [oldosfan](https://github.com/oldosfan)


### Patent trolls attention:

    TL; DR:
    This is an important announcement from the Ministry of Love:
        Patents have been declared immoral and no longer existed.

I do not repond to software patent threats. I will not remove functionality or pay
a licensing fee for software patents, as they are immoral. You can try to sue me, that is, if you
can find out who I am.

I also commit to never patenting original/preixisting software.
