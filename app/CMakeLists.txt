
cmake_minimum_required(VERSION 3.4.1)
add_library(N SHARED src/main/cpp/native.c)
add_library(P SHARED src/main/cpp/libmpty/mpty.c)
add_library(N2 SHARED src/main/cpp/pty.c)
add_executable(libmount.so src/main/cpp/mount.c)

# Android Studio CMake does not pack stl shared libraries, so app needs to pack
# the right shared lib into APK. The following code find right stl type and copy
# the needed shared lib into app's app/src/main/jniLibs, android studio assembles
# it into the final APK
# Helper function to retrieve shared stl path and name in NDK
# stl_path: the path to the NDK's shared lib path; empty if not using shared stl
function(get_stl_info stl_path stl_name)
    # assume app not uses shared stl lib
    set(${stl_path} "" PARENT_SCOPE)
    if(NOT ${ANDROID_STL} MATCHES "_shared")
        return()
    endif()

    # using shared lib, config lib name and path
    if("${ANDROID_STL}" MATCHES "c\\\+\\\+_")
# app uses c++_shared for stl type
set(stlPath "llvm-libc++/libs/${ANDROID_ABI}")
set(stlName "libc++_shared.so")
elseif(${ANDROID_STL} MATCHES "gnustl_")
set(stlPath "gnu-libstdc++/4.9/libs/${ANDROID_ABI}")
set(stlName "libgnustl_shared.so")
else()
# this sample not supporting other stl types
message(FATAL_ERROR "Not Suppored STL type: ${ANDROID_STL}")
return()
endif()

set(${stl_path} ${ANDROID_NDK}/sources/cxx-stl/${stlPath} PARENT_SCOPE)
set(${stl_name} ${stlName} PARENT_SCOPE)
endfunction()

# force copying needed shared stl lib into ${project}/app/src/main/jniLibs
# so it will be packed into APK
get_stl_info(ndk_stl_path  ndk_stl_name)
if(NOT ${ndk_stl_path} STREQUAL "")
set(jniLibs_dir "${CMAKE_CURRENT_SOURCE_DIR}/../jniLibs")
add_custom_command(TARGET P PRE_BUILD
COMMAND "${CMAKE_COMMAND}" -E
copy ${ndk_stl_path}/${ndk_stl_name}
"${jniLibs_dir}/${ANDROID_ABI}/${ndk_stl_name}"
COMMENT "Copying Shared library to the packing directory")
endif()
find_library(log-lib log)
target_link_libraries(N P ${log-lib})
