/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.parser;

public class CStyleLiteral implements CStyleValue
{
    private LiteralType type;
    private Object value;

    public CStyleLiteral(LiteralType type, Object value)
    {
        this.type = type;
        this.value = value;
    }

    public Object getValue()
    {
        return value;
    }

    public LiteralType getType()
    {
        return type;
    }

    public enum LiteralType
    {
        UINT, INT, ULONG, LONG, USHORT, SHORT, UCHAR, CHAR, STRING
    }
}
