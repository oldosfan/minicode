/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.parser.c;

import org.apache.commons.io.input.CharSequenceInputStream;

import java.nio.charset.Charset;

import javacc_parsers.c.CParser;
import javacc_parsers.c.ParseException;
import low.minicode.wrappers.data.tree.Node;
import low.minicode.parser.CStyleDeclaration;

public class CParserGlue
{
    private Node<CStyleDeclaration> parse(String sourceText) throws ParseException
    {
        CParser parser = new CParser(new CharSequenceInputStream(sourceText, Charset.defaultCharset(), 1024));
        parser.TranslationUnit();
        return null;
    }
}
