/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.parser;

public class CPrimitives
{
    public static final CStyleType INTEGER = new CPrimitiveInteger();
    public static final CStyleType UNSIGNED_INTEGER = new CPrimitiveUnsignedInteger();
    public static final CStyleType CHARACTER = new CPrimitiveCharacter();
    public static final CStyleType UNSIGNED_CHARACTER = new CPrimitiveUnsignedCharacter();
    public static final CStyleType SHORT = new CPrimitiveShort();
    public static final CStyleType UNSIGNED_SHORT = new CPrimitiveUnsignedShort();
    public static final CStyleType LONG = new CPrimitiveLong();
    public static final CStyleType UNSIGNED_LONG = new CPrimitiveUnsignedLong();
    public static final CStyleType LONG_LONG = new CPrimitiveLongLong();
    public static final CStyleType UNSIGNED_LONG_LONG = new CPrimitiveUnsignedLongLong();

    public static class CPrimitiveInteger implements CStyleType
    {
    }

    public static class CPrimitiveUnsignedInteger implements CStyleType
    {
    }

    public static class CPrimitiveCharacter implements CStyleType
    {
    }

    public static class CPrimitiveUnsignedCharacter implements CStyleType
    {
    }

    public static class CPrimitiveShort implements CStyleType
    {
    }

    public static class CPrimitiveUnsignedShort implements CStyleType
    {
    }

    public static class CPrimitiveLong implements CStyleType
    {
    }

    public static class CPrimitiveUnsignedLong implements CStyleType
    {
    }

    public static class CPrimitiveLongLong implements CStyleType
    {
    }

    public static class CPrimitiveUnsignedLongLong implements CStyleType
    {
    }

    public abstract static class CSpecialPrimitiveUnion extends CStyleDeclaration implements CStyleType
    {
    }

    public abstract static class CSpecialPrimitiveStructure extends CStyleDeclaration implements CStyleType
    {
    }
}
