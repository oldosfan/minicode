/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.tokenizer.c;

import android.widget.MultiAutoCompleteTextView;

public class CLanguageTokenizer implements MultiAutoCompleteTextView.Tokenizer
{
    @Override
    public int findTokenStart(CharSequence text, int cursor)
    {
        int i = cursor;
        while (i > 0 && text.charAt(i - 1) != '\n' && text.charAt(i - 1) != ' ' && text.charAt(i - 1) != '\t' && text.charAt(i - 1) != ',' && text.charAt(i - 1) != '(' && text.charAt(i - 1) != '}' && text.charAt(i - 1) != '{')
        {
            --i;
        }
        return i;
    }

    @Override
    public int findTokenEnd(CharSequence text, int cursor)
    {
        int i = cursor;
        int l = text.length();
        while (i < l)
        {
            if (text.charAt(i) != '\n' && text.charAt(i) != ' ' && text.charAt(i) != '\t' && text.charAt(i) != ',' && text.charAt(i) != '(' && text.charAt(i) != '}' && text.charAt(i) != '{')
            {
                ++i;
            }
            else
            {
                return i;
            }
        }
        return 0;
    }

    @Override
    public CharSequence terminateToken(CharSequence text)
    {
        if ((text.charAt(text.length() - 1) == '\n') || (text.charAt(text.length() - 1) == '\t') || (text.charAt(text.length() - 1) == ' ') || (text.charAt(text.length() - 1) == ',') || (text.charAt(text.length() - 1) == '(') || (text.charAt(text.length() - 1) == '}') || (text.charAt(text.length() - 1) == '{'))
        {
            return text;
        }
        else
        {
            return text + " ";
        }
    }
}
