/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.git;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.DeleteTagCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTag;
import org.eclipse.jgit.revwalk.RevWalk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.Selection;
import low.minicode.application.Minicode;

import static org.eclipse.jgit.diff.DiffEntry.DEV_NULL;

public class TagListAdapter extends RecyclerView.Adapter<TagListAdapter.ViewHolder>
{
    private static final Object LOCK = new Object();
    private Repository repository;
    private Fragment fragment;
    private ItemTouchHelper touchHelper;
    private Handler handler;
    private Git git;
    private HandlerThread handlerThread;
    private List<TagHolder> tagHolders = new ArrayList<>();
    private int paginationCount = 1;
    private int totalItemCount;
    private boolean flag;

    public TagListAdapter(Repository repository, Fragment fragment)
    {
        this.repository = repository;
        this.fragment = fragment;
        this.touchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
        this.git = new Git(repository);
        this.handlerThread = new HandlerThread("HandlerThread(TagListAdapter)");
        this.handlerThread.start();
        this.handler = new Handler(handlerThread.getLooper());
        Log.d(getClass().getName(), "CommitListAdapter: handler non-null now");
        RevWalk walk = new RevWalk(repository);
        Iterable<Ref> logs;
        try
        {
            logs = git.tagList().call();
        }
        catch (GitAPIException e)
        {
            e.printStackTrace();
            return;
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            return;
        }
        for (Ref ref : logs)
        {
            try
            {
                this.tagHolders.add(new TagHolder(walk.parseTag(ref.getObjectId()), ref, ref.getName()));
            }
            catch (IncorrectObjectTypeException e)
            {
                this.tagHolders.add(new TagHolder(null, ref, ref.getName()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        totalItemCount = tagHolders.size();
        try
        {
            this.tagHolders = this.tagHolders.subList(0, 20 * paginationCount);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
        walk.dispose();
        Log.d(getClass().getName(), tagHolders.toString());

    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout.basic_tag, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void notifyDatasetChangedA()
    {
        this.tagHolders.clear();
        this.flag = true;
        RevWalk walk = new RevWalk(repository);
        Iterable<Ref> logs;
        try
        {
            logs = git.tagList().call();
        }
        catch (GitAPIException e)
        {
            e.printStackTrace();
            return;
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            return;
        }
        for (Ref ref : logs)
        {
            try
            {
                this.tagHolders.add(new TagHolder(walk.parseTag(ref.getObjectId()), ref, ref.getName()));
            }
            catch (IncorrectObjectTypeException e)
            {
                this.tagHolders.add(new TagHolder(null, ref, ref.getName()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        totalItemCount = tagHolders.size();
        try
        {
            this.tagHolders = this.tagHolders.subList(0, 20 * paginationCount);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
        if (20 * paginationCount >= totalItemCount)
        {
            notifyDataSetChanged();
            this.flag = false;
            return;
        }
        try
        {
            this.tagHolders = this.tagHolders.subList(0, 20 * paginationCount);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
        walk.dispose();
        notifyDataSetChanged();
        this.flag = false;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        touchHelper.attachToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() & !flag)
                {
                    ++paginationCount;
                    handler.postDelayed(() -> recyclerView.post(() ->
                    {
                        if (fragment.getView() != null && fragment.getView().findViewById(R.id.branches_srl) != null && fragment.getView().findViewById(R.id.branches_srl) instanceof SwipeRefreshLayout)
                        {
                            fragment.getView().findViewById(R.id.branches_srl).setVisibility(View.VISIBLE);
                            ((SwipeRefreshLayout) fragment.getView().findViewById(R.id.branches_srl)).setRefreshing(true);
                        }
                        notifyDatasetChangedA();
                        recyclerView.postDelayed(() ->
                        {
                            if (fragment.getView() != null && fragment.getView().findViewById(R.id.branches_srl) != null && fragment.getView().findViewById(R.id.branches_srl) instanceof SwipeRefreshLayout)
                            {
                                ((SwipeRefreshLayout) fragment.getView().findViewById(R.id.branches_srl)).setRefreshing(false);
                            }
                        }, 500);

                    }), 100);
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        handler.post(() ->
        {
            new Thread()
            {
                @Override
                public void run()
                {
                    RevWalk walk = new RevWalk(repository);
                    RevTag tag;
                    RevCommit commit;
                    try
                    {
                        tag = tagHolders.get(i).tag;
                    }
                    catch (IndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                        return;
                    }
                    if (tag != null)
                    {
                        try
                        {
                            commit = walk.parseCommit(tag.getObject());
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                            return;
                        }
                    }
                    else
                    {
                        try
                        {
                            commit = walk.parseCommit(tagHolders.get(i).getRef().getObjectId());
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                            return;
                        }
                    }
                    walk.dispose();
                    String name = tag == null ? tagHolders.get(i).getName().split("refs/tags/")[1] : tag.getTagName();
                    String shortMessage = commit.getShortMessage();
                    String id = commit.getId().abbreviate(8).name() + String.format("\n%s", commit.getAuthorIdent().toExternalString());
                    if (viewHolder.mTextHeading.getText().toString().equals(shortMessage) && viewHolder.mTextContent.getText().toString().equals(id))
                    {
                        return;
                    }
                    viewHolder.mTextContent.postDelayed(() ->
                    {
                        viewHolder.mImageButton.setOnClickListener(v ->
                        {
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity(), R.style.Theme_MaterialComponents_Light_Dialog);
                            builder.setTitle(name);
                            ScrollView scrollView = (ScrollView) LayoutInflater.from(Minicode.context).inflate(R.layout._scrollview_mh, null);
                            scrollView.setBackgroundResource(R.drawable.roundable_sharp);
                            TextView textView = new TextView(Minicode.context);
                            textView.setPadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, Minicode.context.getResources().getDisplayMetrics()), 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, Minicode.context.getResources().getDisplayMetrics()), 0);
                            textView.setTypeface(Minicode.theMinicode.getEditorTypeface());
                            scrollView.addView(textView);
                            builder.setView(scrollView);
                            StringBuilder b = new StringBuilder();
                            if (tag == null)
                            {
                                b.append(String.format("Unannotated tag %s\n    -> %s", name, commit.getId().getName()));
                            }
                            else
                            {
                                b.append(name).append('\n').append("Author: ").append(tag.getTaggerIdent().toExternalString()).append('\n').append("Message: \n").append(tag.getFullMessage()).append("\n").append("Target: ").append(commit.getId().getName());
                            }
                            textView.setText(b);
                            builder.setPositiveButton(R.string.ok, (dialog, which) ->
                            {
                            });
                            builder.show();
                        });
                        viewHolder.mTextContent1.setText(shortMessage);
                        viewHolder.mTextHeading.setText(name);
                        viewHolder.mTextContent.setText(id);
                        viewHolder.mCommitButton.setOnClickListener(_v ->
                        {
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                            builder.setCancelable(false);
                            View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout._dialog_commit, null);
                            v.findViewById(R.id.cherry_pick).setVisibility(View.GONE);
                            builder.setView(v);
                            builder.setPositiveButton(R.string.ok, (dialog, which) -> notifyDatasetChangedA());
                            builder.setNeutralButton(R.string.checkout, ((dialog, which) ->
                            {
                                Handler h = new Handler();
                                AlertDialog alertDialog = new AlertDialog.Builder(fragment.getActivity()).setMessage(R.string.checking_out).create();
                                alertDialog.setCancelable(false);
                                alertDialog.show();
                                new Thread()
                                {
                                    @Override
                                    public void run()
                                    {
                                        try
                                        {
                                            CheckoutCommand checkoutCommand = git.checkout();
                                            checkoutCommand.setName(commit.getName());
                                            checkoutCommand.setStartPoint(commit);
                                            checkoutCommand.setForce(true);
                                            checkoutCommand.call();
                                            sleep(100);
                                            h.postDelayed(TagListAdapter.this::notifyDatasetChangedA, 40);
                                            h.post(alertDialog::cancel);
                                        }
                                        catch (GitAPIException | JGitInternalException | InterruptedException e)
                                        {
                                            e.printStackTrace();
                                            h.post(alertDialog::cancel);
                                            h.post(() -> new MaterialAlertDialogBuilder(fragment.getActivity()).setPositiveButton(R.string.ok, (dialog, which) ->
                                            {
                                            }).setMessage(R.string.checkout_failed).setTitle(R.string.error).show());
                                            h.post(TagListAdapter.this::notifyDatasetChangedA);
                                        }
                                    }
                                }.start();
                            }));
                            builder.setTitle(commit.getId().getName());
                            builder.show();
                            TextView commitText = v.findViewById(R.id.commit_description);
                            StringBuilder stringBuilder = new StringBuilder().append("commit ").append(commit.getId().getName()).append('\n').append(new String(commit.getRawBuffer()));
                            commitText.setText(stringBuilder);
                            RecyclerView commitRecycler = v.findViewById(R.id.recycler_commit_files);
                            CommitFilesSubAdapter subAdapter = new CommitFilesSubAdapter(commit, repository, fragment);
                            commitRecycler.setLayoutManager(new LinearLayoutManager(Minicode.context));
                            commitRecycler.setAdapter(subAdapter);
                        });
                        viewHolder.itemView.setClickable(true);
                        viewHolder.itemView.setOnClickListener(_v ->
                        {
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                            builder.setCancelable(false);
                            View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout._dialog_commit, null);
                            builder.setView(v);
                            v.findViewById(R.id.cherry_pick).setVisibility(View.GONE);
                            builder.setPositiveButton(R.string.ok, (dialog, which) -> notifyDatasetChangedA());
                            builder.setNeutralButton(R.string.checkout, ((dialog, which) ->
                            {
                                Handler h = new Handler();
                                AlertDialog alertDialog = new AlertDialog.Builder(fragment.getActivity()).setMessage(R.string.checking_out).create();
                                alertDialog.setCancelable(false);
                                alertDialog.show();
                                new Thread()
                                {
                                    @Override
                                    public void run()
                                    {
                                        try
                                        {
                                            CheckoutCommand checkoutCommand = git.checkout();
                                            checkoutCommand.setName(commit.getName());
                                            checkoutCommand.setStartPoint(commit);
                                            checkoutCommand.setForce(true);
                                            checkoutCommand.call();
                                            sleep(100);
                                            h.postDelayed(TagListAdapter.this::notifyDatasetChangedA, 40);
                                            h.post(alertDialog::cancel);
                                        }
                                        catch (GitAPIException | JGitInternalException | InterruptedException e)
                                        {
                                            e.printStackTrace();
                                            h.post(alertDialog::cancel);
                                            h.post(() -> new MaterialAlertDialogBuilder(fragment.getActivity()).setPositiveButton(R.string.ok, (dialog, which) ->
                                            {
                                            }).setMessage(R.string.checkout_failed).setTitle(R.string.error).show());
                                            h.post(TagListAdapter.this::notifyDatasetChangedA);
                                        }
                                    }
                                }.start();
                            }));
                            builder.setTitle(commit.getId().getName());
                            builder.show();
                            TextView commitText = v.findViewById(R.id.commit_description);
                            StringBuilder stringBuilder = new StringBuilder().append("commit ").append(commit.getId().getName()).append('\n').append(new String(commit.getRawBuffer()));
                            commitText.setText(stringBuilder);
                            RecyclerView commitRecycler = v.findViewById(R.id.recycler_commit_files);
                            CommitFilesSubAdapter subAdapter = new CommitFilesSubAdapter(commit, repository, fragment);
                            commitRecycler.setLayoutManager(new LinearLayoutManager(Minicode.context));
                            commitRecycler.setAdapter(subAdapter);
                        });
                    }, 200);
                }
            }.start();
        });
    }

    @Override
    public int getItemCount()
    {
        return tagHolders.size();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        this.handlerThread.quitSafely();

    }

    public static class TagHolder
    {
        @Nullable
        private final RevTag tag;
        private final Ref ref;
        private final String name;

        public TagHolder(@Nullable RevTag tag, Ref ref, String name)
        {
            this.tag = tag;
            this.ref = ref;
            this.name = name;
        }

        @Nullable
        public RevTag getTag()
        {
            return tag;
        }

        public String getName()
        {
            return name;
        }

        public Ref getRef()
        {
            return ref;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextHeading, mTextContent1, mTextContent;
        public ImageButton mImageButton;
        public LinearLayout mLinearLayout;
        public ImageButton mCommitButton;

        public ViewHolder(View v)
        {
            super(v);
            mTextHeading = v.findViewById(R.id.basic_about_text_header);
            mTextContent = v.findViewById(R.id.basic_about_text_content);
            mTextContent1 = v.findViewById(R.id.basic_about_text_content_line_first);
            mLinearLayout = (LinearLayout) v;
            mCommitButton = v.findViewById(R.id.imagebutton_view_commit);
            mImageButton = itemView.findViewById(R.id.view_tag_info);
        }
    }

    static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private TagListAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(TagListAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = Minicode.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(Minicode.context.getResources().getColor(R.color.holo_red_dark));
            icon = Minicode.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            Handler h = new Handler();
            int i = viewHolder.getAdapterPosition();
            DeleteTagCommand deleteTagCommand = mAdapter.git.tagDelete();
            deleteTagCommand.setTags(mAdapter.tagHolders.get(i).ref.getName());
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    deleteTagCommand.call();
                    h.post(() -> mAdapter.notifyDatasetChangedA());
                    h.post(Library::vibrateSuccess);
                }
                catch (GitAPIException e)
                {
                    Library.vibrateError();
                    e.printStackTrace();
                    MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(mAdapter.fragment.getActivity());
                    materialAlertDialogBuilder.setTitle(R.string.error);
                    materialAlertDialogBuilder.setMessage(e.getLocalizedMessage());
                    materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog, which) -> mAdapter.notifyDatasetChangedA());
                    materialAlertDialogBuilder.setCancelable(false);
                    h.post(materialAlertDialogBuilder::show);
                }
            });

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Drawable background = this.background.getConstantState().newDrawable();
            background.mutate();
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }

    public static class CommitFilesSubAdapter extends RecyclerView.Adapter<CommitFilesSubAdapter.ViewHolder>
    {
        private RevCommit commit;
        private Repository repository;
        private Fragment fragment;
        private List<DiffEntry> diffEntries;
        private Drawable mAdd, mRemove, mNeutral;

        public CommitFilesSubAdapter(RevCommit commit, Repository repository, Fragment fragment)
        {
            this.commit = commit;
            this.repository = repository;
            this.fragment = fragment;
            mRemove = Minicode.context.getResources().getDrawable(R.drawable.ic_remove_black_24dp).getConstantState().newDrawable().mutate();
            mRemove.setTint(Minicode.context.getResources().getColor(R.color.holo_red_light));
            mAdd = Minicode.context.getResources().getDrawable(R.drawable.ic_add_white_24dp).getConstantState().newDrawable().mutate();
            mAdd.setTint(Minicode.context.getResources().getColor(R.color.holo_green_light));
            mNeutral = Minicode.context.getResources().getDrawable(R.drawable.ic_add_white_24dp).getConstantState().newDrawable().mutate();
            mAdd.setTint(Minicode.context.getResources().getColor(R.color.holo_orange_light));
            try
            {
                if (commit.getParentCount() != 0)
                {
                    RevWalk walk = new RevWalk(repository);
                    this.diffEntries = Library.commitDiff(walk.parseCommit(commit.getParent(0)), commit, new Git(repository));
                    walk.dispose();
                }
                else
                {
                    this.diffEntries = Library.commitDiff(null, commit, new Git(repository));
                }
            }
            catch (IOException | ArrayIndexOutOfBoundsException e)
            {
                e.printStackTrace();
                this.diffEntries = new ArrayList<>();
            }
            Log.d(getClass().getName(), String.format("CommitFilesSubAdapter: constructor - diffEntries size: %d - entries %s", diffEntries.size(), Arrays.toString(diffEntries.toArray())));
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            return new ViewHolder(LayoutInflater.from(Minicode.context).inflate(R.layout.basic_commit_file, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position)
        {
            DiffEntry diffEntry = diffEntries.get(position);
            switch (diffEntry.getChangeType())
            {
                case ADD:
                    holder.mImageView.setImageDrawable(mAdd);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    break;
                case COPY:
                    holder.mImageView.setImageDrawable(mNeutral);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_orange_dark)));
                    break;
                case DELETE:
                    holder.mImageView.setImageDrawable(mRemove);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_red_light)));
                    break;
                case MODIFY:
                    holder.mImageView.setImageDrawable(mNeutral);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_orange_dark)));
                    break;
                case RENAME:
                    holder.mImageView.setImageResource(R.drawable.ic_redo_black_24dp);
                    break;
            }
            Executors.newSingleThreadExecutor().submit(() ->
            {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DiffFormatter df = new DiffFormatter(byteArrayOutputStream);
                try
                {
                    df.setRepository(repository);
                    df.format(diffEntry);
                    df.flush();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    df.close();
                }
                String data = new String(byteArrayOutputStream.toByteArray());
                Matcher m = Pattern.compile("^([+\\-].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
                int countadd = 0, countless = 0;
                while (m.find())
                {
                    String substring = data.substring(m.start(), m.end());
                    if (substring.matches("^((---)|(\\+\\+\\+)) .*$"))
                    {
                        continue;
                    }
                    if (substring.charAt(0) == '-')
                    {
                        ++countless;
                    }
                    else if (substring.charAt(0) == '+')
                    {
                        ++countadd;
                    }
                    else if (substring.equals("Binary files differ"))
                    {
                        ++countadd;
                    }
                }
                String result = String.format(Minicode.context.getString(R.string.d_add_d_del), countadd, countless);
                Log.d(getClass().getName(), String.format("%s: result: %s", Thread.currentThread().getName(), result));
                holder.mAddDel.post(() -> holder.mAddDel.setText(result));
            });
            holder.mFileName.setText(diffEntry.getOldPath().equals(DEV_NULL) || diffEntry.getOldPath().equals(diffEntry.getNewPath()) ? diffEntry.getNewPath() : String.format("%s -> %s", diffEntry.getOldPath(), diffEntry.getNewPath()));
            holder.v.setOnClickListener(v ->
            {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DiffFormatter df = new DiffFormatter(byteArrayOutputStream);
                try
                {
                    df.setRepository(repository);
                    df.format(diffEntry);
                    df.flush();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    df.close();
                }
                String data = new String(byteArrayOutputStream.toByteArray());
                ScrollView v13 = (ScrollView) LayoutInflater.from(Minicode.context).inflate(R.layout._scrollview_mh, null);
                v13.setBackgroundResource(R.drawable.roundable_sharp);
                TextView textView = new EditText(Minicode.context);
                textView.setEnabled(false);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                textView.setTypeface(Minicode.theMinicode.getEditorTypeface());
                v13.addView(textView);
                textView.setText(data);
                MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity(), R.style.Theme_MaterialComponents_Light_Dialog);
                dialogBuilder.setView(v13);
                dialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                });
                dialogBuilder.show();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    ArrayList<Selection> selections = new ArrayList<>();
                    ArrayList<Integer> flags = new ArrayList<>();
                    Matcher m = Pattern.compile("^([+\\-@].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
                    while (m.find())
                    {
                        String substring = data.substring(m.start(), m.end());
                        if (substring.charAt(0) == '-')
                        {
                            flags.add(1);
                        }
                        else if (substring.charAt(0) == '+')
                        {
                            flags.add(2);
                        }
                        else if (substring.equals("Binary files differ"))
                        {
                            flags.add(3);
                        }
                        else
                        {
                            flags.add(4);
                        }
                        selections.add(new Selection(m.start(), m.end()));
                    }
                    int i = 0;
                    for (Selection iter : selections)
                    {
                        BackgroundColorSpan backgroundColorSpan;
                        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.WHITE);
                        if (flags.get(i).equals(1))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.red));
                        }
                        else if (flags.get(i).equals(2))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_green_light));
                        }
                        else if (flags.get(i).equals(3))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_orange_light));
                        }
                        else
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                        }
                        textView.post(() -> textView.getEditableText().setSpan(backgroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                        textView.post(() -> textView.getEditableText().setSpan(foregroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                        ++i;
                    }

                });
            });
        }

        @Override
        public int getItemCount()
        {
            return diffEntries.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder
        {
            public ImageView mImageView;
            public TextView mFileName, mAddDel;
            public View v;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);
                this.v = itemView;
                this.mImageView = itemView.findViewById(R.id.commit_imageview_file);
                this.mFileName = itemView.findViewById(R.id.commit_file_name);
                this.mAddDel = itemView.findViewById(R.id.add_del_amount);
            }
        }
    }
}