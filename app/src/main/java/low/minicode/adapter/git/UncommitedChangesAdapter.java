/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.git;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.eclipse.jgit.api.AddCommand;
import org.eclipse.jgit.api.CleanCommand;
import org.eclipse.jgit.api.CommitCommand;
import org.eclipse.jgit.api.DiffCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.RmCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.EmptyTreeIterator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.Selection;
import low.minicode.fragments.git.VCSChangesFragment;

import static org.eclipse.jgit.diff.DiffEntry.DEV_NULL;

public class UncommitedChangesAdapter extends RecyclerView.Adapter<UncommitedChangesAdapter.ViewHolder>
{
    private Git git;
    private Repository repository;
    private VCSChangesFragment fragment;
    private List<DiffEntry> diffEntries;
    private Drawable mAdd, mRemove, mNeutral;
    private String diffOutput = "";
    private String[] diffSplit;
    private Handler handler;

    public UncommitedChangesAdapter(Git git, VCSChangesFragment fragment)
    {
        this.repository = git.getRepository();
        this.fragment = fragment;
        this.git = git;
        this.handler = new Handler();
        this.fragment.getmDiscard().setOnClickListener(v ->
        {
            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
            materialAlertDialogBuilder.setTitle(R.string.discard);
            materialAlertDialogBuilder.setMessage(R.string.discard_unstaged_cannot_undone);
            materialAlertDialogBuilder.setPositiveButton(R.string.yes, ((dialog, which) ->
            {
                MaterialAlertDialogBuilder materialAlertDialogBuilder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                materialAlertDialogBuilder1.setMessage(R.string.dicarding);
                materialAlertDialogBuilder.setCancelable(false);
                Dialog alertDialog = materialAlertDialogBuilder1.create();
                alertDialog.show();
                CleanCommand cleanCommand = git.clean();
                cleanCommand.setCleanDirectories(true);
                try
                {
                    cleanCommand.call();
                }
                catch (GitAPIException | JGitInternalException e)
                {
                    e.printStackTrace();
                    alertDialog.dismiss();
                    notifyDatasetChangedA();
                    new MaterialAlertDialogBuilder(fragment.getActivity()).setTitle(R.string.clean_failed).setMessage(e.getLocalizedMessage()).setPositiveButton(R.string.ok, (dialog1, which1) ->
                    {
                    }).show();
                    return;
                }
                ResetCommand resetCommand = git.reset();
                resetCommand.setMode(ResetCommand.ResetType.HARD);
                try
                {
                    resetCommand.call();
                }
                catch (GitAPIException e)
                {
                    alertDialog.dismiss();
                    notifyDatasetChangedA();
                    new MaterialAlertDialogBuilder(fragment.getActivity()).setTitle(R.string.reset_failed).setMessage(e.getLocalizedMessage()).setPositiveButton(R.string.ok, (dialog1, which1) ->
                    {
                    }).show();
                    return;
                }
                alertDialog.dismiss();
                notifyDatasetChangedA();
            }));
            materialAlertDialogBuilder.show();
        });
        this.fragment.getmCommit().setOnClickListener(v ->
        {
            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
            materialAlertDialogBuilder.setTitle(R.string.commit_message);
            EditText editText = new EditText(fragment.getActivity());
            if (diffEntries.size() == 1)
            {
                switch (diffEntries.get(0).getChangeType())
                {
                    case DELETE:
                    {
                        editText.setHint(String.format("Remove %s", diffEntries.get(0).getOldPath()));
                        break;
                    }
                    case RENAME:
                    {
                        editText.setHint(String.format("Rename %s to %s", diffEntries.get(0).getOldPath(), diffEntries.get(0).getNewPath()));
                        break;
                    }
                    case ADD:
                    {
                        editText.setHint(String.format("Create %s", diffEntries.get(0).getNewPath()));
                        break;
                    }
                    case MODIFY:
                    {
                        editText.setHint(String.format("Modify %s", diffEntries.get(0).getOldPath()));
                        break;
                    }
                    case COPY:
                    {
                        editText.setHint(String.format("Copy %s to %s", diffEntries.get(0).getOldPath(), diffEntries.get(0).getNewPath()));
                        break;
                    }
                }
            }
            materialAlertDialogBuilder.setView(editText);
            materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
            {
                if (editText.getText().toString().isEmpty() && (editText.getHint() == null || editText.getHint().length() == 0))
                {
                    Toast.makeText(Minicode.context, R.string.we_need_something_here, Toast.LENGTH_LONG).show();
                    return;
                }
                else if (editText.getHint() != null && editText.getHint().length() > 0 && editText.getText().toString().isEmpty())
                {
                    editText.setText(editText.getHint());
                }
                boolean rm = false, ad = false;
                AddCommand addCommand = git.add();
                RmCommand rmCommand = git.rm();
                for (DiffEntry diffEntry : diffEntries)
                {
                    if (diffEntry.getChangeType() == DiffEntry.ChangeType.DELETE)
                    {
                        rm = true;
                        rmCommand.addFilepattern(diffEntry.getPath(DiffEntry.Side.OLD));
                    }
                    else
                    {
                        ad = true;
                        addCommand.addFilepattern(diffEntry.getPath(DiffEntry.Side.NEW));
                    }
                }
                try
                {
                    if (ad)
                    {
                        addCommand.call();
                    }
                    if (rm)
                    {
                        rmCommand.call();
                    }
                }
                catch (GitAPIException e)
                {
                    e.printStackTrace();
                    MaterialAlertDialogBuilder alertDialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
                    alertDialogBuilder.setTitle(R.string.stage_error);
                    alertDialogBuilder.setMessage(e.getLocalizedMessage());
                    alertDialogBuilder.setPositiveButton(R.string.ok, (dialog2, which2) ->
                    {
                    });
                    alertDialogBuilder.show();
                    return;
                }
                Log.d(getClass().getName(), "Committing...");
                AlertDialog committing = new AlertDialog.Builder(fragment.getActivity()).setMessage(R.string.committing).setCancelable(false).create();
                committing.show();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    CommitCommand commitCommand = git.commit();
                    commitCommand.setMessage(editText.getText().toString());
                    try
                    {
                        commitCommand.call();
                        fragment.getActivity().runOnUiThread(committing::dismiss);
                        Log.d(getClass().getName(), "Commit success");
                        notifyDatasetChangedA();
                    }
                    catch (GitAPIException e)
                    {
                        e.printStackTrace();
                        fragment.getActivity().runOnUiThread(committing::dismiss);
                        MaterialAlertDialogBuilder alertDialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
                        alertDialogBuilder.setTitle(R.string.commit_error);
                        alertDialogBuilder.setMessage(e.getLocalizedMessage());
                        alertDialogBuilder.setPositiveButton(R.string.ok, (dialog2, which2) ->
                        {
                        });
                        editText.post(alertDialogBuilder::show);
                        Log.d(getClass().getName(), "Commit failed");
                        return;
                    }
                });

            });
            materialAlertDialogBuilder.show();

        });
        diffEntries = new LinkedList<>();
        notifyDatasetChangedA();
        mRemove = Minicode.context.getResources().getDrawable(R.drawable.ic_remove_black_24dp).getConstantState().newDrawable().mutate();
        mRemove.setTint(Minicode.context.getResources().getColor(R.color.holo_red_light));
        mAdd = Minicode.context.getResources().getDrawable(R.drawable.ic_add_white_24dp).getConstantState().newDrawable().mutate();
        mAdd.setTint(Minicode.context.getResources().getColor(R.color.holo_green_light));
        mNeutral = Minicode.context.getResources().getDrawable(R.drawable.ic_add_white_24dp).getConstantState().newDrawable().mutate();
        mAdd.setTint(Minicode.context.getResources().getColor(R.color.holo_orange_light));
    }

    public void notifyDatasetChangedA()
    {
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                if (repository.resolve(Constants.HEAD) == null)
                {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    DiffCommand diffCommand = git.diff().setOutputStream(byteArrayOutputStream).setOldTree(new EmptyTreeIterator());
                    diffEntries = diffCommand.call();
                    diffOutput = byteArrayOutputStream.toString();
                    Log.d(getClass().getName(), "UncommitedChangesAdapter: " + diffOutput);
                }
                else
                {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    DiffCommand diffCommand = git.diff().setOutputStream(byteArrayOutputStream).setOldTree(new CanonicalTreeParser(null, git.getRepository().newObjectReader(), repository.resolve("HEAD^{tree}")));
                    diffEntries = diffCommand.call();
                    diffOutput = byteArrayOutputStream.toString();
                    Log.d(getClass().getName(), "UncommitedChangesAdapter: " + diffOutput);
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (GitAPIException e)
            {
                e.printStackTrace();
            }
            diffSplit = diffOutput.split("(?m)^diff --git .*$");
            handler.post(this::notifyDataSetChanged);
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_commit_file_large, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        DiffEntry diffEntry = diffEntries.get(position);
        switch (diffEntry.getChangeType())
        {
            case ADD:
                holder.mImageView.setImageDrawable(mAdd);
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_green_light)));
                break;
            case COPY:
                holder.mImageView.setImageDrawable(mNeutral);
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_orange_dark)));
                break;
            case DELETE:
                holder.mImageView.setImageDrawable(mRemove);
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_red_light)));
                break;
            case MODIFY:
                holder.mImageView.setImageDrawable(mNeutral);
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_orange_dark)));
                break;
            case RENAME:
                holder.mImageView.setImageResource(R.drawable.ic_redo_black_24dp);
                break;
        }
        Executors.newSingleThreadExecutor().submit(() ->
        {
            String data = diffSplit[position + 1];
            Matcher m = Pattern.compile("^([+\\-].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
            int countadd = 0, countless = 0;
            while (m.find())
            {
                String substring = data.substring(m.start(), m.end());
                if (substring.matches("^((---)|(\\+\\+\\+)) .*$"))
                {
                    continue;
                }
                if (substring.charAt(0) == '-')
                {
                    ++countless;
                }
                else if (substring.charAt(0) == '+')
                {
                    ++countadd;
                }
                else if (substring.equals("Binary files differ"))
                {
                    ++countadd;
                }
            }
            String result = String.format(Minicode.context.getString(R.string.d_add_d_del), countadd, countless);
            Log.d(getClass().getName(), String.format("%s: result: %s", Thread.currentThread().getName(), result));
            holder.mAddDel.post(() -> holder.mAddDel.setText(result));
        });
        holder.mFileName.setText(diffEntry.getOldPath().equals(DEV_NULL) || diffEntry.getOldPath().equals(diffEntry.getNewPath()) ? diffEntry.getNewPath() : String.format("%s -> %s", diffEntry.getOldPath(), diffEntry.getNewPath()));
        holder.v.setOnClickListener(v ->
        {
            String data = diffSplit[position + 1];
            ScrollView v13 = (ScrollView) LayoutInflater.from(Minicode.context).inflate(R.layout._scrollview_mh, null);
            v13.setBackgroundResource(R.drawable.roundable_sharp);
            TextView textView = new EditText(Minicode.context);
            textView.setEnabled(false);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            textView.setTypeface(Minicode.theMinicode.getEditorTypeface());
            v13.addView(textView);
            textView.setText(data);
            MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity(), R.style.Theme_MaterialComponents_Light_Dialog);
            dialogBuilder.setView(v13);
            dialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
            {
            });
            dialogBuilder.show();
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    Thread.sleep(100);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                ArrayList<Selection> selections = new ArrayList<>();
                ArrayList<Integer> flags = new ArrayList<>();
                Matcher m = Pattern.compile("^([+\\-@].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
                while (m.find())
                {
                    String substring = data.substring(m.start(), m.end());
                    if (substring.charAt(0) == '-')
                    {
                        flags.add(1);
                    }
                    else if (substring.charAt(0) == '+')
                    {
                        flags.add(2);
                    }
                    else if (substring.equals("Binary files differ"))
                    {
                        flags.add(3);
                    }
                    else
                    {
                        flags.add(4);
                    }
                    selections.add(new Selection(m.start(), m.end()));
                }
                int i = 0;
                for (Selection iter : selections)
                {
                    BackgroundColorSpan backgroundColorSpan;
                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.WHITE);
                    if (flags.get(i).equals(1))
                    {
                        backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.red));
                    }
                    else if (flags.get(i).equals(2))
                    {
                        backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_green_light));
                    }
                    else if (flags.get(i).equals(3))
                    {
                        backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_orange_light));
                    }
                    else
                    {
                        backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                    }
                    textView.post(() -> textView.getEditableText().setSpan(backgroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                    textView.post(() -> textView.getEditableText().setSpan(foregroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                    ++i;
                }

            });
        });
    }

    @Override
    public int getItemCount()
    {
        return diffEntries.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView mImageView;
        public TextView mFileName, mAddDel;
        public View v;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            this.v = itemView;
            this.mImageView = itemView.findViewById(R.id.commit_imageview_file);
            this.mFileName = itemView.findViewById(R.id.commit_file_name);
            this.mAddDel = itemView.findViewById(R.id.add_del_amount);
        }
    }
}
