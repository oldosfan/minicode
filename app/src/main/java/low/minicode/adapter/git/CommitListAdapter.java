/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.git;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CherryPickCommand;
import org.eclipse.jgit.api.CherryPickResult;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.RevertCommand;
import org.eclipse.jgit.api.TagCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.Selection;
import low.minicode.application.Minicode;

import static org.eclipse.jgit.diff.DiffEntry.DEV_NULL;

public class CommitListAdapter extends RecyclerView.Adapter<CommitListAdapter.ViewHolder>
{
    private static final Object LOCK = new Object();
    private Repository repository;
    private Fragment fragment;
    private ItemTouchHelper touchHelper;
    private Handler handler;
    private Git git;
    private HandlerThread handlerThread;
    private List<RevCommit> commits = new ArrayList<>();
    private int paginationCount = 1;
    private int totalItemCount;
    private boolean flag;

    public CommitListAdapter(Repository repository, Fragment fragment)
    {
        this.repository = repository;
        this.fragment = fragment;
        this.touchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
        this.git = new Git(repository);
        this.handlerThread = new HandlerThread("HandlerThread(CommitsListAdapter)");
        this.handlerThread.start();
        this.handler = new Handler(handlerThread.getLooper());
        Log.d(getClass().getName(), "CommitListAdapter: handler non-null now");
        Iterable<RevCommit> logs;
        try
        {
            logs = git.log().add(repository.resolve(repository.getFullBranch())).call();
        }
        catch (GitAPIException e)
        {
            e.printStackTrace();
            return;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return;
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            return;
        }
        for (RevCommit iter : logs)
        {
            this.commits.add(iter);
        }
        totalItemCount = commits.size();
        try
        {
            this.commits = this.commits.subList(0, 20 * paginationCount);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
        Log.d(getClass().getName(), commits.toString());

    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_commit, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void notifyDatasetChangedA()
    {
        this.commits.clear();
        this.flag = true;
        Iterable<RevCommit> logs;
        try
        {
            logs = git.log().add(repository.resolve(repository.getFullBranch())).call();
        }
        catch (GitAPIException e)
        {
            e.printStackTrace();
            return;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return;
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            return;
        }
        for (RevCommit iter : logs)
        {
            this.commits.add(iter);
        }
        totalItemCount = commits.size();
        if (20 * paginationCount >= totalItemCount)
        {
            notifyDataSetChanged();
            this.flag = false;
            return;
        }
        try
        {
            this.commits = this.commits.subList(0, 20 * paginationCount);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
        notifyDataSetChanged();
        this.flag = false;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        touchHelper.attachToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() & !flag)
                {
                    ++paginationCount;
                    handler.postDelayed(() -> recyclerView.post(() ->
                    {
                        if (fragment.getView() != null && fragment.getView().findViewById(R.id.branches_srl) != null && fragment.getView().findViewById(R.id.branches_srl) instanceof SwipeRefreshLayout)
                        {
                            fragment.getView().findViewById(R.id.branches_srl).setVisibility(View.VISIBLE);
                            ((SwipeRefreshLayout) fragment.getView().findViewById(R.id.branches_srl)).setRefreshing(true);
                        }
                        notifyDatasetChangedA();
                        recyclerView.postDelayed(() ->
                        {
                            if (fragment.getView() != null && fragment.getView().findViewById(R.id.branches_srl) != null && fragment.getView().findViewById(R.id.branches_srl) instanceof SwipeRefreshLayout)
                            {
                                ((SwipeRefreshLayout) fragment.getView().findViewById(R.id.branches_srl)).setRefreshing(false);
                            }
                        }, 500);

                    }), 100);
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        handler.post(() ->
        {
            new Thread()
            {
                @Override
                public void run()
                {
                    RevCommit commit;
                    try
                    {
                        commit = commits.get(i);
                    }
                    catch (IndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                        return;
                    }
                    String shortMessage = commit.getShortMessage();
                    String id = commit.getId().abbreviate(8).name() + String.format("\n%s", commit.getAuthorIdent().toExternalString());
                    if (viewHolder.mTextHeading.getText().toString().equals(shortMessage) && viewHolder.mTextContent.getText().toString().equals(id))
                    {
                        return;
                    }
                    viewHolder.mTextContent.postDelayed(() ->
                    {
                        viewHolder.mTextHeading.setText(shortMessage);
                        viewHolder.mTextContent.setText(id);
                        viewHolder.mCommitButton.setOnClickListener(_v ->
                        {
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                            builder.setCancelable(false);
                            View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout._dialog_commit, null);
                            builder.setView(v);
                            builder.setPositiveButton(R.string.ok, (dialog, which) -> notifyDatasetChangedA());
                            builder.setNegativeButton(R.string.tag, ((dialog, which) ->
                            {
                                MaterialAlertDialogBuilder selection = new MaterialAlertDialogBuilder(fragment.getActivity());
                                selection.setItems(R.array.tag_options, ((dialog1, which1) ->
                                {
                                    if (which1 == 0)
                                    {
                                        Library.askDialogString(true, Minicode.context.getString(R.string.tag), null, fragment.getActivity(), params ->
                                        {
                                            if (params[0].isEmpty())
                                            {
                                                Toast.makeText(Minicode.context, R.string.we_need_something_here, Toast.LENGTH_LONG).show();
                                                return null;
                                            }
                                            else
                                            {
                                                Executors.newSingleThreadExecutor().submit(() ->
                                                {
                                                    TagCommand tagCommand = git.tag();
                                                    tagCommand.setAnnotated(false);
                                                    tagCommand.setName(params[0]);
                                                    tagCommand.setObjectId(commit);
                                                    try
                                                    {
                                                        tagCommand.call();
                                                    }
                                                    catch (GitAPIException e)
                                                    {
                                                        MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                                                        builder1.setTitle(R.string.error);
                                                        builder1.setMessage(e.getLocalizedMessage());
                                                        builder1.setPositiveButton(R.string.ok, ((dialog2, which2) ->
                                                        {
                                                        }));
                                                        viewHolder.itemView.post(builder1::show);
                                                    }
                                                });
                                            }
                                            return null;
                                        });
                                    }
                                    else
                                    {
                                        Library.askDialogString(true, Minicode.context.getString(R.string.tag), null, fragment.getActivity(), params ->
                                        {
                                            if (params[0].isEmpty())
                                            {
                                                Toast.makeText(Minicode.context, R.string.we_need_something_here, Toast.LENGTH_LONG).show();
                                                return null;
                                            }
                                            else
                                            {
                                                TagCommand tagCommand = git.tag();
                                                tagCommand.setAnnotated(true);
                                                tagCommand.setName(params[0]);
                                                tagCommand.setObjectId(commit);
                                                Library.askDialogString(true, Minicode.context.getString(R.string.tag_message), null, fragment.getActivity(), params1 ->
                                                {
                                                    tagCommand.setMessage(params1[0]);
                                                    Executors.newSingleThreadExecutor().submit(() ->
                                                    {
                                                        try
                                                        {
                                                            tagCommand.call();
                                                        }
                                                        catch (GitAPIException e)
                                                        {
                                                            MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                                                            builder1.setTitle(R.string.error);
                                                            builder1.setMessage(e.getLocalizedMessage());
                                                            builder1.setPositiveButton(R.string.ok, ((dialog2, which2) ->
                                                            {
                                                            }));
                                                            viewHolder.itemView.post(builder1::show);
                                                        }
                                                    });
                                                    return null;
                                                });
                                            }
                                            return null;
                                        });
                                    }
                                }));
                                selection.show();
                            }));
                            builder.setNeutralButton(R.string.checkout, ((dialog, which) ->
                            {
                                Handler h = new Handler();
                                AlertDialog alertDialog = new AlertDialog.Builder(fragment.getActivity()).setMessage(R.string.checking_out).create();
                                alertDialog.setCancelable(false);
                                alertDialog.show();
                                new Thread()
                                {
                                    @Override
                                    public void run()
                                    {
                                        try
                                        {
                                            CheckoutCommand checkoutCommand = git.checkout();
                                            checkoutCommand.setName(commit.getName());
                                            checkoutCommand.setStartPoint(commit);
                                            checkoutCommand.setForce(true);
                                            checkoutCommand.call();
                                            sleep(100);
                                            h.postDelayed(CommitListAdapter.this::notifyDatasetChangedA, 40);
                                            h.post(alertDialog::cancel);
                                        }
                                        catch (GitAPIException | JGitInternalException | InterruptedException e)
                                        {
                                            e.printStackTrace();
                                            h.post(alertDialog::cancel);
                                            h.post(() -> new MaterialAlertDialogBuilder(fragment.getActivity()).setPositiveButton(R.string.ok, (dialog, which) ->
                                            {
                                            }).setMessage(R.string.checkout_failed).setTitle(R.string.error).show());
                                            h.post(CommitListAdapter.this::notifyDatasetChangedA);
                                        }
                                    }
                                }.start();
                            }));
                            builder.setTitle(commit.getId().getName());
                            builder.show();
                            TextView commitText = v.findViewById(R.id.commit_description);
                            MaterialButton cherryPick = v.findViewById(R.id.cherry_pick);
                            cherryPick.setOnClickListener(v29 ->
                            {
                                MaterialAlertDialogBuilder cherryPickBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
                                cherryPickBuilder.setTitle(R.string.cherry_pick);
                                List<Ref> refList;
                                try
                                {
                                    refList = git.branchList().call();
                                }
                                catch (GitAPIException e)
                                {
                                    e.printStackTrace();
                                    Library.vibrateError();
                                    return;
                                }
                                List<String> c = new ArrayList<>();
                                for (Ref iter : refList)
                                {
                                    c.add(iter.getName());
                                }
                                cherryPickBuilder.setItems(Arrays.copyOf(c.toArray(), c.size(), String[].class), (dialog, which) ->
                                {
                                    Ref ref = refList.get(which);
                                    CherryPickCommand cherryPickCommand = git.cherryPick();
                                    CheckoutCommand checkoutCommand = git.checkout();
                                    checkoutCommand.setName(ref.getName());
                                    Handler h = new Handler();
                                    MaterialAlertDialogBuilder picking = new MaterialAlertDialogBuilder(fragment.getActivity());
                                    picking.setMessage(R.string.cherry_picking);
                                    picking.setCancelable(false);
                                    Dialog d = picking.show();
                                    Executors.newSingleThreadExecutor().submit(() ->
                                    {
                                        try
                                        {
                                            checkoutCommand.call();
                                            paginationCount = 1;
                                            h.post(CommitListAdapter.this::notifyDatasetChangedA);
                                            cherryPickCommand.include(commit);
                                            if (cherryPickCommand.call().getStatus() != CherryPickResult.CherryPickStatus.OK)
                                            {
                                                throw new GitAPIException("Cherry pick conflicting")
                                                {
                                                };
                                            }
                                            paginationCount = 1;
                                            h.post(CommitListAdapter.this::notifyDatasetChangedA);
                                            h.post(d::dismiss);
                                        }
                                        catch (GitAPIException e)
                                        {
                                            e.printStackTrace();
                                            Library.vibrateError();
                                            MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(fragment.getActivity());
                                            error.setTitle(R.string.cherry_pick_failed);
                                            error.setMessage(e.getLocalizedMessage());
                                            error.setPositiveButton(R.string.ok, (dialog2, which2) ->
                                            {
                                            });
                                            h.post(error::show);
                                            h.post(d::dismiss);
                                        }
                                    });
                                });
                                cherryPickBuilder.show();
                            });
                            StringBuilder stringBuilder = new StringBuilder().append("commit ").append(commit.getId().getName()).append('\n').append(new String(commit.getRawBuffer()));
                            commitText.setText(stringBuilder);
                            RecyclerView commitRecycler = v.findViewById(R.id.recycler_commit_files);
                            CommitFilesSubAdapter subAdapter = new CommitFilesSubAdapter(commit, repository, fragment);
                            commitRecycler.setLayoutManager(new LinearLayoutManager(Minicode.context));
                            commitRecycler.setAdapter(subAdapter);
                        });
                        viewHolder.itemView.setClickable(true);
                        viewHolder.itemView.setOnClickListener(_v ->
                        {
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                            builder.setCancelable(false);
                            View v = LayoutInflater.from(fragment.getActivity()).inflate(R.layout._dialog_commit, null);
                            builder.setView(v);
                            builder.setPositiveButton(R.string.ok, (dialog, which) -> notifyDatasetChangedA());
                            builder.setNegativeButton(R.string.tag, ((dialog, which) ->
                            {
                                MaterialAlertDialogBuilder selection = new MaterialAlertDialogBuilder(fragment.getActivity());
                                selection.setItems(R.array.tag_options, ((dialog1, which1) ->
                                {
                                    if (which1 == 0)
                                    {
                                        Library.askDialogString(true, Minicode.context.getString(R.string.tag), null, fragment.getActivity(), params ->
                                        {
                                            if (params[0].isEmpty())
                                            {
                                                Toast.makeText(Minicode.context, R.string.we_need_something_here, Toast.LENGTH_LONG).show();
                                                return null;
                                            }
                                            else
                                            {
                                                Executors.newSingleThreadExecutor().submit(() ->
                                                {
                                                    TagCommand tagCommand = git.tag();
                                                    tagCommand.setAnnotated(false);
                                                    tagCommand.setName(params[0]);
                                                    tagCommand.setObjectId(commit);
                                                    try
                                                    {
                                                        tagCommand.call();
                                                    }
                                                    catch (GitAPIException e)
                                                    {
                                                        MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                                                        builder1.setTitle(R.string.error);
                                                        builder1.setMessage(e.getLocalizedMessage());
                                                        builder1.setPositiveButton(R.string.ok, ((dialog2, which2) ->
                                                        {
                                                        }));
                                                        viewHolder.itemView.post(builder1::show);
                                                    }
                                                });
                                            }
                                            return null;
                                        });
                                    }
                                    else
                                    {
                                        Library.askDialogString(true, Minicode.context.getString(R.string.tag), null, fragment.getActivity(), params ->
                                        {
                                            if (params[0].isEmpty())
                                            {
                                                Toast.makeText(Minicode.context, R.string.we_need_something_here, Toast.LENGTH_LONG).show();
                                                return null;
                                            }
                                            else
                                            {
                                                TagCommand tagCommand = git.tag();
                                                tagCommand.setAnnotated(true);
                                                tagCommand.setName(params[0]);
                                                tagCommand.setObjectId(commit);
                                                Library.askDialogString(true, Minicode.context.getString(R.string.tag_message), null, fragment.getActivity(), params1 ->
                                                {
                                                    tagCommand.setMessage(params1[0]);
                                                    Executors.newSingleThreadExecutor().submit(() ->
                                                    {
                                                        try
                                                        {
                                                            tagCommand.call();
                                                        }
                                                        catch (GitAPIException e)
                                                        {
                                                            MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                                                            builder1.setTitle(R.string.error);
                                                            builder1.setMessage(e.getLocalizedMessage());
                                                            builder1.setPositiveButton(R.string.ok, ((dialog2, which2) ->
                                                            {
                                                            }));
                                                            viewHolder.itemView.post(builder1::show);
                                                        }
                                                    });
                                                    return null;
                                                });
                                            }
                                            return null;
                                        });
                                    }
                                }));
                                selection.show();
                            }));
                            builder.setNeutralButton(R.string.checkout, ((dialog, which) ->
                            {
                                Handler h = new Handler();
                                AlertDialog alertDialog = new AlertDialog.Builder(fragment.getActivity()).setMessage(R.string.checking_out).create();
                                alertDialog.setCancelable(false);
                                alertDialog.show();
                                new Thread()
                                {
                                    @Override
                                    public void run()
                                    {
                                        try
                                        {
                                            CheckoutCommand checkoutCommand = git.checkout();
                                            checkoutCommand.setName(commit.getName());
                                            checkoutCommand.setStartPoint(commit);
                                            checkoutCommand.setForce(true);
                                            checkoutCommand.call();
                                            sleep(100);
                                            h.postDelayed(CommitListAdapter.this::notifyDatasetChangedA, 40);
                                            h.post(alertDialog::cancel);
                                        }
                                        catch (GitAPIException | JGitInternalException | InterruptedException e)
                                        {
                                            e.printStackTrace();
                                            h.post(alertDialog::cancel);
                                            h.post(() -> new MaterialAlertDialogBuilder(fragment.getActivity()).setPositiveButton(R.string.ok, (dialog, which) ->
                                            {
                                            }).setMessage(R.string.checkout_failed).setTitle(R.string.error).show());
                                            h.post(CommitListAdapter.this::notifyDatasetChangedA);
                                        }
                                    }
                                }.start();
                            }));
                            builder.setTitle(commit.getId().getName());
                            builder.show();
                            TextView commitText = v.findViewById(R.id.commit_description);
                            MaterialButton cherryPick = v.findViewById(R.id.cherry_pick);
                            cherryPick.setOnClickListener(v29 ->
                            {
                                MaterialAlertDialogBuilder cherryPickBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
                                cherryPickBuilder.setTitle(R.string.cherry_pick);
                                List<Ref> refList;
                                try
                                {
                                    refList = git.branchList().call();
                                }
                                catch (GitAPIException e)
                                {
                                    e.printStackTrace();
                                    Library.vibrateError();
                                    return;
                                }
                                List<String> c = new ArrayList<>();
                                for (Ref iter : refList)
                                {
                                    c.add(iter.getName());
                                }
                                cherryPickBuilder.setItems(Arrays.copyOf(c.toArray(), c.size(), String[].class), (dialog, which) ->
                                {
                                    Ref ref = refList.get(which);
                                    CherryPickCommand cherryPickCommand = git.cherryPick();
                                    CheckoutCommand checkoutCommand = git.checkout();
                                    checkoutCommand.setName(ref.getName());
                                    Handler h = new Handler();
                                    MaterialAlertDialogBuilder picking = new MaterialAlertDialogBuilder(fragment.getActivity());
                                    picking.setMessage(R.string.cherry_picking);
                                    picking.setCancelable(false);
                                    Dialog d = picking.show();
                                    Executors.newSingleThreadExecutor().submit(() ->
                                    {
                                        try
                                        {
                                            checkoutCommand.call();
                                            paginationCount = 1;
                                            h.post(CommitListAdapter.this::notifyDatasetChangedA);
                                            cherryPickCommand.include(commit);
                                            if (cherryPickCommand.call().getStatus() != CherryPickResult.CherryPickStatus.OK)
                                            {
                                                throw new GitAPIException("Cherry pick conflicting")
                                                {
                                                };
                                            }
                                            paginationCount = 1;
                                            h.post(CommitListAdapter.this::notifyDatasetChangedA);
                                            h.post(d::dismiss);
                                        }
                                        catch (GitAPIException e)
                                        {
                                            e.printStackTrace();
                                            Library.vibrateError();
                                            MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(fragment.getActivity());
                                            error.setTitle(R.string.cherry_pick_failed);
                                            error.setMessage(e.getLocalizedMessage());
                                            error.setPositiveButton(R.string.ok, (dialog2, which2) ->
                                            {
                                            });
                                            h.post(error::show);
                                            h.post(d::dismiss);
                                        }
                                    });
                                });
                                cherryPickBuilder.show();
                            });
                            StringBuilder stringBuilder = new StringBuilder().append("commit ").append(commit.getId().getName()).append('\n').append(new String(commit.getRawBuffer()));
                            commitText.setText(stringBuilder);
                            RecyclerView commitRecycler = v.findViewById(R.id.recycler_commit_files);
                            CommitFilesSubAdapter subAdapter = new CommitFilesSubAdapter(commit, repository, fragment);
                            commitRecycler.setLayoutManager(new LinearLayoutManager(Minicode.context));
                            commitRecycler.setAdapter(subAdapter);
                        });
                    }, 200);
                }
            }.start();
        });
    }

    @Override
    public int getItemCount()
    {
        return commits.size();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        this.handlerThread.quitSafely();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextHeading, mTextContent;
        public LinearLayout mLinearLayout;
        public ImageButton mCommitButton;

        public ViewHolder(View v)
        {
            super(v);
            mTextHeading = v.findViewById(R.id.basic_about_text_header);
            mTextContent = v.findViewById(R.id.basic_about_text_content);
            mLinearLayout = (LinearLayout) v;
            mCommitButton = v.findViewById(R.id.imagebutton_view_commit);
        }
    }

    static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private CommitListAdapter mAdapter;
        private Drawable iconEnd, iconStart;
        private Drawable background;

        public SwipeToDeleteCallback(CommitListAdapter adapter)
        {
            super(0, ItemTouchHelper.START | ItemTouchHelper.END);
            mAdapter = adapter;
            background = Minicode.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(Minicode.context.getResources().getColor(R.color.holo_red_dark));
            iconEnd = Minicode.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp).getConstantState().newDrawable().mutate();
            iconStart = Minicode.context.getResources().getDrawable(R.drawable.ic_undo_black_24dp).getConstantState().newDrawable().mutate();
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            if (direction == ItemTouchHelper.START)
            {
                Handler h = new Handler();
                int i = viewHolder.getAdapterPosition();
                Iterable<RevCommit> logs;
                Git git = new Git(mAdapter.repository);
                Repository repository = git.getRepository();
                try
                {
                    logs = git.log().add(repository.resolve(repository.getFullBranch())).call();
                }
                catch (GitAPIException e)
                {
                    e.printStackTrace();
                    return;
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    return;
                }
                ArrayList<RevCommit> commits = new ArrayList<>();
                for (RevCommit iter : logs)
                {
                    commits.add(iter);
                }
                RevCommit commit = commits.get(i);
                RevertCommand revertCommand = git.revert();
                revertCommand.include(commit);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        revertCommand.call();
                        h.post(mAdapter::notifyDatasetChangedA);
                        h.post(Library::vibrateSuccess);
                    }
                    catch (GitAPIException e)
                    {
                        Library.vibrateError();
                        e.printStackTrace();
                        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(mAdapter.fragment.getActivity());
                        materialAlertDialogBuilder.setTitle(R.string.error);
                        materialAlertDialogBuilder.setMessage(e.getLocalizedMessage());
                        materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog, which) -> mAdapter.notifyDatasetChangedA());
                        materialAlertDialogBuilder.setCancelable(false);
                        h.post(materialAlertDialogBuilder::show);
                    }
                });
            }
            else
            {
                Handler h = new Handler();
                int i = viewHolder.getAdapterPosition();
                Iterable<RevCommit> logs;
                Git git = new Git(mAdapter.repository);
                Repository repository = git.getRepository();
                try
                {
                    logs = git.log().add(repository.resolve(repository.getFullBranch())).call();
                }
                catch (GitAPIException e)
                {
                    e.printStackTrace();
                    return;
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    return;
                }
                if (i != 0)
                {
                    Library.vibrateError();
                    mAdapter.notifyDatasetChangedA();
                    return;
                }
                ResetCommand resetCommand = git.reset().setMode(ResetCommand.ResetType.SOFT).setRef("HEAD~1");
                MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(mAdapter.fragment.getActivity());
                dialogBuilder.setCancelable(false);
                dialogBuilder.setMessage(R.string.reverting);
                Dialog d = dialogBuilder.show();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        resetCommand.call();
                    }
                    catch (GitAPIException e)
                    {
                        e.printStackTrace();
                        h.post(d::dismiss);
                        h.post(Library::vibrateError);
                        h.post(() ->
                        {
                            MaterialAlertDialogBuilder alertDialogBuilder = new MaterialAlertDialogBuilder(mAdapter.fragment.getActivity());
                            alertDialogBuilder.setTitle(R.string.undo_fail);
                            alertDialogBuilder.setMessage(e.getLocalizedMessage());
                            alertDialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
                            {
                            });
                        });
                    }
                    finally
                    {
                        h.post(d::dismiss);
                        h.post(Library::vibrateSuccess);
                        h.post(mAdapter::notifyDatasetChangedA);
                    }
                });
            }
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Drawable background = this.background.getConstantState().newDrawable();
            background.mutate();
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - iconEnd.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - iconEnd.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + iconEnd.getIntrinsicHeight();
            int iconLeft = 0;
            int iconRight = 0;
            if (dX > 0)
            {
                iconLeft = itemView.getLeft() + iconMargin + iconEnd.getIntrinsicWidth();
                iconRight = itemView.getLeft() + iconMargin;
                iconEnd.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                iconLeft = itemView.getRight() - iconMargin - iconEnd.getIntrinsicWidth();
                iconRight = itemView.getRight() - iconMargin;
                iconEnd.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            iconEnd.draw(c);
            if (dX > 0)
            {
                c.drawBitmap(Library.drawableToBitmap(iconStart), iconLeft, iconTop, null);
            }
        }
    }

    public static class CommitFilesSubAdapter extends RecyclerView.Adapter<CommitFilesSubAdapter.ViewHolder>
    {
        private RevCommit commit;
        private Repository repository;
        private Fragment fragment;
        private List<DiffEntry> diffEntries;
        private Drawable mAdd, mRemove, mNeutral;

        public CommitFilesSubAdapter(RevCommit commit, Repository repository, Fragment fragment)
        {
            this.commit = commit;
            this.repository = repository;
            this.fragment = fragment;
            mRemove = Minicode.context.getResources().getDrawable(R.drawable.ic_remove_black_24dp).getConstantState().newDrawable().mutate();
            mRemove.setTint(Minicode.context.getResources().getColor(R.color.holo_red_light));
            mAdd = Minicode.context.getResources().getDrawable(R.drawable.ic_add_white_24dp).getConstantState().newDrawable().mutate();
            mAdd.setTint(Minicode.context.getResources().getColor(R.color.holo_green_light));
            mNeutral = Minicode.context.getResources().getDrawable(R.drawable.ic_add_white_24dp).getConstantState().newDrawable().mutate();
            mAdd.setTint(Minicode.context.getResources().getColor(R.color.holo_orange_light));
            try
            {
                if (commit.getParentCount() != 0)
                {
                    this.diffEntries = Library.commitDiff(commit.getParent(0), commit, new Git(repository));
                }
                else
                {
                    this.diffEntries = Library.commitDiff(null, commit, new Git(repository));
                }
            }
            catch (IOException | ArrayIndexOutOfBoundsException e)
            {
                e.printStackTrace();
                this.diffEntries = new ArrayList<>();
            }
            Log.d(getClass().getName(), String.format("CommitFilesSubAdapter: constructor - diffEntries size: %d - entries %s", diffEntries.size(), Arrays.toString(diffEntries.toArray())));
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            return new ViewHolder(LayoutInflater.from(Minicode.context).inflate(R.layout.basic_commit_file, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position)
        {
            DiffEntry diffEntry = diffEntries.get(position);
            switch (diffEntry.getChangeType())
            {
                case ADD:
                    holder.mImageView.setImageDrawable(mAdd);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    break;
                case COPY:
                    holder.mImageView.setImageDrawable(mNeutral);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_orange_dark)));
                    break;
                case DELETE:
                    holder.mImageView.setImageDrawable(mRemove);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_red_light)));
                    break;
                case MODIFY:
                    holder.mImageView.setImageDrawable(mNeutral);
                    holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_orange_dark)));
                    break;
                case RENAME:
                    holder.mImageView.setImageResource(R.drawable.ic_redo_black_24dp);
                    break;
            }
            Executors.newSingleThreadExecutor().submit(() ->
            {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DiffFormatter df = new DiffFormatter(byteArrayOutputStream);
                try
                {
                    df.setRepository(repository);
                    df.format(diffEntry);
                    df.flush();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    df.close();
                }
                String data = new String(byteArrayOutputStream.toByteArray());
                Matcher m = Pattern.compile("^([+\\-].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
                int countadd = 0, countless = 0;
                while (m.find())
                {
                    String substring = data.substring(m.start(), m.end());
                    if (substring.matches("^((---)|(\\+\\+\\+)) .*$"))
                    {
                        continue;
                    }
                    if (substring.charAt(0) == '-')
                    {
                        ++countless;
                    }
                    else if (substring.charAt(0) == '+')
                    {
                        ++countadd;
                    }
                    else if (substring.equals("Binary files differ"))
                    {
                        ++countadd;
                    }
                }
                String result = String.format(Minicode.context.getString(R.string.d_add_d_del), countadd, countless);
                Log.d(getClass().getName(), String.format("%s: result: %s", Thread.currentThread().getName(), result));
                holder.mAddDel.post(() -> holder.mAddDel.setText(result));
            });
            holder.mFileName.setText(diffEntry.getOldPath().equals(DEV_NULL) || diffEntry.getOldPath().equals(diffEntry.getNewPath()) ? diffEntry.getNewPath() : String.format("%s -> %s", diffEntry.getOldPath(), diffEntry.getNewPath()));
            holder.v.setOnClickListener(v ->
            {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DiffFormatter df = new DiffFormatter(byteArrayOutputStream);
                try
                {
                    df.setRepository(repository);
                    df.format(diffEntry);
                    df.flush();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    df.close();
                }
                String data = new String(byteArrayOutputStream.toByteArray());
                ScrollView v13 = (ScrollView) LayoutInflater.from(Minicode.context).inflate(R.layout._scrollview_mh, null);
                v13.setBackgroundResource(R.drawable.roundable_sharp);
                TextView textView = new EditText(Minicode.context);
                textView.setEnabled(false);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                textView.setTypeface(Minicode.theMinicode.getEditorTypeface());
                v13.addView(textView);
                textView.setText(data);
                MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity(), R.style.Theme_MaterialComponents_Light_Dialog);
                dialogBuilder.setView(v13);
                dialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                });
                dialogBuilder.show();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    ArrayList<Selection> selections = new ArrayList<>();
                    ArrayList<Integer> flags = new ArrayList<>();
                    Matcher m = Pattern.compile("^([+\\-@].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
                    while (m.find())
                    {
                        String substring = data.substring(m.start(), m.end());
                        if (substring.charAt(0) == '-')
                        {
                            flags.add(1);
                        }
                        else if (substring.charAt(0) == '+')
                        {
                            flags.add(2);
                        }
                        else if (substring.equals("Binary files differ"))
                        {
                            flags.add(3);
                        }
                        else
                        {
                            flags.add(4);
                        }
                        selections.add(new Selection(m.start(), m.end()));
                    }
                    int i = 0;
                    for (Selection iter : selections)
                    {
                        BackgroundColorSpan backgroundColorSpan;
                        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.WHITE);
                        if (flags.get(i).equals(1))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.red));
                        }
                        else if (flags.get(i).equals(2))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_green_light));
                        }
                        else if (flags.get(i).equals(3))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_orange_light));
                        }
                        else
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                        }
                        textView.post(() -> textView.getEditableText().setSpan(backgroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                        textView.post(() -> textView.getEditableText().setSpan(foregroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                        ++i;
                    }

                });
            });
        }

        @Override
        public int getItemCount()
        {
            return diffEntries.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder
        {
            public ImageView mImageView;
            public TextView mFileName, mAddDel;
            public View v;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);
                this.v = itemView;
                this.mImageView = itemView.findViewById(R.id.commit_imageview_file);
                this.mFileName = itemView.findViewById(R.id.commit_file_name);
                this.mAddDel = itemView.findViewById(R.id.add_del_amount);
            }
        }
    }
}