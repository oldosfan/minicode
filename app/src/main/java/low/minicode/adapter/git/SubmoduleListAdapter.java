/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.git;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.SubmoduleAddCommand;
import org.eclipse.jgit.api.SubmoduleInitCommand;
import org.eclipse.jgit.api.SubmoduleUpdateCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.lib.BatchingProgressMonitor;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.storage.file.FileBasedConfig;
import org.eclipse.jgit.submodule.SubmoduleStatus;
import org.eclipse.jgit.transport.ChainingCredentialsProvider;
import org.eclipse.jgit.util.FS;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.git.VersionControlActivity;
import low.minicode.fragments.git.VCSSubmodulesFragment;

import static low.minicode.Library.NEWLINE;
import static org.eclipse.jgit.lib.ConfigConstants.CONFIG_SUBMODULE_SECTION;

public class SubmoduleListAdapter extends RecyclerView.Adapter<SubmoduleListAdapter.ViewHolder>
{
    private Git git;
    private Map<String, SubmoduleStatus> submodules;
    private ArrayList<String> keys;
    private ArrayList<SubmoduleStatus> modules;
    private ItemTouchHelper touchHelper;
    private Fragment fragment;

    public SubmoduleListAdapter(Repository repository, Fragment fragment)
    {
        this.git = new Git(repository);
        this.touchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
        this.fragment = fragment;
        this.changeData();
        if (fragment instanceof VCSSubmodulesFragment)
        {
            ImageButton addNew, sync, newLocal;
            addNew = fragment.getView().findViewById(R.id.submodule_now);
            sync = fragment.getView().findViewById(R.id.submodule_sync_now);
            newLocal = fragment.getView().findViewById(R.id.submodule_new_local);
            addNew.setOnClickListener(v ->
            {
                Library.askDialogString(true, Minicode.context.getString(R.string.new_submodule), Minicode.context.getString(R.string.path), fragment.getActivity(), params ->
                {
                    SubmoduleAddCommand addCommand = git.submoduleAdd();
                    addCommand.setPath(params[0]);
                    Library.askDialogString(true, Minicode.context.getString(R.string.new_submodule), Minicode.context.getString(R.string.url), fragment.getActivity(), params1 ->
                    {
                        Handler h = new Handler();
                        addCommand.setURI(params1[0]);
                        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                        builder.setMessage(R.string.waiting_for_response);
                        builder.setCancelable(false);
                        builder.setTitle(R.string.creating_submodule);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        addCommand.setProgressMonitor(new BatchingProgressMonitor()
                        {
                            @Override
                            protected void onUpdate(String taskName, int workCurr)
                            {
                            }

                            @Override
                            protected void onEndTask(String taskName, int workCurr)
                            {
                            }

                            @Override
                            protected void onUpdate(String taskName, int workCurr, int workTotal, int percentDone)
                            {
                                h.post(() -> dialog.setTitle(taskName));
                                h.post(() -> dialog.setMessage(String.format(Minicode.context.getString(R.string.percent_complete), percentDone)));
                            }

                            @Override
                            protected void onEndTask(String taskName, int workCurr, int workTotal, int percentDone)
                            {
                            }
                        });
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            try
                            {
                                String projectName = ((VCSSubmodulesFragment) fragment).getProjectName();
                                Library.transportCredentials(addCommand, projectName);
                                addCommand.call();
                                h.post(dialog::dismiss);
                                h.postDelayed(SubmoduleListAdapter.this::changeData, 100);
                                h.postDelayed(SubmoduleListAdapter.this::notifyDataSetChanged, 400);
                            }
                            catch (GitAPIException e)
                            {
                                e.printStackTrace();
                                h.post(dialog::dismiss);
                                h.postDelayed(SubmoduleListAdapter.this::changeData, 100);
                                h.postDelayed(SubmoduleListAdapter.this::notifyDataSetChanged, 400);
                                MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                                builder1.setTitle(R.string.submodule_creation_failed);
                                builder1.setMessage(e.getLocalizedMessage());
                                builder1.setPositiveButton(R.string.ok, (dialog1, which) ->
                                {
                                });
                                h.post(builder1::show);
                            }
                        });
                        return null;
                    });
                    return null;
                });
            });
            newLocal.setOnClickListener(v ->
            {
                Library.askDialogString(true, Minicode.context.getString(R.string.new_submodule), Minicode.context.getString(R.string.path), fragment.getActivity(), params ->
                {
                    Repository repository1;
                    try
                    {
                        repository1 = new RepositoryBuilder().setGitDir(new File(new File(fragment.getActivity().getFilesDir(), ((VCSSubmodulesFragment) fragment).getProjectName()), params[0] + "/.git")).setMustExist(false).readEnvironment().findGitDir().build();
                    }
                    catch (JGitInternalException | IOException e)
                    {
                        e.printStackTrace();
                        MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                        builder1.setTitle(R.string.submodule_creation_failed);
                        builder1.setMessage(e.getLocalizedMessage());
                        builder1.setPositiveButton(R.string.ok, (dialog1, which) ->
                        {
                        });
                        builder1.show();
                        return null;
                    }
                    SubmoduleAddCommand submoduleAddCommand = git.submoduleAdd();
                    submoduleAddCommand.setPath(params[0]);
                    try
                    {
                        submoduleAddCommand.setURI(repository1.getDirectory().getCanonicalPath());
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                        MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                        builder1.setTitle(R.string.submodule_creation_failed);
                        builder1.setMessage(e.getLocalizedMessage());
                        builder1.setPositiveButton(R.string.ok, (dialog1, which) ->
                        {
                        });
                        builder1.show();
                        return null;
                    }
                    SubmoduleInitCommand submoduleInitCommand = git.submoduleInit();
                    Handler h = new Handler();
                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                    builder.setCancelable(false);
                    builder.setMessage(R.string.creating_submodule);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            submoduleAddCommand.call();
                            submoduleInitCommand.addPath(params[0]).call();
                            h.post(dialog::dismiss);
                            h.postDelayed(SubmoduleListAdapter.this::changeData, 100);
                            h.postDelayed(SubmoduleListAdapter.this::notifyDataSetChanged, 400);
                        }
                        catch (GitAPIException e)
                        {
                            e.printStackTrace();
                            h.post(dialog::dismiss);
                            h.postDelayed(SubmoduleListAdapter.this::changeData, 100);
                            h.postDelayed(SubmoduleListAdapter.this::notifyDataSetChanged, 400);
                            MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                            builder1.setTitle(R.string.submodule_creation_failed);
                            builder1.setMessage(e.getLocalizedMessage());
                            builder1.setPositiveButton(R.string.ok, (dialog1, which) ->
                            {
                            });
                            h.post(builder1::show);
                        }
                    });
                    return null;
                });
            });
            sync.setOnClickListener(v ->
            {
                SubmoduleUpdateCommand updateCommand = git.submoduleUpdate();
                for (String str : keys)
                {
                    updateCommand.addPath(str);
                }
                Handler h = new Handler();
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                builder.setMessage(R.string.waiting_for_response);
                builder.setCancelable(false);
                builder.setTitle(R.string.syncing_submodules);
                AlertDialog dialog = builder.create();
                dialog.show();
                updateCommand.setProgressMonitor(new BatchingProgressMonitor()
                {
                    @Override
                    protected void onUpdate(String taskName, int workCurr)
                    {
                    }

                    @Override
                    protected void onEndTask(String taskName, int workCurr)
                    {
                    }

                    @Override
                    protected void onUpdate(String taskName, int workCurr, int workTotal, int percentDone)
                    {
                        h.post(() -> dialog.setTitle(taskName));
                        h.post(() -> dialog.setMessage(String.format(Minicode.context.getString(R.string.percent_complete), percentDone)));
                    }

                    @Override
                    protected void onEndTask(String taskName, int workCurr, int workTotal, int percentDone)
                    {
                    }
                });
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        updateCommand.setCredentialsProvider(new ChainingCredentialsProvider());
                        updateCommand.call();
                        h.post(dialog::dismiss);
                        h.postDelayed(SubmoduleListAdapter.this::changeData, 100);
                        h.postDelayed(SubmoduleListAdapter.this::notifyDataSetChanged, 400);
                    }
                    catch (GitAPIException e)
                    {
                        e.printStackTrace();
                        h.post(dialog::dismiss);
                        h.postDelayed(SubmoduleListAdapter.this::changeData, 100);
                        h.postDelayed(SubmoduleListAdapter.this::notifyDataSetChanged, 400);
                        MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(fragment.getActivity());
                        builder1.setTitle(R.string.submodule_sync_failed);
                        builder1.setMessage(e.getLocalizedMessage());
                        builder1.setPositiveButton(R.string.ok, (dialog1, which) ->
                        {
                        });
                        h.post(builder1::show);
                    }
                });
            });
        }
    }

    public void changeData()
    {
        try
        {
            this.submodules = this.git.submoduleStatus().call();
        }
        catch (GitAPIException e)
        {
            e.printStackTrace();
            this.submodules = new LinkedHashMap<>();
        }
        this.keys = new ArrayList<>(submodules.keySet());
        this.modules = new ArrayList<>(submodules.values());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.touchHelper.attachToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(fragment.getActivity()).inflate(R.layout.basic_submodule, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Executors.newSingleThreadExecutor().submit(() ->
        {
            String file = keys.get(position);
            String type;
            switch (modules.get(position).getType())
            {
                case MISSING:
                {
                    type = Minicode.context.getString(R.string.missing);
                    break;
                }
                case INITIALIZED:
                {
                    type = Minicode.context.getString(R.string.initialized);
                    break;
                }
                case UNINITIALIZED:
                {
                    type = Minicode.context.getString(R.string.uninitialized);
                    break;
                }
                case REV_CHECKED_OUT:
                {
                    type = Minicode.context.getString(R.string.rev_checked_out);
                    break;
                }
                default:
                {
                    return;
                }
            }
            holder.itemView.post(() ->
            {
                holder.mTextHeading.setText(file);
                if (modules.get(position).getHeadId() == null)
                {
                    holder.mTextContent.setText(String.format("%s, %s", type, "null"));
                }
                else
                {
                    holder.mTextContent.setText(String.format("%s, %s", type, modules.get(position).getHeadId().getName()));
                }
                holder.mViewInfo.setOnClickListener(v ->
                {
                    MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(fragment.getActivity(), R.style.Theme_MaterialComponents_Light_Dialog);
                    b.setTitle(R.string.submodule);
                    ScrollView scrollView = new ScrollView(Minicode.context);
                    scrollView.setBackgroundResource(R.drawable.roundable_sharp);
                    TextView textView = new TextView(Minicode.context);
                    textView.setTypeface(Minicode.theMinicode.getEditorTypeface());
                    scrollView.addView(textView);
                    StringBuilder stringBuilder = new StringBuilder();
                    if (modules.get(position).getHeadId() != null)
                    {
                        stringBuilder.append(String.format("Submodule %s", file)).append(NEWLINE).append(String.format("Head at %s", modules.get(position).getHeadId().getName())).append(NEWLINE).append(String.format("State: %s", type));
                    }
                    textView.setText(stringBuilder);
                    textView.setPadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, Minicode.context.getResources().getDisplayMetrics()), 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, Minicode.context.getResources().getDisplayMetrics()), 0);
                    b.setPositiveButton(R.string.ok, (dialog, which) ->
                    {
                    });
                    b.setNegativeButton(R.string.git, ((dialog, which) ->
                    {
                        if (fragment.getActivity() instanceof VersionControlActivity)
                        {
                            Intent intent = new Intent(fragment.getActivity(), VersionControlActivity.class);
                            intent.putExtra("projectName", ((VersionControlActivity) fragment.getActivity()).projectName + "/" + modules.get(position).getPath());
                            Log.d(getClass().getName(), ((VersionControlActivity) fragment.getActivity()).projectName + "/" + modules.get(position).getPath());
                            fragment.getActivity().startActivity(intent);
                        }
                    }));
                    b.setView(scrollView);
                    b.show();
                });
            });
        });
    }

    @Override
    public int getItemCount()
    {
        return submodules.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextHeading, mTextContent;
        public ImageButton mViewInfo;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            mTextContent = itemView.findViewById(R.id.basic_about_text_content);
            mTextHeading = itemView.findViewById(R.id.basic_about_text_header);
            mViewInfo = itemView.findViewById(R.id.view_submodule_info);
        }
    }

    public static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private SubmoduleListAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(SubmoduleListAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = Minicode.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(Minicode.context.getResources().getColor(R.color.holo_red_dark));
            icon = Minicode.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            File gitSubmodulesFile = new File(mAdapter.git.getRepository().getWorkTree(), Constants.DOT_GIT_MODULES);
            StoredConfig smConfig = new FileBasedConfig(null, gitSubmodulesFile, FS.DETECTED);
            smConfig.unsetSection(CONFIG_SUBMODULE_SECTION, mAdapter.keys.get(viewHolder.getAdapterPosition()));
            StoredConfig repositoryConfig = mAdapter.git.getRepository().getConfig();
            repositoryConfig.unsetSection(CONFIG_SUBMODULE_SECTION, mAdapter.keys.get(viewHolder.getAdapterPosition()));
            try
            {
                repositoryConfig.save();
                smConfig.save();
                mAdapter.git.rm().addFilepattern(mAdapter.keys.get(viewHolder.getAdapterPosition())).setCached(true).call();
                Library.deleteDirectoryRecursively(new File(mAdapter.git.getRepository().getDirectory() + mAdapter.keys.get(viewHolder.getAdapterPosition())));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (NoFilepatternException e)
            {
                e.printStackTrace();
            }
            catch (GitAPIException e)
            {
                e.printStackTrace();
            }
            mAdapter.changeData();
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }
}
