/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.git;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import low.minicode.R;
import low.minicode.wrappers.data.adapteritem.TitleBodyClickableItem;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder>
{
    private TitleBodyClickableItem[] content;
    private Activity activity;

    public ItemListAdapter(List<TitleBodyClickableItem> s)
    {
        this.activity = activity;
        ArrayList<TitleBodyClickableItem> where = new ArrayList<>(s);
        this.content = new TitleBodyClickableItem[where.size()];
        if (content.length < 1)
        {
            return;
        }
        where.toArray(this.content);
    }

    public void initializeWithNewData(List<TitleBodyClickableItem> s)
    {
        ArrayList<TitleBodyClickableItem> where = new ArrayList<>(s);
        this.content = new TitleBodyClickableItem[where.size()];
        where.toArray(this.content);
        this.notifyDataSetChanged();
        return;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_info_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        viewHolder.mTextContent.setText(content[i].smallText);
        viewHolder.mTextHeading.setText(content[i].bigText);
        if (content[i].onClickListener != null)
        {
            viewHolder.mLinearLayout.setClickable(true);
            viewHolder.mLinearLayout.setOnClickListener(content[i].onClickListener);
        }
        else
        {
            viewHolder.mLinearLayout.setClickable(false);
            viewHolder.mLinearLayout.setOnClickListener(null);
        }
        if (content[i].nullableDrawableRes == null)
        {
            viewHolder.mImageView.setVisibility(View.INVISIBLE);
        }
        else
        {
            viewHolder.mImageView.setVisibility(View.VISIBLE);
            viewHolder.mImageView.setImageResource(content[i].nullableDrawableRes);
        }
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return this.content.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextHeading, mTextContent;
        public LinearLayout mLinearLayout;
        public ImageView mImageView;

        public ViewHolder(View v)
        {
            super(v);
            mTextHeading = v.findViewById(R.id.basic_about_text_header);
            mTextContent = v.findViewById(R.id.basic_about_text_content);
            mLinearLayout = v.findViewById(R.id.about_recycler_layout);
            mImageView = v.findViewById(R.id.basic_about_text_image);
            Log.d("Step", "Step: ViewHolder - Constructer");
        }
    }
}