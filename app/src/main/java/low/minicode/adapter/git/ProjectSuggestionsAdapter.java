/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.git;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.project.navigator.NavigatorActivity;

public class ProjectSuggestionsAdapter extends RecyclerView.Adapter<ProjectSuggestionsAdapter.ViewHolder>
{
    private ArrayList<String> data = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_suggestion_project, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.name.setText(data.get(position));
        holder.itemView.setOnClickListener(v ->
        {
            Minicode.context.getSharedPreferences("low.minicode.project_usage", Context.MODE_PRIVATE).edit().putInt(data.get(position), Minicode.context.getSharedPreferences("low.minicode.project_usage", Context.MODE_PRIVATE).getInt(data.get(position), 0) + 1).apply();
            Intent i = new Intent(v.getContext(), NavigatorActivity.class);
            i.putExtra("projectName", data.get(position));
            i.putExtra("loc", "/");
            v.getContext().startActivity(i);
        });
    }

    @Override
    public int getItemCount()
    {
        data.clear();
        Map<String, ?> prefs = Minicode.context.getSharedPreferences("low.minicode.project_usage", Context.MODE_PRIVATE).getAll();
        Map<String, Integer> tmp = new HashMap<>();
        for (String iter : prefs.keySet())
        {
            if (prefs.get(iter) instanceof Integer)
            {
                tmp.put(iter, (Integer) prefs.get(iter));
            }
        }
        List<Map.Entry<String, Integer>> tmp2 = Library.entriesSortedByValues(tmp);
        for (Map.Entry iter : tmp2)
        {
            if (new File(Minicode.context.getFilesDir(), (String) iter.getKey()).exists())
            {
                data.add((String) iter.getKey());
            }
        }
        if (data.size() > 3)
        {
            data = new ArrayList<>(data.subList(0, 3));
        }
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.project);
        }
    }
}
