/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.git;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.DeleteBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.RebaseCommand;
import org.eclipse.jgit.api.RebaseResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.lib.AnyObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.fragments.git.VCSBranchesFragment;

public class BranchListAdapter extends RecyclerView.Adapter<BranchListAdapter.ViewHolder>
{
    private Repository repository;
    private Fragment fragment;
    private ItemTouchHelper touchHelper;
    private List<Ref> call;
    private int pageCount = 1;
    private Git git;

    public BranchListAdapter(Repository repository, Fragment fragment)
    {
        this.repository = repository;
        this.fragment = fragment;
        this.touchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
        this.git = new Git(repository);
        if (fragment instanceof VCSBranchesFragment)
        {
            fragment.getView().findViewById(R.id.merge_now).setOnClickListener(v ->
            {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                builder.setTitle(R.string.merge_branch);
                String[] strings;
                ArrayList<String> strings1 = new ArrayList<>();
                for (Ref ref : call)
                {
                    strings1.add(ref.getName());
                }
                strings = new String[strings1.size()];
                strings1.toArray(strings);
                builder.setItems(strings, (dialog, which) ->
                {
                    Handler handler = new Handler();
                    Ref branch = call.get(which);
                    MergeCommand mergeCommand = git.merge();
                    mergeCommand.include(branch);
                    MaterialAlertDialogBuilder merging = new MaterialAlertDialogBuilder(fragment.getActivity());
                    merging.setCancelable(false);
                    merging.setMessage(R.string.merging);
                    Dialog d = merging.create();
                    d.show();
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            MergeResult result = mergeCommand.call();
                            changeData();
                            handler.post(this::notifyDataSetChanged);
                            d.dismiss();
                            if (result.getMergeStatus() == MergeResult.MergeStatus.CONFLICTING)
                            {
                                MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
                                materialAlertDialogBuilder.setTitle(R.string.merge_conflicts);
                                RecyclerView recyclerView = new RecyclerView(fragment.getActivity());
                                materialAlertDialogBuilder.setView(recyclerView);
                                recyclerView.setLayoutManager(new LinearLayoutManager(Minicode.context));
                                recyclerView.setAdapter(new MergeConflictSubAdapter(result));
                                materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog1, which1) ->
                                {
                                });
                                handler.post(materialAlertDialogBuilder::show);
                            }
                        }
                        catch (GitAPIException e)
                        {
                            e.printStackTrace();
                            handler.post(() -> new MaterialAlertDialogBuilder(fragment.getActivity()).setTitle(R.string.error).setMessage(e.getLocalizedMessage()).setPositiveButton(R.string.ok, (dialog1, which1) ->
                            {
                            }).show());
                        }
                    });
                });
                builder.show();
            });
            fragment.getView().findViewById(R.id.rebase_now).setOnClickListener(v ->
            {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                builder.setTitle(R.string.rebase_branch);
                String[] strings;
                ArrayList<String> strings1 = new ArrayList<>();
                for (Ref ref : call)
                {
                    strings1.add(ref.getName());
                }
                strings = new String[strings1.size()];
                strings1.toArray(strings);
                builder.setItems(strings, (dialog, which) ->
                {
                    Handler handler = new Handler();
                    RebaseCommand rebaseCommand = git.rebase();
                    try
                    {
                        rebaseCommand.setUpstream(strings1.get(which));
                    }
                    catch (RefNotFoundException e)
                    {
                        e.printStackTrace();
                        MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(fragment.getActivity());
                        error.setTitle(R.string.error);
                        error.setMessage(e.getLocalizedMessage());
                        error.setPositiveButton(R.string.ok, (dialog2, which2) ->
                        {
                        });
                        error.show();
                        return;
                    }
                    MaterialAlertDialogBuilder rebasing = new MaterialAlertDialogBuilder(fragment.getActivity());
                    rebasing.setCancelable(false);
                    rebasing.setMessage(R.string.rebasing);
                    Dialog d = rebasing.create();
                    d.show();
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            RebaseResult result = rebaseCommand.call();
                            changeData();
                            handler.post(this::notifyDataSetChanged);
                            d.dismiss();
                            if (result.getStatus() == RebaseResult.Status.CONFLICTS)
                            {
                                MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(fragment.getActivity());
                                materialAlertDialogBuilder.setTitle(R.string.rebase_conflicts);
                                materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog2, which2) ->
                                {
                                });
                                handler.post(materialAlertDialogBuilder::show);
                            }
                        }
                        catch (GitAPIException e)
                        {
                            e.printStackTrace();
                            handler.post(() -> new MaterialAlertDialogBuilder(fragment.getActivity()).setTitle(R.string.error).setMessage(e.getLocalizedMessage()).setPositiveButton(R.string.ok, (dialog1, which1) ->
                            {
                            }).show());
                        }
                    });
                });
                builder.show();
            });
            fragment.getView().findViewById(R.id.branch_now).setOnClickListener(v ->
            {
                EditText editText = new EditText(fragment.getActivity());
                editText.setHint(R.string.name);
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
                builder.setTitle(R.string.new_branch);
                builder.setView(editText);
                builder.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                    if (editText.getText().toString().isEmpty())
                    {
                        Toast.makeText(Minicode.context, R.string.we_need_something_here, Toast.LENGTH_LONG).show();
                        return;
                    }
                    Thread t = new Thread()
                    {
                        @Override
                        public void run()
                        {
                            CreateBranchCommand createBranchCommand = git.branchCreate();
                            createBranchCommand.setName(editText.getText().toString());
                            try
                            {
                                createBranchCommand.call();
                                changeData();
                                fragment.getActivity().runOnUiThread(() -> notifyDataSetChanged());
                            }
                            catch (GitAPIException | JGitInternalException e)
                            {
                                e.printStackTrace();
                                fragment.getActivity().runOnUiThread(() -> Toast.makeText(Minicode.context, R.string.error, Toast.LENGTH_LONG).show());
                            }
                        }
                    };
                    t.start();
                });
                builder.show();
            });
            try
            {
                fragment.getView().findViewById(R.id.detached_head).setVisibility(git.getRepository().getFullBranch().startsWith("refs/heads/") ? View.INVISIBLE : View.VISIBLE);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        changeData();
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_branch, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void changeData()
    {
        try
        {
            call = git.branchList().call();
        }
        catch (GitAPIException e)
        {
            e.printStackTrace();
            Log.wtf(getClass().getName(), e.getCause());
            call = new ArrayList<>();
        }
        try
        {
            fragment.getView().findViewById(R.id.detached_head).setVisibility(git.getRepository().getFullBranch().startsWith("refs/heads/") ? View.INVISIBLE : View.VISIBLE);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        touchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        String name = call.get(i).getName();
        viewHolder.mTextHeading.setText(name.startsWith("refs/heads/") ? name.split("(?m)^refs/heads/")[1] : name);
        try
        {
            viewHolder.mRadioButton.setChecked(Objects.equals(git.getRepository().getFullBranch(), call.get(i).getName()));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            int[] results = Library.calculateDivergence(call.get(i), git.getRepository().getRefDatabase().getRef("master"), git.getRepository());
            viewHolder.mTextContent.setText(String.format(Minicode.context.getString(R.string.s_behind_master_s_over_master), results[1], results[0]).concat("\n" + call.get(i).getObjectId().getName()));
        }
        catch (IOException | NullPointerException e)
        {
            e.printStackTrace();
        }
        viewHolder.mRadioButton.setOnClickListener(v ->
        {
            try
            {
                if (repository.getFullBranch().equals(call.get(i).getName()))
                {
                    return;
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
                new MaterialAlertDialogBuilder(fragment.getActivity()).setPositiveButton(R.string.ok, (dialog, which) ->
                {
                }).setMessage(R.string.checkout_failed).setTitle(R.string.error).show();
                return;
            }
            Handler h = new Handler();
            AlertDialog alertDialog = new AlertDialog.Builder(this.fragment.getActivity()).setMessage(R.string.checking_out).create();
            alertDialog.show();
            new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        CheckoutCommand checkoutCommand = git.checkout();
                        checkoutCommand.setName(call.get(i).getName());
                        checkoutCommand.setForce(true);
                        checkoutCommand.call();
                        sleep(100);
                        h.postDelayed(() ->
                        {
                            changeData();
                            notifyDataSetChanged();
                        }, 40);
                        h.post(alertDialog::cancel);
                    }
                    catch (GitAPIException | JGitInternalException | InterruptedException e)
                    {
                        e.printStackTrace();
                        h.post(alertDialog::cancel);
                        h.post(() -> new MaterialAlertDialogBuilder(fragment.getActivity()).setPositiveButton(R.string.ok, (dialog, which) ->
                        {
                        }).setMessage(R.string.checkout_failed).setTitle(R.string.error).show());
                        h.post(() ->
                        {
                            changeData();
                            notifyDataSetChanged();
                        });
                    }
                }
            }.start();
        });
        viewHolder.mBranchButton.setOnClickListener(v ->
        {
            EditText editText = new EditText(fragment.getActivity());
            editText.setHint(R.string.name);
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(fragment.getActivity());
            builder.setTitle(R.string.new_branch);
            builder.setView(editText);
            builder.setPositiveButton(R.string.ok, (dialog, which) ->
            {
                if (editText.getText().toString().isEmpty())
                {
                    Toast.makeText(Minicode.context, R.string.we_need_something_here, Toast.LENGTH_LONG).show();
                    return;
                }
                Thread t = new Thread()
                {
                    @Override
                    public void run()
                    {
                        CreateBranchCommand createBranchCommand = git.branchCreate();
                        createBranchCommand.setStartPoint(call.get(i).getName());
                        createBranchCommand.setName(editText.getText().toString());
                        try
                        {
                            createBranchCommand.call();
                            changeData();
                            fragment.getActivity().runOnUiThread(() -> notifyDataSetChanged());
                        }
                        catch (GitAPIException | JGitInternalException e)
                        {
                            e.printStackTrace();
                            fragment.getActivity().runOnUiThread(() -> Toast.makeText(Minicode.context, R.string.error, Toast.LENGTH_LONG).show());
                        }
                    }
                };
                t.start();
            });
            builder.show();
        });
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return call.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextHeading, mTextContent;
        public RadioButton mRadioButton;
        public LinearLayout mLinearLayout;
        public ImageButton mBranchButton;

        public ViewHolder(View v)
        {
            super(v);
            mTextHeading = v.findViewById(R.id.basic_about_text_header);
            mTextContent = v.findViewById(R.id.basic_about_text_content);
            mLinearLayout = (LinearLayout) v;
            mRadioButton = v.findViewById(R.id.selected_branch);
            mBranchButton = v.findViewById(R.id.imagebutton_branch);
            Log.d("Step", "Step: ViewHolder - Constructer");
        }
    }

    static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private BranchListAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(BranchListAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = Minicode.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(Minicode.context.getResources().getColor(R.color.holo_red_dark));
            icon = Minicode.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            int i = viewHolder.getAdapterPosition();
            Git git = new Git(mAdapter.repository);
            List<Ref> call;
            try
            {
                call = git.branchList().call();
            }
            catch (GitAPIException e)
            {
                e.printStackTrace();
                Log.wtf(getClass().getName(), e.getCause());
                return;
            }
            DeleteBranchCommand deleteBranchCommand = git.branchDelete();
            deleteBranchCommand.setBranchNames(call.get(i).getName());
            Library.vibrateAttention();
            new MaterialAlertDialogBuilder(mAdapter.fragment.getActivity()).setTitle(R.string.delete).setMessage(String.format(Minicode.context.getString(R.string.really_delete_s), call.get(i).getName())).setPositiveButton(R.string.yes, ((dialog, which) ->
            {
                try
                {
                    deleteBranchCommand.call();
                    Library.vibrateSuccess();
                }
                catch (GitAPIException e)
                {
                    Library.vibrateError();
                    e.printStackTrace();
                }
                mAdapter.changeData();
                mAdapter.notifyDataSetChanged();
            })).setNeutralButton(R.string.no, (dialog, which) ->
            {
                mAdapter.changeData();
                mAdapter.notifyDataSetChanged();
            }).setOnDismissListener((dialog ->
            {
                mAdapter.changeData();
                mAdapter.notifyDataSetChanged();
            })).show();

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Drawable background = this.background.getConstantState().newDrawable();
            background.mutate();
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }

    public static class MergeConflictSubAdapter extends RecyclerView.Adapter<MergeConflictSubAdapter.ViewHolder>
    {
        List<Conflict> conflicts = new LinkedList<>();

        public MergeConflictSubAdapter(MergeResult mergeResult)
        {
            Map<String, int[][]> allConflicts = mergeResult.getConflicts();
            for (String path : allConflicts.keySet())
            {
                Conflict conflict = new Conflict(path, new ArrayList<>());
                Map<Integer, List<Map<AnyObjectId, Integer>>> subSegment = new HashMap<>();
                int[][] c = allConflicts.get(path);
                for (int i = 0; i < c.length; ++i)
                {
                    ArrayList<Map<AnyObjectId, Integer>> subSubSegment = new ArrayList<>();
                    for (int j = 0; j < (c[i].length) - 1; ++j)
                    {
                        if (c[i][j] >= 0)
                        {
                            Map<AnyObjectId, Integer> map = new HashMap<>();
                            map.put(mergeResult.getMergedCommits()[j], c[i][j]);
                            subSubSegment.add(map);
                        }
                    }
                    subSegment.put(i, subSubSegment);
                }
                conflict.getResults().add(subSegment);
                conflicts.add(conflict);
            }
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            return new ViewHolder(LayoutInflater.from(Minicode.context).inflate(R.layout.basic_merge_conflict, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position)
        {
            Conflict conflict = conflicts.get(position);
            holder.mInfo.setText(String.format(Minicode.context.getString(R.string.merge_conflict_at), conflict.getFile()));
        }

        @Override
        public int getItemCount()
        {
            return conflicts.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder
        {
            public TextView mInfo;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);
                mInfo = itemView.findViewById(R.id.conflict_name);
            }
        }

        public static class Conflict
        {
            private String file;
            private List<Map<Integer, List<Map<AnyObjectId, Integer>>>> results;

            public Conflict(String file, List<Map<Integer, List<Map<AnyObjectId, Integer>>>> results)
            {
                this.file = file;
                this.results = results;
            }

            public List<Map<Integer, List<Map<AnyObjectId, Integer>>>> getResults()
            {
                return results;
            }

            public String getFile()
            {
                return file;
            }
        }
    }
}