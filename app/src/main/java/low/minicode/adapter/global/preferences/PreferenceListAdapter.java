/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.global.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import low.minicode.application.Minicode;
import low.minicode.Preference;
import low.minicode.PreferenceTypes;
import low.minicode.R;

public class PreferenceListAdapter extends RecyclerView.Adapter<PreferenceListAdapter.ViewHolder>
{
    private Preference[] content;
    private RecyclerView recyclerView;

    public PreferenceListAdapter(List<Preference> s)
    {
        ArrayList<Preference> where = new ArrayList<>(s);
        this.content = new Preference[where.size()];
        if (content.length < 1)
        {
            return;
        }
        where.toArray(this.content);
    }

    public void initializeWithNewData(List<Preference> s)
    {
        ArrayList<Preference> where = new ArrayList<>(s);
        this.content = new Preference[where.size()];
        where.toArray(this.content);
        this.notifyDataSetChanged();
        return;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        View v = null;
        if (i == PreferenceTypes.TYPE_CHECKBOX.ordinal())
        {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_preference_view_checkbox, parent, false);
        }
        else if (i == PreferenceTypes.TYPE_DROPDOWN.ordinal())
        {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_preference_view_dropdown, parent, false);
        }
        else if (i == PreferenceTypes.TYPE_SWITCH.ordinal())
        {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_preference_view_switch, parent, false);
        }
        else if (i == PreferenceTypes.TYPE_CLICK.ordinal())
        {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_preference_view_button, parent, false);
        }
        else if (i == PreferenceTypes.TYPE_SLIDE.ordinal())
        {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_preference_view_slider, parent, false);
        }
        return new ViewHolder(v);

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemViewType(int position)
    {
        return content[position].getType().ordinal();
    }

    @SuppressLint("ApplySharedPref")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        viewHolder.mTextContent.setText(content[i].getSmallHeader());
        viewHolder.mTextHeading.setText(content[i].getBigHeader());
        if (content[i].getType() == PreferenceTypes.TYPE_SWITCH)
        {
            viewHolder.mSwitch.setChecked(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean(content[i].getPreferenceName(), (Boolean) content[i].getDefaultValue()));
            viewHolder.mSwitch.setOnCheckedChangeListener((v, val) ->
            {
                Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putBoolean(content[i].getPreferenceName(), val).commit();
                if (content[i].getOnChangeListener() != null)
                {
                    content[i].getOnChangeListener().run();
                }
            });
            if (content[i].getDrawable() != null)
            {
                viewHolder.mImageView.setImageDrawable(content[i].getDrawable());
            }
        }
        if (content[i].getType() == PreferenceTypes.TYPE_CHECKBOX)
        {
            viewHolder.mCheckbox.setChecked(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean(content[i].getPreferenceName(), (Boolean) content[i].getDefaultValue()));
            viewHolder.mCheckbox.setOnCheckedChangeListener((v, val) ->
            {
                Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putBoolean(content[i].getPreferenceName(), val).commit();
                if (content[i].getOnChangeListener() != null)
                {
                    content[i].getOnChangeListener().run();
                }
            });
            if (content[i].getDrawable() != null)
            {
                viewHolder.mImageView.setImageDrawable(content[i].getDrawable());
            }
        }
        if (content[i].getType() == PreferenceTypes.TYPE_DROPDOWN)
        {
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Minicode.context, android.R.layout.simple_spinner_dropdown_item, content[i].getItems())
            {
                public View getView(int position, View convertView, ViewGroup parent)
                {
                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    tv.setTextColor(Color.WHITE);
                    return tv;
                }
            };
            viewHolder.mSpinner.setAdapter(arrayAdapter);
            viewHolder.mSpinner.setSelection(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt(content[i].getPreferenceName(), (Integer) content[i].getDefaultValue()));
            viewHolder.mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    if (position != Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt(content[i].getPreferenceName(), (Integer) content[i].getDefaultValue()))
                    {
                        Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putInt(content[i].getPreferenceName(), position).commit();
                        if (content[i].getOnChangeListener() != null)
                        {
                            content[i].getOnChangeListener().run();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {
                }
            });
            if (content[i].getDrawable() != null)
            {
                viewHolder.mImageView.setImageDrawable(content[i].getDrawable());
            }
        }
        if (content[i].getType() == PreferenceTypes.TYPE_CLICK)
        {
            viewHolder.mLinearLayout.setOnClickListener(content[i].getOnClickListener());
            if (content[i].getDrawable() != null)
            {
                viewHolder.mImageView.setImageDrawable(content[i].getDrawable());
            }
        }
        if (content[i].getType() == PreferenceTypes.TYPE_SLIDE)
        {
            viewHolder.mSeekBar.setMax(content[i].getSeekBarMax());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                viewHolder.mSeekBar.setMin(content[i].getSeekBarMin());
            }
            viewHolder.mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                {
                    if (progress < content[i].getSeekBarMin())
                    {
                        viewHolder.mSeekBar.setProgress(content[i].getSeekBarMin());
                        viewHolder.mTextValue.setText(String.valueOf(viewHolder.mSeekBar.getProgress()));
                        Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putInt(content[i].getPreferenceName(), viewHolder.mSeekBar.getProgress()).commit();
                        return;
                    }
                    viewHolder.mTextValue.setText(String.valueOf(progress));
                    Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putInt(content[i].getPreferenceName(), progress).commit();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar)
                {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar)
                {
                }
            });
            viewHolder.mTextValue.setText(String.valueOf(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt(content[i].getPreferenceName(), (Integer) content[i].getDefaultValue())));
            viewHolder.mSeekBar.setProgress(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt(content[i].getPreferenceName(), (Integer) content[i].getDefaultValue()));
            if (content[i].getDrawable() != null)
            {
                viewHolder.mImageView.setImageDrawable(content[i].getDrawable());
            }
        }
        viewHolder.mImageView.setVisibility(content[i].getDrawable() != null ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return this.content.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextHeading, mTextContent, mTextValue;
        public Spinner mSpinner;
        public CheckBox mCheckbox;
        public Switch mSwitch;
        public LinearLayout mLinearLayout;
        public ImageView mImageView;
        public SeekBar mSeekBar;

        public ViewHolder(View v)
        {
            super(v);
            mTextHeading = v.findViewById(R.id.basic_about_text_header);
            mTextContent = v.findViewById(R.id.basic_about_text_content);
            mSpinner = v.findViewById(R.id.spinner_pref);
            mCheckbox = v.findViewById(R.id.checkbox_pref);
            mSwitch = v.findViewById(R.id.switch_pref);
            mLinearLayout = v.findViewById(R.id.tap_pref);
            mImageView = v.findViewById(R.id.preferences_imageview);
            mSeekBar = v.findViewById(R.id.slider_pref);
            mTextValue = v.findViewById(R.id.value_pref);
        }
    }
}