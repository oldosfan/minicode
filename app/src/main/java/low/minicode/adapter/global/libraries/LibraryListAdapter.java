/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.global.libraries;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;

public class LibraryListAdapter extends RecyclerView.Adapter<LibraryListAdapter.ViewHolder>
{
    private ArrayList<File> libraries = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_customlibrary, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.textView.setText(libraries.get(position).getName());
        holder.remove.setOnClickListener(v ->
        {
            Handler h = new Handler();
            Executors.newSingleThreadExecutor().submit(() ->
            {
                Library.deleteDirectoryRecursively(libraries.get(position));
                h.post(() ->
                {
                    notifyItemRemoved(position);
                    if (libraries.size() <= 1)
                    {
                        notifyDataSetChanged();
                    }
                    Snackbar.make(holder.textView, R.string.item_removed, Snackbar.LENGTH_SHORT).show();
                });
            });
        });
    }

    @Override
    public int getItemCount()
    {
        libraries.clear();
        libraries.addAll(Arrays.asList(Library.getCustomLibDir().listFiles()));
        return libraries.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder
    {
        protected TextView textView;
        protected ImageButton remove;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            textView = itemView.findViewById(R.id.name);
            remove = itemView.findViewById(R.id.remove);
        }
    }
}
