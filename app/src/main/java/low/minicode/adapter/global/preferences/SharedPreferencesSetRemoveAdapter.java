/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.global.preferences;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import low.minicode.application.Minicode;
import low.minicode.R;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesSetRemoveAdapter extends RecyclerView.Adapter<SharedPreferencesSetRemoveAdapter.ViewHolder>
{
    private String[] content;

    public SharedPreferencesSetRemoveAdapter(Set<String> s)
    {
        this.content = new String[s.size()];
        if (content.length < 1)
        {
            return;
        }
        s.toArray(this.content);
    }

    public void initializeWithNewData(Set<String> s)
    {
        this.content = new String[s.size()];
        s.toArray(this.content);
        Minicode.context.getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit().putStringSet("auto_complete_additions", new ConcurrentSkipListSet<>(Arrays.asList(content))).apply();
        this.notifyDataSetChanged();
        return;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_customautocomplete_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @SuppressLint("ApplySharedPref")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        viewHolder.mTextContent.setText(content[i]);
        viewHolder.mRemoveButton.setOnClickListener(v ->
        {
            ArrayList<String> tmp = new ArrayList<>(Arrays.asList(content));
            tmp.remove(i);
            content = new String[tmp.size()];
            tmp.toArray(content);
            this.notifyDataSetChanged();
            Set<String> stringSet = new ConcurrentSkipListSet<>(tmp);
            Minicode.context.getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit().putStringSet("auto_complete_additions", stringSet).commit();
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return this.content.length;
    }

    public Set<String> getSet()
    {
        return new ConcurrentSkipListSet<>(Arrays.asList(content));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextContent;
        public ImageButton mRemoveButton;

        public ViewHolder(View v)
        {
            super(v);
            mRemoveButton = v.findViewById(R.id.remove_entry);
            mTextContent = v.findViewById(R.id.basic_autocomplete_name);
            Log.d("Step", "Step: ViewHolder - Constructer");
        }
    }
}