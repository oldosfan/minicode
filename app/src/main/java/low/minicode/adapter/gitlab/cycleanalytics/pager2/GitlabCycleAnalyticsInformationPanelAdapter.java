package low.minicode.adapter.gitlab.cycleanalytics.pager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import low.minicode.R;

public class GitlabCycleAnalyticsInformationPanelAdapter extends RecyclerView.Adapter<GitlabCycleAnalyticsInformationPanelAdapter.GitlabCycleAnalyticsViewHolder>
{
    private JSONObject object;

    public GitlabCycleAnalyticsInformationPanelAdapter(JSONObject cycleAnalytics)
    {
        this.object = cycleAnalytics;
    }

    @NonNull
    @Override
    public GitlabCycleAnalyticsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabCycleAnalyticsViewHolder(parent);
    }

    @Override
    public int getItemCount()
    {
        try
        {
            return object.getJSONArray("stats").length();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabCycleAnalyticsViewHolder holder, int position)
    {
        try
        {
            JSONObject currentStatus = object.getJSONArray("stats").getJSONObject(position);
            holder.amount.setText(currentStatus.isNull("value") ? holder.itemView.getContext().getString(R.string.not_enough_information) : currentStatus.getString("value"));
            holder.what.setText(currentStatus.isNull("description") ? holder.itemView.getContext().getString(R.string.not_enough_information) : currentStatus.getString("description"));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public static class GitlabCycleAnalyticsViewHolder extends RecyclerView.ViewHolder
    {
        public TextView what, amount;

        public GitlabCycleAnalyticsViewHolder(@NonNull View itemView)
        {
            super(itemView);
            what = itemView.findViewById(R.id.cycle_analytics_info_what);
            amount = itemView.findViewById(R.id.cycle_analytics_info_how_many);
        }

        public GitlabCycleAnalyticsViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_cycleanalytics_information_view, parentView, false));
        }
    }
}
