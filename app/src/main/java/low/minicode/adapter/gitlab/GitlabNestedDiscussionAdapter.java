/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.commons.io.input.CharSequenceReader;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.Note;
import org.gitlab4j.api.models.Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import low.minicode.GitlabDiscussionViewHolder;
import low.minicode.application.Minicode;
import low.minicode.R;
import ru.noties.markwon.Markwon;
import ru.noties.markwon.core.CorePlugin;
import ru.noties.markwon.image.ImagesPlugin;

public class GitlabNestedDiscussionAdapter extends RecyclerView.Adapter<GitlabDiscussionViewHolder>
{
    private final GitLabApi api;
    private final Project project;
    private final List<Discussion> discussions;
    private final List<Note> notes;
    private final Markwon markwon;
    private ReplyDeleteHandler handler;

    public GitlabNestedDiscussionAdapter(GitLabApi api, Project project, List<Discussion> discussions)
    {
        this.api = api;
        this.project = project;
        this.discussions = discussions;
        this.notes = new ArrayList<>();
        this.markwon = Markwon.builder(Minicode.context).usePlugin(CorePlugin.create()).usePlugin(ImagesPlugin.create(Minicode.context)).build();
        for (Discussion discussion : discussions)
        {
            notes.addAll(discussion.getNotes());
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        return notes.get(position).getSystem() ? GitlabDiscussionViewHolder.TYPE_ACTION : GitlabDiscussionViewHolder.TYPE_DISCUSSION;
    }

    @NonNull
    @Override
    public GitlabDiscussionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabDiscussionViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabDiscussionViewHolder holder, int position)
    {
        Note note = notes.get(position);
        holder.author.setText(note.getAuthor().getName());
        markwon.setMarkdown(holder.content, note.getBody());
        if (getItemViewType(position) == GitlabDiscussionViewHolder.TYPE_DISCUSSION)
        {
            holder.reply.setOnClickListener(v ->
            {
                MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(v.getContext());
                materialAlertDialogBuilder.setTitle(R.string.reply);
                EditText editText = new EditText(v.getContext());
                materialAlertDialogBuilder.setView(editText);
                StringBuilder reply = new StringBuilder().append(String.format("@%s\n", note.getAuthor().getName()));
                BufferedReader bufferedReader = new BufferedReader(new CharSequenceReader(note.getBody()));
                String line;
                try
                {
                    while ((line = bufferedReader.readLine()) != null)
                    {
                        reply.append("> ").append(line).append("\n");
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                editText.setText(reply);
                materialAlertDialogBuilder.setPositiveButton(R.string.reply, (dialog, which) ->
                {
                    if (handler != null)
                    {
                        handler.reply(note, v.getContext(), editText.getText().toString());
                    }
                });
                materialAlertDialogBuilder.show();
            });
            holder.remove.setOnClickListener(v ->
            {
                holder.remove.setClickable(false);
                if (handler != null)
                {
                    handler.delete(note, v.getContext());
                }
            });
        }
    }

    public void setNotes(List<Note> notes)
    {
        notes = notes;
    }

    @Override
    public int getItemCount()
    {
        return notes.size();
    }

    public void setHandler(ReplyDeleteHandler handler)
    {
        this.handler = handler;
    }

    public interface ReplyDeleteHandler
    {
        void reply(Note note, Context c, String newContent);

        void delete(Note note, Context c);

        void edit(Note note, Context c, String newContent);
    }
}
