/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.ci.jobs;

import android.os.Handler;
import android.os.HandlerThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Job;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.R;

public class GitlabCIJobListAdapter extends RecyclerView.Adapter<GitlabCIJobListAdapter.GitlabJobViewHolder>
{
    private final GitLabApi api;
    private final Project project;
    private final List<Job> jobList;
    private final Handler pagingHandler;
    private Pager<Job> pager;

    public GitlabCIJobListAdapter(GitLabApi api, Project project)
    {
        this.api = api;
        this.project = project;
        this.jobList = new ArrayList<>();
        HandlerThread handlerThread = new HandlerThread("Pager");
        handlerThread.start();
        pagingHandler = new Handler(handlerThread.getLooper());
    }

    public void refresh() throws GitLabApiException
    {
        jobList.clear();
        pager = api.getJobApi().getJobs(project.getId(), 25);
        if (pager.hasNext())
        {
            jobList.addAll(pager.next());
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                AtomicBoolean flag = new AtomicBoolean(true);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() && flag.get() && pager != null && pager.hasNext())
                {
                    flag.set(false);
                    pagingHandler.post(() ->
                    {
                        int presize = jobList.size();
                        jobList.addAll(pager.next());
                        recyclerView.post(() ->
                        {
                            recyclerView.post(() -> notifyItemRangeInserted(presize, jobList.size()));
                            flag.set(true);
                        });
                    });
                }
            }
        });
    }

    @NonNull
    @Override
    public GitlabJobViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabJobViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabJobViewHolder holder, int position)
    {
        Job job = jobList.get(position);
        holder.name.setText(job.getName());
        holder.target.setText(job.getStage());
        switch (job.getStatus())
        {
            case RUNNING:
                holder.status.setText(R.string.running);
                break;
            case SKIPPED:
                holder.status.setText(R.string.skipped);
                break;
            case FAILED:
                holder.status.setText(R.string.failed);
                break;
            case MANUAL:
                holder.status.setText(R.string.manual);
                break;
            case CREATED:
                holder.status.setText(R.string.created);
                break;
            case PENDING:
                holder.status.setText(R.string.pending);
                break;
            case SUCCESS:
                holder.status.setText(R.string.finished);
                break;
            case CANCELED:
                holder.status.setText(R.string.canceled);
                break;
        }
    }

    @Override
    public int getItemCount()
    {
        return jobList.size();
    }

    public static class GitlabJobViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name, target, status;

        public GitlabJobViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.job_name);
            target = itemView.findViewById(R.id.job_target);
            status = itemView.findViewById(R.id.job_state);
        }

        public GitlabJobViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_cijob, parentView, false));
        }
    }
}
