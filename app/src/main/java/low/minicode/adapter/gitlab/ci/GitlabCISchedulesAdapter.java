/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.ci;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.PipelineSchedule;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.application.Minicode;
import low.minicode.R;

public class GitlabCISchedulesAdapter extends RecyclerView.Adapter<GitlabCISchedulesAdapter.GitlabScheduleViewHolder>
{
    private final GitLabApi api;
    private final Project project;
    private final List<PipelineSchedule> scheduleList;
    private final List<PipelineSchedule> removals;
    private final ItemTouchHelper itemTouchHelper;

    public GitlabCISchedulesAdapter(GitLabApi api, Project project)
    {
        this.api = api;
        this.project = project;
        this.scheduleList = new ArrayList<>();
        this.removals = new ArrayList<>();
        this.itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
    }

    @NonNull
    @Override
    public GitlabScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabScheduleViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabScheduleViewHolder holder, int position)
    {
        PipelineSchedule schedule = scheduleList.get(position);
        holder.name.setText(schedule.getDescription());
        holder.crontab.setText(schedule.getCron() + " (" + schedule.getCronTimezone() + ")");
        holder.nextRun.setText(holder.itemView.getContext().getString(R.string.next_run_s, schedule.getNextRunAt().toString()));
        holder.branch.setText(schedule.getRef());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount()
    {
        Iterator<PipelineSchedule> pipelineScheduleIterator = removals.iterator();
        while (pipelineScheduleIterator.hasNext())
        {
            if (!scheduleList.contains(pipelineScheduleIterator.next()))
            {
                pipelineScheduleIterator.remove();
            }
        }
        pipelineScheduleIterator = scheduleList.iterator();
        while (pipelineScheduleIterator.hasNext())
        {
            if (removals.contains(pipelineScheduleIterator.next()))
            {
                pipelineScheduleIterator.remove();
            }
        }
        return scheduleList.size();
    }

    public void refresh() throws GitLabApiException
    {
        scheduleList.clear();
        scheduleList.addAll(api.getPipelineApi().getPipelineSchedules(project));
    }

    public static class GitlabScheduleViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name, crontab, branch, nextRun;

        public GitlabScheduleViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.schedule_name);
            crontab = itemView.findViewById(R.id.schedule_crontab);
            branch = itemView.findViewById(R.id.schedule_target);
            nextRun = itemView.findViewById(R.id.next_run);
        }

        public GitlabScheduleViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_cischedule, parentView, false));
        }
    }

    private static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private GitlabCISchedulesAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(GitlabCISchedulesAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = Minicode.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(Minicode.context.getResources().getColor(R.color.holo_red_dark));
            icon = Minicode.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            int i = viewHolder.getAdapterPosition();
            PipelineSchedule schedule = mAdapter.scheduleList.get(i);
            mAdapter.removals.add(schedule);
            mAdapter.scheduleList.remove(i);
            mAdapter.notifyItemRemoved(i);
            AtomicBoolean flag = new AtomicBoolean();
            flag.set(false);
            Snackbar.make(viewHolder.itemView, R.string.item_removed, Snackbar.LENGTH_LONG).setAction(R.string.undo, v ->
            {
                flag.set(true);
                mAdapter.removals.remove(schedule);
                mAdapter.scheduleList.add(i, schedule);
                mAdapter.notifyItemInserted(i);
            }).addCallback(new Snackbar.Callback()
            {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event)
                {
                    super.onDismissed(transientBottomBar, event);
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        if (!flag.get())
                        {
                            try
                            {
                                mAdapter.api.getPipelineApi().deletePipelineSchedule(mAdapter.project, schedule.getId());
                                mAdapter.removals.remove(null);
                                mAdapter.refresh();
                                viewHolder.itemView.post(mAdapter::notifyDataSetChanged);
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }).show();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Drawable background = this.background.getConstantState().newDrawable();
            background.mutate();
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }
}
