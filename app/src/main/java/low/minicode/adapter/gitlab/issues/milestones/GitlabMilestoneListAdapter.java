package low.minicode.adapter.gitlab.issues.milestones;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Milestone;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.R;
import low.minicode.activity.gitlab.issues.GitlabIssuesActivity;

public class GitlabMilestoneListAdapter extends RecyclerView.Adapter<GitlabMilestoneListAdapter.GitlabMilestoneViewHolder>
{
    private GitLabApi api;
    private Project project;
    private List<Milestone> milestones;
    private Pager<Milestone> milestonePager;
    private Handler handler;

    public GitlabMilestoneListAdapter(GitLabApi api, Project project)
    {
        this.api = api;
        this.project = project;
        this.milestones = new ArrayList<>();
        HandlerThread handlerThread = new HandlerThread("Paging HandlerThread");
        handlerThread.start();
        this.handler = new Handler(handlerThread.getLooper());
    }

    @NonNull
    @Override
    public GitlabMilestoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabMilestoneViewHolder(parent);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                AtomicBoolean flag = new AtomicBoolean(true);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() && flag.get() && milestonePager != null && milestonePager.hasNext())
                {
                    flag.set(false);
                    handler.post(() ->
                    {
                        int presize = milestones.size();
                        milestones.addAll(milestonePager.next());
                        recyclerView.post(() ->
                        {
                            recyclerView.post(() -> notifyItemRangeInserted(presize, milestones.size()));
                            flag.set(true);
                        });
                    });
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabMilestoneViewHolder holder, int position)
    {
        Milestone milestone = milestones.get(position);
        holder.name.setText(milestone.getTitle());
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                int amount = api.getMilestonesApi().getIssues(project, milestone.getId()).size();
                holder.issues.post(() -> holder.issues.setText(holder.itemView.getContext().getString(amount > 1 ? R.string.d_issues : R.string.one_issue, amount)));
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
        holder.itemView.setOnClickListener(v ->
        {
            Intent i = new Intent(v.getContext(), GitlabIssuesActivity.class);
            i.putExtra("project_id", project.getId());
            i.putExtra("milestone_id", milestone.getId());
            i.putExtra("token", api.getAuthToken());
            v.getContext().startActivity(i);
        });
    }

    @Override
    public int getItemCount()
    {
        return milestones.size();
    }

    public void refresh() throws GitLabApiException
    {
        milestones.clear();
        milestonePager = api.getMilestonesApi().getMilestones(project, 25);
        if (milestonePager.hasNext())
        {
            milestones.addAll(milestonePager.next());
        }
    }

    public static class GitlabMilestoneViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name, issues;

        public GitlabMilestoneViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.milestone_name);
            issues = itemView.findViewById(R.id.milestone_issues);
        }

        public GitlabMilestoneViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_gitlab_milestone, parentView, false));
        }
    }
}
