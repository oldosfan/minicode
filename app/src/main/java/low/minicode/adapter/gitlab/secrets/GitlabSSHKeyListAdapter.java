/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.secrets;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.SshKey;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.application.Minicode;
import low.minicode.R;

public class GitlabSSHKeyListAdapter extends RecyclerView.Adapter<GitlabSSHKeyListAdapter.ViewHolder>
{
    private final GitLabApi api;
    private final List<SshKey> sshKeys;
    private final List<SshKey> removals;
    private final ItemTouchHelper helper;

    public GitlabSSHKeyListAdapter(GitLabApi api)
    {
        this.api = api;
        this.sshKeys = new ArrayList<>();
        this.removals = new ArrayList<>();
        this.helper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        helper.attachToRecyclerView(recyclerView);
    }

    public void refresh() throws GitLabApiException
    {
        this.sshKeys.clear();
        this.sshKeys.addAll(api.getUserApi().getSshKeys());
        Iterator<SshKey> sshKeyIterator = removals.iterator();
        while (sshKeyIterator.hasNext())
        {
            if (!sshKeys.contains(sshKeyIterator.next()))
            {
                sshKeyIterator.remove();
            }
        }
        sshKeyIterator = sshKeys.iterator();
        while (sshKeyIterator.hasNext())
        {
            if (removals.contains(sshKeyIterator.next()))
            {
                sshKeyIterator.remove();
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        SshKey key = sshKeys.get(position);
        holder.at.setText(key.getCreatedAt().toString());
        holder.info.setText(key.getKey().length() > 15 ? key.getKey().substring(0, 15) + "..." : key.getKey());
        holder.name.setText(key.getTitle());
    }

    @Override
    public int getItemCount()
    {
        return sshKeys.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name, at, info;

        private ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.key_name);
            at = itemView.findViewById(R.id.key_date);
            info = itemView.findViewById(R.id.key_info);
        }

        public ViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_sshkey, parentView, false));
        }
    }

    private static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private GitlabSSHKeyListAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(GitlabSSHKeyListAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = Minicode.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(Minicode.context.getResources().getColor(R.color.holo_red_dark));
            icon = Minicode.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            int i = viewHolder.getAdapterPosition();
            SshKey sshKey = mAdapter.sshKeys.get(i);
            mAdapter.removals.add(sshKey);
            mAdapter.sshKeys.remove(i);
            mAdapter.notifyItemRemoved(i);
            AtomicBoolean flag = new AtomicBoolean();
            flag.set(false);
            Snackbar.make(viewHolder.itemView, R.string.item_removed, Snackbar.LENGTH_LONG).setAction(R.string.undo, v ->
            {
                flag.set(true);
                mAdapter.removals.remove(sshKey);
                mAdapter.sshKeys.add(i, sshKey);
                mAdapter.notifyItemInserted(i);
            }).addCallback(new Snackbar.Callback()
            {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event)
                {
                    super.onDismissed(transientBottomBar, event);
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        if (!flag.get())
                        {
                            try
                            {
                                mAdapter.api.getUserApi().deleteSshKey(sshKey.getId());
                                mAdapter.removals.remove(sshKey);
                                mAdapter.refresh();
                                viewHolder.itemView.post(mAdapter::notifyDataSetChanged);
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }).show();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Drawable background = this.background.getConstantState().newDrawable();
            background.mutate();
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }
}
