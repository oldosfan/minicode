/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.navigator;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.TreeItem;

import java.util.ArrayList;
import java.util.List;

import low.minicode.R;
import low.minicode.activity.gitlab.navigation.GitlabFileSummaryActivity;
import low.minicode.activity.gitlab.navigation.GitlabNavigatorActivity;

public class GitlabFilesAdapter extends RecyclerView.Adapter<GitlabFilesAdapter.ViewHolder>
{
    private List<TreeItem> files;
    private String location;
    private Project project;
    private GitLabApi api;
    private String ref;

    public GitlabFilesAdapter(String location, Project project, GitLabApi api, String ref)
    {
        this.files = new ArrayList<>();
        this.location = location;
        this.project = project;
        this.api = api;
        this.ref = ref;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_gitlab_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.folder.setVisibility(files.get(position).getType() == TreeItem.Type.TREE ? View.VISIBLE : View.GONE);
        holder.name.setText(files.get(position).getName());
        holder.itemView.setOnClickListener(v ->
        {
            if (files.get(position).getType() == TreeItem.Type.TREE)
            {
                Intent i = new Intent(v.getContext(), GitlabNavigatorActivity.class);
                i.putExtra("loc", files.get(position).getPath());
                i.putExtra("project_id", project.getId());
                i.putExtra("ref", ref);
                i.putExtra("token", api.getAuthToken());
                v.getContext().startActivity(i);
            }
            else if (files.get(position).getType() == TreeItem.Type.BLOB)
            {
                Intent i = new Intent(v.getContext(), GitlabFileSummaryActivity.class);
                i.putExtra("loc", files.get(position).getPath());
                i.putExtra("project_id", project.getId());
                i.putExtra("ref", ref);
                i.putExtra("token", api.getAuthToken());
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return files.size();
    }

    public void refresh() throws GitLabApiException
    {
        if (ref == null)
        {
            files = new ArrayList<>();
        }
        else
        {
            files = api.getRepositoryApi().getTree(project.getId(), location, ref);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name;
        public ImageView folder;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            folder = itemView.findViewById(R.id.folder_identifier);
        }
    }
}
