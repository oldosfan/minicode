/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.ci.pager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Project;

import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.fragments.gitlab.ci.GitlabContinuousIntegrationJobsFragment;
import low.minicode.fragments.gitlab.ci.GitlabContinuousIntegrationPipelinesFragment;
import low.minicode.fragments.gitlab.ci.GitlabContinuousIntegrationSchedulesFragment;

public class GitlabCIFragmentPagerAdapter extends FragmentPagerAdapter
{
    @NonNull
    private final FragmentManager fm;
    private final GitLabApi api;
    private final Project project;

    public GitlabCIFragmentPagerAdapter(@NonNull FragmentManager fm, GitLabApi api, Project project)
    {
        super(fm);
        this.fm = fm;
        this.api = api;
        this.project = project;
    }

    @NonNull
    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0:
                return new GitlabContinuousIntegrationPipelinesFragment(project, api);
            case 1:
                return new GitlabContinuousIntegrationJobsFragment(project, api);
            case 2:
                return new GitlabContinuousIntegrationSchedulesFragment(project, api);
            default:
                throw new RuntimeException("this adapter did not expect more than three tabs!");
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position)
    {
        switch (position)
        {
            case 0:
                return Minicode.context.getString(R.string.pipelines);
            case 1:
                return Minicode.context.getString(R.string.jobs);
            case 2:
                return Minicode.context.getString(R.string.schedules);
            default:
                throw new RuntimeException("this adapter can't handle more than three tabs!");
        }
    }

    @Override
    public int getCount()
    {
        return 3;
    }
}
