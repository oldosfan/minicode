/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Executors;

import low.minicode.R;
import low.minicode.activity.gitlab.projects.GitlabProjectSummaryActivity;

public class GitlabProjectsListAdapter extends RecyclerView.Adapter<GitlabProjectsListAdapter.ViewHolder>
{
    private ArrayList<Project> projectArrayList;
    private String apiToken;
    private GitLabApi api;

    public GitlabProjectsListAdapter(String apiToken)
    {
        this.apiToken = apiToken;
        projectArrayList = new ArrayList<>();
        api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, apiToken);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_gitlab_project, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Project project = projectArrayList.get(position);
        holder.itemView.setOnClickListener(v ->
        {
            Intent i = new Intent(v.getContext(), GitlabProjectSummaryActivity.class);
            i.putExtra("project", project.getPathWithNamespace());
            i.putExtra("apiKey", apiToken);
            v.getContext().startActivity(i);
        });
        holder.mName.setText(project.getName());
        holder.mDesc.setText(project.getDescription());
        holder.mDesc.setVisibility(project.getDescription() == null || (project.getDescription().isEmpty() || project.getDescription().trim().isEmpty()) ? View.GONE : View.VISIBLE);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            synchronized (holder)
            {
                Log.d(getClass().getName(), "Loading avatar for ViewHolder: " + holder.getClass().getName() + "@" + holder.hashCode());
                try
                {
                    if (projectArrayList.get(position).getAvatarUrl() != null)
                    {
                        holder.itemView.post((() -> holder.mAvatar.setVisibility(View.VISIBLE)));
                        Log.d(getClass().getName(), "Avatar for ViewHolder: " + holder.getClass().getName() + "@" + holder.hashCode() + " is not null");
                        Bitmap m = BitmapFactory.decodeStream(new URL(project.getAvatarUrl()).openStream());
                        holder.mAvatar.post(() -> holder.mAvatar.setImageBitmap(m));
                        Log.d(getClass().getName(), "Loaded avatar for ViewHolder: " + holder.getClass().getName() + "@" + holder.hashCode());
                    }
                    else
                    {
                        Log.w(getClass().getName(), "Avatar for ViewHolder: " + holder.getClass().getName() + "@" + holder.hashCode() + " is null! Hiding avatar view!");
                        holder.itemView.post((() -> holder.mAvatar.setVisibility(View.GONE)));
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    public GitLabApi getApi()
    {
        return api;
    }

    @Override
    public int getItemCount()
    {
        return projectArrayList.size();
    }

    public void refresh() throws GitLabApiException
    {
        projectArrayList.clear();
        projectArrayList.addAll(api.getProjectApi().getOwnedProjects());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mName, mDesc;
        public ImageView mAvatar;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.gitlab_project_icon);
            mName = itemView.findViewById(R.id.project);
            mDesc = itemView.findViewById(R.id.project_description);
        }
    }
}
