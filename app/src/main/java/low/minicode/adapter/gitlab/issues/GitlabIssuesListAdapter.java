/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.issues;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.R;
import low.minicode.activity.gitlab.issues.GitlabIssueSummaryActivity;

public class GitlabIssuesListAdapter extends RecyclerView.Adapter<GitlabIssuesListAdapter.ViewHolder>
{
    private final GitLabApi api;
    private final Project project;
    @Nullable
    private final Integer milestoneId;
    private final List<Issue> issueList;
    private final Handler handler;
    private Pager<Issue> pager;

    public GitlabIssuesListAdapter(GitLabApi api, Project project, @Nullable Integer milestoneId)
    {
        this.api = api;
        this.project = project;
        this.milestoneId = milestoneId;
        this.issueList = new ArrayList<>();
        HandlerThread tmp = new HandlerThread("");
        tmp.start();
        this.handler = new Handler(tmp.getLooper());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                AtomicBoolean flag = new AtomicBoolean(true);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() && flag.get() && pager != null && pager.hasNext())
                {
                    flag.set(false);
                    handler.post(() ->
                    {
                        int presize = issueList.size();
                        issueList.addAll(pager.next());
                        recyclerView.post(() ->
                        {
                            recyclerView.post(() -> notifyItemRangeInserted(presize, issueList.size()));
                            flag.set(true);
                        });
                    });
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Issue issue = issueList.get(position);
        holder.name.setText(issue.getTitle());
        holder.status.setText(issue.getState() == Constants.IssueState.CLOSED ? R.string.closed : R.string.open);
        holder.status.setTextColor(issue.getState() == Constants.IssueState.CLOSED ? holder.itemView.getContext().getResources().getColor(R.color.md_red_700) : holder.itemView.getContext().getResources().getColor(R.color.md_green_700));
        holder.no.setText(String.format("#%d", issue.getIid()));
        holder.author.setText(issue.getAuthor() != null ? issue.getAuthor().getName() : "");
        StringBuilder builder = new StringBuilder();
        builder.append(holder.itemView.getContext().getString(R.string.assigned_to_c));
        if (issue.getAssignees().size() == 1)
        {
            builder.append(" ").append(issue.getAssignee().getName());
        }
        else if (issue.getAssignees().size() > 1)
        {
            builder.append(" ").append(issue.getAssignee().getName()).append(holder.itemView.getContext().getString(R.string.comma_and_d_more, issue.getAssignees().size() - 1));
        }
        else
        {
            builder.append(" ").append(holder.itemView.getContext().getString(R.string.no_assignee));
        }
        holder.assignee.setText(builder);
        holder.itemView.setOnClickListener(v ->
        {
            Intent intent = new Intent(v.getContext(), GitlabIssueSummaryActivity.class);
            intent.putExtra("token", api.getAuthToken());
            intent.putExtra("issue_id", issue.getIid());
            intent.putExtra("project_id", project.getId());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return issueList.size();
    }

    public void refresh() throws GitLabApiException
    {
        issueList.clear();
        pager = milestoneId == null ? api.getIssuesApi().getIssues(project.getId(), 25) : null;
        if (milestoneId == null && pager != null && pager.hasNext())
        {
            issueList.addAll(pager.next());
        }
        else if (milestoneId != null)
        {
            issueList.addAll(api.getMilestonesApi().getIssues(project, milestoneId));
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView author, name, no, status, assignee;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            author = itemView.findViewById(R.id.issue_by);
            name = itemView.findViewById(R.id.issue_title);
            no = itemView.findViewById(R.id.issue_no);
            status = itemView.findViewById(R.id.issue_status);
            assignee = itemView.findViewById(R.id.issue_assignee);
        }

        public ViewHolder(@NonNull ViewGroup parent)
        {
            this(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_gitlab_issue, parent, false));
        }
    }
}
