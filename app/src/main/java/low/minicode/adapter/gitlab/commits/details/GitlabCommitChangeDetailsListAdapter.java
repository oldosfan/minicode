package low.minicode.adapter.gitlab.commits.details;

import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.github.difflib.UnifiedDiffUtils;
import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.Chunk;
import com.github.difflib.patch.DeltaType;
import com.github.difflib.patch.Patch;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Diff;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.application.Minicode;

public class GitlabCommitChangeDetailsListAdapter extends RecyclerView.Adapter<GitlabCommitChangeDetailsListAdapter.GitlabCommitFileViewHolder>
{
    private final GitLabApi api;
    private final Project project;
    private final Commit commit;
    private final List<Diff> diffList;

    public GitlabCommitChangeDetailsListAdapter(GitLabApi api, Project project, Commit commit)
    {
        this.api = api;
        this.project = project;
        this.commit = commit;
        this.diffList = new ArrayList<>();
    }

    public void refresh() throws GitLabApiException
    {
        Log.d(getClass().getName(), "refresh: refreshing diff list");
        diffList.clear();
        Log.d(getClass().getName(), "refresh: retrieving diff list");
        diffList.addAll(api.getCommitsApi().getDiff(project, commit.getId()));
        Log.d(getClass().getName(), "refresh: refresh complete. diff list size: " + diffList.size());
    }

    @NonNull
    @Override
    public GitlabCommitFileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabCommitFileViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabCommitFileViewHolder holder, int position)
    {
        Diff diff = diffList.get(position);
        holder.diffSource.setTypeface(Minicode.theMinicode.getEditorTypeface());
        holder.diffTarget.setTypeface(Minicode.theMinicode.getEditorTypeface());
        holder.information.setTitle(diff.getDeletedFile() ? "- " + diff.getOldPath() : (diff.getOldPath().equals(diff.getNewPath()) ? diff.getOldPath() : diff.getOldPath() + " -> " + diff.getNewPath()));
        Log.d(getClass().getName(), "onBindViewHolder: parsing diff: \n" + diff.getDiff());
        String validDiff = diff.getNewFile() ? "---- /dev/null\n++++ a/file\n" : (diff.getDeletedFile() ? "---- a/file\n++++ /dev/null\n" : "---- a/file\n++++ b/file\n") + diff.getDiff();
        Patch<String> patch = UnifiedDiffUtils.parseUnifiedDiff(Arrays.asList(validDiff.split("[\\n\\r]")));
        Log.d(getClass().getName(), "onBindViewHolder: split diff into segments: " + Arrays.toString(validDiff.split("[\\n\\r]")));
        {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            for (AbstractDelta<String> delta : patch.getDeltas())
            {
                Log.d(getClass().getName(), "onBindViewHolder: parser found");
                Chunk<String> chunk = delta.getSource();
                Log.d(getClass().getName(), "onBindViewHolder: diff parser parsed chunk with lines: " + chunk.getLines() + " and type " + delta.getType().name());
                for (String line : chunk.getLines())
                {
                    builder.append(line, new BackgroundColorSpan(holder.itemView.getContext().getResources().getColor(delta.getType() == DeltaType.EQUAL ? R.color.surface_color : (delta.getType() == DeltaType.DELETE ? R.color.red_200 : (delta.getType() == DeltaType.INSERT ? R.color.green_200 : R.color.blue_200)))), 0).append(Library.NEWLINE);
                }
            }
            holder.diffSource.setText(builder);
        }
        {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            for (AbstractDelta<String> delta : patch.getDeltas())
            {
                Log.d(getClass().getName(), "onBindViewHolder: parser found");
                Chunk<String> chunk = delta.getTarget();
                Log.d(getClass().getName(), "onBindViewHolder: diff parser parsed chunk with lines: " + chunk.getLines() + " and type " + delta.getType().name());
                for (String line : chunk.getLines())
                {
                    builder.append(line, new BackgroundColorSpan(holder.itemView.getContext().getResources().getColor(delta.getType() == DeltaType.EQUAL ? R.color.surface_color : (delta.getType() == DeltaType.DELETE ? R.color.red_200 : (delta.getType() == DeltaType.INSERT ? R.color.green_200 : R.color.blue_200)))), 0).append(Library.NEWLINE);
                }
            }
            holder.diffTarget.setText(builder);
        }
    }

    @Override
    public int getItemCount()
    {
        return diffList.size();
    }

    public static class GitlabCommitFileViewHolder extends RecyclerView.ViewHolder
    {
        public TextView diffSource, diffTarget;
        public Toolbar information;

        public GitlabCommitFileViewHolder(@NonNull View itemView)
        {
            super(itemView);
            diffSource = itemView.findViewById(R.id.commit_file_diff_source);
            diffTarget = itemView.findViewById(R.id.commit_file_diff_target);
            information = itemView.findViewById(R.id.commit_file_toolbar);
        }

        public GitlabCommitFileViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_commit_fileview, parentView, false));
        }
    }
}
