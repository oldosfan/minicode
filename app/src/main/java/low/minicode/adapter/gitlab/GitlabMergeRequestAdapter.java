/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.R;
import low.minicode.activity.gitlab.mergerequests.GitlabMergeRequestSummaryActivitiy;

public class GitlabMergeRequestAdapter extends RecyclerView.Adapter<GitlabMergeRequestAdapter.MergeRequestViewHolder>
{
    private final GitLabApi api;
    private final Project project;
    private final List<MergeRequest> mergeRequests;
    private final Handler handler;
    private Pager<MergeRequest> requestPager;

    public GitlabMergeRequestAdapter(GitLabApi api, Project project)
    {
        this.api = api;
        this.project = project;
        this.mergeRequests = new ArrayList<>();
        HandlerThread temporaryThread = new HandlerThread("HandlerThread@" + getClass().getCanonicalName() + "@" + hashCode());
        temporaryThread.start();
        this.handler = new Handler(temporaryThread.getLooper());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                AtomicBoolean flag = new AtomicBoolean(true);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() && flag.get() && requestPager != null && requestPager.hasNext())
                {
                    flag.set(false);
                    handler.post(() ->
                    {
                        int presize = mergeRequests.size();
                        mergeRequests.addAll(requestPager.next());
                        recyclerView.post(() ->
                        {
                            recyclerView.post(() -> notifyItemRangeInserted(presize, mergeRequests.size()));
                            flag.set(true);
                        });
                    });
                }
            }
        });
    }

    public void refresh() throws GitLabApiException
    {
        mergeRequests.clear();
        requestPager = api.getMergeRequestApi().getMergeRequests(project, 25);
        if (requestPager.hasNext())
        {
            mergeRequests.addAll(requestPager.next());
        }
    }

    @NonNull
    @Override
    public MergeRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new MergeRequestViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull MergeRequestViewHolder holder, int position)
    {
        MergeRequest mergeRequest = mergeRequests.get(position);
        holder.branchName.setText(String.format("%s -> %s", mergeRequest.getSourceBranch(), mergeRequest.getTargetBranch()));
        holder.user.setText(mergeRequest.getAuthor().getName());
        holder.name.setText(mergeRequest.getTitle());
        holder.iid.setText("#" + mergeRequest.getIid());
        holder.status.setText(mergeRequest.getState().contains("pen") ? R.string.open : R.string.closed);
        holder.status.setTextColor(mergeRequest.getState().contains("pen") ? holder.itemView.getContext().getResources().getColor(R.color.md_green_700) : holder.itemView.getContext().getResources().getColor(R.color.md_green_700));
        holder.itemView.setOnClickListener(v ->
        {
            Intent intent = new Intent(v.getContext(), GitlabMergeRequestSummaryActivitiy.class);
            intent.putExtra("project_id", project.getId());
            intent.putExtra("token", api.getAuthToken());
            intent.putExtra("merge_request_iid", mergeRequest.getIid());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return mergeRequests.size();
    }

    public static class MergeRequestViewHolder extends RecyclerView.ViewHolder
    {
        private TextView user, name, branchName, iid, status;

        public MergeRequestViewHolder(@NonNull View itemView)
        {
            super(itemView);
            user = itemView.findViewById(R.id.merge_request_user);
            name = itemView.findViewById(R.id.merge_request_name);
            branchName = itemView.findViewById(R.id.merge_request_branch_name);
            iid = itemView.findViewById(R.id.merge_request_iid);
            status = itemView.findViewById(R.id.merge_request_status);
        }

        public MergeRequestViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_gitlab_mr, parentView, false));
        }
    }
}
