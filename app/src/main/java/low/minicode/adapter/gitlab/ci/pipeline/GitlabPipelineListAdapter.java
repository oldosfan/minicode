/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.ci.pipeline;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Artifact;
import org.gitlab4j.api.models.Job;
import org.gitlab4j.api.models.Pipeline;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.R;

public class GitlabPipelineListAdapter extends RecyclerView.Adapter<GitlabPipelineListAdapter.GitlabPipelineViewHolder>
{
    private final List<Pipeline> pipelines;
    private final Project project;
    private final GitLabApi api;
    private final Handler recyclerPagingHandler;
    private Pager<Pipeline> pipelinePager;

    public GitlabPipelineListAdapter(Project project, GitLabApi api)
    {
        this.api = api;
        this.pipelines = new ArrayList<>();
        this.project = project;
        HandlerThread handlerThread = new HandlerThread("Paging HandlerThread at RecyclerView.Adapter" + hashCode());
        handlerThread.start();
        recyclerPagingHandler = new Handler(handlerThread.getLooper());
    }

    public void refresh() throws GitLabApiException
    {
        pipelinePager = api.getPipelineApi().getPipelines(project, 25);
        if (!pipelines.isEmpty())
        {
            int currentPage = pipelines.size() / 25;
            if (pipelinePager.getTotalPages() >= currentPage)
            {
                for (int i = 1; i <= currentPage; ++i)
                {
                    pipelines.addAll(pipelinePager.page(i));
                }
            }
        }
        else
        {
            if (pipelinePager.hasNext())
            {
                pipelines.addAll(pipelinePager.next());
            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                AtomicBoolean flag = new AtomicBoolean(true);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() && flag.get() && pipelinePager != null && pipelinePager.hasNext())
                {
                    flag.set(false);
                    recyclerPagingHandler.post(() ->
                    {
                        int presize = pipelines.size();
                        try
                        {
                            pipelines.addAll(pipelinePager.hasNext() ? pipelinePager.next() : Collections.emptyList());
                            recyclerView.post(() ->
                            {
                                recyclerView.post(() -> notifyItemRangeInserted(presize, pipelines.size()));
                                flag.set(true);
                            });
                        }
                        catch (NoSuchElementException e)
                        {
                            e.printStackTrace();
                        }
                    });
                }
            }
        });
    }

    @NonNull
    @Override
    public GitlabPipelineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabPipelineViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabPipelineViewHolder holder, int position)
    {
        Pipeline pipeline = pipelines.get(position);
        holder.stages.setAdapter(null);
        holder.id.setText("#" + pipeline.getId());
        holder.branch.setText(pipeline.getRef());
        holder.commit.setText(pipeline.getSha());
        holder.downloadArtifacts.setOnCreateContextMenuListener(((menu, v, menuInfo) -> menu.clear()));
        holder.stages.setLayoutManager(new LinearLayoutManager(holder.itemView.getContext(), RecyclerView.HORIZONTAL, false));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            Log.d(getClass().getName(), "onBindViewHolder: resolving artifacts");
            List<String> artifacts = new ArrayList<>();
            try
            {
                for (Job job : api.getJobApi().getJobsForPipeline(project, pipeline.getId()))
                {
                    Log.d(getClass().getName(), "onBindViewHolder: *    resolving artifacts for job: " + job.getName());
                    for (Artifact artifact : job.getArtifacts())
                    {
                        Log.d(getClass().getName(), "onBindViewHolder: *        resolved artifact " + artifact.getFilename() + " for job " + job.getName());
                        artifacts.add(job.getName() + ": " + artifact.getFilename());
                    }
                }
            }
            catch (GitLabApiException e)
            {
                Log.d(getClass().getName(), "onBindViewHolder: exception caught while resolving artifacts for pipeline " + pipeline.getId() + "! printing stack!");
                e.printStackTrace();
            }
            holder.downloadArtifacts.post(() -> holder.downloadArtifacts.setOnCreateContextMenuListener((menu, v, menuInfo) ->
            {
                for (String iter : artifacts)
                {
                    menu.add(iter);
                }
            }));
        });
        holder.downloadArtifacts.setOnClickListener(view ->
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                holder.downloadArtifacts.showContextMenu(holder.downloadArtifacts.getX(), holder.downloadArtifacts.getY());
            }
            else
            {
                holder.downloadArtifacts.showContextMenu();
            }
        });
        GitlabPipelineStagesAdapter adapter = new GitlabPipelineStagesAdapter(api, pipeline, project);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            Log.d(getClass().getName(), "onBindViewHolder: resolving mini-graph");
            try
            {
                Log.d(getClass().getName(), "onBindViewHolder: Refreshing mini-graph");
                adapter.refreshAndSort();
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
            Log.d(getClass().getName(), "onBindViewHolder: setting adapter");
            holder.itemView.post(() -> holder.stages.setAdapter(adapter));
            Log.d(getClass().getName(), "onBindViewHolder: notifing adapter of data change");
            holder.itemView.post(adapter::notifyDataSetChanged);
        });
    }

    @Override
    public int getItemCount()
    {
        return pipelines.size();
    }

    public static class GitlabPipelineViewHolder extends RecyclerView.ViewHolder
    {
        public TextView id, branch, commit;
        public RecyclerView stages;
        public ImageButton downloadArtifacts;

        public GitlabPipelineViewHolder(@NonNull View itemView)
        {
            super(itemView);
            id = itemView.findViewById(R.id.pipeline_no);
            branch = itemView.findViewById(R.id.pipeline_branch);
            commit = itemView.findViewById(R.id.pipeline_commit);
            stages = itemView.findViewById(R.id.pipeline_minigraph);
            downloadArtifacts = itemView.findViewById(R.id.download_artifacts);
            stages.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.HORIZONTAL, false));
        }

        public GitlabPipelineViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_gitlab_pipeline, parentView, false));
        }
    }
}
