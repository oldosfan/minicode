/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.issues;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Assignee;
import org.gitlab4j.api.models.Issue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;

public class GitlabIssueAssigneeListAdapter extends RecyclerView.Adapter<GitlabIssueAssigneeListAdapter.IssueAssigneeViewHolder>
{
    private Issue issue;
    private GitLabApi api;
    private List<Assignee> assignees;
    private List<Assignee> removals;
    private OnStateChangeListener onStateChangeListener;
    private ItemTouchHelper itemTouchHelper;

    public GitlabIssueAssigneeListAdapter(Issue issue, GitLabApi api)
    {
        this.issue = issue;
        this.api = api;
        this.assignees = new ArrayList<>();
        this.itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
        this.removals = new ArrayList<>();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    public void refresh() throws GitLabApiException
    {
        assignees.clear();
        assignees.addAll((issue = api.getIssuesApi().getIssue(issue.getProjectId(), issue.getIid())).getAssignees());
        Iterator<Assignee> assigneeIterator = removals.iterator();
        while (assigneeIterator.hasNext())
        {
            if (!assignees.contains(assigneeIterator.next()))
            {
                assigneeIterator.remove();
            }
        }
        assigneeIterator = assignees.iterator();
        while (assigneeIterator.hasNext())
        {
            if (removals.contains(assigneeIterator.next()))
            {
                assigneeIterator.remove();
            }
        }
    }

    @NonNull
    @Override
    public IssueAssigneeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new IssueAssigneeViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull IssueAssigneeViewHolder holder, int position)
    {
        Assignee assignee = assignees.get(position);
        holder.username.setText(assignee.getUsername());
        holder.name.setText(assignee.getName());
    }

    @Override
    public int getItemCount()
    {
        return assignees.size();
    }

    public List<Assignee> getAssignees()
    {
        return assignees;
    }

    public void setOnStateChangeListener(OnStateChangeListener onStateChangeListener)
    {
        this.onStateChangeListener = onStateChangeListener;
    }

    public interface OnStateChangeListener
    {
        void onChanged();
    }

    public static class IssueAssigneeViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name, username;

        public IssueAssigneeViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.assignee_name);
            username = itemView.findViewById(R.id.assignee_username);
        }

        public IssueAssigneeViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_issueassignee, parentView, false));
        }
    }

    private static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private GitlabIssueAssigneeListAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(GitlabIssueAssigneeListAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = Minicode.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(Minicode.context.getResources().getColor(R.color.holo_red_dark));
            icon = Minicode.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            int i = viewHolder.getAdapterPosition();
            Assignee assignee = mAdapter.assignees.get(i);
            mAdapter.removals.add(assignee);
            mAdapter.assignees.remove(i);
            mAdapter.notifyItemRemoved(i);
            AtomicBoolean flag = new AtomicBoolean(false);
            Snackbar.make(viewHolder.itemView, R.string.item_removed, Snackbar.LENGTH_LONG).setAction(R.string.undo, v ->
            {
                flag.set(true);
                mAdapter.removals.remove(assignee);
                mAdapter.assignees.add(i, assignee);
                mAdapter.notifyItemInserted(i);
            }).addCallback(new Snackbar.Callback()
            {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event)
                {
                    super.onDismissed(transientBottomBar, event);
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        if (flag.get())
                        {
                            return;
                        }
                        Log.d(getClass().getName(), "removing assignee: " + assignee.getUsername() + " (" + assignee.getId() + ")");
                        try
                        {
                            Log.d(getClass().getName(), "iterating through assignees to remove assignee " + assignee.getUsername() + " (" + assignee.getId() + ")");
                            ArrayList<Integer> integers = new ArrayList<>();
                            for (Assignee assignee2 : mAdapter.issue.getAssignees())
                            {
                                Log.d(getClass().getName(), "*    processing assignee" + assignee2.getUsername() + " (" + assignee2.getId() + ")");
                                if (assignee2 != assignee)
                                {
                                    Log.d(getClass().getName(), "    *    assignee " + assignee2.getUsername() + " (" + assignee2.getId() + ") is not to be removed");
                                    integers.add(assignee2.getId());
                                }
                                else
                                {
                                    Log.d(getClass().getName(), "    *    assignee " + assignee2.getUsername() + " (" + assignee2.getId() + ") will be removed");
                                }
                            }
                            Log.d(getClass().getName(), "result: " + integers.toString());
                            Log.d(getClass().getName(), "sending data to server");
                            if (integers.isEmpty())
                            {
                                Log.d(getClass().getName(), "*    data is empty... assuming all assignees have been removed. setting data to NONE");
                                integers.add(0);
                            }
                            mAdapter.api.getIssuesApi().updateIssue(mAdapter.issue.getProjectId(), mAdapter.issue.getIid(), null, null, null, integers, null, null, null, null, null);
                            Log.d(getClass().getName(), "data sent. clearing removals");
                            mAdapter.removals.remove(assignee);
                            Log.d(getClass().getName(), "reloading data");
                            mAdapter.refresh();
                            Log.d(getClass().getName(), "notifying adapter of data change");
                            viewHolder.itemView.post(mAdapter::notifyDataSetChanged);
                            if (mAdapter.onStateChangeListener != null)
                            {
                                mAdapter.onStateChangeListener.onChanged();
                                Log.d(getClass().getName(), "*    running the state changed listener");
                            }
                        }
                        catch (GitLabApiException e)
                        {
                            Log.e(getClass().getName(), "Unexpected exception caught: printing stack");
                            e.printStackTrace();
                            viewHolder.itemView.post(() -> Library.displayErrorDialogFromThrowable(viewHolder.itemView.getContext(), e, true));
                        }
                    });
                }
            }).show();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Drawable background = this.background.getConstantState().newDrawable();
            background.mutate();
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }
}
