/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.ci.pipeline;

import android.content.res.ColorStateList;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Job;
import org.gitlab4j.api.models.JobStatus;
import org.gitlab4j.api.models.Pipeline;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.R;

public class GitlabPipelineStagesAdapter extends RecyclerView.Adapter<GitlabPipelineStagesAdapter.GitlabPipelineStageViewHolder>
{
    private final GitLabApi api;
    private final Pipeline pipeline;
    private final Project project;
    private final List<Job> jobs;
    private final List<String> stages;
    private int failThreshold = -1;

    public GitlabPipelineStagesAdapter(GitLabApi api, Pipeline pipeline, Project project)
    {
        this.api = api;
        this.pipeline = pipeline;
        this.project = project;
        this.jobs = new ArrayList<>();
        this.stages = new ArrayList<>();
        Handler h = new Handler();
        Executors.newSingleThreadExecutor().submit(() ->
        {
            while (true)
            {
                wait(450);
                refreshAndSort();
                h.post(this::notifyDataSetChanged);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull GitlabPipelineStageViewHolder holder, int position)
    {
        String stage = stages.get(position);
        holder.startIndicator.setVisibility(position < (failThreshold != -1 ? failThreshold + 1 : getItemCount()) && position < getItemCount() ? View.VISIBLE : View.GONE);
        holder.endIndicator.setVisibility((position < (failThreshold != -1 ? failThreshold + 1 : Integer.MAX_VALUE)) && position < (getItemCount() - 1) ? View.VISIBLE : View.GONE);
        holder.chip.setText(stage);
        for (Job j : jobs)
        {
            if (j.getStage().equals(stage))
            {
                failThreshold = -1;
                if (j.getStatus() == JobStatus.FAILED || j.getStatus() == JobStatus.CANCELED)
                {
                    holder.chip.setChipIconResource(R.drawable.ic_err_24dp);
                    holder.chip.setChipIconTint(ColorStateList.valueOf(holder.itemView.getContext().getResources().getColor(R.color.red_500)));
                    failThreshold = position;
                    break;
                }
                else if (j.getStatus() == JobStatus.PENDING)
                {
                    holder.chip.setChipIconResource(R.drawable.ic_av_timer_black_24dp);
                    holder.chip.setChipIconTint(ColorStateList.valueOf(holder.itemView.getContext().getResources().getColor(R.color.yellow_700)));
                }
                else if (j.getStatus() == JobStatus.SKIPPED)
                {
                    holder.chip.setChipIconResource(R.drawable.ic_err_24dp);
                    holder.chip.setChipIconTint(ColorStateList.valueOf(holder.itemView.getContext().getResources().getColor(R.color.grey_500)));
                }
                else if (j.getStatus() == JobStatus.RUNNING)
                {
                    holder.chip.setChipIconResource(R.drawable.ic_play_arrow_black_24dp);
                    holder.chip.setChipIconTint(ColorStateList.valueOf(holder.itemView.getContext().getResources().getColor(R.color.green_500)));
                }
                else
                {
                    holder.chip.setChipIconResource(R.drawable.ic_check_white);
                    holder.chip.setChipIconTint(ColorStateList.valueOf(holder.itemView.getContext().getResources().getColor(R.color.gplus_color_3)));
                }
            }
        }
    }

    @NonNull
    @Override
    public GitlabPipelineStageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new GitlabPipelineStageViewHolder(parent);
    }

    public void refreshAndSort() throws GitLabApiException
    {
        Log.d(getClass().getName(), "refreshAndSort: refreshing dataset");
        jobs.clear();
        stages.clear();
        jobs.addAll(api.getJobApi().getJobsForPipeline(project, pipeline.getId()));
        for (Job j : jobs)
        {
            if (!stages.contains(j.getStage()))
            {
                stages.add(j.getStage());
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return stages.size();
    }

    public static class GitlabPipelineStageViewHolder extends RecyclerView.ViewHolder
    {
        private Chip chip;
        private View startIndicator, endIndicator;

        public GitlabPipelineStageViewHolder(@NonNull View itemView)
        {
            super(itemView);
            chip = itemView.findViewById(R.id.pipeline_stage);
            startIndicator = itemView.findViewById(R.id.pipeline_connector_start);
            endIndicator = itemView.findViewById(R.id.pipeline_connector_end);
        }

        public GitlabPipelineStageViewHolder(@NonNull ViewGroup parentView)
        {
            this(LayoutInflater.from(parentView.getContext()).inflate(R.layout.basic_gitlab_pipeline_stage, parentView, false));
        }
    }
}
