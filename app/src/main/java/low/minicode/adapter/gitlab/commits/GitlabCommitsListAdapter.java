/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.gitlab.commits;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Diff;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.Selection;
import low.minicode.activity.gitlab.commits.GitlabCommitSummaryActivity;
import low.minicode.application.Minicode;

public class GitlabCommitsListAdapter extends RecyclerView.Adapter<GitlabCommitsListAdapter.ViewHolder>
{
    private final GitLabApi api;
    private final Project project;
    private final String ref;
    private @Nullable
    final String file;
    private final Handler handler;
    private final Executor imageResolutionExecutor;
    private Pager<Commit> pager;
    private List<Commit> commits;
    private static final Object WRITE_CACHE_LOCK = new Object();

    public GitlabCommitsListAdapter(GitLabApi api, Project project, String ref, @Nullable String file)
    {
        this.api = api;
        this.project = project;
        this.ref = ref;
        this.file = file;
        this.commits = new ArrayList<>();
        HandlerThread handlerThread = new HandlerThread("GitlabCommitsIndexThread");
        handlerThread.start();
        this.handler = new Handler(handlerThread.getLooper());
        this.imageResolutionExecutor = new ThreadPoolExecutor(5, 25, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                AtomicBoolean flag = new AtomicBoolean(true);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if ((visibleItemCount + firstVisibleItemPosition) >= getItemCount() && flag.get() && pager != null && pager.hasNext())
                {
                    flag.set(false);
                    handler.post(() ->
                    {
                        int presize = commits.size();
                        commits.addAll(pager.next());
                        recyclerView.post(() ->
                        {
                            recyclerView.post(() -> notifyItemRangeInserted(presize, commits.size()));
                            flag.set(true);
                        });
                    });
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Commit commit = commits.get(position);
        holder.avatar.setImageBitmap(null);
        imageResolutionExecutor.execute(() ->
        {
            try
            {
                User user = api.getUserApi().getUser(commit.getAuthorName());
                if (holder.getAdapterPosition() != position)
                {
                    return;
                }
                if (user != null && user.getAvatarUrl() != null)
                {
                    File cachedImage = new File(Library.getImagesCacheDir(), "cached-image-png-" + Library.getMD5Hash(user.getAvatarUrl()));
                    Bitmap bitmap = BitmapFactory.decodeStream(cachedImage.exists() ? new FileInputStream(cachedImage) : new URL(user.getAvatarUrl()).openStream());
                    if (holder.getAdapterPosition() != position)
                    {
                        return;
                    }
                    holder.avatar.post(() -> holder.avatar.setImageBitmap(bitmap));
                    if (!cachedImage.exists())
                    {
                        synchronized (WRITE_CACHE_LOCK)
                        {
                            FileOutputStream outputStream = new FileOutputStream(cachedImage);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                            outputStream.flush();
                            outputStream.close();
                        }
                    }
                }
            }
            catch (GitLabApiException | IOException e)
            {
                e.printStackTrace();
            }
        });
        holder.description.setText(commits.get(position).getTitle());
        holder.hash.setText(commits.get(position).getId());
        holder.user.setText(String.format("%s [%s]", commits.get(position).getAuthorName(), commits.get(position).getCommitterEmail()));
        holder.itemView.setOnClickListener(v2 ->
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(holder.itemView.getContext());
            builder.setCancelable(false);
            View v = LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout._dialog_commit, null);
            builder.setView(v);
            builder.setPositiveButton(R.string.ok, (dialog, which) ->
            {
            });
            builder.setNeutralButton(R.string.more_information, (dialog, which) ->
            {
                Intent i = new Intent(v.getContext(), GitlabCommitSummaryActivity.class);
                i.putExtra("token", api.getAuthToken());
                i.putExtra("serialized_project", project.toString());
                i.putExtra("serialized_commit", commit.toString());
                v.getContext().startActivity(i);
            });
            builder.setTitle(commits.get(position).getId());
            builder.show();
            TextView commitText = v.findViewById(R.id.commit_description);
            MaterialButton cherryPick = v.findViewById(R.id.cherry_pick);
            cherryPick.setVisibility(View.GONE);
            StringBuilder stringBuilder = new StringBuilder().append("commit ").append(commit.getId()).append('\n').append(commit.toString());
            commitText.setText(stringBuilder);
            RecyclerView commitRecycler = v.findViewById(R.id.recycler_commit_files);
            commitRecycler.setLayoutManager(new LinearLayoutManager(v2.getContext()));
            CommitFilesSubAdapter adapter;
            commitRecycler.setAdapter(adapter = new CommitFilesSubAdapter(project, commit, api));
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    adapter.refresh();
                }
                catch (GitLabApiException e)
                {
                    e.printStackTrace();
                }
                v2.post(adapter::notifyDataSetChanged);
            });
        });
    }

    @Override
    public int getItemCount()
    {
        Log.d(getClass().getName() + "@" + hashCode(), "getItemCount: " + commits.size());
        return commits.size();
    }

    public void refresh() throws GitLabApiException
    {
        commits.clear();
        if (file == null)
        {
            pager = api.getCommitsApi().getCommits(project.getId(), ref, null, null, null, 25);
        }
        else
        {
            pager = api.getCommitsApi().getCommits(project, ref, null, null, file, 25);
        }
        commits.addAll(pager.hasNext() ? pager.next() : Collections.emptyList());
        Log.d(getClass().getName() + "@" + hashCode(), "refresh: " + commits.toString());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView avatar;
        public TextView user, hash, description;

        public ViewHolder(ViewGroup parent)
        {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_gitlab_commit, parent, false));
            avatar = itemView.findViewById(R.id.commit_avatar);
            user = itemView.findViewById(R.id.commit_by);
            hash = itemView.findViewById(R.id.commit_shasum);
            description = itemView.findViewById(R.id.commit_title);
        }

        public ViewHolder(View itemView)
        {
            super(itemView);
            avatar = itemView.findViewById(R.id.commit_avatar);
            user = itemView.findViewById(R.id.commit_by);
            hash = itemView.findViewById(R.id.commit_shasum);
            description = itemView.findViewById(R.id.commit_title);
        }
    }

    public static class CommitFilesSubAdapter extends RecyclerView.Adapter<GitlabCommitsListAdapter.CommitFilesSubAdapter.ViewHolder>
    {
        private Project project;
        private Commit commit;
        private GitLabApi api;
        private List<Diff> diffEntries;

        public CommitFilesSubAdapter(Project project, Commit commit, GitLabApi api)
        {
            this.project = project;
            this.commit = commit;
            this.api = api;
            this.diffEntries = new ArrayList<>();
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_commit_file, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position)
        {
            Diff diff = diffEntries.get(position);
            String data = diff.getDiff();
            if (diff.getNewFile())
            {
                holder.mImageView.setImageDrawable(holder.itemView.getContext().getDrawable(R.drawable.ic_add_white_24dp));
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_green_light)));
            }
            else if (diff.getDeletedFile())
            {
                holder.mImageView.setImageDrawable(holder.itemView.getContext().getDrawable(R.drawable.ic_remove_black_24dp));
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_red_light)));
            }
            else if (diff.getRenamedFile())
            {
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.accent_device_default_light)));
                holder.mImageView.setImageResource(R.drawable.ic_redo_black_24dp);
            }
            else
            {
                holder.mImageView.setImageDrawable(holder.itemView.getContext().getDrawable(R.drawable.ic_add_white_24dp));
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_green_light)));
                holder.mImageView.setImageTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.holo_orange_dark)));
            }
            Executors.newSingleThreadExecutor().submit(() ->
            {
                Matcher m = Pattern.compile("^([+\\-].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
                int countadd = 0, countless = 0;
                while (m.find())
                {
                    String substring = data.substring(m.start(), m.end());
                    if (substring.matches("^((---)|(\\+\\+\\+)) .*$"))
                    {
                        continue;
                    }
                    if (substring.charAt(0) == '-')
                    {
                        ++countless;
                    }
                    else if (substring.charAt(0) == '+')
                    {
                        ++countadd;
                    }
                    else if (substring.equals("Binary files differ"))
                    {
                        ++countadd;
                    }
                }
                String result = String.format(Minicode.context.getString(R.string.d_add_d_del), countadd, countless);
                Log.d(getClass().getName(), String.format("%s: result: %s", Thread.currentThread().getName(), result));
                holder.mAddDel.post(() -> holder.mAddDel.setText(result));
            });
            holder.mFileName.setText(diff.getOldPath());
            holder.itemView.setOnClickListener(v ->
            {
                ScrollView v13 = (ScrollView) LayoutInflater.from(Minicode.context).inflate(R.layout._scrollview_mh, null);
                v13.setBackgroundResource(R.drawable.roundable_sharp);
                TextView textView = new EditText(Minicode.context);
                textView.setEnabled(false);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                textView.setTypeface(Minicode.theMinicode.getEditorTypeface());
                v13.addView(textView);
                textView.setText(data);
                MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(v.getContext(), R.style.Theme_MaterialComponents_Light_Dialog);
                dialogBuilder.setView(v13);
                dialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                });
                dialogBuilder.show();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    ArrayList<Selection> selections = new ArrayList<>();
                    ArrayList<Integer> flags = new ArrayList<>();
                    Matcher m = Pattern.compile("^([+\\-@].*$)|(Binary files differ)", Pattern.MULTILINE).matcher(data);
                    while (m.find())
                    {
                        String substring = data.substring(m.start(), m.end());
                        if (substring.charAt(0) == '-')
                        {
                            flags.add(1);
                        }
                        else if (substring.charAt(0) == '+')
                        {
                            flags.add(2);
                        }
                        else if (substring.equals("Binary files differ"))
                        {
                            flags.add(3);
                        }
                        else
                        {
                            flags.add(4);
                        }
                        selections.add(new Selection(m.start(), m.end()));
                    }
                    int i = 0;
                    for (Selection iter : selections)
                    {
                        BackgroundColorSpan backgroundColorSpan;
                        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.WHITE);
                        if (flags.get(i).equals(1))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.red));
                        }
                        else if (flags.get(i).equals(2))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_green_light));
                        }
                        else if (flags.get(i).equals(3))
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.holo_orange_light));
                        }
                        else
                        {
                            backgroundColorSpan = new BackgroundColorSpan(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                        }
                        textView.post(() -> textView.getEditableText().setSpan(backgroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                        textView.post(() -> textView.getEditableText().setSpan(foregroundColorSpan, iter.getStart(), iter.getEnd(), 0));
                        ++i;
                    }

                });
            });
        }

        public void refresh() throws GitLabApiException
        {
            this.diffEntries.clear();
            this.diffEntries.addAll(api.getCommitsApi().getDiff(project.getId(), commit.getId()));
        }

        @Override
        public int getItemCount()
        {
            return diffEntries.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder
        {
            public ImageView mImageView;
            public TextView mFileName, mAddDel;
            public View v;

            public ViewHolder(@NonNull View itemView)
            {
                super(itemView);
                this.v = itemView;
                this.mImageView = itemView.findViewById(R.id.commit_imageview_file);
                this.mFileName = itemView.findViewById(R.id.commit_file_name);
                this.mAddDel = itemView.findViewById(R.id.add_del_amount);
            }
        }
    }
}
