/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.compiler;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import low.minicode.R;
import low.minicode.activity.compiler.errors.ViewErrorsActivity;
import low.minicode.activity.editor.EditorActivity;
import low.minicode.application.Minicode;
import low.minicode.wrappers.data.error.Error;

public class ErrorAdapter extends RecyclerView.Adapter<ErrorAdapter.ViewHolder>
{
    private Error[] content;
    private ViewErrorsActivity activity;

    public ErrorAdapter(List<Error> err, ViewErrorsActivity activity)
    {
        ArrayList<Error> where = new ArrayList<>(err);
        this.activity = activity;
        this.content = new Error[where.size()];
        if (content.length < 1)
        {
            return;
        }
        where.toArray(this.content);
        for (Error iter : this.content)
        {
            Log.d("ErrorList", iter.toString());
        }
    }

    public void initializeWithNewData(List<Error> s)
    {
        ArrayList<Error> where = new ArrayList<>(s);
        this.content = new Error[where.size()];
        where.toArray(this.content);
        for (Error iter : this.content)
        {
            Log.d("Err", iter.toString());
        }
        this.notifyDataSetChanged();
        return;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_errorview, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        String state;
        if (content[i].type.contains("error"))
        {
            state = Minicode.context.getString(R.string.error);
            viewHolder.mErrorType.setTextColor(Minicode.context.getResources().getColor(R.color.red));
            viewHolder.mViewError.setBackgroundTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.red)));
        }
        else if (content[i].type.contains("warning"))
        {
            state = Minicode.context.getString(R.string.warning);
        }
        else if (content[i].type.contains("note"))
        {
            state = Minicode.context.getString(R.string.note);
            viewHolder.mErrorType.setTextColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
            viewHolder.mViewError.setBackgroundTintList(ColorStateList.valueOf(Minicode.context.getResources().getColor(R.color.accent_device_default_700)));
        }
        else
        {
            throw new RuntimeException(Objects.toString(new Object()));
        }
        viewHolder.mErrorType.setText(state);
        viewHolder.mErrorMessage.setText(content[i].desc);
        viewHolder.mErrorFile.setText(String.format(Minicode.context.getString(R.string.at_s), content[i].file, content[i].line));
        viewHolder.mViewError.setOnClickListener(v ->
        {
            String loc = content[i].file;
            Intent intent = new Intent(activity, EditorActivity.class);
            intent.putExtra("file", new File(loc));
            intent.putExtra("line", content[i].line);
            intent.putExtra("read_only", true);
            if (false && new File(content[i].file) == new File(activity.callingFile))
            {
                activity.setResult(content[i].line);
                activity.finish();
            }
            else
            {
                activity.startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return this.content.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView mErrorType, mErrorMessage, mErrorFile;
        Button mViewError;

        public ViewHolder(View v)
        {
            super(v);
            Log.d("Step", "Step: ViewHolder - Constructer");
            mErrorFile = v.findViewById(R.id.warning_file);
            mErrorMessage = v.findViewById(R.id.error);
            mErrorType = v.findViewById(R.id.error_type);
            mViewError = v.findViewById(R.id.view_warning_button);
        }
    }
}