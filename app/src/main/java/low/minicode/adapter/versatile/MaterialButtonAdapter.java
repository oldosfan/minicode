/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.versatile;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MultiAutoCompleteTextView;

import androidx.annotation.NonNull;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import low.minicode.AdvancedLanguage;
import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;

public class MaterialButtonAdapter extends PushButtonAdapter
{
    private final boolean codify;
    private final AdvancedLanguage language;

    public MaterialButtonAdapter(List<String> s, MultiAutoCompleteTextView tv, boolean codify, AdvancedLanguage language)
    {
        super(s, tv);
        this.codify = codify;
        this.language = language;
        this.atv = tv;
        this.typedAmount = 0;
        ArrayList<String> where = new ArrayList<>(s);
        this.content = new String[where.size()];
        if (content.length < 1)
        {
            return;
        }
        where.toArray(this.content);
        for (String iter : this.content)
        {
            Log.d("Files", iter);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_materialbutton_view, parent, false));
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull PushButtonAdapter.ViewHolder viewHolder2, final int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        ViewHolder viewHolder = (ViewHolder) viewHolder2;
        viewHolder.mMaterialButton.setText(this.content[i]);
        viewHolder.mMaterialButton.setOnClickListener(v ->
        {
            int pos = atv.getSelectionStart();
            atv.getText().insert(pos, content[i].substring(typedAmount));
            atv.setSelection(pos + content[i].substring(typedAmount).length());
            return;
        });
        if (codify)
        {
            viewHolder.mMaterialButton.setTypeface(Minicode.theMinicode.getEditorTypeface());
            viewHolder.mMaterialButton.setTextColor(Library.getColorForKeyword(content[i], language));
        }
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return this.content.length;
    }

    public static class ViewHolder extends PushButtonAdapter.ViewHolder
    {
        public MaterialButton mMaterialButton;

        public ViewHolder(View v)
        {
            super(v);
            mMaterialButton = v.findViewById(R.id.basic_button_adapter_material);
        }
    }
}