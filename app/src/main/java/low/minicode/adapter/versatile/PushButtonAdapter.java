/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.versatile;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import low.minicode.R;

public class PushButtonAdapter extends RecyclerView.Adapter<PushButtonAdapter.ViewHolder>
{
    protected String[] content;
    protected AutoCompleteTextView atv;
    protected int typedAmount;

    public PushButtonAdapter(List<String> s, MultiAutoCompleteTextView tv)
    {
        this.atv = tv;
        this.typedAmount = 0;
        ArrayList<String> where = new ArrayList<>(s);
        this.content = new String[where.size()];
        if (content.length < 1)
        {
            return;
        }
        where.toArray(this.content);
        for (String iter : this.content)
        {
            Log.d("Files", iter);
        }
    }

    public void initializeWithNewData(List<String> s, int typedAmount)
    {
        ArrayList<String> where = new ArrayList<String>();
        this.typedAmount = typedAmount;
        where.addAll(s);
        this.content = new String[where.size()];
        where.toArray(this.content);
        for (String iter : this.content)
        {
            Log.d("Files", iter);
        }
        this.notifyDataSetChanged();
        return;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_pushbutton_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        viewHolder.mButton.setText(this.content[i]);
        viewHolder.mButton.setOnClickListener(v ->
        {
            int pos = atv.getSelectionStart();
            atv.getText().insert(pos, content[i].substring(typedAmount));
            atv.setSelection(pos + content[i].substring(typedAmount).length());
            return;
        });
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return this.content.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public Button mButton;

        public ViewHolder(View v)
        {
            super(v);
            Log.d("Step", "Step: ViewHolder - Constructor");
            mButton = v.findViewById(R.id.basic_button_adapter_view);
        }
    }
}