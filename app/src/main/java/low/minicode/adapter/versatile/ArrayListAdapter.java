/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.versatile;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import low.minicode.R;

public class ArrayListAdapter extends RecyclerView.Adapter<ArrayListAdapter.ViewHolder>
{
    private String[] content;

    public ArrayListAdapter(List<String> s)
    {
        ArrayList<String> where = new ArrayList<>(s);
        this.content = new String[where.size()];
        if (content.length < 1)
        {
            return;
        }
        where.toArray(this.content);
        for (String iter : this.content)
        {
            Log.d("Files", iter);
        }
    }

    public void initializeWithNewData(List<String> s)
    {
        ArrayList<String> where = new ArrayList<>(s);
        this.content = new String[where.size()];
        where.toArray(this.content);
        for (String iter : this.content)
        {
            Log.d("Files", iter);
        }
        this.notifyDataSetChanged();
        return;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_recyclerview_content, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        viewHolder.mTextView.setText(this.content[i]);
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return this.content.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextView;

        public ViewHolder(View v)
        {
            super(v);
            Log.d("Step", "Step: ViewHolder - Constructer");
            mTextView = v.findViewById(R.id.basic_arraylist_adapter_view);
        }
    }
}