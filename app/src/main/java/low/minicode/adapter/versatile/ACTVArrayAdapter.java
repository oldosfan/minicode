/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.versatile;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class ACTVArrayAdapter extends BaseAdapter implements Filterable
{
    private final PushButtonAdapter mAdapterPush;
    private List<String> mData;
    private List<String> mInitData;

    public ACTVArrayAdapter(PushButtonAdapter adapter, List<String> data)
    {
        this.mAdapterPush = adapter;
        this.mData = data;
        this.mInitData = data;
    }

    public void addData(String data)
    {
        mData.add(data);
    }

    public void reset()
    {
        mData.clear();
        mData.addAll(mInitData);
    }

    @Override
    public int getItemViewType(int position)
    {
        return mAdapterPush.getItemViewType(position);
    }

    @Override
    public int getCount()
    {
        return mAdapterPush.getItemCount();
    }

    @Override
    public Object getItem(int position)
    {
        //NOFIXME getItem is not required
        return null;
    }

    @Override
    public Filter getFilter()
    {
        return new ACTVArrayAdapterFilter(mData, this);
    }

    @Override
    public long getItemId(int position)
    {
        return mAdapterPush.getItemId(position);
    }

    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        PushButtonAdapter.ViewHolder holder;
        if (convertView == null)
        {
            holder = mAdapterPush.createViewHolder(parent, getItemViewType(position));
            convertView = holder.itemView;
            convertView.setTag(holder);
        }
        else
        {
            holder = (PushButtonAdapter.ViewHolder) convertView.getTag();
        }
        mAdapterPush.bindViewHolder(holder, position);
        return holder.itemView;
    }

    private class ACTVArrayAdapterFilter extends Filter
    {
        List<String> mData;
        ACTVArrayAdapter mAdapter;

        public ACTVArrayAdapterFilter(@NonNull List<String> data, @NonNull ACTVArrayAdapter adapter)
        {
            this.mData = data;
            this.mAdapter = adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {
            FilterResults fr = new FilterResults();
            ArrayList<String> val = new ArrayList<String>();
            for (String iter : mData)
            {
                if (iter.startsWith(constraint.toString()))
                {
                    val.add(iter);
                }
            }
            fr.values = val;
            fr.count = val.size();
            return fr;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            mAdapterPush.initializeWithNewData((results.values != null ? (List<String>) results.values : new ArrayList<String>()), (constraint != null ? constraint.length() : 0));
            return;
        }
    }
}
