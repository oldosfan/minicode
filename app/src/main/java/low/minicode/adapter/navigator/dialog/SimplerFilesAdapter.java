/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.navigator.dialog;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class SimplerFilesAdapter extends RecyclerView.Adapter<SimplerFilesAdapter.ViewHolder>
{
    protected ArrayList<File> files = new ArrayList<>();
    private ThemedAppCompatActivity appCompatActivity;
    private File file;

    public SimplerFilesAdapter(ThemedAppCompatActivity appCompatActivity, File file)
    {
        if (file == null)
        {
            Log.d(getClass().getName(), "file null!");
        }
        this.appCompatActivity = appCompatActivity;
        this.file = file;
        updateData();
    }

    public void updateData()
    {
        files.clear();
        if (file.listFiles() != null)
        {
            files.addAll(Arrays.asList(file.listFiles()));
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(appCompatActivity).inflate(R.layout.basic_simple_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        if (files.get(position).isDirectory())
        {
            holder.mFolder.setVisibility(View.VISIBLE);
            holder.itemView.setOnClickListener(v ->
            {
                Intent i = appCompatActivity.getIntent();
                i.putExtra("loc", i.getStringExtra("loc") + File.separator + files.get(position).getName());
                appCompatActivity.startActivityForResult(i, ThemedAppCompatActivity.REQUEST_SELF);
            });
        }
        else
        {
            holder.mFolder.setVisibility(View.INVISIBLE);
        }
        holder.mTextView.setText(files.get(position).getName());
    }

    @Override
    public int getItemCount()
    {
        return files.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextView;
        public View mFolder;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            mTextView = itemView.findViewById(R.id.file);
            mFolder = itemView.findViewById(R.id.folder_identifier);
        }
    }
}
