/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.navigator.dialog;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

import low.minicode.R;
import low.minicode.activity.project.navigator.dialog.NavigatorReturnPathActivity;

@TargetApi(21)
public class FilesListReturnAdapter extends RecyclerView.Adapter<FilesListReturnAdapter.ViewHolder>
{
    protected RecyclerView mRecyclerView;
    protected File[] positionArray;
    protected File currentPosition;
    private String[] files;
    private String projectName, currentPath;
    private NavigatorReturnPathActivity activity;

    public FilesListReturnAdapter(File[] files, NavigatorReturnPathActivity activity, String projectName, String currentPath, File currentPosition)
    {
        this.projectName = projectName;
        this.currentPath = currentPath;
        this.activity = activity;
        this.positionArray = files;
        if (files == null)
        {
            Log.w("FilePosition", "File[] files was *null*!");
        }
        ArrayList<String> where = new ArrayList<String>();
        if (files == null)
        {
            return;
        }
        if (files.length < 1)
        {
            return;
        }
        for (File iter : files)
        {
            where.add(iter.getName());
        }
        this.files = new String[where.size()];
        where.toArray(this.files);
        for (String iter : this.files)
        {
            Log.d("Files", iter);
        }
    }

    public void initializeWithNewData(File[] files)
    {
        this.positionArray = new File[0];
        this.files = new String[0];
        this.notifyDataSetChanged();
        this.notifyItemRangeChanged(0, 0);
        this.mRecyclerView.getRecycledViewPool().clear();
        this.positionArray = files;
        ArrayList<String> where = new ArrayList<String>();
        for (File iter : files)
        {
            where.add(iter.getName());
        }
        this.files = new String[where.size()];
        where.toArray(this.files);
        for (String iter : this.files)
        {
            Log.d("Files", iter);
        }
        this.notifyDataSetChanged();
        return;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int i)
    {
        Log.d("Step", "Step: onCreateViewHolder");
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_fileview_floating, parent, false);
        final Activity tmpa = this.activity;
        v.setOnClickListener(v12 ->
        {
            Intent newIntent = new Intent(v12.getContext(), NavigatorReturnPathActivity.class);
            int pos = mRecyclerView.getChildAdapterPosition(v12);
            String location = currentPath + "/" + files[pos];
            Log.d("Pos", "Next: " + tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]);
            Log.d("Pos", "Now: " + tmpa.getFilesDir() + "/" + projectName + "/" + currentPath);
            Log.d("Pos", "Current: " + currentPath);
            if (!new File(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]).isDirectory())
            {
                activity.gotPath(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]);
                return;
            }
            newIntent.putExtra("loc", location);
            newIntent.putExtra("projectName", projectName);
            tmpa.startActivityForResult(newIntent, 0);
        });
        v.setLongClickable(true);
        v.setOnLongClickListener(v1 ->
        {
            final int pos = mRecyclerView.getChildAdapterPosition(v1);
            AlertDialog.Builder builder = new AlertDialog.Builder(v1.getContext());
            builder.setTitle(files[pos]);
            builder.setItems(R.array.file_modifications_dialog, (dialogInterface, i2) ->
            {
                if (i2 == 0)
                {
                    final EditText editText = new EditText(v1.getContext());
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(v1.getContext());
                    builder1.setView(editText);
                    builder1.setTitle(R.string.rename_to);
                    builder1.setPositiveButton("OK", (dialog, which) ->
                    {
                        if (editText.getText().toString().isEmpty())
                        {
                            return;
                        }
                        File file = positionArray[pos];
                        File dest = new File(positionArray[pos].getParent(), editText.getText().toString());
                        if (dest.exists())
                        {
                            return;
                        }
                        file.renameTo(dest);
                        initializeWithNewData(new File(dest.getParent()).listFiles());
                    }).show();
                }
            });
            builder.show();
            return true;
        });
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        Log.d("Step", "Step: onBindViewHolder");
        viewHolder.mTextView.setText(this.files[i]);
        final int tmpi = i;
        final Activity tmpa = this.activity;
        if (!positionArray[i].isDirectory())
        {
            viewHolder.mFolderIcon.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.mFolderIcon.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount()
    {
        Log.d("Step", "Step: getItemCount");
        return (this.files != null ? this.files.length : 0);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextView;
        public ImageView mFolderIcon;

        public ViewHolder(View v)
        {
            super(v);
            Log.d("Step", "Step: ViewHolder - Constructer");
            mTextView = v.findViewById(R.id.file_textview);
            mFolderIcon = v.findViewById(R.id.folder_identifier);
        }
    }
}