/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.navigator.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.FileInfoViewer;
import low.minicode.Library;
import low.minicode.wrappers.data.tree.Node;
import low.minicode.ProjectManifest;
import low.minicode.R;
import low.minicode.activity.editor.EditorActivity;
import low.minicode.activity.project.navigator.NavigatorActivity;
import low.minicode.activity.project.navigator.search.NavigatorSearchActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;

public class FindFileAdapter extends RecyclerView.Adapter<FindFileAdapter.ViewHolder>
{
    private ArrayList<File> data;
    private File directory;
    private String projectName;
    private Activity parent;
    private Handler uiThread;
    private Library.FilterCallback<File> callback;
    private Thread thread;

    public FindFileAdapter(File directory, String projectName, Activity parent)
    {
        this.directory = directory;
        this.projectName = projectName;
        this.parent = parent;
        this.uiThread = new Handler();
        this.data = new ArrayList<>();
        this.thread = new FindFileThread();
        thread.start();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_found_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.name.setText(data.get(position).getName());
        holder.itemView.setOnClickListener(v ->
        {
            if (data.get(position).isDirectory())
            {
                Intent i = new Intent(parent, NavigatorActivity.class);
                i.putExtra("projectName", projectName);
                i.putExtra("loc", new File(parent.getFilesDir(), projectName).equals(data.get(position)) ? "/" : Library.relative(new File(parent.getFilesDir(), projectName), data.get(position)));
                parent.startActivityForResult(i, 0);
            }
            else if (!data.get(position).canExecute())
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    FileInfoViewer fileInfoViewer = new FileInfoViewer(data.get(position));
                    Log.d(getClass().getName(), "result: " + fileInfoViewer.getResult());
                    if (fileInfoViewer.getResult().matches(".*text.*") || fileInfoViewer.getResult().matches("empty") || Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1 || fileInfoViewer.getResult().contains("very short") || fileInfoViewer.getResult().contains("JSON"))
                    {
                        parent.runOnUiThread(() ->
                        {
                            Intent editorIntent = new Intent(parent, EditorActivity.class);
                            editorIntent.putExtra("project_name", projectName);
                            editorIntent.putExtra("file", data.get(position));
                            if ((data.get(position).getParent().equals(new File(parent.getFilesDir(), projectName).getAbsolutePath()) && data.get(position).getName().equals("project_manifest.json")) || !data.get(position).canWrite())
                            {
                                editorIntent.putExtra("read_only", "true");
                            }
                            parent.startActivityForResult(editorIntent, 0);
                        });
                    }
                    else
                    {
                        parent.runOnUiThread(() ->
                        {
                            Library.vibrateError();
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(parent);
                            builder.setTitle(R.string.not_text_file);
                            builder.setMessage(R.string.open_anyway);
                            builder.setNeutralButton(R.string.open_anyway2, (dialog, which) ->
                            {
                                Intent editorIntent = new Intent(parent, EditorActivity.class);
                                editorIntent.putExtra("project_name", projectName);
                                editorIntent.putExtra("file", data.get(position));
                                if ((data.get(position).getParent().equals(new File(parent.getFilesDir(), projectName).getAbsolutePath()) && data.get(position).getName().equals("project_manifest.json")) || !data.get(position).canWrite())
                                {
                                    editorIntent.putExtra("read_only", "true");
                                }
                                parent.startActivityForResult(editorIntent, 0);
                            });
                            builder.setPositiveButton(R.string.open_with, (dialog, which) ->
                            {
                                MimeTypeMap myMime = MimeTypeMap.getSingleton();
                                String mimeType = myMime.getMimeTypeFromExtension(FilenameUtils.getExtension(data.get(position).getName()));
                                if (data.get(position).canExecute())
                                {
                                    mimeType = "application/x-elf";
                                }
                                Intent sharingIntent = new Intent(Intent.ACTION_VIEW);
                                Uri uri = FileProvider.getUriForFile(parent, "low.minicode.authority", data.get(position));
                                sharingIntent.setDataAndType(uri, mimeType);
                                if (parent.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("allow_edit", false))
                                {
                                    sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                }
                                else
                                {
                                    sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                                }
                                parent.startActivity(Intent.createChooser(sharingIntent, parent.getString(R.string.open_with)));
                            });
                            builder.setNegativeButton(R.string.ok, (dialog, which) ->
                            {
                            });
                            builder.show();
                        });
                    }
                });
            }
            else if (data.get(position).canExecute())
            {
                MaterialAlertDialogBuilder ab = new MaterialAlertDialogBuilder(parent);
                Library.vibrateAttention();
                ab.setMessage(R.string.is_executable).setPositiveButton(R.string.exec, (dialog, which) ->
                {
                    Intent i2 = new Intent(parent, NDKTerminalActvitiy.class);
                    i2.putExtra("binary", data.get(position).getAbsolutePath());
                    List<String> tmp = new ArrayList<>();
                    tmp.add(0, data.get(position).getAbsolutePath());
                    if (parent instanceof NavigatorSearchActivity)
                    {
                        tmp.addAll(new ProjectManifest(new File(new File(parent.getFilesDir(), ((NavigatorSearchActivity) parent).getProjectName()), "project_manifest.json")).compileFiles);
                    }
                    i2.putExtra("binaryArray", Arrays.copyOf(tmp.toArray(), tmp.size(), String[].class));
                    parent.startActivityForResult(i2, 0);
                });
                ab.show();
            }
        });
        if (new File(parent.getFilesDir(), projectName).equals(data.get(position)))
        {
            holder.path.setText(parent.getString(R.string.s_s, projectName, ""));
        }
        else
        {
            holder.path.setText(parent.getString(R.string.s_s, projectName, Library.relative(new File(parent.getFilesDir(), projectName), data.get(position))));
        }
    }

    @Override
    public int getItemCount()
    {
        return data == null ? 0 : data.size();
    }

    public Library.FilterCallback<File> getCallback()
    {
        return callback;
    }

    public void setCallback(Library.FilterCallback<File> callback)
    {
        this.callback = callback;
    }

    public class FindFileThread extends Thread
    {
        @Override
        public void run()
        {
            while (true)
            {
                if (isInterrupted())
                {
                    return;
                }
                try
                {
                    ArrayList<File> lastData = new ArrayList<>(data);
                    if (callback == null)
                    {
                        data.clear();
                        if (!lastData.isEmpty())
                        {
                            uiThread.post(FindFileAdapter.this::notifyDataSetChanged);
                        }
                        continue;
                    }
                    Node<File> fileNode = Library.indexFiles(directory, 4);
                    data.clear();
                    data.addAll(Library.flattenNode(fileNode));
                    Iterator<File> fileIterator = data.iterator();
                    while (fileIterator.hasNext())
                    {
                        if (callback != null && !callback.handle(fileIterator.next()))
                        {
                            fileIterator.remove();
                        }
                    }
                    if (!data.equals(lastData))
                    {
                        uiThread.post(FindFileAdapter.this::notifyDataSetChanged);
                    }
                    try
                    {
                        sleep(150);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    return;
                }
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name, path;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.file_name);
            path = itemView.findViewById(R.id.file_path);
        }
    }
}
