/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.navigator;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import low.minicode.Copy;
import low.minicode.FileInfoViewer;
import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.widget.files.PinnedFilesWidget;
import low.minicode.R;
import low.minicode.activity.editor.EditorActivity;
import low.minicode.activity.project.navigator.NavigatorActivity;
import low.minicode.activity.project.navigator.dialog.PackageTarballActivity;
import low.minicode.activity.project.navigator.export.ExportFileActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;

@TargetApi(21)
public class FilesListAdapter extends RecyclerView.Adapter<FilesListAdapter.ViewHolder>
{
    protected String[] files;
    protected RecyclerView mRecyclerView;
    protected File[] positionArray;
    protected File currentPosition;
    protected ArrayList<FileType> fileTypes;
    private String projectName, currentPath;
    private ArrayList<String> removals;
    private Activity activity;
    private ItemTouchHelper touchHelper;

    public FilesListAdapter(File[] files, Activity activity, String projectName, String currentPath)
    {
        this.projectName = projectName;
        this.currentPath = currentPath;
        this.activity = activity;
        this.positionArray = files;
        this.removals = new ArrayList<>();
        this.fileTypes = new ArrayList<>();
        this.currentPosition = new File(activity.getFilesDir(), projectName + "/" + new File(currentPath));
        this.touchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
        if (files == null)
        {
            Log.w("FilePosition", "File[] files was *null*!");
        }
        ArrayList<String> where = new ArrayList<String>();
        if (files == null)
        {
            return;
        }
        if (files.length < 1)
        {
            return;
        }
        for (File iter : files)
        {
            where.add(iter.getName());
        }
        this.files = new String[where.size()];
        where.toArray(this.files);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        mRecyclerView = recyclerView;
        touchHelper.attachToRecyclerView(recyclerView);
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int i)
    {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_fileview, parent, false);
        final Activity tmpa = this.activity;
        v.setOnClickListener(v1 ->
        {
            Intent newIntent = new Intent(v1.getContext(), NavigatorActivity.class);
            int pos = mRecyclerView.getChildAdapterPosition(v1);
            if (positionArray[pos].isDirectory())
            {
                if (!(positionArray[pos].canRead() && positionArray[pos].canWrite()))
                {
                    Library.vibrateError();
                    Snackbar.make(v1, R.string.need_rw, Snackbar.LENGTH_LONG).show();
                    return;
                }
            }
            else if (!positionArray[pos].canRead())
            {
                Library.vibrateError();
                Snackbar.make(v1, R.string.need_read, Snackbar.LENGTH_LONG).show();
                return;
            }
            String location = currentPath + "/" + files[pos];
            if (!new File(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]).isDirectory())
            {
                File c = new File(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]);
                boolean isExecutable = c.canExecute();
                if (!isExecutable)
                {
                    new Thread()
                    {
                        @Override
                        public void run()
                        {
                            super.run();
                            FileInfoViewer fileInfoViewer = new FileInfoViewer(positionArray[pos]);
                            if (fileInfoViewer.getResult().matches(".*text.*") || fileInfoViewer.getResult().matches("empty") || Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1 || fileInfoViewer.getResult().contains("very short") || fileInfoViewer.getResult().contains("JSON"))
                            {
                                activity.runOnUiThread(() ->
                                {
                                    Intent editorIntent = new Intent(v1.getContext(), EditorActivity.class);
                                    editorIntent.putExtra("project_name", projectName);
                                    editorIntent.putExtra("file", new File(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]));
                                    if ((positionArray[pos].getParent().equals(new File(tmpa.getFilesDir() + "/" + projectName).getAbsolutePath()) && files[pos].equals("project_manifest.json")) || !positionArray[pos].canWrite())
                                    {
                                        editorIntent.putExtra("read_only", true);
                                    }
                                    activity.startActivityForResult(editorIntent, 0);
                                });
                            }
                            else
                            {
                                activity.runOnUiThread(() ->
                                {
                                    Library.vibrateError();
                                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity);
                                    builder.setTitle(R.string.not_text_file);
                                    builder.setMessage(R.string.open_anyway);
                                    builder.setNeutralButton(R.string.open_anyway2, (dialog, which) ->
                                    {
                                        Intent editorIntent = new Intent(v1.getContext(), EditorActivity.class);
                                        editorIntent.putExtra("project_name", projectName);
                                        editorIntent.putExtra("file", new File(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]));
                                        if ((positionArray[pos].getParent().equals(new File(tmpa.getFilesDir() + "/" + projectName).getAbsolutePath()) && files[pos].equals("project_manifest.json")) || !positionArray[pos].canWrite())
                                        {
                                            editorIntent.putExtra("read_only", true);
                                        }
                                        activity.startActivityForResult(editorIntent, 0);
                                    });
                                    builder.setPositiveButton(R.string.open_with, (dialog, which) ->
                                    {
                                        MimeTypeMap myMime = MimeTypeMap.getSingleton();
                                        String mimeType = myMime.getMimeTypeFromExtension(FilenameUtils.getExtension(files[pos]));
                                        if (positionArray[pos].canExecute())
                                        {
                                            mimeType = "application/x-elf";
                                        }
                                        Intent sharingIntent = new Intent(Intent.ACTION_VIEW);
                                        Uri uri = FileProvider.getUriForFile(activity, "low.minicode.authority", positionArray[pos]);
                                        sharingIntent.setDataAndType(uri, mimeType);
                                        if (activity.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("allow_edit", false))
                                        {
                                            sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                        }
                                        else
                                        {
                                            sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        }
                                        activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.open_with)));
                                    });
                                    builder.setNegativeButton(R.string.ok, (dialog, which) ->
                                    {
                                    });
                                    builder.show();
                                });
                            }
                        }
                    }.start();
                }
                else
                {
                    MaterialAlertDialogBuilder ab = new MaterialAlertDialogBuilder(tmpa);
                    Library.vibrateAttention();
                    ab.setMessage(R.string.is_executable).setPositiveButton(R.string.exec, (dialog, which) ->
                    {
                        Intent i2 = new Intent(tmpa, NDKTerminalActvitiy.class);
                        i2.putExtra("binary", c.getAbsolutePath());
                        List<String> tmp = ((NavigatorActivity) activity).manifest.args;
                        tmp.add(0, c.getAbsolutePath());
                        i2.putExtra("binaryArray", Arrays.copyOf(tmp.toArray(), tmp.size(), String[].class));
                        this.activity.startActivityForResult(i2, 0);
                    });
                    ab.setNegativeButton(R.string.edit, (dialog, which) ->
                    {
                        new Thread()
                        {
                            @Override
                            public void run()
                            {
                                super.run();
                                FileInfoViewer fileInfoViewer = new FileInfoViewer(positionArray[pos]);
                                if (fileInfoViewer.getResult().matches(".*text.*") || fileInfoViewer.getResult().matches("empty") || Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1 || fileInfoViewer.getResult().contains("very short") || fileInfoViewer.getResult().contains("JSON"))
                                {
                                    activity.runOnUiThread(() ->
                                    {
                                        Intent editorIntent = new Intent(v1.getContext(), EditorActivity.class);
                                        editorIntent.putExtra("project_name", projectName);
                                        editorIntent.putExtra("file", new File(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]));
                                        if ((positionArray[pos].getParent().equals(new File(tmpa.getFilesDir() + "/" + projectName).getAbsolutePath()) && files[pos].equals("project_manifest.json")) || !positionArray[pos].canWrite())
                                        {
                                            editorIntent.putExtra("read_only", true);
                                        }
                                        activity.startActivityForResult(editorIntent, 0);
                                    });
                                }
                                else
                                {
                                    activity.runOnUiThread(() ->
                                    {
                                        Library.vibrateError();
                                        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity);
                                        builder.setTitle(R.string.not_text_file);
                                        builder.setMessage(R.string.open_anyway);
                                        builder.setNeutralButton(R.string.open_anyway2, (dialog, which) ->
                                        {
                                            Intent editorIntent = new Intent(v1.getContext(), EditorActivity.class);
                                            editorIntent.putExtra("project_name", projectName);
                                            editorIntent.putExtra("file", new File(tmpa.getFilesDir() + "/" + projectName + "/" + currentPath + "/" + files[pos]));
                                            if ((positionArray[pos].getParent().equals(new File(tmpa.getFilesDir() + "/" + projectName).getAbsolutePath()) && files[pos].equals("project_manifest.json")) || !positionArray[pos].canWrite())
                                            {
                                                editorIntent.putExtra("read_only", true);
                                            }
                                            activity.startActivityForResult(editorIntent, 0);
                                        });
                                        builder.setPositiveButton(R.string.open_with, (dialog, which) ->
                                        {
                                            MimeTypeMap myMime = MimeTypeMap.getSingleton();
                                            String mimeType = myMime.getMimeTypeFromExtension(FilenameUtils.getExtension(files[pos]));
                                            if (positionArray[pos].canExecute())
                                            {
                                                mimeType = "application/x-elf";
                                            }
                                            Intent sharingIntent = new Intent(Intent.ACTION_VIEW);
                                            Uri uri = FileProvider.getUriForFile(activity, "low.minicode.authority", positionArray[pos]);
                                            sharingIntent.setDataAndType(uri, mimeType);
                                            if (activity.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("allow_edit", false))
                                            {
                                                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                            }
                                            else
                                            {
                                                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            }
                                            activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.open_with)));
                                        });
                                        builder.setNegativeButton(R.string.ok, (dialog, which) ->
                                        {
                                        });
                                        builder.show();
                                    });
                                }
                            }
                        }.start();
                    });
                    ab.setNeutralButton(Minicode.context.getString(R.string.strip), (dialog, which) ->
                    {
                        MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(this.activity);
                        b.setMessage(R.string.stripping_period);
                        b.setCancelable(false);
                        Dialog d = b.create();
                        d.show();
                        Future<Integer> future = Library.strip(positionArray[pos], args ->
                        {
                            activity.runOnUiThread(() ->
                            {
                                d.cancel();
                                if (args[0] != 0)
                                {
                                    Library.vibrateError();
                                    MaterialAlertDialogBuilder b2 = new MaterialAlertDialogBuilder(this.activity);
                                    b2.setTitle(R.string.error);
                                    b2.setMessage(R.string.strip_failed);
                                    b2.setPositiveButton(R.string.ok, ((dialog1, which1) ->
                                    {
                                    }));
                                    b2.show();
                                }
                                else
                                {
                                    Library.vibrateSuccess();
                                    MaterialAlertDialogBuilder b2 = new MaterialAlertDialogBuilder(this.activity);
                                    b2.setTitle(R.string.success);
                                    b2.setMessage(R.string.strip_completed);
                                    b2.setPositiveButton(R.string.ok, ((dialog1, which1) ->
                                    {
                                    }));
                                    b2.show();
                                    notifyDataSetChanged();
                                }
                            });
                            return null;
                        });
                    });
                    ab.show().getButton(DialogInterface.BUTTON_POSITIVE).setOnLongClickListener(v345 ->
                    {
                        MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(this.activity);
                        b.setItems(R.array.run_options, (dialog, which) ->
                        {
                            Intent i2 = new Intent(tmpa, NDKTerminalActvitiy.class);
                            i2.putExtra("binary", Library.lldb(projectName, c));
                            i2.putExtra("cwd", new File(Minicode.context.getFilesDir(), projectName).getAbsolutePath());
                            i2.putExtra("binaryArray", Arrays.copyOf(((NavigatorActivity) activity).manifest.args.toArray(), ((NavigatorActivity) activity).manifest.args.size(), String[].class));
                            this.activity.startActivityForResult(i2, 0);
                        });
                        b.show();
                        return true;
                    });
                }
                return;
            }
            newIntent.putExtra("loc", location);
            newIntent.putExtra("projectName", projectName);
            tmpa.startActivityForResult(newIntent, 0);
        });
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @SuppressLint("ApplySharedPref")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        viewHolder.mTextView.setText(this.files[i]);
        viewHolder.mDeleteButton.setText(R.string.ddelete);
        final int tmpi = i;
        final Activity tmpa = this.activity;
        Executors.newSingleThreadExecutor().submit(() ->
        {
            FileInfoViewer result = new FileInfoViewer(positionArray[i]);
            activity.runOnUiThread(() -> viewHolder.mFileType.setText(result.getResult()));
        });
        viewHolder.mExpandButton.animate().rotation(viewHolder.mActionsContainer.getVisibility() == View.VISIBLE ? 180 : 0);
        viewHolder.mExpandButton.setOnClickListener(v ->
        {
            viewHolder.mActionsContainer.setVisibility(viewHolder.mActionsContainer.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            Future<Void> future = Executors.newSingleThreadExecutor().submit(() ->
            {
                String fileResult = new FileInfoViewer(positionArray[i]).getResult();
                activity.runOnUiThread(() -> viewHolder.mFileType.setText(fileResult));
                activity.runOnUiThread(() ->
                {
                    viewHolder.mFileSize.setText(positionArray[i].isDirectory() ? String.format(Minicode.context.getString(R.string.size), "Folder") : String.format(Minicode.context.getString(R.string.size), Library.getBetterFilesize(positionArray[i].length())));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    {
                        viewHolder.mUnixPerms.setVisibility(View.VISIBLE);
                        try
                        {
                            viewHolder.mUnixPerms.setText(String.format(Minicode.context.getString(R.string.permissions), PosixFilePermissions.toString(Files.getPosixFilePermissions(Paths.get(positionArray[i].toURI())))));
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        viewHolder.mUnixPerms.setVisibility(View.GONE);
                    }
                    MimeTypeMap myMime = MimeTypeMap.getSingleton();
                    String mimeType = myMime.getMimeTypeFromExtension(FilenameUtils.getExtension(files[i]));
                    viewHolder.mMimeType.setText(String.format(Minicode.context.getString(R.string.mime_type), mimeType));
                });
                return null;
            });
            v.animate().rotation(viewHolder.mActionsContainer.getVisibility() == View.VISIBLE ? 180 : 0);
        });
        viewHolder.mDeleteButton.setOnClickListener(v92 ->
        {
            int adapterPosition = viewHolder.getAdapterPosition();
            String i3 = files[viewHolder.getAdapterPosition()];
            File file = positionArray[viewHolder.getAdapterPosition()];
            removals.add(i3);
            final boolean[] flag = {false};
            notifyItemRemoved(viewHolder.getAdapterPosition());
            Snackbar.make(v92, R.string.item_removed, Snackbar.LENGTH_LONG).setAction(R.string.undo, view ->
            {
                flag[0] = true;
                while (removals.contains(i3))
                {
                    removals.remove(i3);
                }
                notifyItemInserted(adapterPosition);
            }).addCallback(new Snackbar.Callback()
            {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event)
                {
                    super.onDismissed(transientBottomBar, event);
                    Log.d(getClass().getName() + "@" + hashCode(), "onDismissed: dismiss. flag[0]=" + flag[0]);
                    if (!flag[0])
                    {
                        Library.deleteDirectoryRecursively(file);
                        while (removals.contains(i3))
                        {
                            removals.remove(i3);
                        }
                    }
                }
            }).show();
            return;
        });
        if (positionArray[i].isDirectory())
        {
            viewHolder.mFolderIdentifier.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.mFolderIdentifier.setVisibility(View.INVISIBLE);
        }
        if ((files[i].equals("project_manifest.json") || files[i].equals("Makefile")) && (positionArray[i].getAbsolutePath().equals(new File(this.activity.getFilesDir(), this.projectName + "/project_manifest.json").getAbsolutePath()) || positionArray[i].getAbsolutePath().equals(new File(this.activity.getFilesDir(), this.projectName + "/Makefile").getAbsolutePath())))
        {
            viewHolder.mDeleteButton.setOnClickListener(v -> Snackbar.make(v, String.format(tmpa.getString(R.string.to_delete_s_long_press_s), files[i].equals("Makefile") ? "Makefile" : "manifest", files[i].equals("Makefile") ? "Makefile" : "Manifest"), Snackbar.LENGTH_SHORT).show());
            viewHolder.mDeleteButton.setLongClickable(true);
            viewHolder.mDeleteButton.setOnLongClickListener(v92 ->
            {
                int adapterPosition = viewHolder.getAdapterPosition();
                String i3 = files[viewHolder.getAdapterPosition()];
                File file = positionArray[viewHolder.getAdapterPosition()];
                removals.add(i3);
                final boolean[] flag = {false};
                notifyItemRemoved(viewHolder.getAdapterPosition());
                Snackbar.make(v92, R.string.item_removed, Snackbar.LENGTH_LONG).setAction(R.string.undo, view ->
                {
                    flag[0] = true;
                    while (removals.contains(i3))
                    {
                        removals.remove(i3);
                    }
                    notifyItemInserted(adapterPosition);
                }).addCallback(new Snackbar.Callback()
                {
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event)
                    {
                        super.onDismissed(transientBottomBar, event);
                        Log.d(getClass().getName() + "@" + hashCode(), "onDismissed: dismiss. flag[0]=" + flag[0]);
                        if (!flag[0])
                        {
                            Library.deleteDirectoryRecursively(file);
                            while (removals.contains(i3))
                            {
                                removals.remove(i3);
                            }
                        }
                    }
                }).show();
                return true;
            });
            viewHolder.mDeleteButton.setText(files[i].equals("Makefile") ? "Makefile" : "Manifest");
            viewHolder.mDeleteButton.setTextColor(activity.getResources().getColor(R.color.accent_device_default_700));
        }
        else
        {
            viewHolder.mDeleteButton.setLongClickable(false);
            viewHolder.mDeleteButton.setTextColor(viewHolder.mDeleteButton.getContext().getResources().getColor(R.color.red));
        }
        viewHolder.mOpenButton.setVisibility(positionArray[i].isDirectory() ? View.GONE : View.VISIBLE);
        viewHolder.mOpenButton.setOnClickListener(v ->
        {
            Library.vibrateSuccess();
            MimeTypeMap myMime = MimeTypeMap.getSingleton();
            String mimeType = myMime.getMimeTypeFromExtension(FilenameUtils.getExtension(files[i]));
            if (positionArray[i].canExecute())
            {
                mimeType = "application/x-elf";
            }
            Intent sharingIntent = new Intent(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(activity, "low.minicode.authority", positionArray[i]);
            sharingIntent.setDataAndType(uri, mimeType);
            if (activity.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("allow_edit", false))
            {
                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
            else
            {
                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.open_with)));
        });
        viewHolder.mOpenButton.setOnLongClickListener(v ->
        {
            Intent sharingIntent = new Intent(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(activity, "low.minicode.authority", positionArray[i]);
            sharingIntent.setDataAndType(uri, "*/*");
            if (activity.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("allow_edit", false))
            {
                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
            else
            {
                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.open_with)));
            return true;
        });
        viewHolder.mRenameButton.setClickable(true);
        viewHolder.mExportButton.setVisibility(positionArray[i].isDirectory() ? View.GONE : View.VISIBLE);
        viewHolder.mExportButton.setOnClickListener(v ->
        {
            Intent intent = new Intent(activity, ExportFileActivity.class);
            intent.putExtra("fileName", positionArray[i].getAbsolutePath());
            activity.startActivity(intent);
        });
        viewHolder.mShareButton.setVisibility(positionArray[i].isDirectory() ? View.GONE : View.VISIBLE);
        viewHolder.mShareButton.setOnClickListener(v ->
        {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            Uri uri = FileProvider.getUriForFile(activity, "low.minicode.authority", positionArray[i]);
            sharingIntent.setType("*/*");
            sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
            activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.share)));
        });
        viewHolder.mPermButton.setOnClickListener(v43 ->
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity);
            View v = activity.getLayoutInflater().inflate(R.layout._dialog_posix_file_permissions, null, false);
            CheckBox read, write, exec;
            read = v.findViewById(R.id.check_readable);
            write = v.findViewById(R.id.check_writable);
            exec = v.findViewById(R.id.check_executable);
            read.setChecked(positionArray[i].canRead());
            write.setChecked(positionArray[i].canWrite());
            exec.setChecked(positionArray[i].canExecute());
            read.setOnCheckedChangeListener((v142, val) ->
            {
                positionArray[i].setReadable(val);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    viewHolder.mUnixPerms.setVisibility(View.VISIBLE);
                    try
                    {
                        viewHolder.mUnixPerms.setText(String.format(Minicode.context.getString(R.string.permissions), PosixFilePermissions.toString(Files.getPosixFilePermissions(Paths.get(positionArray[i].toURI())))));
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    viewHolder.mUnixPerms.setVisibility(View.GONE);
                }
            });
            write.setOnCheckedChangeListener((v142, val) ->
            {
                positionArray[i].setWritable(val);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    viewHolder.mUnixPerms.setVisibility(View.VISIBLE);
                    try
                    {
                        viewHolder.mUnixPerms.setText(String.format(Minicode.context.getString(R.string.permissions), PosixFilePermissions.toString(Files.getPosixFilePermissions(Paths.get(positionArray[i].toURI())))));
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    viewHolder.mUnixPerms.setVisibility(View.GONE);
                }
            });
            exec.setOnCheckedChangeListener((v142, val) ->
            {
                positionArray[i].setExecutable(val);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    viewHolder.mUnixPerms.setVisibility(View.VISIBLE);
                    try
                    {
                        viewHolder.mUnixPerms.setText(String.format(Minicode.context.getString(R.string.permissions), PosixFilePermissions.toString(Files.getPosixFilePermissions(Paths.get(positionArray[i].toURI())))));
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    viewHolder.mUnixPerms.setVisibility(View.GONE);
                }
            });
            builder.setView(v);
            builder.show();
        });
        viewHolder.mRenameButton.setOnClickListener(v12 ->
        {
            final EditText editText = new EditText(v12.getContext());
            editText.setText(files[i]);
            MaterialAlertDialogBuilder builder1 = new MaterialAlertDialogBuilder(v12.getContext());
            builder1.setView(editText);
            builder1.setTitle(R.string.rename_to);
            builder1.setPositiveButton("OK", (dialog, which) ->
            {
                if (editText.getText().toString().isEmpty())
                {
                    return;
                }
                File file = positionArray[i];
                File dest = new File(positionArray[i].getParent(), editText.getText().toString());
                if (dest.exists())
                {
                    return;
                }
                file.renameTo(dest);
                notifyItemChanged(viewHolder.getAdapterPosition());
            }).show();
        });
        viewHolder.mCopy.setOnClickListener(v293 -> Copy.currentEntry = new Copy(positionArray[i]));
        viewHolder.mPin.setOnClickListener(v194 ->
        {
            if (!Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>()).contains(Library.relative(Minicode.context.getFilesDir(), positionArray[i])))
            {
                Set<String> tmp = Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>());
                tmp.add(Library.relative(Minicode.context.getFilesDir(), positionArray[i]));
                Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).edit().remove("pinned_files").commit();
                Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).edit().putStringSet("pinned_files", tmp).commit();
                viewHolder.mPin.setImageResource(Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>()).contains(Library.relative(Minicode.context.getFilesDir(), positionArray[i])) ? R.drawable.ic_pin_24dp : R.drawable.ic_pin_off_outline);
                Intent i2 = new Intent(Minicode.context, PinnedFilesWidget.class);
                i2.setAction(PinnedFilesWidget.UPDATE_ACTION);
                try
                {
                    PendingIntent.getBroadcast(Minicode.context, 0, i2, PendingIntent.FLAG_CANCEL_CURRENT).send();
                }
                catch (PendingIntent.CanceledException e)
                {
                    e.printStackTrace();
                }
                Log.d(getClass().getName(), "lambada(v194): broadcast " + i2);
            }
            else
            {
                Set<String> tmp = Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>());
                tmp.remove(Library.relative(Minicode.context.getFilesDir(), positionArray[i]));
                Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).edit().remove("pinned_files").commit();
                Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).edit().putStringSet("pinned_files", tmp).commit();
                viewHolder.mPin.setImageResource(Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>()).contains(Library.relative(Minicode.context.getFilesDir(), positionArray[i])) ? R.drawable.ic_pin_24dp : R.drawable.ic_pin_off_outline);
                Intent i2 = new Intent(Minicode.context, PinnedFilesWidget.class);
                i2.setAction(PinnedFilesWidget.UPDATE_ACTION);
                Minicode.context.sendBroadcast(i2);
                Log.d(getClass().getName(), "lambada(v194): broadcast " + i2);
            }
        });
        viewHolder.mPin.setImageResource(Minicode.context.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>()).contains(Library.relative(Minicode.context.getFilesDir(), positionArray[i])) ? R.drawable.ic_pin_24dp : R.drawable.ic_pin_off_outline);
        viewHolder.mPackage.setOnClickListener(v8732 ->
        {
            Intent packageTarballRequest = new Intent(activity, PackageTarballActivity.class);
            packageTarballRequest.putExtra("file", positionArray[i]);
            activity.startActivityForResult(packageTarballRequest, 0);
        });
    }

    @Override
    public int getItemCount()
    {
        File[] fileList = currentPosition.listFiles();
        ArrayList<String> where = new ArrayList<String>();
        for (File iter : fileList)
        {
            where.add(iter.getName());
        }
        this.files = new String[where.size()];
        where.toArray(this.files);
        this.positionArray = fileList;
        this.files = Library.filterOut(String.class, files, object -> removals.contains(object));
        this.positionArray = Library.filterOut(File.class, positionArray, object -> removals.contains(object.getName()));
        Log.d(getClass().getName(), "getItemCount: " + Arrays.toString(files));
        return (this.files != null ? this.files.length : 0);
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView mTextView, mFileType, mMimeType, mFileSize, mUnixPerms;
        Button mDeleteButton, mRenameButton, mShareButton, mOpenButton, mExportButton, mPermButton, mPackage;
        ImageButton mExpandButton, mCopy, mPin;
        ImageView mFolderIdentifier;
        LinearLayout mActionsContainer;

        @SuppressLint("ClickableViewAccessibility")
        ViewHolder(View v)
        {
            super(v);
            mDeleteButton = v.findViewById(R.id.delete_file_button);
            mTextView = v.findViewById(R.id.file_textview);
            mExpandButton = v.findViewById(R.id.file_expand);
            mActionsContainer = v.findViewById(R.id.file_actions);
            mFolderIdentifier = v.findViewById(R.id.folder_indicator);
            mFileType = v.findViewById(R.id.file_type);
            mRenameButton = v.findViewById(R.id.rename_file_button);
            mShareButton = v.findViewById(R.id.share_file_button);
            mOpenButton = v.findViewById(R.id.open_file_button);
            mMimeType = v.findViewById(R.id.file_mime);
            mFileSize = v.findViewById(R.id.file_size);
            mUnixPerms = v.findViewById(R.id.file_permissions);
            mExportButton = v.findViewById(R.id.export_file_button);
            mPermButton = v.findViewById(R.id.chmod_file_button);
            mPackage = v.findViewById(R.id.package_file_button);
            mPin = v.findViewById(R.id.pin);
            mCopy = v.findViewById(R.id.copy_file);
            View.OnTouchListener onTouchListener = (v2, event) ->
            {
                if (event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                }
                return false;
            };
            v.findViewById(R.id.buttons_fileview_scroll).setOnTouchListener(onTouchListener);
            mDeleteButton.setOnTouchListener(onTouchListener);
            mRenameButton.setOnTouchListener(onTouchListener);
            mShareButton.setOnTouchListener(onTouchListener);
            mOpenButton.setOnTouchListener(onTouchListener);
            mExportButton.setOnTouchListener(onTouchListener);
            mPermButton.setOnTouchListener(onTouchListener);
            mPackage.setOnTouchListener(onTouchListener);
        }
    }

    static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private FilesListAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(FilesListAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = mAdapter.activity.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(mAdapter.activity.getResources().getColor(R.color.holo_red_dark));
            icon = mAdapter.activity.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            int i = viewHolder.getAdapterPosition();
            File file = mAdapter.positionArray[viewHolder.getAdapterPosition()];
            String i3 = mAdapter.files[viewHolder.getAdapterPosition()];
            mAdapter.removals.add(i3);
            final boolean[] flag = {false};
            mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            Snackbar.make(viewHolder.itemView, R.string.item_removed, Snackbar.LENGTH_LONG).setAction(R.string.undo, view ->
            {
                flag[0] = true;
                while (mAdapter.removals.contains(i3))
                {
                    mAdapter.removals.remove(i3);
                }
                mAdapter.notifyItemInserted(i);
            }).addCallback(new Snackbar.Callback()
            {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event)
                {
                    super.onDismissed(transientBottomBar, event);
                    Log.d(getClass().getName() + "@" + hashCode(), "onDismissed: dismiss. flag state" + flag[0]);
                    if (!flag[0])
                    {
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            Library.deleteDirectoryRecursively(file);
                            mAdapter.activity.runOnUiThread(() ->
                            {
                                while (mAdapter.removals.contains(i3))
                                {
                                    mAdapter.removals.remove(i3);
                                }
                            });
                        });
                    }
                }
            }).setActionTextColor(Minicode.context.getResources().getColor(R.color.accent_device_default_light)).show();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            Drawable background = this.background.getConstantState().newDrawable();
            background.mutate();
            try
            {
                if ((this.mAdapter.positionArray[viewHolder.getAdapterPosition()].getName().equals("project_manifest.json") || this.mAdapter.positionArray[viewHolder.getAdapterPosition()].getName().equals("Makefile")) && this.mAdapter.positionArray[viewHolder.getAdapterPosition()].getParent().equals(new File(Minicode.context.getFilesDir() + "/" + this.mAdapter.projectName).getAbsolutePath()))
                {
                    background.setTint(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                }
            }
            catch (IndexOutOfBoundsException e)
            {
            }
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 40;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }

    private class FileType
    {
        int i;
        String type;

        private FileType(String type, int i)
        {
            this.type = type;
            this.i = i;
        }
    }
}