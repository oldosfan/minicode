/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.adapter.projects;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.project.navigator.NavigatorActivity;
import low.minicode.application.Minicode;

public class RedesignedProjectsListAdapter extends RecyclerView.Adapter<RedesignedProjectsListAdapter.ViewHolder>
{
    private File[] buf;
    private Context context;
    private ItemTouchHelper touchHelper;
    private Library.FilterCallback<File> fileFilterCallback;
    private ArrayList<String> toDelete;
    private StateChangeListener stateChangeListener;

    public RedesignedProjectsListAdapter(Context context)
    {
        this.context = context;
        this.touchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(this));
        this.toDelete = new ArrayList<>();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        touchHelper.attachToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_project, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.name.setText(buf[position].getName());
        holder.itemView.setOnClickListener(v ->
        {
            Minicode.context.getSharedPreferences("low.minicode.project_usage", Context.MODE_PRIVATE).edit().putInt(buf[position].getName(), Minicode.context.getSharedPreferences("low.minicode.project_usage", Context.MODE_PRIVATE).getInt(buf[position].getName(), 0) + 1).apply();
            Intent i = new Intent(context, NavigatorActivity.class);
            i.putExtra("projectName", buf[position].getName());
            i.putExtra("loc", "/");
            context.startActivity(i);
        });
        holder.bookmark.setImageResource(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getStringSet("pinned_projects", new ConcurrentSkipListSet<>()).contains(buf[position].getName()) ? R.drawable.ic_round_star_rate_filled_24px : R.drawable.ic_round_star_rate_24px);
        holder.bookmark.setOnClickListener(v ->
        {
            Set<String> stringSet = Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getStringSet("pinned_projects", new ConcurrentSkipListSet<>());
            Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().remove("pinned_projects").commit();
            if (stringSet.contains(buf[position].getName()))
            {
                stringSet.remove(buf[position].getName());
                Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putStringSet("pinned_projects", stringSet).commit();
                holder.bookmark.setImageResource(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getStringSet("pinned_projects", new ConcurrentSkipListSet<>()).contains(buf[position].getName()) ? R.drawable.ic_round_star_rate_filled_24px : R.drawable.ic_round_star_rate_24px);
                if (stateChangeListener != null)
                {
                    stateChangeListener.onRemoveBookmark(buf[position].getName());
                }
            }
            else
            {
                stringSet.add(buf[position].getName());
                Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putStringSet("pinned_projects", stringSet).commit();
                holder.bookmark.setImageResource(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getStringSet("pinned_projects", new ConcurrentSkipListSet<>()).contains(buf[position].getName()) ? R.drawable.ic_round_star_rate_filled_24px : R.drawable.ic_round_star_rate_24px);
                if (stateChangeListener != null)
                {
                    stateChangeListener.onBookmark(buf[position].getName());
                }
            }
        });
        holder.pin.setImageResource(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("pinned", null) != null && Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("pinned", null).equals(buf[position].getName()) ? R.drawable.ic_pin_24dp : R.drawable.ic_pin_off_outline);
        holder.pin.setOnClickListener(v ->
        {
            if (Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("pinned", null) != null && Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("pinned", null).equals(buf[position].getName()))
            {
                Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().remove("pinned").commit();
                if (stateChangeListener != null)
                {
                    stateChangeListener.onPin(null);
                }
            }
            else
            {
                Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putString("pinned", buf[position].getName()).commit();
                if (stateChangeListener != null)
                {
                    stateChangeListener.onPin(buf[position].getName());
                }
            }
            Minicode.theMinicode.updateShortcuts();
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount()
    {
        Iterator<String> stringIterator = toDelete.iterator();
        while (stringIterator.hasNext())
        {
            if (!new File(context.getFilesDir(), stringIterator.next()).exists())
            {
                stringIterator.remove();
            }
        }
        buf = Library.filterOut(File.class, context.getFilesDir().listFiles(), object ->
        {
            if (fileFilterCallback == null)
            {
                return object.getName().equals(".res") || object.getName().equals("man_pages") || toDelete.contains(object.getName());
            }
            else
            {
                return fileFilterCallback.handle(object) || object.getName().equals(".res") || object.getName().equals("man_pages") || toDelete.contains(object.getName());
            }
        });
        if (stateChangeListener != null)
        {
            stateChangeListener.onItemsCounted(buf.length);
        }
        return buf.length;
    }

    public Library.FilterCallback<File> getFileFilterCallback()
    {
        return fileFilterCallback;
    }

    public void setFileFilterCallback(Library.FilterCallback<File> fileFilterCallback)
    {
        this.fileFilterCallback = fileFilterCallback;
    }

    public void setStateChangeListener(StateChangeListener stateChangeListener)
    {
        this.stateChangeListener = stateChangeListener;
    }

    public interface StateChangeListener
    {
        void onRemoveBookmark(@NonNull String project);

        void onBookmark(@NonNull String project);

        void onUndo(@NonNull String project);

        void onPin(@Nullable String project);

        void onRemove(@NonNull String project);

        void onItemsCounted(int count);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name;
        public ImageButton bookmark, pin;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.project_name);
            bookmark = itemView.findViewById(R.id.bookmark);
            pin = itemView.findViewById(R.id.pin);
        }
    }

    private static class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
    {
        private RedesignedProjectsListAdapter mAdapter;
        private Drawable icon;
        private Drawable background;

        public SwipeToDeleteCallback(RedesignedProjectsListAdapter adapter)
        {
            super(0, ItemTouchHelper.START);
            mAdapter = adapter;
            background = mAdapter.context.getResources().getDrawable(R.drawable.roundable_sharp).getConstantState().newDrawable().mutate();
            background.setTint(mAdapter.context.getResources().getColor(R.color.holo_red_dark));
            icon = mAdapter.context.getResources().getDrawable(R.drawable.ic_delete_sweep_black_24dp);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
        {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
        {
            int position = viewHolder.getAdapterPosition();
            boolean[] flag = {false};
            mAdapter.toDelete.add(mAdapter.buf[position].getName());
            File file = mAdapter.buf[position];
            mAdapter.notifyItemRemoved(position);
            if (mAdapter.stateChangeListener != null)
            {
                mAdapter.stateChangeListener.onRemove(file.getName());
            }
            Snackbar.make(viewHolder.itemView, R.string.item_removed, Snackbar.LENGTH_LONG).setActionTextColor(mAdapter.context.getResources().getColor(R.color.accent_device_default_light)).setAction(R.string.undo, v ->
            {
                flag[0] = true;
                while (mAdapter.toDelete.contains(file.getName()))
                {
                    mAdapter.toDelete.remove(file.getName());
                }
                mAdapter.notifyItemInserted(position);
                if (mAdapter.stateChangeListener != null)
                {
                    mAdapter.stateChangeListener.onUndo(file.getName());
                }
            }).addCallback(new Snackbar.Callback()
            {
                @Override
                public void onDismissed(Snackbar transientBottomBar, int event)
                {
                    super.onDismissed(transientBottomBar, event);
                    if (!flag[0])
                    {
                        Executors.newSingleThreadExecutor().submit(() -> Library.deleteDirectoryRecursively(file));
                    }
                }
            }).show();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 20;
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();
            if (dX > 0)
            {
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            }
            else if (dX < 0)
            {
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset, itemView.getTop(), itemView.getRight(), itemView.getBottom());
            }
            else
            {
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            icon.draw(c);
        }
    }
}
