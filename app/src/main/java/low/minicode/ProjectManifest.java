/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

@SuppressWarnings({"ResultOfMethodCallIgnored", "WeakerAccess"})
public class ProjectManifest
{
    public String compilerFlags;
    public String linkerFlags;
    public String passHash;
    public List<String> compileFiles;
    public List<String> args;
    public boolean useGmake;
    public boolean isShared;
    protected JSONObject jsonObject;
    protected JSONArray jsonArgsArray;
    protected JSONArray compileFilesJSON;
    public File manifestLocation;

    public ProjectManifest(File manifest)
    {
        this.manifestLocation = manifest;
        this.compileFiles = new ArrayList<>();
        this.args = new ArrayList<>();
        if (manifest.isDirectory())
        {
            throw new ManifestException("Manifest initialization error: Manifest is directory");
        }
        Scanner scanner;
        try
        {
            scanner = new Scanner(this.manifestLocation);
        }
        catch (FileNotFoundException e)
        {
            try
            {
                manifest.createNewFile();
                scanner = new Scanner(this.manifestLocation);
            }
            catch (IOException e2)
            {
                e2.printStackTrace();
                throw new ManifestException("File initialization error: Attempted to create manifest after catching FileNotFoundException, but caught IOException: " + e2.getMessage());
            }
            catch (Exception e2)
            {
                e2.printStackTrace();
                throw new ManifestException("File initialization error: caught something?" + " Caught unknown exception: " + e2.getMessage());
            }
        }
        String jsonText;
        try
        {
            jsonText = scanner.useDelimiter("\\A").next();
        }
        catch (NoSuchElementException e3)
        {
            jsonText = "{}";
        }
        try
        {
            this.jsonObject = new JSONObject(jsonText);
        }
        catch (JSONException e)
        {
            try
            {
                Library.writeFileAsBytes(this.manifestLocation.getAbsolutePath(), "".getBytes());
            }
            catch (IOException e2)
            {
                throw new ManifestException("File error: Caught IOException after" + " trying to fix JSON syntax error by overwriting file: " + e2.getMessage());
            }
        }
        try
        {
            this.compileFilesJSON = jsonObject.getJSONArray("compile_files");
        }
        catch (JSONException e)
        {
            this.compileFilesJSON = new JSONArray();
        }
        try
        {
            this.linkerFlags = jsonObject.getString("ld_flags");
        }
        catch (JSONException e)
        {
            this.linkerFlags = "";
        }
        try
        {
            this.compilerFlags = jsonObject.getString("cc_flags");
        }
        catch (JSONException e)
        {
            this.compilerFlags = "";
        }
        for (int i = 0; i < compileFilesJSON.length(); ++i)
        {
            try
            {
                if (compileFilesJSON.getString(i).startsWith("/"))
                {
                    this.compileFiles.add(compileFilesJSON.getString(i));
                }
                else
                {
                    this.compileFiles.add(new File(manifest.getParentFile(), compileFilesJSON.getString(i)).getAbsolutePath());
                }
            }
            catch (JSONException e)
            {
                throw new ManifestException("Illegal manifest type? Generic error in manifest object" + " \"compile_files\"");
            }
        }
        try
        {
            this.jsonArgsArray = this.jsonObject.getJSONArray("args");
        }
        catch (JSONException e)
        {
            this.jsonArgsArray = new JSONArray();
        }
        for (int i = 0; i < this.jsonArgsArray.length(); ++i)
        {
            try
            {
                this.args.add(this.jsonArgsArray.getString(i));
            }
            catch (JSONException e)
            {
                throw new ManifestException("Syntax error? JSONException caught while populating" + " args with jsonArgsArray");
            }
        }
        try
        {
            this.useGmake = this.jsonObject.getBoolean("use_gmake");
        }
        catch (JSONException e)
        {
            this.useGmake = false;
        }
        try
        {
            this.passHash = this.jsonObject.getString("pass_hash");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            this.passHash = "";
        }
        try
        {
            this.isShared = this.jsonObject.getBoolean("shared_library");
        }
        catch (JSONException e)
        {
            this.isShared = false;
        }
    }

    public void putOnArgsArray(String arg)
    {
        this.args.add(arg);
        this.jsonArgsArray.put(arg);
    }

    public void removeFromArgsArray(int arr)
    {
        this.args.remove(arr);
        this.jsonArgsArray.remove(arr);
    }

    public void putOnFilesList(String file)
    {
        boolean flag = false;
        for (int i = 0; i < this.compileFiles.size(); ++i)
        {
            if (this.compileFiles.get(i).equals(file))
            {
                flag = true;
                return;
            }
        }
        if (!flag)
        {
            this.compileFiles.add(file);
            this.compileFilesJSON.put(file);
        }
        return;
    }

    public void removeFromFilesList(String file)
    {
        for (int i = 0; i < this.compileFiles.size(); ++i)
        {
            if (this.compileFiles.get(i).equals(file))
            {
                this.compileFiles.remove(i);
                this.compileFilesJSON.remove(i);
            }
        }
    }

    public void removeFromFilesList(int i)
    {
        this.compileFiles.remove(i);
        this.compileFilesJSON.remove(i);
    }

    public void writeToFile(String f)
    {
        try
        {
            jsonObject.put("compile_files", this.compileFilesJSON);
            jsonObject.put("ld_flags", this.linkerFlags);
            jsonObject.put("cc_flags", this.compilerFlags);
            jsonObject.put("args", this.jsonArgsArray);
            jsonObject.put("use_gmake", this.useGmake);
            jsonObject.put("pass_hash", this.passHash);
            jsonObject.put("shared_library", this.isShared);
        }
        catch (JSONException e)
        {
            throw new ManifestException("Caught JSONException when populating JSON. Invalid JSON? Unknown error");
        }
        try
        {
            Library.writeFileAsBytes(f, jsonObject.toString().getBytes());
        }
        catch (IOException e)
        {
            throw new ManifestException("Caught IOException when writing to file: " + e.getMessage());
        }
        return;
    }

    public class ManifestException extends RuntimeException
    {
        public ManifestException(String e)
        {
            super(e);
            return;
        }
    }
}
