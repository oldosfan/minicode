/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Ordering;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import com.jcraft.jsch.KeyPairRSA;
import com.jcraft.jsch.Session;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CharSequenceReader;
import org.apache.http.conn.UnsupportedSchemeException;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.TransportCommand;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.RevWalkUtils;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.EmptyTreeIterator;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.io.DisabledOutputStream;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.PatternSyntaxException;

import javax.annotation.Nullable;
import javax.net.ssl.HttpsURLConnection;

import jsyntaxpane.Token;
import jsyntaxpane.TokenType;
import jsyntaxpane.lexers.CLexer;
import jsyntaxpane.lexers.CppLexer;
import jsyntaxpane.lexers.DefaultJFlexLexer;
import jsyntaxpane.lexers.PythonLexer;
import low.minicode.application.Minicode;
import low.minicode.fragments.git.VCSSettingsFragment;
import low.minicode.terminal.view.TerminalView;
import low.minicode.wrappers.data.DualDataStructure;
import low.minicode.wrappers.data.tree.Node;

import static android.content.Context.VIBRATOR_SERVICE;
import static low.minicode.AdvancedLanguage.C;
import static low.minicode.AdvancedLanguage.CC;
import static low.minicode.AdvancedLanguage.PYTHON3;
import static low.minicode.fragments.git.VCSSettingsFragment.NONE;
import static low.minicode.fragments.git.VCSSettingsFragment.PASSWORD;
import static low.minicode.fragments.git.VCSSettingsFragment.SSH;

public class Library
{
    public static final char NEWLINE = (char) 10;
    public static final String NEWLINE_S = "\n";
    private static final String TAG = "LibraryUtils";
    public static String makefileMessage = "# Special requirements for writing miniCode makefiles\n" + "# 1) Use the CC and CXX variables, and do *not* set them yourself\n" + "# 2) Only *append* CCFLAGS! Don't clear or rewrite the CCFLAGS variable\n" + "# 3) Use CCFLAGS or CXXFLAGS. Don't use your own variables.\n" + "# 4) gmake only understands indentation with tabs. Use the [[TAB]] button to input a tab (\t) instead of spaces ( )\n" + "# 5) A target must be present named minicode, that calls all needed dependencies, or the build will fail\n" + "# 6) Don't tamper with the path setting\n" + String.format("export PATH := %s/bin:$(PATH)", getPrefixDir());

    public static boolean isCrontabValid(String crontab)
    {
        crontab = crontab.trim();
        if (crontab.isEmpty())
        {
            Log.d("LibraryUtils", "isCrontabValid: crontab is empty, so can't posibilty be valid");
            return false;
        }
        else if (crontab.split(" ").length != 5)
        {
            Log.d("LibraryUtils", "isCrontabValid: the crontab doesn't contain exactly 5 entries. it's invalid");
            Log.d("LibraryUtils", "isCrontabValid: this is the crontab split up: " + Arrays.toString(crontab.split(" ")));
            return false;
        }
        for (String iter : crontab.split(" "))
        {
            if (!(iter.equals("*") || Character.isDigit(iter.charAt(0))))
            {
                Log.d("LibraryUtils", "isCrontabValid: the crontab contains invalid characters");
                return false;
            }
        }
        Log.d("LibraryUtils", "isCrontabValid: the crontab appears to be valid");
        return true;
    }

    @UiThread
    public static void emphasizeBlinkView(View enphasizeView)
    {
        Handler h = new Handler();
        Executors.newSingleThreadExecutor().submit(() ->
        {
            for (int iter : Library.range(1, 4))
            {
                h.post(enphasizeView::requestFocus);
                try
                {
                    Thread.sleep(100);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                h.post(enphasizeView::clearFocus);
                try
                {
                    Thread.sleep(100);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            try
            {
                Thread.sleep(100);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            h.post(enphasizeView::requestFocus);
        });
    }

    public static void askDialogString(boolean material, String title, String hint, Context windowContext, ReturningCallback<String, Void> callback)
    {
        AlertDialog.Builder builder = material ? new MaterialAlertDialogBuilder(windowContext) : new AlertDialog.Builder(windowContext);
        EditText view = new EditText(windowContext);
        view.setHint(hint != null ? hint : "");
        builder.setTitle(title);
        builder.setView(view);
        builder.setPositiveButton(R.string.ok, ((dialog, which) -> callback.execute(view.getText().toString())));
        builder.show();
    }

    public static Bitmap drawableToBitmap(Drawable drawable)
    {
        Bitmap bitmap;
        if (drawable instanceof BitmapDrawable)
        {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null)
            {
                return bitmapDrawable.getBitmap();
            }
        }
        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0)
        {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        }
        else
        {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /*
    This is LOS sample code
     */
    public static int getLineageSDKVersion()
    {
        try
        {
            Process process = Runtime.getRuntime().exec("getprop ro.lineage.build.version.plat.sdk");
            BufferedReader reader;
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder output = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
            {
                output.append(line);
            }
            String result = output.toString();
            return TextUtils.isEmpty(result) ? 0 : Integer.parseInt(result);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (NumberFormatException ignored)
        {
        }
        return 0;
    }

    public static void massert(boolean firstCondition, String err, boolean... conditions)
    {
        for (boolean b : conditions)
        {
            if (!(b && firstCondition))
            {
                throw new AssertionError(err);
            }
        }
    }

    public static void appendColoredText(TerminalView tv, String text, int color, @Nullable Integer background, boolean bold)
    {
        tv.append(text, new TerminalView.CellState(color, background == null ? -1 : background, bold));
    }

    public static void deleteDirectoryRecursively(File directory)
    {
        if (!directory.isDirectory())
        {
            directory.delete();
            return;
        }
        else if (directory.listFiles().length < 1)
        {
            directory.delete();
        }
        else
        {
            File[] list = directory.listFiles();
            for (File iter : list)
            {
                deleteDirectoryRecursively(iter);
            }
        }
        directory.delete();
    }

    public static void intactDeleteDirectoryContentsRecursively(File directory)
    {
        if (!directory.isDirectory() || directory.listFiles().length < 1)
        {
            return;
        }
        else
        {
            for (File iter : directory.listFiles())
            {
                deleteDirectoryRecursively(directory);
            }
        }
    }

    public static File getCompilerBinary()
    {
        return new File(String.format("%s/clang", getPrefixDir() + "/bin"));
    }

    public static File getPython3Prefix()
    {
        return new File(getResDir(), String.format("python3/%s/prefix", getArchName()));
    }

    public static File getPython3Executable()
    {
        return new File(getPython3Prefix(), "bin/python3.7m");
    }

    public static void writeFileAsBytes(String fullPath, byte[] bytes) throws IOException
    {
        OutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(fullPath));
        InputStream inputStream = new ByteArrayInputStream(bytes);
        int token = -1;
        while ((token = inputStream.read()) != -1)
        {
            bufferedOutputStream.write(token);
        }
        bufferedOutputStream.flush();
        bufferedOutputStream.close();
        inputStream.close();
    }

    public static File getTempDir()
    {
        return new File(getResDir(), ".tmp");
    }

    public static void ensureTempDir()
    {
        if (getTempDir().exists())
        {
            return;
        }
        getTempDir().mkdirs();
    }

    public static void displayErrorDialogFromThrowable(Context windowContext, Throwable error, boolean material)
    {
        displayErrorDialogFromThrowable(windowContext, error, material, null);
    }

    public static void displayErrorDialogFromThrowable(Context windowContext, Throwable error, boolean material, @Nullable Runnable callback)
    {
        if (material)
        {
            new MaterialAlertDialogBuilder(windowContext).setTitle(R.string.error).setCancelable(false).setMessage(error.getLocalizedMessage()).setPositiveButton(R.string.ok, (dialog, which) ->
            {
                if (callback != null)
                {
                    callback.run();
                }
            }).show();
        }
        else
        {
            new AlertDialog.Builder(windowContext).setTitle(R.string.error).setCancelable(false).setMessage(error.getLocalizedMessage()).setPositiveButton(R.string.ok, (dialog, which) ->
            {
                if (callback != null)
                {
                    callback.run();
                }
            }).show();
        }
    }

    public static String relative(final File base, final File file)
    {
        int rootLength = base.getAbsolutePath().length();
        String absl = file.getAbsolutePath();
        return absl.substring(rootLength + 1);
    }

    public static List<String[]> compilerStrings(List<String> files, String ldFlags, String ccFlags, boolean sharedLibrary, String projectName)
    {
        ArrayList<String[]> result = new ArrayList<>();
        boolean cpp = false;
        for (String file : files)
        {
            if (file.matches("^.*c(pp|c|xx?|mm|mpp)$"))
            {
                cpp = true;
            }
            ArrayList<String> tmp = new ArrayList<>(Arrays.asList(getCompilerBinary().getAbsolutePath(), String.format("-I%s", getPrefixDir() + "/include"), ccFlags, file, "-c", "-o", getTempDir() + "/" + getMD5Hash(file) + ".o"));
            for (File iter : Library.getCustomLibDir().listFiles())
            {
                if (new File(iter, "include").exists() && new File(iter, "include").isDirectory())
                {
                    tmp.add("-I" + new File(iter, "include").getAbsolutePath());
                }
            }
            result.add(Arrays.copyOf(tmp.toArray(), tmp.size(), String[].class));
        }
        ArrayList<String> temp = new ArrayList<>();
        temp.add(getCompilerBinary().getAbsolutePath());
        for (String file : files)
        {
            temp.add(new File(getTempDir(), getMD5Hash(file) + ".o").getAbsolutePath());
        }
        if (cpp)
        {
            temp.add("-lc++_shared");
        }
        temp.addAll(Arrays.asList(String.format("-L%s", getPrefixDir() + "/lib"), String.format("-L%s", Minicode.context.getApplicationInfo().nativeLibraryDir), ldFlags));
        for (File iter : Library.getCustomLibDir().listFiles())
        {
            if (new File(iter, "lib").exists() && new File(iter, "lib").isDirectory())
            {
                temp.add("-L" + new File(iter, "lib").getAbsolutePath());
            }
        }
        if (sharedLibrary)
        {
            temp.add("-shared");
        }
        temp.add("-o");
        temp.add(sharedLibrary ? getTempDir() + "/" + projectName + ".so" : getTempDir() + "/" + projectName);
        result.add(Arrays.copyOf(temp.toArray(), temp.size(), String[].class));
        return result;
    }

    public static String getMD5Hash(String data)
    {
        String result = null;
        try
        {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] hash = digest.digest(data.getBytes(StandardCharsets.UTF_8));
            return bytesToHex(hash);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return result;
    }

    public static String bytesToHex(byte[] hash)
    {
        StringBuilder s = new StringBuilder();
        for (byte b : hash)
        {
            s.append(Integer.toString(b, 16));
        }
        return s.toString();
    }

    public static Object safeGetThingFromJSONArray(JSONArray jsonArray, int i)
    {
        try
        {
            return jsonArray.get(i);
        }
        catch (JSONException e)
        {
            return null;
        }
    }

    public static boolean wereAppAssetsUpdated(Context a)
    {
        SharedPreferences sp = a.getSharedPreferences("low.minicode.lastversion", Context.MODE_PRIVATE);
        return sp.getInt("v", Minicode.assetVersion + 100003) < Minicode.assetVersion;
    }

    @SuppressLint("ApplySharedPref")
    public static void storeVersionToSharedPrefs(Context a)
    {
        SharedPreferences sp = a.getSharedPreferences("low.minicode.lastversion", Context.MODE_PRIVATE);
        sp.edit().putInt("v", Minicode.assetVersion).commit();
    }

    public static File getPrefixDir()
    {
        return new File(Minicode.context.getFilesDir(), ".res/" + getArchName());
    }

    public static File getImagesCacheDir()
    {
        if (!new File(Minicode.context.getCacheDir(), "images").isDirectory())
        {
            new File(Minicode.context.getCacheDir(), "images").mkdirs();
        }
        return new File(Minicode.context.getCacheDir(), "images");
    }

    public static File getResDir()
    {
        return new File(Minicode.context.getFilesDir(), ".res");
    }

    public static String getArchName()
    {
        String arch = System.getProperty("os.arch");
        assert arch != null;
        String name;
        if (arch.contains("arm"))
        {
            name = "arm";
        }
        else if (arch.contains("arch64"))
        {
            name = "aarch64";
        }
        else if (arch.contains("86") && !arch.contains("64"))
        {
            name = "i686";
        }
        else if (arch.contains("86") && arch.contains("64"))
        {
            name = "x86_64";
        }
        else
        {
            throw new AssertionError("Unsupported architecture");
        }
        return name;

    }

    public static String[] compilerStrings(ProjectManifest m, String outputFolder)
    {
        if (m.compileFiles.size() < 1)
        {
            return new String[] { };
        }
        String makeObject = "%s/clang --save-temps=cwd %s %s -I%s -L%s -L%s %s -lc++ -lstdc++ -lc++_shared -o \"%s\"";
        ArrayList<String> results = new ArrayList<>();
        deleteDirectoryRecursively(new File(getPrefixDir(), ".temps/"));
        //noinspection ResultOfMethodCallIgnored
        new File(getPrefixDir(), ".temps/").mkdirs();
        StringBuilder sb = new StringBuilder();
        for (String iter : m.compileFiles)
        {
            sb.append("\"" + iter + "\"").append(" ");
        }
        results.add(String.format(makeObject, getPrefixDir().getAbsolutePath() + "/bin", sb.toString(), m.compilerFlags, getPrefixDir().getAbsolutePath() + "/include", Minicode.context.getApplicationInfo().nativeLibraryDir, getPrefixDir().getAbsolutePath() + "/lib", m.linkerFlags, new File(m.manifestLocation.getParent(), outputFolder + "/executableOutput").getAbsolutePath()));
        String[] arr = new String[results.size()];
        results.toArray(arr);
        return arr;
    }

    public static String compiler(File output, File file)
    {
        String makeObject = "%s/clang --save-temps=cwd %s -I%s -L%s -L%s -lc++ -lstdc++ -lc++_shared -o \"%s\"";
        ArrayList<String> results = new ArrayList<>();
        deleteDirectoryRecursively(new File(getPrefixDir(), ".temps/"));
        //noinspection ResultOfMethodCallIgnored
        new File(getPrefixDir(), ".temps").mkdirs();
        return String.format(makeObject, getPrefixDir().getAbsolutePath() + "/bin", file.getAbsolutePath(), getPrefixDir() + "/include", getPrefixDir() + "/lib", Minicode.context.getApplicationInfo().nativeLibraryDir, output.getAbsolutePath());
    }

    public static String lldb(String projectName, File executable)
    {
        String makeObject = "%s/lldb %s";
        return String.format(getPrefixDir().getAbsolutePath() + "/bin", executable.getAbsolutePath());
    }

    public static void logProcess(Process process, String tag)
    {
        BufferedReader br1 = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        BufferedReader br2 = new BufferedReader(new InputStreamReader(process.getInputStream()));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                String s;
                while ((s = br1.readLine()) != null)
                {
                    Log.e(tag, s);
                }
            }
            catch (IOException e)
            {
            }
        });
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                String s;
                while ((s = br2.readLine()) != null)
                {
                    Log.d(tag, s);
                }
            }
            catch (IOException e)
            {
            }
        });
    }

    @Nullable
    public static String getGmakeInvocation(Context c)
    {
        String arch = getArchName();
        String base = "%s/gmake/%s/prefix/bin/make CC=\"%s\" CXX=\"%s\" MCROOT=\"%s\" CCFLAGS=\"--save-temps=obj -I\\\"%s\\\" -L\\\"%s\\\"\" CXXFLAGS=\"-L\\\"%s\\\"\" SHELL=/system/bin/sh";
        String cc = new File(getPrefixDir().getAbsolutePath() + ("/bin/clang")).getAbsolutePath();
        String cxx = new File(getPrefixDir().getAbsolutePath() + ("/bin/clang++")).getAbsolutePath();
        return String.format(base, c.getFilesDir().getAbsolutePath() + "/.res", arch, cc, cxx, getPrefixDir().getAbsolutePath(), getPrefixDir() + "/include", getPrefixDir() + "/lib", c.getApplicationInfo().nativeLibraryDir);
    }

    public static <T extends Object> boolean containedIn(ArrayList<T> t, T a)
    {
        for (T iter : t)
        {
            if (a.equals(iter))
            {
                return true;
            }
        }
        return false;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri)
    {
        Cursor cursor = null;
        try
        {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index) != null ? cursor.getString(column_index) : "";
        }
        catch (Exception e)
        {
            return "";
        }
        finally
        {
            if (cursor != null)
            {
                cursor.close();
            }
        }
    }

    public static String getFileName(Uri uri, Context c)
    {
        String result = null;
        if (uri.getScheme().equals("content"))
        {
            Cursor cursor = c.getContentResolver().query(uri, null, null, null, null);
            try
            {
                if (cursor != null && cursor.moveToFirst())
                {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }
            finally
            {
                cursor.close();
            }
        }
        if (result == null)
        {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1)
            {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static String[] getFileInvocation(String absloutePath)
    {
        return new String[] {String.format("%s/.res/file/%s/bin/file", Minicode.context.getFilesDir(), getArchName()), "-m", String.format("%s/.res/file/%s/share/misc/magic.mgc", Minicode.context.getFilesDir(), getArchName()), absloutePath};
    }

    public static String getFileLibPath()
    {
        return String.format("%s/.res/file/%s/lib", Minicode.context.getFilesDir(), getArchName());
    }

    public static String getBetterFilesize(long bytes)
    {
        int kbLimit = 1024;
        int mbLimit = 1024 * kbLimit;
        int gbLimit = 1024 * mbLimit;
        if (bytes < kbLimit)
        {
            return String.format("%d b", bytes);
        }
        else if (bytes < mbLimit)
        {
            return String.format("%f kb", (double) bytes / kbLimit);
        }
        else if (bytes < gbLimit)
        {
            return String.format("%f mb", (double) bytes / mbLimit);
        }
        else if (bytes >= gbLimit)
        {
            return String.format("%f gb", (double) bytes / gbLimit);
        }
        else
        {
            return String.valueOf(bytes);
        }
    }

    public static double percentage(double total, double amount)
    {
        return (amount / total) * 100;
    }

    public static void vibrateError()
    {
        if (!Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("haptic", true))
        {
            return;
        }
        long[] vibrations = {100, 10, 100, 10, 100, 10, 500};
        Vibrator v2 = (Vibrator) Minicode.context.getSystemService(VIBRATOR_SERVICE);
        Executors.newSingleThreadExecutor().submit(() -> v2.vibrate(vibrations, -1));
    }

    public static void vibrateSuccess()
    {
        if (!Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("haptic", true))
        {
            return;
        }
        long[] vibrations = {100, 40, 200, 10, 90};
        Vibrator v2 = (Vibrator) Minicode.context.getSystemService(VIBRATOR_SERVICE);
        Executors.newSingleThreadExecutor().submit(() -> v2.vibrate(vibrations, -1));
    }

    public static void vibrateAttention()
    {
        if (!Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("haptic", true))
        {
            return;
        }
        long[] vibrations = {100, 10, 600, 10, 100, 10, 60};
        Vibrator v2 = (Vibrator) Minicode.context.getSystemService(VIBRATOR_SERVICE);
        Executors.newSingleThreadExecutor().submit(() -> v2.vibrate(vibrations, -1));
    }

    public static void vibratePulse()
    {
        if (!Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getBoolean("haptic", true))
        {
            return;
        }
        long[] vibrations = {50, 50, 50};
        Vibrator v2 = (Vibrator) Minicode.context.getSystemService(VIBRATOR_SERVICE);
        Executors.newSingleThreadExecutor().submit(() -> v2.vibrate(vibrations, -1));
    }

    public static Future<Integer> strip(File executable, ReturningCallback<Integer, Void> onResultHandler)
    {
        String invocation = String.format(String.format("%s/strip", getPrefixDir().getAbsolutePath() + "/bin"));
        String[] invocate = {invocation, executable.getAbsolutePath()};
        return Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                Process p = Runtime.getRuntime().exec(invocate, new String[] {String.format("LD_LIBRARY_PATH=%s:%s", Minicode.context.getApplicationInfo().nativeLibraryDir, Library.getPrefixDir() + "/lib")});
                Log.d(Library.class.getName(), "(S) strip: invocation: " + Arrays.toString(invocate));
                int result = p.waitFor();
                onResultHandler.execute(result);
                return result;
            }
            catch (IOException e)
            {
                e.printStackTrace();
                onResultHandler.execute(1);
                return 1;
            }
        });
    }

    public static int[] calculateDivergence(Ref local, Ref tracking, Repository repository) throws IOException
    {
        RevWalk walk = new RevWalk(repository);
        int aheadCount, behindCount;
        try
        {
            RevCommit localCommit = walk.parseCommit(local.getObjectId());
            RevCommit trackingCommit = walk.parseCommit(tracking.getObjectId());
            walk.setRevFilter(RevFilter.MERGE_BASE);
            walk.markStart(localCommit);
            walk.markStart(trackingCommit);
            RevCommit mergeBase = walk.next();
            walk.reset();
            walk.setRevFilter(RevFilter.ALL);
            aheadCount = RevWalkUtils.count(walk, localCommit, mergeBase);
            behindCount = RevWalkUtils.count(walk, trackingCommit, mergeBase);
        }
        finally
        {
            walk.dispose();
        }
        return new int[] {aheadCount, behindCount};
    }

    public static Iterable<Long> range(long start, long end)
    {
        ArrayList<Long> c = new ArrayList<>();
        for (long i = start; i <= end; ++i)
        {
            c.add(i);
        }
        return c;
    }

    public static Iterable<Integer> range(int start, int end)
    {
        ArrayList<Integer> c = new ArrayList<>();
        for (int i = start; i <= end; ++i)
        {
            c.add(i);
        }
        return c;
    }

    public static String getLocalIPAddress()
    {
        try
        {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); )
            {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); )
                {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() & !inetAddress.isLinkLocalAddress() && inetAddress.isSiteLocalAddress())
                    {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<DiffEntry> commitDiff(@Nullable RevCommit a, @NonNull RevCommit b, @NonNull Git git) throws IOException
    {
        if (a == null)
        {
            EmptyTreeIterator emptyTreeIterator = new EmptyTreeIterator();
            ObjectReader reader = git.getRepository().newObjectReader();
            AbstractTreeIterator bTreeIter = new CanonicalTreeParser(null, reader, b.getTree());
            DiffFormatter diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE);
            diffFormatter.setRepository(git.getRepository());
            return diffFormatter.scan(emptyTreeIterator, bTreeIter);
        }
        Log.d(Library.class.getName(), String.format("commitDiff: %s -> %s", a.getTree(), b.getTree()));
        ObjectReader reader = git.getRepository().newObjectReader();
        CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
        oldTreeIter.reset(reader, a.getTree());
        CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
        newTreeIter.reset(reader, b.getTree());
        DiffFormatter diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE);
        diffFormatter.setRepository(git.getRepository());
        return diffFormatter.scan(oldTreeIter, newTreeIter);
    }

    public static DualDataStructure<AbstractTreeIterator> getTreeIterators(RevCommit a, Repository repository)
    {
        try
        {
            ObjectReader reader = repository.newObjectReader();
            DualDataStructure<AbstractTreeIterator> dataStructure = null;
            dataStructure = new DualDataStructure<>(new CanonicalTreeParser(null, reader, a.getTree()), a.getParentCount() == 0 ? new EmptyTreeIterator() : new CanonicalTreeParser(null, reader, a.getParent(0).getTree()));
            return dataStructure;
        }
        catch (IOException e)
        {
            return new DualDataStructure<>(new EmptyTreeIterator(), new EmptyTreeIterator());
        }
    }

    @SuppressLint("ApplySharedPref")
    public static KeyPairRSA generateRSAKeyPair() throws JSchException
    {
        JSch jSch = new JSch();
        KeyPairRSA keyPairRSA = (KeyPairRSA) KeyPair.genKeyPair(jSch, KeyPair.RSA);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        keyPairRSA.writePrivateKey(byteArrayOutputStream);
        Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putString("ssh_private_key", byteArrayOutputStream.toString()).commit();
        try
        {
            byteArrayOutputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        byteArrayOutputStream = new ByteArrayOutputStream();
        keyPairRSA.writePublicKey(byteArrayOutputStream, "minicode");
        Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).edit().putString("ssh_public_key", byteArrayOutputStream.toString()).commit();
        try
        {
            byteArrayOutputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return keyPairRSA;
    }

    public static SshSessionFactory createSSHAuthenticator()
    {
        return new JschConfigSessionFactory()
        {
            @Override
            protected JSch createDefaultJSch(FS fs) throws JSchException
            {
                JSch sch = super.createDefaultJSch(fs);
                String pub = Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_public_key", null);
                String priv = Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_private_key", null);
                if (pub == null || priv == null)
                {
                    generateRSAKeyPair().dispose();
                    pub = Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_public_key", null);
                    priv = Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_private_key", null);
                }
                assert priv != null;
                assert pub != null;
                sch.addIdentity("id_rsa", priv.getBytes(), pub.getBytes(), null);
                return sch;
            }

            @Override
            protected void configure(OpenSshConfig.Host hc, Session session)
            {
                session.setConfig("StrictHostKeyChecking", "no");
            }
        };
    }

    public static int getAvaliablePort(Iterable<Integer> ports)
    {
        for (int i : ports)
        {
            try
            {
                ServerSocket s = new ServerSocket(i);
                s.close();
                return i;
            }
            catch (IOException e)
            {
            }
        }
        return -1;
    }

    public static synchronized long getPidOfProcess(Process p)
    {
        long pid = -1;
        try
        {
            if (p.getClass().getName().equals("java.lang.UNIXProcess"))
            {
                Field f = p.getClass().getDeclaredField("pid");
                f.setAccessible(true);
                pid = f.getLong(p);
                f.setAccessible(false);
            }
        }
        catch (Exception e)
        {
            pid = -1;
        }
        return pid;
    }

    public static int getAuthTypeFromID(@IdRes int id)
    {
        switch (id)
        {
            case R.id.password_auth:
            {
                return VCSSettingsFragment.PASSWORD;
            }
            case R.id.ssh_auth:
            {
                return VCSSettingsFragment.SSH;
            }
            case R.id.no_auth:
            {
                return VCSSettingsFragment.NONE;
            }
            default:
            {
                return VCSSettingsFragment.NONE;
            }
        }
    }

    public static int getIDFromAuthType(int id)
    {
        switch (id)
        {
            case VCSSettingsFragment.PASSWORD:
            {
                return R.id.password_auth;
            }
            case VCSSettingsFragment.SSH:
            {
                return R.id.ssh_auth;
            }
            case VCSSettingsFragment.NONE:
            {
                return R.id.no_auth;
            }
            default:
            {
                return 0;
            }
        }
    }

    public static String referRepoNameFromUri(String uri)
    {
        try
        {
            return new URIish(uri).getHumanishName();
        }
        catch (URISyntaxException e)
        {
            return String.format("cannot_refer-%s", UUID.randomUUID().toString());
        }
    }

    public static void transportCredentials(TransportCommand transportCommand, String projectName)
    {
        int auth_type = Minicode.context.getSharedPreferences("low.minicode.preferences." + projectName.hashCode(), Context.MODE_PRIVATE).getInt("auth_type", NONE);
        if (auth_type == PASSWORD)
        {
            String user = Minicode.context.getSharedPreferences("low.minicode.preferences." + projectName.hashCode(), Context.MODE_PRIVATE).getString("username", "");
            String password = Minicode.context.getSharedPreferences("low.minicode.preferences." + projectName.hashCode(), Context.MODE_PRIVATE).getString("password", "");
            transportCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(user, password));
        }
        else if (auth_type == SSH)
        {
            transportCommand.setTransportConfigCallback(transport -> ((SshTransport) transport).setSshSessionFactory(Library.createSSHAuthenticator()));
        }
    }

    public static String followHTTPRedirects(String url, boolean post) throws IOException, URISyntaxException
    {
        URI uri = new URI(url);
        if (uri.getScheme().equals("http"))
        {
            HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod(post ? "POST" : "GET");
            connection.connect();
            Log.d(TAG, String.format("followHTTPRedirects: requestcode: %d", connection.getResponseCode()));
            if (connection.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP || connection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM || connection.getResponseCode() == HttpURLConnection.HTTP_SEE_OTHER || connection.getResponseCode() == 308 || connection.getResponseCode() == 307)
            {
                if (connection.getHeaderField("Location") == null)
                {
                    return url;
                }
                return followHTTPRedirects(connection.getHeaderField("Location"), post);
            }
            else
            {
                return url;
            }
        }
        else if (uri.getScheme().equals("https"))
        {
            HttpsURLConnection connection = (HttpsURLConnection) uri.toURL().openConnection();
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod(post ? "POST" : "GET");
            connection.connect();
            Log.d(TAG, String.format("followHTTPRedirects: requestcode: %d", connection.getResponseCode()));
            if (connection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM || connection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM || connection.getResponseCode() == HttpURLConnection.HTTP_SEE_OTHER || connection.getResponseCode() == 308 || connection.getResponseCode() == 307)
            {
                if (connection.getHeaderField("Location") == null)
                {
                    return url;
                }
                return followHTTPRedirects(connection.getHeaderField("Location"), post);
            }
            else
            {
                return url;
            }
        }
        else
        {
            throw new UnsupportedSchemeException(uri.getScheme());
        }
    }

    public static void injectScriptFile(WebView view, String script)
    {
        String encoded = Base64.encodeToString(script.getBytes(), Base64.NO_WRAP);
        view.loadUrl("javascript:(function()" + "{" + "var parent = document.getElementsByTagName('head').item(0);" + "var script = document.createElement('script');" + "script.type = 'text/javascript';" + "script.innerHTML = window.atob('" + encoded + "');" + "parent.appendChild(script)" + "}) ()");
    }

    public static File getChrootDir()
    {
        return new File(Library.getResDir(), "chroot");
    }

    public static boolean hasRoot()
    {
        String[] paths = {"/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths)
        {
            if (new File(path).exists())
            {
                return true;
            }
        }
        return false;
    }

    public static String getRoot()
    {
        String[] paths = {"/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths)
        {
            if (new File(path).exists())
            {
                return path;
            }
        }
        return "/system/xbin/su";
    }

    public static String[] getLibMountInvocation()
    {
        if (!new File(getResDir(), "tmp").exists())
        {
            new File(getResDir(), "tmp").mkdirs();
        }
        return new String[] {getRoot(), "-c", "TERM=vt100 " + new File(Minicode.context.getApplicationInfo().nativeLibraryDir + "/libmount.so").getAbsolutePath() + " " + "/proc " + "/sys " + "/dev " + new File(getResDir(), "tmp").getAbsolutePath() + " " + getChrootDir()};
    }

    public static ArrayList<File> findManual(String name, @Nullable Integer sec)
    {
        ArrayList<File> f = new ArrayList<>();
        ArrayList<File> orig = indexManual(sec);
        for (File iter : orig)
        {
            try
            {
                if (iter.getName().matches(name + "\\.\\d+\\.formatted\\.col"))
                {
                    f.add(iter);
                }
            }
            catch (PatternSyntaxException e)
            {
            }
        }
        return f;
    }

    public static ArrayList<File> indexManual(Integer sec)
    {
        ArrayList<File> files = new ArrayList<>();
        if (sec == null || sec == 0)
        {
            for (int i = 1; i < 9; ++i)
            {
                File[] things = new File(Minicode.context.getFilesDir(), String.format("man_pages/manual/section.%d", i)).listFiles();
                for (File iter : things)
                {
                    if (!iter.getName().equalsIgnoreCase("DS_Store"))
                    {
                        files.add(iter);
                    }
                }
            }
        }
        else
        {
            File[] things = new File(Minicode.context.getFilesDir(), String.format("man_pages/manual/section.%d", sec)).listFiles();
            for (File iter : things)
            {
                if (!iter.getName().equalsIgnoreCase(".DS_Store"))
                {
                    files.add(iter);
                }
            }
        }
        return files;
    }

    public static ArrayList<Integer> indexManualSects(String name)
    {
        ArrayList<Integer> sects = new ArrayList<>();
        for (int i = 1; i < 9; ++i)
        {
            File[] things = new File(Minicode.context.getFilesDir(), String.format("man_pages/manual/section.%d", i)).listFiles();
            for (File iter : things)
            {
                try
                {
                    if (iter.getName().matches(name + "\\.\\d+\\.formatted\\.col"))
                    {
                        sects.add(i);
                        break;
                    }
                }
                catch (PatternSyntaxException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return sects;
    }

    public static String getGentoo()
    {
        switch (getArchName())
        {
            case "arm":
                return "http://distfiles.gentoo.org/releases/arm/autobuilds/20180831/stage3-armv4tl-20180831.tar.bz2";
            case "aarch64":
                return "http://distfiles.gentoo.org/releases/arm/autobuilds/20161129/stage3-armv7a-20161129.tar.bz2";
            case "x86":
                return "hhttp://distfiles.gentoo.org/releases/x86/autobuilds/20190409T214502Z/stage3-i486-20190409T214502Z.tar.xz";
            case "x86_64":
                return "http://distfiles.gentoo.org/releases/amd64/autobuilds/20190407T214502Z/stage3-amd64-20190407T214502Z.tar.xz";
            default:
                return "http://distfiles.gentoo.org/releases/arm/autobuilds/20180831/stage3-armv4tl-20180831.tar.bz2";
        }
    }

    public static <T> List<T> flattenNode(Node<T> node)
    {
        ArrayList<T> result = new ArrayList<>(Collections.singleton(node.getObject()));
        for (Node<T> node1 : node.getChildren())
        {
            result.addAll(flattenNode(node1));
        }
        return result;
    }

    public static Node<File> indexFiles(File start)
    {
        return indexFiles(start, -1, 0);
    }

    public static Node<File> indexFiles(File start, int layers)
    {
        return indexFiles(start, layers, 0);
    }

    private static Node<File> indexFiles(File start, int layers, int c)
    {
        File[] files = start.listFiles();
        Node<File> result = new Node<>(start);
        result.addAllObjects(Arrays.asList(files));
        for (File iter : files)
        {
            if (iter.isDirectory() && (c < layers) || (layers == -1))
            {
                Node<File> node = result.findNodeByObject(iter);
                if (node != null)
                {
                    node.pullFrom(indexFiles(iter, layers, ++c));
                }
            }
        }
        return result;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map)
    {
        Comparator<K> valueComparator = Ordering.natural().onResultOf(Functions.forMap(map));
        return ImmutableSortedMap.copyOf(map, valueComparator);
    }

    public static <K, V extends Comparable<? super V>> List<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map)
    {
        List<Map.Entry<K, V>> sortedEntries = new ArrayList<>(map.entrySet());
        Collections.sort(sortedEntries, (e1, e2) -> e2.getValue().compareTo(e1.getValue()));
        return sortedEntries;
    }

    public static <T> T[] filterOut(Class<T> clazz, T[] objects, FilterCallback<T> callback)
    {
        ArrayList<T> tmp = new ArrayList<>(Arrays.asList(objects));
        Iterator<T> iter = tmp.iterator();
        while (iter.hasNext())
        {
            if (callback.handle(iter.next()))
            {
                iter.remove();
            }
        }
        T[] array = (T[]) Array.newInstance(clazz, tmp.size());
        tmp.toArray(array);
        return array;
    }

    public static String readUntilLastNewlineOrStart(String text)
    {
        if (text.charAt(text.length() - 1) == NEWLINE)
        {
            text = text.substring(0, text.length() - 1);
        }
        if (text.lastIndexOf(NEWLINE) == -1)
        {
            return text;
        }
        return text.substring(text.lastIndexOf(NEWLINE) + 1);
    }

    public static int getColorForKeyword(String keyword, AdvancedLanguage language)
    {
        int color = Minicode.context.getResources().getColor(R.color.accent_device_default_700);
        if (language != C && language != CC && language != PYTHON3)
        {
            return color;
        }
        else
        {
            DefaultJFlexLexer lexer = (language == C ? new CLexer(new CharSequenceReader(keyword)) : (language == CC ? new CppLexer(new CharSequenceReader(keyword)) : new PythonLexer(new CharSequenceReader(keyword))));
            try
            {
                Token token = lexer.yylex();
                Log.d("LibraryUtils", "getColorForKeyword: result: " + token.type);
                if (token.type == TokenType.COMMENT || token.type == TokenType.COMMENT2)
                {
                    color = Minicode.context.getResources().getColor(android.R.color.darker_gray);
                }
                else if (token.type == TokenType.STRING)
                {
                    color = Minicode.context.getResources().getColor(android.R.color.tab_indicator_text);
                }
                else if (token.type == TokenType.STRING2)
                {
                    color = Minicode.context.getResources().getColor(R.color.holo_green_light);
                }
                else if (token.type == TokenType.NUMBER)
                {
                    if (keyword.matches("0(x[\\dabcdefABCDEF]+)|(b[01]+)"))
                    {
                        color = Minicode.context.getResources().getColor(R.color.accent_device_default_dark);
                    }
                    else
                    {
                        color = Minicode.context.getResources().getColor(R.color.accent_device_default_700);
                    }
                }
                else if (keyword.equals("return") && token.type == TokenType.KEYWORD)
                {
                    color = Minicode.context.getResources().getColor(R.color.holo_green_light);
                }
                else
                {
                    if (token.type == TokenType.TYPE || token.type == TokenType.TYPE2 || token.type == TokenType.TYPE3)
                    {
                        color = Minicode.context.getResources().getColor(R.color.md_green_900);
                    }
                    else if (token.type == TokenType.KEYWORD)
                    {
                        color = Minicode.context.getResources().getColor(R.color.md_orange_900);
                    }
                    else if (token.type == TokenType.KEYWORD2)
                    {
                        color = Minicode.context.getResources().getColor(R.color.gplus_color_3);
                    }
                    else if (token.type == TokenType.ERROR)
                    {
                        color = Minicode.context.getResources().getColor(R.color.md_red_600);
                    }
                    else
                    {
                        color = Minicode.context.getResources().getColor(R.color.md_deep_purple_500);
                    }
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return color;
            }
        }
        return color;
    }

    public static int getIndentLevel(String text, int indentLevel)
    {
        Log.d("LibraryUtils", "getIndentLevel: " + text);
        if (text.isEmpty())
        {
            return 0;
        }
        for (int i = 0; i < text.length(); ++i)
        {
            if (text.charAt(i) != ' ')
            {
                Log.d("LibraryUtils", "getIndentLevel: found indent");
                return Math.round((float) i / (float) indentLevel);
            }
            else if (i >= text.length() - 1)
            {
                return Math.round((float) i / (float) indentLevel);
            }
        }
        return Math.round((float) text.length() - 1 / (float) indentLevel);
    }

    public static File getCustomLibDir()
    {
        if (!new File(Minicode.context.getFilesDir(), ".res/libs").exists())
        {
            new File(Minicode.context.getFilesDir(), ".res/libs").mkdirs();
        }
        return new File(Minicode.context.getFilesDir(), ".res/libs");
    }

    public static void recursiveAddToTar(TarArchiveOutputStream outputStream, File file, String base) throws IOException
    {
        String entryName = base + file.getName();
        TarArchiveEntry tarEntry = new TarArchiveEntry(file, entryName);
        outputStream.putArchiveEntry(tarEntry);
        if (file.isFile())
        {
            IOUtils.copy(new FileInputStream(file), outputStream);
            outputStream.closeArchiveEntry();
        }
        else
        {
            outputStream.closeArchiveEntry();
            File[] children = file.listFiles();
            if (children != null)
            {
                for (File child : children)
                {
                    recursiveAddToTar(outputStream, child, entryName + "/");
                }
            }
        }
    }

    public interface FilterCallback<T>
    {
        boolean handle(T object);
    }
}