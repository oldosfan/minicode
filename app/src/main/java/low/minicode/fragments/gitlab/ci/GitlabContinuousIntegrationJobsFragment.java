/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.gitlab.ci;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.R;
import low.minicode.adapter.gitlab.ci.jobs.GitlabCIJobListAdapter;

public class GitlabContinuousIntegrationJobsFragment extends Fragment
{
    private GitLabApi api;
    private Project project;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private GitlabCIJobListAdapter adapter;

    public GitlabContinuousIntegrationJobsFragment()
    {
    }

    public GitlabContinuousIntegrationJobsFragment(Project project, GitLabApi api)
    {
        this.project = project;
        this.api = api;
    }

    @Deprecated
    public static GitlabContinuousIntegrationJobsFragment newInstance(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        GitlabContinuousIntegrationJobsFragment fragment = new GitlabContinuousIntegrationJobsFragment();
        Gson gson = new Gson();
        Bundle args = new Bundle();
        args.putString("serialized_project", gson.toJson(project));
        args.putString("serialized_merge_request", gson.toJson(mergeRequest));
        args.putString("serialized_api", gson.toJson(api));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            Gson gson = new Gson();
            project = gson.fromJson(getArguments().getString("serialized_project"), Project.class);
            api = gson.fromJson(getArguments().getString("serialized_api"), GitLabApi.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_vcsrecycler, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        recyclerView = getView().findViewById(R.id.vcs_branches_recycler);
        swipeRefreshLayout = getView().findViewById(R.id.branches_srl);
        swipeRefreshLayout.setEnabled(false);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
                (adapter = new GitlabCIJobListAdapter(api, project)).refresh();
                swipeRefreshLayout.post(() ->
                {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerView.setAdapter(adapter);
                    adapter.notifyItemRangeInserted(0, adapter.getItemCount());
                });
                swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(false));
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
    }
}
