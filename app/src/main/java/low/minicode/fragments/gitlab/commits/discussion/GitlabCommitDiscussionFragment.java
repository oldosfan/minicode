/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.gitlab.commits.discussion;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Note;
import org.gitlab4j.api.models.Project;

import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.R;
import low.minicode.adapter.gitlab.GitlabNestedDiscussionAdapter;

public class GitlabCommitDiscussionFragment extends Fragment
{
    private GitLabApi api;
    private Project project;
    private Commit commit;
    private RecyclerView recyclerView;
    private GitlabNestedDiscussionAdapter adapter;

    public GitlabCommitDiscussionFragment()
    {
    }

    public GitlabCommitDiscussionFragment(Project project, Commit commit, GitLabApi api)
    {
        this.project = project;
        this.commit = commit;
        this.api = api;
    }

    @Deprecated
    public static GitlabCommitDiscussionFragment newInstance(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        GitlabCommitDiscussionFragment fragment = new GitlabCommitDiscussionFragment();
        Gson gson = new Gson();
        Bundle args = new Bundle();
        args.putString("serialized_project", gson.toJson(project));
        args.putString("serialized_merge_request", gson.toJson(mergeRequest));
        args.putString("serialized_api", gson.toJson(api));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            Gson gson = new Gson();
            project = gson.fromJson(getArguments().getString("serialized_project"), Project.class);
            commit = gson.fromJson(getArguments().getString("serialized_commit"), Commit.class);
            api = gson.fromJson(getArguments().getString("serialized_api"), GitLabApi.class);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add(R.string.new_note);
        menu.getItem(0).setIcon(R.drawable.ic_edit_black_24dp);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            menu.getItem(0).setIconTintList(getResources().getColorStateList(R.color.accent_device_default_light));
        }
        else
        {
            menu.getItem(0).setIcon(menu.getItem(0).getIcon().getConstantState().newDrawable().mutate());
            menu.getItem(0).getIcon().setTint(getResources().getColor(R.color.accent_device_default_light));
        }
        menu.getItem(0).setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_gitlab_commit_discussion, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        setHasOptionsMenu(true);
        recyclerView = getView().findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                List<Discussion> discussions = api.getDiscussionsApi().getCommitDiscussions(project, commit.getId());
                recyclerView.post(() -> recyclerView.setAdapter(adapter = new GitlabNestedDiscussionAdapter(api, project, discussions)));
                recyclerView.post(() ->
                {
                    adapter.setHandler(new GitlabNestedDiscussionAdapter.ReplyDeleteHandler()
                    {
                        @Override
                        public void reply(Note note, Context c, String newContent)
                        {
                            try
                            {
                                api.getDiscussionsApi().addCommitDiscussionNote(project, commit.getId(), discussions.get(0).getId(), newContent, null);
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void delete(Note note, Context c)
                        {
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                // TODO add note deletion functionality
                            });
                        }

                        @Override
                        public void edit(Note note, Context c, String newContent)
                        {
                            // TODO add note edit functionality
                        }
                    });
                });
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
    }
}
