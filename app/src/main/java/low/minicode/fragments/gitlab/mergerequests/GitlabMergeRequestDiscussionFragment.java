/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.gitlab.mergerequests;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Note;
import org.gitlab4j.api.models.Project;

import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.adapter.gitlab.GitlabNestedDiscussionAdapter;

public class GitlabMergeRequestDiscussionFragment extends Fragment
{
    private GitLabApi api;
    private Project project;
    private MergeRequest mergeRequest;
    private RecyclerView discussionRecycler;
    private GitlabNestedDiscussionAdapter adapter;
    private FloatingActionButton newDiscussion;

    public GitlabMergeRequestDiscussionFragment()
    {
    }

    public GitlabMergeRequestDiscussionFragment(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        this.project = project;
        this.mergeRequest = mergeRequest;
        this.api = api;
    }

    @Deprecated
    public static GitlabMergeRequestDiscussionFragment newInstance(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        GitlabMergeRequestDiscussionFragment fragment = new GitlabMergeRequestDiscussionFragment();
        Gson gson = new Gson();
        Bundle args = new Bundle();
        args.putString("serialized_project", gson.toJson(project));
        args.putString("serialized_merge_request", gson.toJson(mergeRequest));
        args.putString("serialized_api", gson.toJson(api));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            Gson gson = new Gson();
            project = gson.fromJson(getArguments().getString("serialized_project"), Project.class);
            mergeRequest = gson.fromJson(getArguments().getString("serialized_merge_request"), MergeRequest.class);
            api = gson.fromJson(getArguments().getString("serialized_api"), GitLabApi.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_gitlab_merge_request_discussion, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        newDiscussion = getView().findViewById(R.id.new_discussion);
        newDiscussion.setOnClickListener(v ->
        {
            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(v.getContext());
            materialAlertDialogBuilder.setTitle(R.string._new);
            EditText editText = new EditText(this.getActivity());
            materialAlertDialogBuilder.setView(editText);
            materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        api.getNotesApi().createMergeRequestNote(project, mergeRequest.getIid(), editText.getText().toString());
                        List<Discussion> discussionList = api.getDiscussionsApi().getMergeRequestDiscussions(project, mergeRequest.getIid());
                        adapter = new GitlabNestedDiscussionAdapter(api, project, discussionList);
                        getActivity().runOnUiThread(() ->
                        {
                            discussionRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        });
                    }
                    catch (GitLabApiException e)
                    {
                        Library.vibrateError();
                        getView().post(() -> Library.displayErrorDialogFromThrowable(this.getActivity(), e, false));
                        e.printStackTrace();
                    }
                });
            });
            materialAlertDialogBuilder.show();
        });
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                List<Discussion> discussionList = api.getDiscussionsApi().getMergeRequestDiscussions(project, mergeRequest.getIid());
                adapter = new GitlabNestedDiscussionAdapter(api, project, discussionList);
                adapter.setHandler(new GitlabNestedDiscussionAdapter.ReplyDeleteHandler()
                {
                    @Override
                    public void reply(Note note, Context c, String newContent)
                    {
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            try
                            {
                                api.getNotesApi().createMergeRequestNote(project, mergeRequest.getIid(), newContent);
                                List<Discussion> discussionList = api.getDiscussionsApi().getMergeRequestDiscussions(project, mergeRequest.getIid());
                                adapter = new GitlabNestedDiscussionAdapter(api, project, discussionList);
                                getActivity().runOnUiThread(() ->
                                {
                                    discussionRecycler.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                });
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void delete(Note note, Context c)
                    {
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            try
                            {
                                api.getNotesApi().deleteIssueNote(project, mergeRequest.getIid(), note.getId());
                                List<Discussion> discussionList = api.getDiscussionsApi().getMergeRequestDiscussions(project, mergeRequest.getIid());
                                adapter = new GitlabNestedDiscussionAdapter(api, project, discussionList);
                                getActivity().runOnUiThread(() ->
                                {
                                    discussionRecycler.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                });
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void edit(Note note, Context c, String newContent)
                    {
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            try
                            {
                                api.getNotesApi().updateMergeRequestNote(project, mergeRequest.getIid(), note.getId(), newContent);
                                List<Discussion> discussionList = api.getDiscussionsApi().getMergeRequestDiscussions(project, mergeRequest.getIid());
                                adapter = new GitlabNestedDiscussionAdapter(api, project, discussionList);
                                getActivity().runOnUiThread(() ->
                                {
                                    discussionRecycler.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                });
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                        });
                    }
                });
                getActivity().runOnUiThread(() ->
                {
                    discussionRecycler = getView().findViewById(R.id.fragment_discussions_recyclerview);
                    discussionRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
                    discussionRecycler.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                });
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
    }
}
