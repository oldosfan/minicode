/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.gitlab.mergerequests;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.R;
import low.minicode.activity.gitlab.projects.GitlabProjectSummaryActivity;
import ru.noties.markwon.Markwon;
import ru.noties.markwon.core.CorePlugin;
import ru.noties.markwon.image.ImagesPlugin;

public class GitlabMergeRequestSummaryFragment extends Fragment
{
    private GitLabApi api;
    private Project project;
    private MergeRequest mergeRequest;
    private TextView title, author, lastUpdated, description, status, sourceAndTarget;
    private MaterialButton sourceBranch, targetBranch, merge;

    public GitlabMergeRequestSummaryFragment()
    {
    }

    public GitlabMergeRequestSummaryFragment(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        this.project = project;
        this.mergeRequest = mergeRequest;
        this.api = api;
    }

    @Deprecated
    public static GitlabMergeRequestSummaryFragment newInstance(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        GitlabMergeRequestSummaryFragment fragment = new GitlabMergeRequestSummaryFragment();
        Gson gson = new Gson();
        Bundle args = new Bundle();
        args.putString("serialized_project", gson.toJson(project));
        args.putString("serialized_merge_request", gson.toJson(mergeRequest));
        args.putString("serialized_api", gson.toJson(api));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            Gson gson = new Gson();
            project = gson.fromJson(getArguments().getString("serialized_project"), Project.class);
            mergeRequest = gson.fromJson(getArguments().getString("serialized_merge_request"), MergeRequest.class);
            api = gson.fromJson(getArguments().getString("serialized_api"), GitLabApi.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_gitlab_merge_request_summary, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        title = getView().findViewById(R.id.mr_title);
        lastUpdated = getView().findViewById(R.id.mr_date);
        author = getView().findViewById(R.id.mr_author);
        status = getView().findViewById(R.id.mr_status);
        description = getView().findViewById(R.id.merge_request_description);
        sourceAndTarget = getView().findViewById(R.id.source_target);
        sourceBranch = getView().findViewById(R.id.show_source_branch);
        targetBranch = getView().findViewById(R.id.show_target_branch);
        merge = getView().findViewById(R.id.mr_merge);
        merge.setOnClickListener(v ->
        {
            // TODO add merge functionality
        });
        sourceBranch.setEnabled(false);
        targetBranch.setOnClickListener(v ->
        {
            Intent i = new Intent(this.getContext(), GitlabProjectSummaryActivity.class);
            i.putExtra("project", project.getPathWithNamespace());
            i.putExtra("apiKey", api.getAuthToken());
            i.putExtra("branch", mergeRequest.getTargetBranch());
            v.getContext().startActivity(i);
        });
        sourceAndTarget.setText(getString(R.string.s_arrow_s, mergeRequest.getSourceBranch(), mergeRequest.getTargetBranch()));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                Project project = api.getProjectApi().getProject(mergeRequest.getSourceProjectId());
                if (api.getRepositoryApi().getBranch(project, mergeRequest.getSourceBranch()) != null)
                {
                    sourceBranch.post(() ->
                    {
                        sourceBranch.setEnabled(true);
                        sourceBranch.setOnClickListener(v ->
                        {
                            Intent i = new Intent(this.getContext(), GitlabProjectSummaryActivity.class);
                            i.putExtra("project", project.getPathWithNamespace());
                            i.putExtra("apiKey", api.getAuthToken());
                            i.putExtra("branch", mergeRequest.getSourceBranch());
                            v.getContext().startActivity(i);
                        });
                    });
                }
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
        title.setText(mergeRequest.getTitle());
        lastUpdated.setText(getString(R.string.last_updated_s, mergeRequest.getUpdatedAt().toString()));
        author.setText(getString(R.string.author_s, mergeRequest.getAuthor().getName()));
        status.setText(mergeRequest.getState().contains("pen") ? R.string.open : R.string.clone);
        status.setTextColor(mergeRequest.getState().contains("pen") ? getResources().getColor(R.color.md_green_700) : getResources().getColor(R.color.md_red_700));
        Markwon.builder(this.getContext()).usePlugin(CorePlugin.create()).usePlugin(ImagesPlugin.create(this.getContext())).build().setMarkdown(description, mergeRequest.getDescription());
    }
}
