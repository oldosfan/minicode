/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.gitlab.commits.summary;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.adapter.gitlab.commits.details.GitlabCommitChangeDetailsListAdapter;

public class GitlabCommitSummaryFragment extends Fragment
{
    private GitLabApi api;
    private Project project;
    private Commit commit;
    private RecyclerView recyclerView;
    private GitlabCommitChangeDetailsListAdapter adapter;

    public GitlabCommitSummaryFragment()
    {
    }

    public GitlabCommitSummaryFragment(Project project, Commit commit, GitLabApi api)
    {
        this.project = project;
        this.commit = commit;
        this.api = api;
    }

    @Deprecated
    public static GitlabCommitSummaryFragment newInstance(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        GitlabCommitSummaryFragment fragment = new GitlabCommitSummaryFragment();
        Gson gson = new Gson();
        Bundle args = new Bundle();
        args.putString("serialized_project", gson.toJson(project));
        args.putString("serialized_merge_request", gson.toJson(mergeRequest));
        args.putString("serialized_api", gson.toJson(api));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            Gson gson = new Gson();
            project = gson.fromJson(getArguments().getString("serialized_project"), Project.class);
            commit = gson.fromJson(getArguments().getString("serialized_commit"), Commit.class);
            api = gson.fromJson(getArguments().getString("serialized_api"), GitLabApi.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_gitlab_commit_summary, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        recyclerView = getView().findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        adapter = new GitlabCommitChangeDetailsListAdapter(api, project, commit);
        recyclerView.setAdapter(adapter);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                Log.d(getClass().getName(), "onStart: refreshing commit details adapter content");
                adapter.refresh();
                Log.d(getClass().getName(), "onStart: commit details adapter refreshed! notifying adapter of content change");
                recyclerView.post(adapter::notifyDataSetChanged);

            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
                getView().post(() -> Library.displayErrorDialogFromThrowable(getActivity(), e, true, requireActivity()::finish));
            }
        });
    }
}
