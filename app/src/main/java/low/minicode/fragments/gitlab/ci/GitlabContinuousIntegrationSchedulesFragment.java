/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.gitlab.ci;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.PipelineSchedule;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.adapter.gitlab.ci.GitlabCISchedulesAdapter;
import low.minicode.wrappers.gitlab.branch.BranchStringWrapper;

public class GitlabContinuousIntegrationSchedulesFragment extends Fragment
{
    private GitLabApi api;
    private Project project;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private GitlabCISchedulesAdapter adapter;

    public GitlabContinuousIntegrationSchedulesFragment()
    {
    }

    public GitlabContinuousIntegrationSchedulesFragment(Project project, GitLabApi api)
    {
        this.project = project;
        this.api = api;
    }

    @Deprecated
    public static GitlabContinuousIntegrationSchedulesFragment newInstance(Project project, MergeRequest mergeRequest, GitLabApi api)
    {
        GitlabContinuousIntegrationSchedulesFragment fragment = new GitlabContinuousIntegrationSchedulesFragment();
        Gson gson = new Gson();
        Bundle args = new Bundle();
        args.putString("serialized_project", gson.toJson(project));
        args.putString("serialized_merge_request", gson.toJson(mergeRequest));
        args.putString("serialized_api", gson.toJson(api));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            Gson gson = new Gson();
            project = gson.fromJson(getArguments().getString("serialized_project"), Project.class);
            api = gson.fromJson(getArguments().getString("serialized_api"), GitLabApi.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_vcsrecycler, container, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        menu.clear();
        requireActivity().getMenuInflater().inflate(R.menu.new_key, menu);
        menu.getItem(0).setOnMenuItemClickListener(menuItem ->
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this.getActivity());
            View v = getLayoutInflater().inflate(R.layout._dialog_new_cischedule, null, false);
            builder.setView(v);
            EditText crontab, title;
            RadioGroup crontype;
            Spinner refs = v.findViewById(R.id.schedule_ref);
            crontab = v.findViewById(R.id.schedule_crontab);
            title = v.findViewById(R.id.schedule_title);
            crontype = v.findViewById(R.id.cron_type);
            crontab.setTypeface(Typeface.MONOSPACE);
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    Log.d(getClass().getName(), "onCreateOptionsMenu: loading available pipeline targets");
                    List<Branch> branches = api.getRepositoryApi().getBranches(project);
                    Log.d(getClass().getName(), "onCreateOptionsMenu: loading default pipeline target");
                    Branch defaultBranch = api.getRepositoryApi().getBranch(project, project.getDefaultBranch());
                    Log.d(getClass().getName(), "onCreateOptionsMenu: locating the position of the default target in the list of available targets");
                    int defaultBranchIndex = -1;
                    int i = 0;
                    for (Branch branch : branches)
                    {
                        if (branch.getName().equals(defaultBranch.getName()))
                        {
                            defaultBranchIndex = i;
                            break;
                        }
                        ++i;
                    }
                    if (defaultBranchIndex == -1)
                    {
                        Log.wtf(getClass().getName(), "onCreateOptionsMenu: the default branch isn't included in the branches list! frightsome magic happens");
                        throw new RuntimeException("frightsome magic happens: the default branch wasn't found in the branches list");
                    }
                    List<BranchStringWrapper> stringWrappers = new ArrayList<>();
                    for (Branch branch : branches)
                    {
                        stringWrappers.add(new BranchStringWrapper(branch));
                    }
                    int finalDefaultBranchIndex = defaultBranchIndex;
                    requireActivity().runOnUiThread(() ->
                    {
                        refs.setAdapter(new ArrayAdapter<>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, stringWrappers));
                        refs.setSelection(finalDefaultBranchIndex);
                    });
                }
                catch (GitLabApiException e)
                {
                    e.printStackTrace();
                }
            });
            ((MaterialRadioButton) crontype.findViewById(R.id.custom)).setChecked(true);
            crontype.setOnCheckedChangeListener((group, checkedId) ->
            {
                switch (checkedId)
                {
                    case R.id.custom:
                        crontab.setEnabled(true);
                        break;
                    case R.id.day:
                        crontab.setEnabled(false);
                        crontab.setText("0 4 * * *");
                        break;
                    case R.id.week:
                        crontab.setEnabled(false);
                        crontab.setText("0 4 * * 0");
                        break;
                    case R.id.month:
                        crontab.setEnabled(false);
                        crontab.setText("0 4 1 * *");
                        break;

                }
            });
            builder.setPositiveButton(R.string.ok, null);
            AlertDialog d = builder.create();
            d.setOnShowListener(dialog ->
            {
                d.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v2 ->
                {
                    if (title.getText().toString().trim().isEmpty())
                    {
                        Library.vibrateError();
                        Library.emphasizeBlinkView(title);
                    }
                    else if (crontab.getText().toString().trim().isEmpty())
                    {
                        Library.vibrateError();
                        Library.emphasizeBlinkView(crontab);
                    }
                    else if (!Library.isCrontabValid(crontab.getText().toString().trim()))
                    {
                        Library.vibrateError();
                        Library.emphasizeBlinkView(crontab);
                        Snackbar.make(v2, R.string.invalid_crontab, Snackbar.LENGTH_LONG).show();
                    }
                    else if (refs.getSelectedItem() == null)
                    {
                        Library.vibrateError();
                        Library.emphasizeBlinkView(refs);
                        Snackbar.make(v2, R.string.no_target, Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        d.dismiss();
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            PipelineSchedule schedule = new PipelineSchedule();
                            schedule.setCron(crontab.getText().toString());
                            schedule.setDescription(title.getText().toString());
                            schedule.setCronTimezone("UTC");
                            schedule.setRef(refs.getSelectedItem().toString());
                            try
                            {
                                api.getPipelineApi().createPipelineSchedule(project, schedule);
                                swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
                                adapter.refresh();
                                swipeRefreshLayout.post(() ->
                                {
                                    adapter.notifyDataSetChanged();
                                    swipeRefreshLayout.setRefreshing(false);
                                });
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                                requireActivity().runOnUiThread(() -> Library.displayErrorDialogFromThrowable(requireActivity(), e, true));
                            }
                        });
                    }
                });
            });
            d.show();
            return true;
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        setHasOptionsMenu(true);
        recyclerView = getView().findViewById(R.id.vcs_branches_recycler);
        swipeRefreshLayout = getView().findViewById(R.id.branches_srl);
        swipeRefreshLayout.setEnabled(false);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
                (adapter = new GitlabCISchedulesAdapter(api, project)).refresh();
                swipeRefreshLayout.post(() ->
                {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerView.setAdapter(adapter);
                    adapter.notifyItemRangeInserted(0, adapter.getItemCount());
                });
                swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(false));
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
    }
}
