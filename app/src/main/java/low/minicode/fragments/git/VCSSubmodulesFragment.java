/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.git;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;

import java.io.File;
import java.io.IOException;

import low.minicode.R;
import low.minicode.adapter.git.SubmoduleListAdapter;

public class VCSSubmodulesFragment extends Fragment
{
    private String mProjectName;
    private String mUnused;
    private Repository mRepository;
    private RecyclerView recyclerView;
    private SubmoduleListAdapter tagListAdapter;

    public VCSSubmodulesFragment()
    {
        this.setRetainInstance(true);
    }

    public static VCSSubmodulesFragment newInstance(String param1, String param2)
    {
        VCSSubmodulesFragment fragment = new VCSSubmodulesFragment();
        Bundle args = new Bundle();
        args.putString("projectName", param1);
        args.putString("unused", param2);
        fragment.setArguments(args);
        fragment.mProjectName = param1;
        fragment.setRetainInstance(true);
        return fragment;
    }

    public String getProjectName()
    {
        return mProjectName;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        if (getRepository() == null)
        {
            return;
        }
        recyclerView = getView().findViewById(R.id.vcs_branches_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(80);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        tagListAdapter = new SubmoduleListAdapter(getRepository(), this);
        recyclerView.setAdapter(tagListAdapter);
        getView().findViewById(R.id.branches_srl).setEnabled(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_vcssubmodules, container, false);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        tagListAdapter.changeData();
        tagListAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity context)
    {
        super.onAttach(context);
        if (getArguments() != null)
        {
            mProjectName = getArguments().getString("projectName");
        }
        try
        {
            Log.d(getClass().getName(), String.format("onAttach: init repo %s", mProjectName));
            mRepository = new RepositoryBuilder().setGitDir(new File(context.getFilesDir(), mProjectName + "/.git")).readEnvironment().setMustExist(true).findGitDir().build();
            Log.d(getClass().getName(), "onAttach: init repo success");

        }
        catch (IOException e)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage(R.string.git_init_failed);
            builder.setPositiveButton(R.string.ok, ((dialog, which) -> context.finish()));
            builder.show();
            Log.d(getClass().getName(), "onAttach: init repo fail");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public Repository getRepository()
    {
        return mRepository;
    }

    public void setRepository(Repository mRepository)
    {
        this.mRepository = mRepository;
    }
}
