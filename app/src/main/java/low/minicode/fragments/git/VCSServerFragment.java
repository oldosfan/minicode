/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.git;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.transport.Daemon;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.application.Minicode;
import low.minicode.global.server.registry.ServerRegistry;
import low.minicode.wrappers.server.FTPServer;
import low.minicode.wrappers.server.GitServer;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.google.android.material.snackbar.Snackbar.LENGTH_LONG;

public class VCSServerFragment extends Fragment
{
    private String mProjectName;
    private Repository mRepository;
    private SwitchCompat gitServer, ftpServer;
    private TextView ftpState, gitState;
    private TextInputEditText password;

    public VCSServerFragment()
    {
        this.setRetainInstance(true);
    }

    public static VCSServerFragment newInstance(String param1, String param2)
    {
        VCSServerFragment fragment = new VCSServerFragment();
        Bundle args = new Bundle();
        args.putString("projectName", param1);
        args.putString("unused", param2);
        fragment.setArguments(args);
        fragment.mProjectName = param1;
        fragment.setRetainInstance(false);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint({"ClickableViewAccessibility", "ApplySharedPref"})
    @Override
    public void onStart()
    {
        super.onStart();
        if (getRepository() == null)
        {
            return;
        }
        ftpServer = getView().findViewById(R.id.start_ftp_server);
        gitServer = getView().findViewById(R.id.start_git_server);
        ftpState = getView().findViewById(R.id.ftp_server_status);
        gitState = getView().findViewById(R.id.git_server_status);
        password = getView().findViewById(R.id.ftp_server_password);
        ftpState.setVisibility(((ServerRegistry.ftpRegistry.get(mProjectName) == null) || ServerRegistry.ftpRegistry.get(mProjectName).getFtpServer().isStopped()) ? GONE : VISIBLE);
        gitState.setVisibility(((ServerRegistry.gitRegistry.get(mProjectName) == null) || !ServerRegistry.gitRegistry.get(mProjectName).getDaemon().isRunning()) ? GONE : VISIBLE);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            if (ServerRegistry.ftpRegistry.get(mProjectName) != null)
            {
                ftpState.post(() -> ftpState.setText(getString(R.string.ftp_server_is_accessible, String.format("ftp://%s:%d", Library.getLocalIPAddress() == null ? "unknown" : Library.getLocalIPAddress(), ServerRegistry.ftpRegistry.get(mProjectName).getPort()))));
            }
            if (ServerRegistry.gitRegistry.get(mProjectName) != null)
            {
                gitState.post(() -> gitState.setText(getString(R.string.git_server_accessible, String.format("git://%s:%d/%s", Library.getLocalIPAddress() == null ? "unknown" : Library.getLocalIPAddress(), ServerRegistry.gitRegistry.get(mProjectName).getPort(), mProjectName))));
            }
        });
        ftpServer.setChecked(ServerRegistry.ftpRegistry.get(mProjectName) != null && !ServerRegistry.ftpRegistry.get(mProjectName).getFtpServer().isStopped());
        gitServer.setChecked(ServerRegistry.gitRegistry.get(mProjectName) != null && ServerRegistry.gitRegistry.get(mProjectName).getDaemon().isRunning());
        ftpServer.setOnCheckedChangeListener(((buttonView, isChecked) ->
        {
            if (!isChecked)
            {
                if (ServerRegistry.ftpRegistry.get(mProjectName) != null)
                {
                    ServerRegistry.ftpRegistry.get(mProjectName).getFtpServer().stop();
                    ServerRegistry.ftpRegistry.remove(mProjectName);
                }
            }
            else
            {
                if (password.getText().toString().isEmpty())
                {
                    Library.vibrateError();
                    Snackbar.make(password, R.string.you_need_to_set_a_password, LENGTH_LONG).show();
                    ftpServer.setChecked(false);
                }
                else
                {
                    int port = Library.getAvaliablePort(Library.range(34546, 49813));
                    if (port == -1)
                    {
                        Snackbar.make(password, R.string.no_port, LENGTH_LONG).show();
                        return;
                    }
                    FTPServer server = ServerRegistry.ftpRegistry.put(mProjectName, new FTPServer(mProjectName, password.getText().toString(), port, Minicode.context));
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        ftpState.post(() -> ftpState.setText(getString(R.string.ftp_server_is_accessible, String.format("ftp://%s:%d", Library.getLocalIPAddress() == null ? "unknown" : Library.getLocalIPAddress(), port))));
                    });
                }
            }
            ftpState.setVisibility(((ServerRegistry.ftpRegistry.get(mProjectName) == null) || ServerRegistry.ftpRegistry.get(mProjectName).getFtpServer().isStopped()) ? GONE : VISIBLE);
        }));
        gitServer.setOnCheckedChangeListener(((buttonView, isChecked) ->
        {
            if (!isChecked)
            {
                if (ServerRegistry.gitRegistry.get(mProjectName) != null)
                {
                    ServerRegistry.gitRegistry.get(mProjectName).getDaemon().stop();
                    ServerRegistry.gitRegistry.remove(mProjectName);
                }
            }
            else
            {
                int port = Library.getAvaliablePort(Library.range(Daemon.DEFAULT_PORT, 25961));
                if (port == -1)
                {
                    Snackbar.make(password, R.string.no_port, LENGTH_LONG).show();
                    return;
                }
                GitServer server = ServerRegistry.gitRegistry.put(mProjectName, new GitServer(mProjectName, port, Minicode.context));
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    gitState.post(() -> gitState.setText(getString(R.string.git_server_accessible, String.format("git://%s:%d/%s", Library.getLocalIPAddress() == null ? "unknown" : Library.getLocalIPAddress(), port, mProjectName))));
                });
            }
            gitState.setVisibility(((ServerRegistry.gitRegistry.get(mProjectName) == null) || !ServerRegistry.gitRegistry.get(mProjectName).getDaemon().isRunning()) ? GONE : VISIBLE);
        }));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_vcshttpserver, container, false);
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity context)
    {
        super.onAttach(context);
        if (getArguments() != null)
        {
            mProjectName = getArguments().getString("projectName");
        }
        try
        {
            Log.d(getClass().getName(), String.format("onAttach: init repo %s", mProjectName));
            mRepository = new RepositoryBuilder().setGitDir(new File(context.getFilesDir(), mProjectName + "/.git")).readEnvironment().setMustExist(true).findGitDir().build();
            Log.d(getClass().getName(), "onAttach: init repo success");

        }
        catch (IOException e)
        {
            e.printStackTrace();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage(R.string.git_init_failed);
            builder.setPositiveButton(R.string.ok, ((dialog, which) -> context.finish()));
            builder.show();
            Log.d(getClass().getName(), "onAttach: init repo fail");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public Repository getRepository()
    {
        return mRepository;
    }

    public void setRepository(Repository mRepository)
    {
        this.mRepository = mRepository;
    }
}
