/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.git;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.StashApplyCommand;
import org.eclipse.jgit.api.StashCreateCommand;
import org.eclipse.jgit.api.StashListCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.R;
import low.minicode.adapter.git.UncommitedChangesAdapter;

public class VCSChangesFragment extends Fragment
{
    private String mProjectName;
    private String mUnused;
    private Repository mRepository;
    private RecyclerView recyclerView;
    private ImageButton mCommit, mDiscard, mStash, mUnstash;
    private UncommitedChangesAdapter uncommitedChangesAdapter;

    public VCSChangesFragment()
    {
        this.setRetainInstance(true);
    }

    public static VCSChangesFragment newInstance(String param1, String param2)
    {
        VCSChangesFragment fragment = new VCSChangesFragment();
        Bundle args = new Bundle();
        args.putString("projectName", param1);
        args.putString("unused", param2);
        fragment.setArguments(args);
        fragment.mProjectName = param1;
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        if (getRepository() == null)
        {
            return;
        }
        mCommit = getView().findViewById(R.id.commit_now);
        mDiscard = getView().findViewById(R.id.discard_now);
        mStash = getView().findViewById(R.id.stash_now);
        mUnstash = getView().findViewById(R.id.unstash_now);
        recyclerView = getView().findViewById(R.id.vcs_branches_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(80);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        uncommitedChangesAdapter = new UncommitedChangesAdapter(new Git(getRepository()), this);
        recyclerView.setAdapter(uncommitedChangesAdapter);
        getView().findViewById(R.id.branches_srl).setEnabled(false);
        mStash.setOnClickListener(v ->
        {
            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(getActivity());
            materialAlertDialogBuilder.setMessage(R.string.stashing_changes);
            materialAlertDialogBuilder.setCancelable(false);
            Dialog d = materialAlertDialogBuilder.show();
            Executors.newSingleThreadExecutor().submit(() ->
            {
                Git git = new Git(getRepository());
                StashCreateCommand stashCreateCommand = git.stashCreate();
                try
                {
                    stashCreateCommand.setIncludeUntracked(true);
                    stashCreateCommand.setIndexMessage("miniCode() stash " + uncommitedChangesAdapter.getItemCount());
                    stashCreateCommand.call();
                    getActivity().runOnUiThread(uncommitedChangesAdapter::notifyDatasetChangedA);
                    getActivity().runOnUiThread(d::dismiss);

                }
                catch (GitAPIException | JGitInternalException e)
                {
                    getActivity().runOnUiThread(d::dismiss);
                    e.printStackTrace();
                    MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(getActivity());
                    error.setTitle(R.string.error);
                    error.setMessage(e.getLocalizedMessage());
                    error.setPositiveButton(R.string.ok, (dialog, which) ->
                    {
                    });
                    getActivity().runOnUiThread(error::show);
                }
            });
        });
        mUnstash.setOnClickListener(v ->
        {
            StashListCommand stashListCommand = new StashListCommand(getRepository());
            List<RevCommit> stashList;
            try
            {
                stashList = new ArrayList<>(stashListCommand.call());
            }
            catch (GitAPIException e)
            {
                e.printStackTrace();
                return;
            }
            ArrayList<String> names = new ArrayList<>();
            for (RevCommit iter : stashList)
            {
                names.add(iter.getFullMessage());
            }
            MaterialAlertDialogBuilder selectStashes = new MaterialAlertDialogBuilder(getActivity());
            selectStashes.setItems(Arrays.copyOf(names.toArray(), names.size(), String[].class), (dialog, which) ->
            {
                MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(getActivity());
                materialAlertDialogBuilder.setMessage(R.string.applying_stashed_changes);
                materialAlertDialogBuilder.setCancelable(false);
                Dialog d = materialAlertDialogBuilder.show();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    Git git = new Git(getRepository());
                    StashApplyCommand stashApplyCommand = git.stashApply();
                    try
                    {
                        stashApplyCommand.setApplyUntracked(true);
                        stashApplyCommand.setStashRef(stashList.get(which).getName());
                        stashApplyCommand.call();
                        git.stashDrop().setStashRef(which).call();
                        getActivity().runOnUiThread(d::dismiss);
                        getActivity().runOnUiThread(uncommitedChangesAdapter::notifyDatasetChangedA);

                    }
                    catch (GitAPIException | JGitInternalException e)
                    {
                        getActivity().runOnUiThread(d::dismiss);
                        e.printStackTrace();
                        MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(getActivity());
                        error.setTitle(R.string.error);
                        error.setMessage(e.getLocalizedMessage());
                        error.setPositiveButton(R.string.ok, (dialog2, which2) ->
                        {
                        });
                        getActivity().runOnUiThread(error::show);
                    }
                });
            });
            selectStashes.show();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_vcschanges, container, false);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        uncommitedChangesAdapter.notifyDatasetChangedA();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity context)
    {
        super.onAttach(context);
        if (getArguments() != null)
        {
            mProjectName = getArguments().getString("projectName");
        }
        try
        {
            Log.d(getClass().getName(), String.format("onAttach: init repo %s", mProjectName));
            mRepository = new RepositoryBuilder().setGitDir(new File(context.getFilesDir(), mProjectName + "/.git")).readEnvironment().setMustExist(true).findGitDir().build();
            Log.d(getClass().getName(), "onAttach: init repo success");

        }
        catch (IOException e)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage(R.string.git_init_failed);
            builder.setPositiveButton(R.string.ok, ((dialog, which) -> context.finish()));
            builder.show();
            Log.d(getClass().getName(), "onAttach: init repo fail");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public Repository getRepository()
    {
        return mRepository;
    }

    public void setRepository(Repository mRepository)
    {
        this.mRepository = mRepository;
    }

    public ImageButton getmCommit()
    {
        return mCommit;
    }

    public ImageButton getmDiscard()
    {
        return mDiscard;
    }
}
