/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.fragments.git;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.jcraft.jsch.JSchException;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.lib.StoredConfig;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.wrappers.data.StrongReference;

public class VCSSettingsFragment extends Fragment
{
    public static final int PASSWORD = 0;
    public static final int SSH = 1;
    public static final int NONE = 2;
    private static final Object SAVE_LOCK = new Object();
    private String mProjectName;
    private String mUnused;
    private Repository mRepository;
    private TextInputEditText sshPub, sshPriv, pass, user, email, name, origin;
    private Button newKey;
    private RadioGroup radioGroup;

    public VCSSettingsFragment()
    {
        this.setRetainInstance(true);
    }

    public static VCSSettingsFragment newInstance(String param1, String param2)
    {
        VCSSettingsFragment fragment = new VCSSettingsFragment();
        Bundle args = new Bundle();
        args.putString("projectName", param1);
        args.putString("unused", param2);
        fragment.setArguments(args);
        fragment.mProjectName = param1;
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint({"ClickableViewAccessibility", "ApplySharedPref"})
    @Override
    public void onStart()
    {
        super.onStart();
        if (getRepository() == null)
        {
            return;
        }
        StrongReference<String> pub = new StrongReference<>(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_public_key", null));
        StrongReference<String> priv = new StrongReference<>(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_private_key", null));
        if (pub.get() == null || priv.get() == null)
        {
            try
            {
                Library.generateRSAKeyPair().dispose();
                pub.set(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_public_key", null));
                priv.set(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_private_key", null));
            }
            catch (JSchException e)
            {
                e.printStackTrace();
            }
        }
        sshPub = getView().findViewById(R.id.ssh_public_key);
        sshPriv = getView().findViewById(R.id.ssh_private_key);
        pass = getView().findViewById(R.id.password);
        user = getView().findViewById(R.id.username);
        email = getView().findViewById(R.id.email);
        name = getView().findViewById(R.id.name);
        origin = getView().findViewById(R.id.origin);
        newKey = getView().findViewById(R.id.new_key);
        user.setText(Minicode.context.getSharedPreferences("low.minicode.preferences." + mProjectName.hashCode(), Context.MODE_PRIVATE).getString("username", ""));
        pass.setText(Minicode.context.getSharedPreferences("low.minicode.preferences." + mProjectName.hashCode(), Context.MODE_PRIVATE).getString("password", ""));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            email.post(() -> email.setText(getRepository().getConfig().getString("user", null, "email")));
            name.post(() -> name.setText(getRepository().getConfig().getString("user", null, "name")));
            origin.post(() -> origin.setText(getRepository().getConfig().getString("remote", "origin", "url")));
        });
        origin.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    synchronized (SAVE_LOCK)
                    {
                        StoredConfig c = getRepository().getConfig();
                        c.setString("remote", "origin", "url", s.toString());
                        try
                        {
                            c.save();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        email.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    synchronized (SAVE_LOCK)
                    {
                        StoredConfig c = getRepository().getConfig();
                        c.setString("user", null, "email", s.toString());
                        try
                        {
                            c.save();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        name.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    synchronized (SAVE_LOCK)
                    {
                        StoredConfig c = getRepository().getConfig();
                        c.setString("user", null, "name", s.toString());
                        try
                        {
                            c.save();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        pass.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                Minicode.context.getSharedPreferences("low.minicode.preferences." + mProjectName.hashCode(), Context.MODE_PRIVATE).edit().putString("password", s.toString()).commit();
            }
        });
        user.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                Minicode.context.getSharedPreferences("low.minicode.preferences." + mProjectName.hashCode(), Context.MODE_PRIVATE).edit().putString("username", s.toString()).commit();
            }
        });
        sshPub.setText(pub.get());
        sshPub.setOnFocusChangeListener((v, val) ->
        {
            if (!val)
            {
                return;
            }
            ClipboardManager clipboard = (ClipboardManager) Minicode.context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(pub.get());
            Toast.makeText(Minicode.context, R.string.copied_to_clipboard, Toast.LENGTH_LONG).show();
        });
        sshPriv.setText(priv.get());
        radioGroup = getView().findViewById(R.id.auth_type);
        int auth_type = Minicode.context.getSharedPreferences("low.minicode.preferences." + mProjectName.hashCode(), Context.MODE_PRIVATE).getInt("auth_type", NONE);
        radioGroup.check(Library.getIDFromAuthType(auth_type));
        radioGroup.setOnCheckedChangeListener((group, checkedId) ->
        {
            Minicode.context.getSharedPreferences("low.minicode.preferences." + mProjectName.hashCode(), Context.MODE_PRIVATE).edit().putInt("auth_type", Library.getAuthTypeFromID(checkedId)).commit();
        });
        newKey.setOnClickListener(v ->
        {
            try
            {
                Library.generateRSAKeyPair().dispose();
                pub.set(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_public_key", null));
                priv.set(Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_private_key", null));
                sshPub.setText(pub.get());
                sshPriv.setText(priv.get());
            }
            catch (JSchException e)
            {
                e.printStackTrace();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_vcssettings, container, false);
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity context)
    {
        super.onAttach(context);
        if (getArguments() != null)
        {
            mProjectName = getArguments().getString("projectName");
        }
        try
        {
            Log.d(getClass().getName(), String.format("onAttach: init repo %s", mProjectName));
            mRepository = new RepositoryBuilder().setGitDir(new File(context.getFilesDir(), mProjectName + "/.git")).readEnvironment().setMustExist(true).findGitDir().build();
            Log.d(getClass().getName(), "onAttach: init repo success");

        }
        catch (IOException e)
        {
            e.printStackTrace();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage(R.string.git_init_failed);
            builder.setPositiveButton(R.string.ok, ((dialog, which) -> context.finish()));
            builder.show();
            Log.d(getClass().getName(), "onAttach: init repo fail");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public Repository getRepository()
    {
        return mRepository;
    }

    public void setRepository(Repository mRepository)
    {
        this.mRepository = mRepository;
    }
}
