/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.editor.preview;

import android.os.Bundle;
import android.widget.TextView;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import ru.noties.markwon.Markwon;
import ru.noties.markwon.core.CorePlugin;
import ru.noties.markwon.image.ImagesPlugin;

public class MarkdownPreviewActivity extends ThemedAppCompatActivity
{
    private TextView markdown;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_markdown_preview);
        markdown = findViewById(R.id.markwon);
        whitify();
        if (getIntent().getStringExtra("markdown_text") == null)
        {
            finish();
            return;
        }
        Markwon markwon = Markwon.builder(this).usePlugin(CorePlugin.create()).usePlugin(ImagesPlugin.create(this)).build();
        markwon.setMarkdown(markdown, getIntent().getStringExtra("markdown_text"));
    }
}
