/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.editor;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.Editable;
import android.text.InputType;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.vic797.syntaxhighlight.LineCountLayout;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.util.BlockList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.annotation.Nullable;

import low.minicode.AdvancedLanguage;
import low.minicode.FindTask;
import low.minicode.Indexer;
import low.minicode.Library;
import low.minicode.ProjectManifest;
import low.minicode.R;
import low.minicode.SpanWatchMultiAutoCompleteTextView;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.compiler.options.RunOptionsActivity;
import low.minicode.activity.editor.preview.MarkdownPreviewActivity;
import low.minicode.activity.manual.man.QuickManualActivity;
import low.minicode.activity.setup.languages.JDKSetupActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;
import low.minicode.activity.web.FloatingWebActivity;
import low.minicode.adapter.versatile.ACTVArrayAdapter;
import low.minicode.adapter.versatile.MaterialButtonAdapter;
import low.minicode.adapter.versatile.PushButtonAdapter;
import low.minicode.application.Minicode;
import low.minicode.tokenizer.c.CLanguageTokenizer;
import low.minicode.wrappers.data.DualDataStructure;
import low.minicode.wrappers.data.DualDataStructure2;
import low.minicode.wrappers.data.editor.EditState;

public class EditorActivity extends ThemedAppCompatActivity
{
    private Toolbar primaryToolbar;
    private SpanWatchMultiAutoCompleteTextView editField;
    private EditText scrollTo, quickFind, manPage;
    private RecyclerView autocompleteView;
    private File file;
    private Indexer indexer;
    private FindTask findTask;
    private Handler indexerHandler;
    private Handler saveHandler;
    private Handler findHandler;
    private Handler undoHandler;
    private Handler manIndexerHandler;
    private AdvancedLanguage language;
    private ACTVArrayAdapter adapter;
    private ImageButton save;
    private ImageButton scroll;
    private ImageButton find;
    private ImageButton findSettings;
    private ImageButton run;
    private ImageButton undo;
    private ImageButton redo;
    private ImageButton man;
    private CheckBox imeAuto;
    private ProgressBar saveSpinner;
    private LinkedList<EditState> undoBuffer, redoBuffer;
    private List<DualDataStructure2<DualDataStructure<Integer>, Integer>> stash;
    private Toolbar manToolbar;
    private @Nullable
    String projectName;
    private int regexFlags = Pattern.LITERAL;
    private int change;
    private long entryTime;
    private boolean readOnly, undoWatcherFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_editor);
        stash = new BlockList<>();
        man = findViewById(R.id.quick_man_go);
        manPage = findViewById(R.id.quick_man);
        primaryToolbar = findViewById(R.id.primary_editor_toolbar);
        editField = findViewById(R.id.edit);
        autocompleteView = findViewById(R.id.editor_autocomplete_view);
        editField.setHorizontallyScrolling(true);
        saveHandler = new Handler();
        save = findViewById(R.id.save);
        manToolbar = findViewById(R.id.toolbar_man_preview);
        scroll = findViewById(R.id.go_line_go);
        scrollTo = findViewById(R.id.go_line);
        findSettings = findViewById(R.id.quick_find_settings);
        quickFind = findViewById(R.id.quick_find);
        find = findViewById(R.id.quick_find_go);
        saveSpinner = findViewById(R.id.save_bar);
        run = findViewById(R.id.run);
        undo = findViewById(R.id.undo);
        redo = findViewById(R.id.redo);
        imeAuto = findViewById(R.id.ime_auto);
        undoBuffer = new LinkedList<>();
        redoBuffer = new LinkedList<>();
        HandlerThread handlerThread = new HandlerThread("IndexerLauncher");
        handlerThread.start();
        HandlerThread handlerThread1 = new HandlerThread("UndoLauncher");
        handlerThread1.start();
        HandlerThread handlerThread2 = new HandlerThread("ManualIndexerLauncher");
        handlerThread2.start();
        manIndexerHandler = new Handler(handlerThread2.getLooper());
        indexerHandler = new Handler(handlerThread.getLooper());
        findHandler = new Handler(handlerThread.getLooper());
        undoHandler = new Handler(handlerThread1.getLooper());
        readOnly = getIntent().getBooleanExtra("read_only", false);
        projectName = getIntent().getStringExtra("project_name");
        file = (File) getIntent().getSerializableExtra("file");
        if (file == null)
        {
            finish();
        }
        if (projectName != null)
        {
            getSharedPreferences("low.minicode.usage_stats-" + projectName.hashCode(), MODE_PRIVATE).edit().putInt(Library.relative(new File(getFilesDir(), projectName), file), getSharedPreferences("low.minicode.usage_stats-" + projectName.hashCode(), MODE_PRIVATE).getInt(Library.relative(new File(getFilesDir(), projectName), file), 0) + 1).apply();
        }
        imeAuto.setOnCheckedChangeListener((buttonView, isChecked) -> editField.setInputType(!isChecked ? editField.getInputType() | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE : editField.getInputType() & ~InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE));
        whitify();
        saveSpinner.setVisibility(View.GONE);
        man.setOnClickListener(v ->
        {
            if (!manPage.getText().toString().isEmpty())
            {
                Intent i = new Intent(this, QuickManualActivity.class);
                i.putExtra("manual_page", manPage.getText().toString());
                startActivity(i);
            }
        });
        setActionBar(primaryToolbar);
        scroll.setOnClickListener(v ->
        {
            try
            {
                scrollTo(Integer.valueOf(scrollTo.getText().toString()));
            }
            catch (NumberFormatException e)
            {
                e.printStackTrace();
            }
        });
        save.setOnClickListener(v ->
        {
            saveHandler.post(() ->
            {
                save.setClickable(false);
                save.setVisibility(View.GONE);
                saveSpinner.setVisibility(View.VISIBLE);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Library.writeFileAsBytes(file.getAbsolutePath(), editField.getText().toString().getBytes());
                        change = editField.getText().toString().hashCode();
                        if (editField.getText().toString().hashCode() != change)
                        {
                            save.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.md_red_600)));
                        }
                        else
                        {
                            save.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.gplus_color_1)));
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    runOnUiThread(() ->
                    {
                        save.setClickable(true);
                        save.setImageResource(R.drawable.ic_check_white);
                        new Handler().postDelayed(() -> save.setImageResource(R.drawable.ic_save_black_24dp), 200);
                        save.setVisibility(View.VISIBLE);
                        saveSpinner.setVisibility(View.GONE);
                    });
                });
            });
        });
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                if (file.length() != 0)
                {
                    String string = new Scanner(new FileInputStream(file)).useDelimiter("\\A").next();
                    change = string.hashCode();
                    editField.post(() -> editField.setText(string));
                    undoBuffer.push(new EditState(0, 0, string));
                }
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            runOnUiThread(this::initialize);
        });
    }

    private void initialize()
    {
        editField.setCustomSelectionActionModeCallback(new ActionMode.Callback()
        {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu)
            {
                menu.add(Menu.NONE, 64 ^ 43, Menu.NONE, R.string.man_7);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu)
            {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item)
            {
                if (item.getItemId() == (64 ^ 43))
                {
                    Intent i = new Intent(EditorActivity.this, QuickManualActivity.class);
                    i.putExtra("manual_page", editField.getText().subSequence(editField.getSelectionStart(), editField.getSelectionEnd()).toString());
                    startActivity(i);
                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode)
            {
            }
        });
        editField.addTextChangedListener(new TextWatcher()
        {
            EditState beforeTextChanged = new EditState(0, 0, null);

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                beforeTextChanged.curloc = editField.getSelectionStart();
                beforeTextChanged.sherloc = editField.getSelectionEnd();
                beforeTextChanged.curtext = s.toString();
                try
                {
                    if (count != 0)
                    {
                        return;
                    }
                    boolean b = undoWatcherFlag;
                    EditState copy = beforeTextChanged;
                    undoHandler.removeCallbacksAndMessages(null);
                    undoHandler.postDelayed(() ->
                    {
                        try
                        {
                            if (!b)
                            {
                                redoBuffer.clear();
                            }
                            if (b)
                            {
                                return;
                            }
                            else if (s.subSequence(start - 1, start).toString().matches(".*[\\s.\\[\\]+\\-*/&\\n<>;].*"))
                            {
                                undoBuffer.push(beforeTextChanged);
                            }
                        }
                        catch (IndexOutOfBoundsException | NullPointerException e)
                        {
                            e.printStackTrace();
                        }
                    }, 50);
                }
                catch (IndexOutOfBoundsException | NullPointerException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                try
                {
                    if (manToolbar.getVisibility() == View.VISIBLE)
                    {
                        int x = manToolbar.getRight();
                        int y = manToolbar.getBottom();
                        int startRadius = 0;
                        int endRadius = (int) Math.hypot(primaryToolbar.getWidth(), primaryToolbar.getHeight());
                        Animator anim = ViewAnimationUtils.createCircularReveal(primaryToolbar, x, y, startRadius, endRadius);
                        manToolbar.setVisibility(View.GONE);
                        anim.addListener(new Animator.AnimatorListener()
                        {
                            @Override
                            public void onAnimationStart(Animator animation)
                            {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation)
                            {
                                getWindow().setStatusBarColor(getResources().getColor(!isDark() ? R.color.md_grey_200 : R.color.md_grey_900));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !isDark())
                                {
                                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation)
                            {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation)
                            {
                            }
                        });
                        anim.start();
                    }
                    if (count < 1)
                    {
                        return;
                    }
                    EditState tmp = beforeTextChanged;
                    boolean b = undoWatcherFlag;
                    undoHandler.removeCallbacksAndMessages(null);
                    undoHandler.postDelayed(() ->
                    {
                        try
                        {
                            if (!b)
                            {
                                redoBuffer.clear();
                            }
                            if (b)
                            {
                                return;
                            }
                            else if (s.subSequence(start, start + count).toString().matches(".*[\\s.\\[\\]+\\-*/&\\n<>;].*") || count > 1)
                            {
                                undoBuffer.push(tmp);
                            }
                        }
                        catch (IndexOutOfBoundsException | NullPointerException e)
                        {
                            e.printStackTrace();
                        }
                    }, 50);
                }
                catch (IndexOutOfBoundsException | NullPointerException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });
        primaryToolbar.setTitle(file.getName());
        editField.setEnabled(!readOnly);
        run.setOnClickListener(v ->
        {
            if (language == AdvancedLanguage.PYTHON3)
            {
                if (!Library.getPython3Prefix().exists())
                {
                    MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(this);
                    error.setTitle(R.string.no_python3);
                    error.setMessage(R.string.please_extract_lang_pack);
                    error.setPositiveButton(R.string.ok, (dialog, which) ->
                    {
                    });
                    error.setNeutralButton(R.string.extract, (dialog, which) -> startActivity(new Intent(this, JDKSetupActivity.class)));
                    error.show();
                }
                else
                {
                    Intent i = new Intent(this, NDKTerminalActvitiy.class);
                    i.putExtra("binary", Library.getPython3Executable().getAbsolutePath());
                    i.putExtra("cwd", new File(getFilesDir(), projectName).getAbsolutePath());
                    ProjectManifest m = new ProjectManifest(new File(getFilesDir(), projectName + "/project_manifest.json"));
                    List<String> tmp = m.args;
                    tmp.add(0, Library.getPython3Executable().getAbsolutePath());
                    tmp.add(1, file.getAbsolutePath());
                    i.putExtra("binaryArray", Arrays.copyOf(tmp.toArray(), tmp.size(), String[].class));
                    startActivityForResult(i, 0);
                    return;
                }
            }
            else if (language == AdvancedLanguage.SHELL)
            {
                Intent i = new Intent(this, NDKTerminalActvitiy.class);
                i.putExtra("cwd", new File(getFilesDir(), projectName).getAbsolutePath());
                ProjectManifest m = new ProjectManifest(new File(getFilesDir(), projectName + "/project_manifest.json"));
                List<String> tmp = m.args;
                if (new File("/system/xbin/bash").exists())
                {
                    i.putExtra("binary", "/system/xbin/bash");
                    tmp.add(0, "/system/xbin/bash");
                }
                else
                {
                    i.putExtra("binary", "/system/bin/sh");
                    tmp.add(0, "/system/bin/sh");
                }
                tmp.add(1, file.getAbsolutePath());
                i.putExtra("binaryArray", Arrays.copyOf(tmp.toArray(), tmp.size(), String[].class));
                startActivityForResult(i, 0);
                return;
            }
            else if (language == AdvancedLanguage.HTML)
            {
                Intent i = new Intent(this, FloatingWebActivity.class);
                i.setData(FileProvider.getUriForFile(this, "low.minicode.authority", file));
                startActivity(i);
                return;
            }
            else if (language == AdvancedLanguage.MARKDOWN)
            {
                Intent i = new Intent(this, MarkdownPreviewActivity.class);
                i.putExtra("markdown_text", editField.getText().toString());
                startActivity(i);
                return;
            }
            Intent i = new Intent(this, RunOptionsActivity.class);
            i.putExtra("projectName", projectName);
            i.putExtra("runFile", file);
            startActivity(i);
        });
        run.setOnLongClickListener(v ->
        {
            Intent i = new Intent(this, RunOptionsActivity.class);
            i.putExtra("projectName", projectName);
            startActivity(i);
            return true;
        });
        findSettings.setOnClickListener(v -> this.showRegexOptionsDialog());
        quickFind.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                quickFind.post(() -> quickFind.setTextColor(Color.BLACK));
                findHandler.removeCallbacksAndMessages(null);
                findHandler.post(() ->
                {
                    try
                    {
                        if (findTask != null)
                        {
                            findTask.interrupt();
                        }
                        findTask = new FindTask(regexFlags, editField, Pattern.compile(s.toString(), regexFlags));
                        findTask.start();
                    }
                    catch (PatternSyntaxException e)
                    {
                        quickFind.post(() -> quickFind.setTextColor(Color.RED));
                    }
                });
            }
        });
        editField.setTypeface(Minicode.theMinicode.getEditorTypeface());
        editField.setTextSize(TypedValue.COMPLEX_UNIT_SP, Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt("font_size", 15));
        ((LineCountLayout) findViewById(R.id.lines)).attachEditText(editField);
        if (file.getName().matches("^.*\\.[ch]$"))
        {
            language = AdvancedLanguage.C;
        }
        else if (file.getName().matches("^.*\\.(cc|hh|cpp|hpp)$"))
        {
            language = AdvancedLanguage.CC;
        }
        else if (file.getName().matches("^.*\\.(m|hm)$"))
        {
            language = AdvancedLanguage.M;
        }
        else if (file.getName().matches("^.*\\.(mm|hmm)$"))
        {
            language = AdvancedLanguage.MM;
        }
        else if (file.getName().matches("^.*\\.java$"))
        {
            language = AdvancedLanguage.JAVA;
        }
        else if (file.getName().matches("^.*\\.js$"))
        {
            language = AdvancedLanguage.JS;
        }
        else if (file.getName().matches("^.*\\.sh$"))
        {
            language = AdvancedLanguage.SHELL;
        }
        else if (file.getName().matches("^.*\\.py3$"))
        {
            language = AdvancedLanguage.PYTHON3;
        }
        else if (file.getName().matches("^.*\\.htm(l)?$"))
        {
            language = AdvancedLanguage.HTML;
        }
        else if (file.getName().matches("^.*\\.md"))
        {
            language = AdvancedLanguage.MARKDOWN;
        }
        if (language == AdvancedLanguage.C || language == AdvancedLanguage.CC || language == AdvancedLanguage.M || language == AdvancedLanguage.MM)
        {
            String[] array = new String[] {"int", "void", "char", "int[]", "void[]", "char[]", "[]", "==", "!=", "->", "const", "*", "**", "()", "(void)", "#include", "#if", "#ifndef", "#ifdef", "#else", "#define", "#pragma", "#undef", "#elif", "#endif", "#error", "__DATE__", "__FILE__", "__LINE__", "__TIME__", "__VERSION__", "__STDC__", "%", "-", "+", "/", "return", "typedef", "short", "long", "unsigned", "long[]", "short[]", "int * ", "char * ", "long * ", "short * ", "void * ", "if", "while", "for", "case", "switch", "break", "continue", "default", "do", "else", "else if", "NULL", "extern", "extern char", "extern int", "struct"};
            ArrayList<String> a = new ArrayList<>(Arrays.asList(array));
            if (language == AdvancedLanguage.CC || language == AdvancedLanguage.MM)
            {
                a.addAll(new ArrayList<>(Arrays.asList("new", "delete", "delete[]", "try", "catch", "alignas", "alignof", "and", "and_eq", "asm", "atomic_cancel", "atomic_commit", "atomic_noexcept", "auto", "bitand", "bitor", "bool", "char16_t", "char32_t", "class", "compl", /* "concept", */ "const_cast", "co_await", "co_return", "co_yield", "decltype", "dynamic_cast", "enum", "explicit", "friend", "public", "private", "protected", "operator", /* "required", */ "reinterpret_cast", "static", "static_assert", "synchronized", "this", "thread_local", "using", "throw", "true", "false", "nullptr", "super")));
            }
            if (language == AdvancedLanguage.M || language == AdvancedLanguage.MM)
            {
                a.addAll(new ArrayList<>(Arrays.asList("#import", "@interface", "@implementation", "@end", "BOOL", "YES", "NO", "id", "Class", "Protocol", "nil")));
            }
            String[] things = Arrays.copyOf(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getStringSet("auto_complete_additions", new ConcurrentSkipListSet<>()).toArray(), getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getStringSet("auto_complete_additions", new ConcurrentSkipListSet<>()).toArray().length, String[].class);
            a.addAll(Arrays.asList(things));
            PushButtonAdapter pushButtonAdapter = new MaterialButtonAdapter(a, editField, true, language);
            autocompleteView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            autocompleteView.setAdapter(pushButtonAdapter);
            editField.setDropDownHeight(0);
            editField.setTokenizer(new CLanguageTokenizer());
            editField.setAdapter((adapter = new ACTVArrayAdapter(pushButtonAdapter, a)));
        }
        else if (language == AdvancedLanguage.PYTHON3)
        {
            String[] array = new String[] {"def", "class", "int", "str", "float", "if", "elif", "else", "not", "True", "False", "global", "and", "assert", "break", "except", "try", "finally", "class", "in", "range", "for", "while"};
            PushButtonAdapter pushButtonAdapter = new MaterialButtonAdapter(Arrays.asList(array), editField, true, language);
            autocompleteView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            autocompleteView.setAdapter(pushButtonAdapter);
            editField.setDropDownHeight(0);
            editField.setTokenizer(new CLanguageTokenizer());
            editField.setAdapter((adapter = new ACTVArrayAdapter(pushButtonAdapter, Arrays.asList(array))));
        }
        undo.setOnClickListener(v ->
        {
            redoBuffer.add(new EditState(editField.getSelectionStart(), editField.getSelectionEnd(), editField.getText().toString()));
            try
            {
                EditState state = undoBuffer.pop();
                undoWatcherFlag = true;
                editField.setText(state.curtext);
                try
                {
                    editField.setSelection(state.sherloc, state.curloc);
                }
                catch (IndexOutOfBoundsException e)
                {
                }
                undoWatcherFlag = false;
            }
            catch (NoSuchElementException e)
            {
                e.printStackTrace();
            }
        });
        redo.setOnClickListener(v ->
        {
            try
            {
                Log.d(getClass().getName(), redoBuffer.toString());
                EditState state = redoBuffer.pollLast();
                editField.setText(state.curtext);
                try
                {
                    editField.setSelection(state.sherloc, state.curloc);
                }
                catch (IndexOutOfBoundsException e)
                {
                }
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
            }
        });
        undo.setOnLongClickListener(v ->
        {
            MaterialAlertDialogBuilder select = new MaterialAlertDialogBuilder(this);
            ArrayList<String> stateAsString = new ArrayList<>();
            for (EditState s : undoBuffer)
            {
                stateAsString.add(s.curtext);
            }
            select.setItems(Arrays.copyOf(stateAsString.toArray(), stateAsString.size(), CharSequence[].class), (dialog, which) ->
            {
            });
            select.show();
            return true;
        });
        editField.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @SuppressWarnings("deprecation")
            @Override
            public void afterTextChanged(Editable s)
            {
                indexerHandler.removeCallbacksAndMessages(null);
                indexerHandler.postDelayed(() ->
                {
                    stash.clear();
                    if (indexer != null)
                    {
                        indexer.interrupt();
                    }
                    indexer = new Indexer(editField, s.toString(), language, stash);
                    indexer.start();
                }, 50);
                if (quickFind.getText().length() != 0)
                {
                    quickFind.post(() -> quickFind.setTextColor(Color.BLACK));
                    findHandler.removeCallbacksAndMessages(null);
                    findHandler.post(() ->
                    {
                        try
                        {
                            if (findTask != null)
                            {
                                findTask.interrupt();
                            }
                            findTask = new FindTask(regexFlags, editField, Pattern.compile(quickFind.getText().toString(), regexFlags));
                            findTask.start();
                        }
                        catch (PatternSyntaxException e)
                        {
                            quickFind.post(() -> quickFind.setTextColor(Color.RED));
                        }
                    });
                }
                if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getBoolean("auto_save", true))
                {
                    saveHandler.removeCallbacksAndMessages(null);
                    saveHandler.postDelayed(() ->
                    {
                        save.setClickable(false);
                        save.setVisibility(View.GONE);
                        saveSpinner.setVisibility(View.VISIBLE);
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            try
                            {
                                Library.writeFileAsBytes(file.getAbsolutePath(), editField.getText().toString().getBytes());
                                change = s.toString().hashCode();
                                if (s.toString().hashCode() != change)
                                {
                                    save.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.md_red_600)));
                                }
                                else
                                {
                                    save.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.gplus_color_1)));
                                }
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            runOnUiThread(() ->
                            {
                                save.setClickable(true);
                                save.setImageResource(R.drawable.ic_check_white);
                                new Handler().postDelayed(() -> save.setImageResource(R.drawable.ic_save_black_24dp), 200);
                                save.setVisibility(View.VISIBLE);
                                saveSpinner.setVisibility(View.GONE);
                            });
                        });
                    }, getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("save_delay", 1000));
                }
            }
        });
        indexer = new Indexer(editField, editField.getText().toString(), language);
        indexer.start();
        TextView rFacingCurly, lFacingCurly, semi, equals, quote, tab, comment;
        rFacingCurly = findViewById(R.id.right_facing_curly_bracket_button);
        lFacingCurly = findViewById(R.id.left_facing_curly_bracket_button);
        semi = findViewById(R.id.semicolon_button);
        equals = findViewById(R.id.equals_button);
        quote = findViewById(R.id.quote_button);
        tab = findViewById(R.id.tab_button);
        comment = findViewById(R.id.comment_button);
        primaryToolbar.setNavigationOnClickListener(v -> onNavigateUp());
        if (getIntent().getStringExtra("readOnly") == null)
        {
            rFacingCurly.setOnClickListener(v ->
            {
                int pos = editField.getSelectionStart();
                editField.getText().insert(pos, "{");
                editField.setSelection(pos + 1);
            });
            lFacingCurly.setOnClickListener(v ->
            {
                int pos = editField.getSelectionStart();
                editField.getText().insert(pos, "}");
                editField.setSelection(pos + 1);
            });
            semi.setOnClickListener(v ->
            {
                int pos = editField.getSelectionStart();
                editField.getText().insert(pos, ";");
                editField.setSelection(pos + 1);
            });
            equals.setOnClickListener(v ->
            {
                int pos = editField.getSelectionStart();
                editField.getText().insert(pos, "=");
                editField.setSelection(pos + 1);
            });
            quote.setOnClickListener(v ->
            {
                int pos = editField.getSelectionStart();
                editField.getText().insert(pos, "\"\"");
                editField.setSelection(pos + 1);
            });
            comment.setOnClickListener(v ->
            {
                int posa = editField.getSelectionStart();
                int posb = editField.getSelectionEnd();
                if (posa == posb)
                {
                    editField.getText().insert(posa, (language == AdvancedLanguage.SHELL || language == AdvancedLanguage.PYTHON3) ? "# " : "// ");
                    editField.setSelection((language == AdvancedLanguage.SHELL || language == AdvancedLanguage.PYTHON3) ? posa + 2 : posa + 3);
                }
                else if (language != AdvancedLanguage.SHELL)
                {
                    String orig = editField.getText().subSequence(posa, posb).toString();
                    String res = String.format(language != AdvancedLanguage.PYTHON3 ? "/* %s */" : "''' %s '''", orig);
                    editField.getEditableText().replace(posa, posb, res);
                }
                else
                {
                    String orig = editField.getText().subSequence(posa, posb).toString();
                    String res = String.format(": ' \n %s '", orig);
                    editField.getEditableText().replace(posa, posb, res);
                }
            });
            if (!file.getName().matches(".*[Mm]akefile$"))
            {
                tab.setOnClickListener(v ->
                {
                    int pos = editField.getSelectionStart();
                    editField.getText().insert(pos, "    ");
                    editField.setSelection(pos + 4);
                });
            }
            else
            {
                tab.setText(R.string.tab);
                tab.setOnClickListener(v ->
                {
                    int pos = editField.getSelectionStart();
                    editField.getText().insert(pos, "\t");
                    editField.setSelection(pos + 1);
                });
            }
        }
        editField.addSelectionHandler((selStart, selEnd, text) ->
        {
            manIndexerHandler.removeCallbacksAndMessages(null);
            manIndexerHandler.post(() ->
            {
                CharSequence n = "";
                ArrayList<File> f = null;
                if (selStart != selEnd)
                {
                    n = text.subSequence(selStart > selEnd ? selEnd : selStart, selStart > selEnd ? selStart : selEnd).toString();
                    f = Library.findManual(n.toString(), null);
                }
                else
                {
                    for (int i = 25; i > 0; --i)
                    {
                        if (selEnd - i < 0)
                        {
                            break;
                        }
                        n = text.subSequence(selEnd - i, selEnd);
                        if (n.toString().contains(" ") || n.toString().contains("\t") || n.toString().contains("?") || n.toString().contains("*") || n.toString().contains("+") || n.toString().contains("/") || n.toString().contains("^") || n.toString().contains("<") || n.toString().contains(">") || n.toString().contains("(") || n.toString().contains(")") || n.toString().contains(":") || n.toString().contains(";"))
                        {
                            continue;
                        }
                        f = Library.findManual(n.toString(), null);
                        if (!f.isEmpty())
                        {
                            break;
                        }
                    }
                }
                if (f == null || f.isEmpty())
                {
                    if (manToolbar.getVisibility() == View.VISIBLE)
                    {
                        manToolbar.post(() ->
                        {
                            int x = manToolbar.getRight();
                            int y = manToolbar.getBottom();
                            int startRadius = 0;
                            int endRadius = (int) Math.hypot(primaryToolbar.getWidth(), primaryToolbar.getHeight());
                            Animator anim = ViewAnimationUtils.createCircularReveal(primaryToolbar, x, y, startRadius, endRadius);
                            anim.addListener(new Animator.AnimatorListener()
                            {
                                @Override
                                public void onAnimationStart(Animator animation)
                                {
                                }

                                @Override
                                public void onAnimationEnd(Animator animation)
                                {
                                    getWindow().setStatusBarColor(getResources().getColor(!isDark() ? R.color.md_grey_200 : R.color.md_grey_900));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                    {
                                        runOnUiThread(() -> getWindow().getDecorView().setSystemUiVisibility(!isDark() ? View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR : 0));
                                    }
                                }

                                @Override
                                public void onAnimationCancel(Animator animation)
                                {
                                }

                                @Override
                                public void onAnimationRepeat(Animator animation)
                                {
                                }
                            });
                            manToolbar.setVisibility(View.GONE);
                            anim.start();
                        });
                    }
                    return;
                }
                try
                {
                    BufferedReader r = new BufferedReader(new FileReader(f.get(0)));
                    String line = "", last;
                    int amt = 0;
                    while ((++amt) < 25)
                    {
                        last = line;
                        line = r.readLine();
                        if (last.trim().equals("NAME"))
                        {
                            String finalN = (String) n;
                            String finalLine = line;
                            manToolbar.post(() ->
                            {
                                manToolbar.setTitle(finalN);
                                manToolbar.setSubtitle(finalLine.trim());
                                manToolbar.setOnClickListener(v ->
                                {
                                    Intent i = new Intent(this, QuickManualActivity.class);
                                    i.putExtra("manual_page", finalN);
                                    startActivity(i);
                                });
                            });
                            if (manToolbar.getVisibility() != View.VISIBLE)
                            {
                                manToolbar.post(() ->
                                {
                                    int x = primaryToolbar.getRight();
                                    int y = primaryToolbar.getBottom();
                                    int startRadius = 0;
                                    int endRadius = (int) Math.hypot(manToolbar.getWidth(), manToolbar.getHeight());
                                    Animator anim = ViewAnimationUtils.createCircularReveal(manToolbar, x, y, startRadius, endRadius);
                                    anim.addListener(new Animator.AnimatorListener()
                                    {
                                        @Override
                                        public void onAnimationStart(Animator animation)
                                        {
                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animation)
                                        {
                                            getWindow().setStatusBarColor(getResources().getColor(isDark() ? R.color.md_grey_200 : R.color.md_grey_900));
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                            {
                                                getWindow().getDecorView().setSystemUiVisibility(!isDark() ? 0 : View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                                            }
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation)
                                        {
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation)
                                        {
                                        }
                                    });
                                    manToolbar.setVisibility(View.VISIBLE);
                                    anim.start();
                                });
                            }
                            return;
                        }
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    if (manToolbar.getVisibility() == View.VISIBLE)
                    {
                        manToolbar.post(() ->
                        {
                            int x = manToolbar.getRight();
                            int y = manToolbar.getBottom();
                            int startRadius = 0;
                            int endRadius = (int) Math.hypot(primaryToolbar.getWidth(), primaryToolbar.getHeight());
                            Animator anim = ViewAnimationUtils.createCircularReveal(primaryToolbar, x, y, startRadius, endRadius);
                            anim.addListener(new Animator.AnimatorListener()
                            {
                                @Override
                                public void onAnimationStart(Animator animation)
                                {
                                }

                                @Override
                                public void onAnimationEnd(Animator animation)
                                {
                                    getWindow().setStatusBarColor(getResources().getColor(!isDark() ? R.color.md_grey_200 : R.color.md_grey_900));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                    {
                                        runOnUiThread(() -> getWindow().getDecorView().setSystemUiVisibility(!isDark() ? View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR : 0));
                                    }
                                }

                                @Override
                                public void onAnimationCancel(Animator animation)
                                {
                                }

                                @Override
                                public void onAnimationRepeat(Animator animation)
                                {
                                }
                            });
                            manToolbar.setVisibility(View.GONE);
                            anim.start();
                        });
                    }
                }
            });
        });
        editField.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().hashCode() != change)
                {
                    save.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.md_red_600)));
                }
                else
                {
                    save.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.gplus_color_1)));
                }
                if (s.length() > 0 && s.charAt(s.length() - 1) == ' ')
                {
                }
            }
        });
        entryTime = System.currentTimeMillis();
        if (language != null)
        {
            editField.setOnEditorActionListener((v, keyCode, event) ->
            {
                Log.d(getClass().getName(), "onEditorAction: event: " + event + " keyCode: " + keyCode);
                if (event != null && event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
                {
                    Log.d(getClass().getName(), "onEditorAction: KEY_ENTER");
                    int indentLevel = Library.getIndentLevel(Library.readUntilLastNewlineOrStart(editField.getText().toString().substring(0, editField.getSelectionEnd())), 4);
                    Log.d(getClass().getName(), "onEditorAction: Indent level (4): " + indentLevel);
                    editField.getEditableText().replace(editField.getSelectionStart(), editField.getSelectionEnd(), "\n" + StringUtils.repeat(' ', indentLevel * 4));
                    return true;
                }
                return false;
            });
        }
        scrollTo(getIntent().getIntExtra("line", 0));
    }

    @Override
    public boolean onNavigateUp()
    {
        finish();
        return true;
    }

    public void scrollTo(int line)
    {
        ViewTreeObserver.OnGlobalLayoutListener r = new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                Log.d(getClass().getName(), "onGlobalLayout: scroll");
                Layout mLayout = editField.getLayout();
                ScrollView sv = (ScrollView) editField.getParent().getParent();
                try
                {
                    int height = mLayout.getLineBottom(line);
                    Log.d(getClass().getName(), "onGlobalLayout: height " + height);
                    sv.post(() -> sv.scrollTo(editField.getScrollX(), height - sv.getHeight()));
                }
                catch (IndexOutOfBoundsException e)
                {
                    sv.post(() -> sv.scrollTo(editField.getScrollX(), editField.getHeight()));
                }
                editField.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        };
        editField.getViewTreeObserver().addOnGlobalLayoutListener(r);
        editField.requestLayout();
    }

    protected void showRegexOptionsDialog()
    {
        View v = LayoutInflater.from(this).inflate(R.layout._dialog_regex, null);
        CheckBox literal, multiline, nocase, dotall, unixLines;
        literal = v.findViewById(R.id.literal);
        multiline = v.findViewById(R.id.multiline);
        nocase = v.findViewById(R.id.ignore_case);
        dotall = v.findViewById(R.id.dotall);
        unixLines = v.findViewById(R.id.unix_lines);
        literal.setChecked((regexFlags & Pattern.LITERAL) == 0);
        multiline.setChecked((regexFlags & Pattern.MULTILINE) != 0);
        nocase.setChecked((regexFlags & Pattern.CASE_INSENSITIVE) != 0);
        dotall.setChecked((regexFlags & Pattern.DOTALL) != 0);
        unixLines.setChecked((regexFlags & Pattern.DOTALL) != 0);
        literal.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked)
            {
                regexFlags &= ~Pattern.LITERAL;
            }
            else
            {
                regexFlags |= Pattern.LITERAL;
            }
            if (quickFind.getText().length() != 0)
            {
                quickFind.post(() -> quickFind.setTextColor(Color.BLACK));
                findHandler.removeCallbacksAndMessages(null);
                findHandler.post(() ->
                {
                    try
                    {
                        if (findTask != null)
                        {
                            findTask.interrupt();
                        }
                        findTask = new FindTask(regexFlags, editField, Pattern.compile(editField.getText().toString(), regexFlags));
                        findTask.start();
                    }
                    catch (PatternSyntaxException e)
                    {
                        quickFind.post(() -> quickFind.setTextColor(Color.RED));
                    }
                });
            }
        });
        multiline.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked)
            {
                regexFlags |= Pattern.MULTILINE;
            }
            else
            {
                regexFlags &= ~Pattern.MULTILINE;
            }
            if (quickFind.getText().length() != 0)
            {
                quickFind.post(() -> quickFind.setTextColor(Color.BLACK));
                findHandler.removeCallbacksAndMessages(null);
                findHandler.post(() ->
                {
                    try
                    {
                        if (findTask != null)
                        {
                            findTask.interrupt();
                        }
                        findTask = new FindTask(regexFlags, editField, Pattern.compile(editField.getText().toString(), regexFlags));
                        findTask.start();
                    }
                    catch (PatternSyntaxException e)
                    {
                        quickFind.post(() -> quickFind.setTextColor(Color.RED));
                    }
                });
            }
        });
        nocase.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked)
            {
                regexFlags |= Pattern.CASE_INSENSITIVE;
            }
            else
            {
                regexFlags &= ~Pattern.CASE_INSENSITIVE;
            }
            if (quickFind.getText().length() != 0)
            {
                quickFind.post(() -> quickFind.setTextColor(Color.BLACK));
                findHandler.removeCallbacksAndMessages(null);
                findHandler.post(() ->
                {
                    try
                    {
                        if (findTask != null)
                        {
                            findTask.interrupt();
                        }
                        findTask = new FindTask(regexFlags, editField, Pattern.compile(editField.getText().toString(), regexFlags));
                        findTask.start();
                    }
                    catch (PatternSyntaxException e)
                    {
                        quickFind.post(() -> quickFind.setTextColor(Color.RED));
                    }
                });
            }
        });
        dotall.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked)
            {
                regexFlags |= Pattern.DOTALL;
            }
            else
            {
                regexFlags &= ~Pattern.DOTALL;
            }
        });
        unixLines.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked)
            {
                regexFlags |= Pattern.UNIX_LINES;
            }
            else
            {
                regexFlags &= ~Pattern.UNIX_LINES;
            }
            if (quickFind.getText().length() != 0)
            {
                quickFind.post(() -> quickFind.setTextColor(Color.BLACK));
                findHandler.removeCallbacksAndMessages(null);
                findHandler.post(() ->
                {
                    try
                    {
                        if (findTask != null)
                        {
                            findTask.interrupt();
                        }
                        findTask = new FindTask(regexFlags, editField, Pattern.compile(editField.getText().toString(), regexFlags));
                        findTask.start();
                    }
                    catch (PatternSyntaxException e)
                    {
                        quickFind.post(() -> quickFind.setTextColor(Color.RED));
                    }
                });
            }
        });
        MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(this).setView(v);
        dialogBuilder.show();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        long timespent = System.currentTimeMillis() - entryTime;
        if (projectName != null && file != null)
        {
            getSharedPreferences("low.minicode.usage_stats-" + projectName.hashCode(), MODE_PRIVATE).edit().putInt(Library.relative(new File(getFilesDir(), projectName), file), (int) (getSharedPreferences("low.minicode.usage_stats-" + projectName.hashCode(), MODE_PRIVATE).getInt(Library.relative(new File(getFilesDir(), projectName), file), 0) + (timespent / 10000))).apply();
        }
        indexerHandler.getLooper().quit();
        undoHandler.getLooper().quit();
        manIndexerHandler.getLooper().quit();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        indexerHandler.getLooper().quit();
        undoHandler.getLooper().quit();
        manIndexerHandler.getLooper().quit();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        HandlerThread handlerThread = new HandlerThread("IndexerHandler");
        handlerThread.start();
        HandlerThread handlerThread1 = new HandlerThread("UndoHandler");
        handlerThread1.start();
        HandlerThread handlerThread2 = new HandlerThread("UndoHandler");
        handlerThread2.start();
        manIndexerHandler = new Handler(handlerThread2.getLooper());
        indexerHandler = new Handler(handlerThread.getLooper());
        findHandler = new Handler(handlerThread.getLooper());
        undoHandler = new Handler(handlerThread1.getLooper());
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                if (file.length() != 0)
                {
                    String string = new Scanner(new FileInputStream(file)).useDelimiter("\\A").next();
                    if (change != string.hashCode())
                    {
                        change = string.hashCode();
                        editField.post(() -> editField.setText(string));
                    }
                }
                else
                {
                    editField.setText("");
                }
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        if (change != editField.getText().toString().hashCode())
        {
            if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getBoolean("auto_save", true))
            {
                Library.vibratePulse();
                try
                {
                    Library.writeFileAsBytes(file.getAbsolutePath(), editField.getText().toString().getBytes());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    Library.vibratePulse();
                }
                super.onBackPressed();
                Toast.makeText(this, getString(R.string.file_saved), Toast.LENGTH_LONG).show();
                return;
            }
            else
            {
                Library.vibrateError();
                MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(this);
                b.setTitle(R.string.save_changes);
                b.setMessage(R.string.save_quit_or);
                b.setPositiveButton(R.string.cancel, (dialog, which) ->
                {
                    return;
                });
                b.setNegativeButton(R.string.exit, (dialog, which) ->
                {
                    super.onBackPressed();
                    return;
                });
                b.setNeutralButton(R.string.save_and_exit, (dialog, which) ->
                {
                    try
                    {
                        Library.writeFileAsBytes(file.getAbsolutePath(), editField.getText().toString().getBytes());
                        super.onBackPressed();
                        Toast.makeText(this, getString(R.string.file_saved), Toast.LENGTH_LONG).show();
                        return;
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                        return;
                    }
                });
                b.show();
                return;
            }
        }
        super.onBackPressed();
    }
}
