/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.editor.preferences;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import org.zeroturnaround.zip.commons.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class EditorSetFontActivity extends ThemedAppCompatActivity
{
    final static int SELECT_ID = 8233;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_set_font);
        TextView test = findViewById(R.id.editor_font_test);
        try
        {
            test.setTypeface(Minicode.theMinicode.getEditorTypeface());
        }
        catch (RuntimeException e)
        {
            Snackbar.make(findViewById(R.id.select_font), getString(R.string.error_setting_font), Snackbar.LENGTH_SHORT);
            return;
        }
        findViewById(R.id.select_font).setOnClickListener(v ->
        {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, SELECT_ID);
        });
        findViewById(R.id.reset_font).setOnClickListener(v ->
        {
            new File(getFilesDir(), ".res/fonts/active.ttf").delete();
            try
            {
                test.setTypeface(Minicode.theMinicode.getEditorTypeface());
                Library.vibrateSuccess();
            }
            catch (RuntimeException e)
            {
                Snackbar.make(findViewById(R.id.select_font), getString(R.string.error_setting_font), Snackbar.LENGTH_SHORT);
                Library.vibrateError();
                return;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_ID && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            try
            {
                new File(getFilesDir(), ".res/fonts").mkdirs();
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                if (inputStream != null)
                {
                    FileUtils.copy(inputStream, new File(getFilesDir(), ".res/fonts/active.ttf"));
                    Library.vibrateSuccess();
                    recreate();
                }
                else
                {
                    return;
                }
            }
            catch (FileNotFoundException e)
            {
                Library.vibrateError();
                e.printStackTrace();
                return;
            }
            catch (IOException e)
            {
                Library.vibrateError();
                e.printStackTrace();
                return;
            }
        }
        else
        {
            return;
        }
    }
}
