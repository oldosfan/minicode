/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.manual.man;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class ManualActivity extends ThemedAppCompatActivity
{
    Toolbar toolbar;
    EditText pagerText;
    TextView textView;
    MaterialButton button;

    private ArrayList<File> indexManual(Integer sec)
    {
        ArrayList<File> files = new ArrayList<>();
        if (sec == null || sec == 0)
        {
            for (int i = 1; i < 9; ++i)
            {
                File[] things = new File(getFilesDir(), String.format("man_pages/manual/section.%d", i)).listFiles();
                for (File iter : things)
                {
                    Log.d("sucess", String.valueOf(i));
                    if (!iter.getName().contains("DS_Store"))
                    {
                        files.add(iter);
                    }
                }
            }
        }
        else
        {
            File[] things = new File(getFilesDir(), String.format("man_pages/manual/section.%d", sec)).listFiles();
            for (File iter : things)
            {
                if (!iter.getName().contains(".DS_Store"))
                {
                    files.add(iter);
                }
            }
        }
        return files;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);
        button = findViewById(R.id.material_button);
        pagerText = findViewById(R.id.pager_utils_button);
        textView = findViewById(R.id.manual);
        toolbar = findViewById(R.id.man_toolbar);
        setActionBar(toolbar);
        textView.setTypeface(Minicode.theMinicode.getEditorTypeface());
        button.setOnClickListener(v ->
        {
            ArrayList<File> manuals = indexManual(null);
            ArrayList<File> foundManuals = new ArrayList<>();
            ArrayList<String> foundManualsName = new ArrayList<>();
            if (pagerText.getText().length() < 1)
            {
                return;
            }
            for (File iter : manuals)
            {
                if (iter.getName().split("\\.\\d+\\.formatted\\.col")[0].equals(Objects.requireNonNull(pagerText.getText()).toString()))
                {
                    foundManuals.add(iter);
                    Log.d("found-1", iter.getName());
                }
            }
            for (File iter : foundManuals)
            {
                foundManualsName.add(iter.getName().split("\\.formatted\\.col")[0]);
                Log.d("found", iter.getName());
            }
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.setTitle(getString(R.string.man_found));
            String[] mans = new String[foundManualsName.size()];
            foundManualsName.toArray(mans);
            b.setItems(mans, (dialog, which) ->
            {
                toolbar.setTitle(foundManualsName.get(which));
                Scanner scanner = null;
                try
                {
                    scanner = new Scanner(foundManuals.get(which));
                }
                catch (FileNotFoundException e)
                {
                    textView.setText(e.getCause().getLocalizedMessage());
                    return;
                }
                textView.setText(scanner.useDelimiter("\\A").next());
            }).show();
        });
        if (getIntent().getStringExtra("manual_page") != null)
        {
            pagerText.setText(getIntent().getStringExtra("manual_page"));
            ArrayList<File> manuals = indexManual(null);
            ArrayList<File> foundManuals = new ArrayList<>();
            ArrayList<String> foundManualsName = new ArrayList<>();
            if (pagerText.getText().length() < 1)
            {
                return;
            }
            for (File iter : manuals)
            {
                if (iter.getName().split("\\.\\d+\\.formatted\\.col")[0].equals(Objects.requireNonNull(pagerText.getText()).toString()))
                {
                    foundManuals.add(iter);
                    Log.d("found-1", iter.getName());
                }
            }
            for (File iter : foundManuals)
            {
                foundManualsName.add(iter.getName().split("\\.formatted\\.col")[0]);
                Log.d("found", iter.getName());
            }
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.setTitle(getString(R.string.man_found));
            String[] mans = new String[foundManualsName.size()];
            foundManualsName.toArray(mans);
            b.setItems(mans, (dialog, which) ->
            {
                toolbar.setTitle(foundManualsName.get(which));
                Scanner scanner = null;
                try
                {
                    scanner = new Scanner(foundManuals.get(which));
                }
                catch (FileNotFoundException e)
                {
                    textView.setText(e.getCause().getLocalizedMessage());
                    return;
                }
                textView.setText(scanner.useDelimiter("\\A").next());
            }).show();
        }
    }
}
