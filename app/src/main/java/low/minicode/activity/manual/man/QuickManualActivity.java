/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.manual.man;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class QuickManualActivity extends ThemedAppCompatActivity
{
    private TextView text;
    private MaterialButton dismiss;
    private Toolbar toolbar;
    private AppCompatSpinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_manual);
        text = findViewById(R.id.text);
        dismiss = findViewById(R.id.dismiss);
        toolbar = findViewById(R.id.toolbar);
        spinner = findViewById(R.id.man_section);
        dismiss.setOnClickListener(this::finish);
        BottomSheetBehavior.from(findViewById(R.id.manual_container)).setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState)
            {
                if (newState == BottomSheetBehavior.STATE_HIDDEN)
                {
                    finish();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset)
            {
            }
        });
        String manualPage = getIntent().getStringExtra("manual_page");
        if (manualPage == null)
        {
            Library.vibrateError();
            finish();
            return;
        }
        ArrayList<File> manualPages = Library.findManual(manualPage, null);
        ArrayList<Integer> manualSections = Library.indexManualSects(manualPage);
        if (manualPages.isEmpty())
        {
            Library.vibrateError();
            finish();
            return;
        }
        try
        {
            text.setText(new Scanner(manualPages.get(0)).useDelimiter("\\A").next());
            toolbar.setTitle(manualPage);
            toolbar.setSubtitle(getString(R.string.sect_d, manualSections.get(0)));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(Minicode.context, android.R.layout.simple_spinner_dropdown_item, Arrays.copyOf(manualSections.toArray(), manualSections.size(), Integer[].class))
        {
            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                return tv;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
            {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                return tv;
            }
        };
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                ArrayList<File> manualPages2 = Library.findManual(manualPage, manualSections.get(position));
                if (manualPages2.isEmpty())
                {
                    return;
                }
                try
                {
                    text.setText(new Scanner(manualPages2.get(0)).useDelimiter("\\A").next());
                    toolbar.setTitle(manualPage);
                    toolbar.setSubtitle(getString(R.string.sect_d, manualSections.get(position)));
                }
                catch (FileNotFoundException error)
                {
                    error.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        BottomSheetBehavior mBottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.manual_container));
        if (event.getAction() == MotionEvent.ACTION_UP)
        {
            if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
            {
                Rect outRect = new Rect();
                findViewById(R.id.manual_container).getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    return true;
                }
            }
            else if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
            {
                Rect outRect = new Rect();
                findViewById(R.id.manual_container).getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    return true;
                }
            }
        }
        return super.onTouchEvent(event);
    }
}
