/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.manual.python;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.global.server.registry.ServerRegistry;
import low.minicode.jna.PtyLibrary;
import low.minicode.wrappers.data.DualDataStructure2;

public class PydocActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pydoc);
        while (ServerRegistry.pydocServer == null || PtyLibrary.LibC.makeInstance().kill((int) Library.getPidOfProcess(ServerRegistry.pydocServer.getA()), 0) != 0)
        {
            try
            {
                int port = Library.getAvaliablePort(Library.range(32146, 39214));
                if (port == -1)
                {
                    finish();
                }
                Log.d(getClass().getName(), Arrays.toString(new String[] {Library.getPython3Executable().getAbsolutePath(), Library.getPython3Prefix() + "/bin/pydoc3.7", "-p", String.valueOf(port)}));
                Process p = Runtime.getRuntime().exec(new String[] {Library.getPython3Executable().getAbsolutePath(), Library.getPython3Prefix() + "/bin/pydoc3.7", "-p", String.valueOf(port)});
                ServerRegistry.pydocServer = new DualDataStructure2<>(p, port);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    BufferedReader errorStream = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        while (true)
                        {
                            Log.d("PydocServer", inputStream.readLine());
                        }
                    });
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        while (true)
                        {
                            Log.d("PydocServer", errorStream.readLine());
                        }
                    });
                });
            }
            catch (IOException e)
            {
                finish();
            }
        }
        Log.d(getClass().getName(), "Pydoc: " + ServerRegistry.pydocServer.getB());
        ((WebView) findViewById(R.id.pydoc)).getSettings().setJavaScriptEnabled(true);
        ((WebView) findViewById(R.id.pydoc)).setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                ((TextView) findViewById(R.id.pydoc_title)).setText(view.getTitle());
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)
            {
                view.loadUrl(request.getUrl().toString());
                return true;
            }
        });
        new Handler().postDelayed(() -> ((WebView) findViewById(R.id.pydoc)).loadUrl(String.format("http://localhost:%d/", ServerRegistry.pydocServer.getB())), 2420);
    }

    @Override
    public void onBackPressed()
    {
        if (((WebView) findViewById(R.id.pydoc)).canGoBack())
        {
            ((WebView) findViewById(R.id.pydoc)).goBack();
            return;
        }
        super.onBackPressed();
    }
}
