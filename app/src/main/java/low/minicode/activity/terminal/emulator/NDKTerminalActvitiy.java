/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.terminal.emulator;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Arrays;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.jna.JNAHelper;
import low.minicode.jna.PtyLibWrap;
import low.minicode.jna.PtyLibrary;
import low.minicode.terminal.global.pty.GlobalPty;
import low.minicode.terminal.task.AsyncTerminalTask;
import low.minicode.terminal.view.TerminalView;
import low.minicode.wrappers.terminal.NativePty;

import static android.content.res.Configuration.KEYBOARDHIDDEN_NO;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NDKTerminalActvitiy extends ThemedAppCompatActivity
{
    public short width, length;
    public int pid = 0;
    public NativePty nativePty;
    public PtyLibrary libraryInstance;
    public TerminalView terminalView;
    public AsyncTerminalTask task;
    public Chip chip, tab, export;
    public String cwd;
    public ImageButton left, right, up, down;
    public LinearLayout keypad;
    public View led1, led2, led3, led4, contentView;
    private boolean keypadShown = false;
    private boolean waiting = false;
    private boolean control;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        System.gc();
        this.setContentView(R.layout.activity_ndkterminal_actvitiy);
        chip = findViewById(R.id.ctrl);
        tab = findViewById(R.id.tab);
        export = findViewById(R.id.export_session);
        chip.setChecked(false);
        chip.setOnCheckedChangeListener((buttonView, isChecked) -> control = isChecked);
        left = findViewById(R.id.left);
        right = findViewById(R.id.right);
        up = findViewById(R.id.up);
        down = findViewById(R.id.down);
        keypad = findViewById(R.id.terminal_keypad);
        led1 = findViewById(R.id.term_led1);
        led2 = findViewById(R.id.term_led2);
        led3 = findViewById(R.id.term_led3);
        led4 = findViewById(R.id.term_led4);
        contentView = getWindow().getDecorView().findViewById(android.R.id.content);
        led1.setVisibility(View.INVISIBLE);
        led2.setVisibility(View.INVISIBLE);
        led3.setVisibility(View.INVISIBLE);
        led4.setVisibility(View.INVISIBLE);
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(() ->
        {
            Rect r = new Rect();
            contentView.getWindowVisibleDisplayFrame(r);
            int screenHeight = contentView.getRootView().getHeight();
            int keypadHeight = screenHeight - r.bottom;
            if (keypadHeight > screenHeight * 0.15)
            {
                ((FloatingActionButton) findViewById(R.id.keyboard_button)).setImageResource(R.drawable.ic_keyboard_hide_black_24dp);
                ((FloatingActionButton) findViewById(R.id.keyboard_button)).setSupportImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.primary_device_default_light)));
            }
            else
            {
                ((FloatingActionButton) findViewById(R.id.keyboard_button)).setImageResource(R.drawable.ic_keyboard_outline);
                ((FloatingActionButton) findViewById(R.id.keyboard_button)).setSupportImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.primary_device_default_light)));
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            chip.setFocusable(View.NOT_FOCUSABLE);
            tab.setFocusable(View.NOT_FOCUSABLE);
            export.setFocusable(View.NOT_FOCUSABLE);
        }
        else
        {
            chip.setFocusable(false);
            tab.setFocusable(false);
            export.setFocusable(false);
        }
        this.libraryInstance = PtyLibrary.makeInstance("P");
        this.findViewById(R.id.keyboard_button).setOnClickListener(v ->
        {
            this.keypadShown = !this.keypadShown;
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            if (!this.keypadShown)
            {
                ((FloatingActionButton) this.findViewById(R.id.keyboard_button)).setImageResource(R.drawable.ic_keyboard_black_24dp);
            }
            else
            {
                ((FloatingActionButton) this.findViewById(R.id.keyboard_button)).setImageResource(R.drawable.ic_keyboard_hide_black_24dp);
            }
        });
        while (GlobalPty.myPty == null)
        {
            GlobalPty.myPty = JNAHelper.INSTANCE.open_pty((short) 100, (short) 100);
            JNAHelper.INSTANCE.set_tty_erase((byte) 127, GlobalPty.myPty);
        }
        if (this.getIntent().getStringExtra("binary") == null)
        {
            finish();
            return;
        }
        findViewById(R.id.up_down_vol_hint).setVisibility(getSharedPreferences("low.minicode.hints", MODE_PRIVATE).getBoolean("ud_vol", false) ? GONE : VISIBLE);
        findViewById(R.id.ok).setOnClickListener(v ->
        {
            findViewById(R.id.up_down_vol_hint).setVisibility(GONE);
            getSharedPreferences("low.minicode.hints", MODE_PRIVATE).edit().putBoolean("ud_vol", true).apply();
        });
        String binary = this.getIntent().getStringExtra("binary");
        String[] binaries = (this.getIntent().getStringArrayExtra("binaryArray") == null ? new String[] {binary} : this.getIntent().getStringArrayExtra("binaryArray"));
        this.cwd = getIntent().getStringExtra("cwd");
        if (cwd == null)
        {
            cwd = getFilesDir().getAbsolutePath();
        }
        this.terminalView = findViewById(R.id.textviewterm);
        this.terminalView.setOnLongClickListener(v ->
        {
            MaterialAlertDialogBuilder alertDialogBuilder = new MaterialAlertDialogBuilder(this);
            alertDialogBuilder.setItems(R.array.term_options, (dialog, which) ->
            {
                if (which == 0)
                {
                    CharSequence s = ((ClipboardManager) getSystemService(CLIPBOARD_SERVICE)).getText();
                    if (s != null)
                    {
                        JNAHelper.INSTANCE.pty_write(nativePty, new JNAHelper.SizeT(s.toString().getBytes().length), s.toString().getBytes());
                    }
                }
            });
            alertDialogBuilder.show();
            return true;
        });
        this.nativePty = GlobalPty.myPty;
        TextView tty, exec;
        tty = findViewById(R.id.terminal_tty);
        exec = findViewById(R.id.terminal_executable);
        tty.setText(this.nativePty.slave_path);
        exec.setText(binary);
        this.pid = JNAHelper.INSTANCE.exec_pty(this.nativePty, binary, binaries, new String[] {String.format("LD_LIBRARY_PATH=%s:%s", getApplicationInfo().nativeLibraryDir, Library.getPrefixDir() + "/lib"), "TERM=vt100"}, cwd);
        JNAHelper.INSTANCE.pty_setutf8(this.nativePty);
        this.task = new AsyncTerminalTask(this.terminalView, this.nativePty, this.pid, this);
        this.task.execute();
        this.terminalView.setEchoQueryHandler(() -> JNAHelper.INSTANCE.pty_is_echo(nativePty) != 0);
        if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1) != -1)
        {
            tty.setTextColor(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1));
            exec.setTextColor(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1));
            terminalView.setTextColor(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1));
            findViewById(R.id.terminal).setBackgroundTintList(ColorStateList.valueOf(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_background", -1)));
            up.setImageTintList(ColorStateList.valueOf(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1)));
            down.setImageTintList(ColorStateList.valueOf(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1)));
            left.setImageTintList(ColorStateList.valueOf(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1)));
            right.setImageTintList(ColorStateList.valueOf(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("term_color_text", -1)));
        }
        up.setOnClickListener(v ->
        {
            byte[] vals = "\u001b[A".getBytes();
            JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(3), vals);
        });
        down.setOnClickListener(v ->
        {
            byte[] vals = "\u001b[B".getBytes();
            JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(3), vals);
        });
        left.setOnClickListener(v ->
        {
            byte[] vals = "\u001b[D".getBytes();
            JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(3), vals);
        });
        right.setOnClickListener(v ->
        {
            byte[] vals = "\u001b[C".getBytes();
            JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(3), vals);
        });
        terminalView.setOnResizeHandler((visibleLines, horizontal) ->
        {
            JNAHelper.INSTANCE.resize_pty(new JNAHelper.WindowSize((short) visibleLines, (short) horizontal), nativePty);
            Log.d(getClass().getName(), String.format("resize handler: l%dc%d", visibleLines, horizontal));
        });
        export.setOnClickListener(v ->
        {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.putExtra(Intent.EXTRA_TEXT, terminalView.getCellGrid().toString());
            i.setType("text/plain");
            startActivity(Intent.createChooser(i, getString(R.string.open_with)));
        });
        tab.setOnClickListener(v -> JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(1), new byte[] {'\t'}));
        findViewById(R.id.keyboard_button).setFocusable(false);
    }

    @Override
    protected void onDestroy()
    {
        PtyLibrary.LibC cLib = PtyLibrary.LibC.makeInstance();
        if (PtyLibWrap.kill(this.pid, 0) >= 0)
        {
            cLib.kill(this.pid, 9);
        }
        this.libraryInstance.closepty(this.nativePty);
        GlobalPty.myPty = null;
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent)
    {
        terminalView.requestFocus();
        Log.d(getClass().getName(), "onKeyDown: init");
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            this.onBackPressed();
            return true;
        }
        else
        {
            if (KeyEvent.isModifierKey(keyCode))
            {
                if (keyCode == KeyEvent.KEYCODE_CTRL_LEFT || keyCode == KeyEvent.KEYCODE_CTRL_RIGHT)
                {
                    control = !control;
                    chip.setChecked(control);
                }
                return false;
            }
            else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP)
            {
                byte[] vals = "\u001b[A".getBytes();
                JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(3), vals);
                return true;
            }
            else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN)
            {
                byte[] vals = "\u001b[B".getBytes();
                JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(3), vals);
                return true;
            }
            else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ESCAPE)
            {
                JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(2), new byte[] {27});
                return true;
            }
            else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DEL)
            {
                JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(1), new byte[] {127});
                Log.d(getClass().getName(), "onKeyDown: key event: backspace");
                return true;
            }
            else if (control)
            {
                control = false;
                chip.setChecked(false);
                byte cha = (byte) (keyEvent.getUnicodeChar() & 0x1f);
                JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(1), new byte[] {cha});
                return true;
            }
            byte[] cha = new String(Character.toChars(keyEvent.getUnicodeChar(keyEvent.getMetaState()))).getBytes();
            Log.d(getClass().getName(), "onKeyDown: key event: bytes: " + Arrays.toString(cha));
            Log.d(getClass().getName(), "onKeyDown: recieved char " + new String(Character.toChars(keyEvent.getUnicodeChar(keyEvent.getMetaState()))));
            JNAHelper.INSTANCE.pty_write(this.nativePty, new JNAHelper.SizeT(cha.length), cha);
            return true;
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        ((FloatingActionButton) findViewById(R.id.keyboard_button)).setImageResource(newConfig.keyboardHidden == KEYBOARDHIDDEN_NO ? R.drawable.ic_keyboard_outline : R.drawable.ic_keyboard_hide_black_24dp);
        ((FloatingActionButton) findViewById(R.id.keyboard_button)).setSupportImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.primary_device_default_light)));
    }

    @Override
    public void onBackPressed()
    {
        this.task.cancel(true);
        if (PtyLibWrap.kill(this.pid, 0) >= 0)
        {
            MaterialAlertDialogBuilder d = new MaterialAlertDialogBuilder(this);
            d.setMessage(waiting ? R.string.proc_still_running : R.string.killproc_force_long);
            d.setPositiveButton(waiting ? R.string.killproc : R.string.terminate, (dialog, which) ->
            {
                if (!waiting)
                {
                    PtyLibrary.LibC cLib = PtyLibrary.LibC.makeInstance();
                    cLib.kill(this.pid, 1);
                    terminalView.append("\nWaiting for process to complete...", new TerminalView.CellState(-1, -1, false));
                    waiting = true;
                    GlobalPty.myPty = null;
                }
                else
                {
                    PtyLibrary.LibC cLib = PtyLibrary.LibC.makeInstance();
                    cLib.kill(this.pid, 9);
                    terminalView.append("\nKilled process!", new TerminalView.CellState(getResources().getColor(R.color.holo_red_dark), -1, false));
                    waiting = true;
                    this.libraryInstance.closepty(this.nativePty);
                    GlobalPty.myPty = null;
                }
            }).show();
        }
        else
        {
            super.onBackPressed();
        }
    }
}
