/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.terminal.preferences;

import android.animation.Animator;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class TerminalColorsActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal_colors);
        setChecked(false);
    }

    public void tap(View v)
    {
        SharedPreferences.Editor editor = getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit();
        if (v.getId() == R.id.terminal_style_sepia)
        {
            editor.putString("term_color", "sepia");
            editor.putInt("term_color_background", getResources().getColor(R.color.sepia));
            editor.putInt("term_color_text", getResources().getColor(R.color.md_black_1000));
        }
        else if (v.getId() == R.id.terminal_style_black_white)
        {
            editor.putString("term_color", "bw");
            editor.putInt("term_color_background", getResources().getColor(R.color.md_black_1000));
            editor.putInt("term_color_text", getResources().getColor(R.color.primary_device_default_light));
        }
        else if (v.getId() == R.id.terminal_style_glass)
        {
            editor.putString("term_color", "glass");
            editor.putInt("term_color_background", getResources().getColor(R.color.glass));
            editor.putInt("term_color_text", getResources().getColor(R.color.primary_device_default_light));
        }
        else if (v.getId() == R.id.terminal_style_paper)
        {
            editor.putString("term_color", "paper");
            editor.putInt("term_color_background", getResources().getColor(R.color.primary_device_default_light));
            editor.putInt("term_color_text", getResources().getColor(R.color.md_black_1000));
        }
        else if (v.getId() == R.id.terminal_style_retro)
        {
            editor.putString("term_color", "retro");
            editor.putInt("term_color_background", getResources().getColor(R.color.primary_dark_device_default_dark));
            editor.putInt("term_color_text", getResources().getColor(R.color.md_green_800));
        }
        else
        {
            editor.remove("term_color");
            editor.remove("term_color_background");
            editor.remove("term_color_text");
        }
        editor.apply();
        setChecked(true);
    }

    private void setChecked(boolean anim)
    {
        View v;
        String s = getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("term_color", "none");
        int[] ids = new int[] {R.id.terminal_style_sepia, R.id.terminal_style_paper, R.id.terminal_style_glass, R.id.terminal_style_black_white, R.id.terminal_style_default, R.id.terminal_style_retro};
        for (int iter : ids)
        {
            View v2 = findViewById(iter);
            for (int i = 0; i <= ((ViewGroup) v2).getChildCount(); ++i)
            {
                if (((ViewGroup) v2).getChildAt(i) instanceof ImageView)
                {
                    ((ImageView) ((ViewGroup) v2).getChildAt(i)).setImageDrawable(null);
                }
                if (((ViewGroup) v2).getChildAt(i) instanceof TextView)
                {
                    ((ViewGroup) v2).getChildAt(i).setVisibility(View.VISIBLE);
                }
            }
        }
        switch (s)
        {
            case "sepia":
            {
                v = findViewById(R.id.terminal_style_sepia);
                break;
            }
            case "bw":
            {
                v = findViewById(R.id.terminal_style_black_white);
                break;
            }
            case "glass":
            {
                v = findViewById(R.id.terminal_style_glass);
                break;
            }
            case "paper":
            {
                v = findViewById(R.id.terminal_style_paper);
                break;
            }
            case "none":
            {
                v = findViewById(R.id.terminal_style_default);
                break;
            }
            case "retro":
            {
                v = findViewById(R.id.terminal_style_retro);
                break;
            }
            default:
            {
                throw new UnsupportedOperationException("no view");
            }
        }
        ImageView imageView = null;
        for (int i = 0; i <= ((ViewGroup) v).getChildCount(); ++i)
        {
            if (((ViewGroup) v).getChildAt(i) instanceof ImageView)
            {
                imageView = (ImageView) ((ViewGroup) v).getChildAt(i);
            }
            if (((ViewGroup) v).getChildAt(i) instanceof TextView)
            {
                if (imageView == null)
                {
                    throw new UnsupportedOperationException("no view");
                }
                Animator m = null;
                if (anim)
                {
                    m = ViewAnimationUtils.createCircularReveal(imageView, imageView.getWidth() / 2, imageView.getHeight() / 2, 0, getResources().getDrawable(R.drawable.ic_check_white).getMinimumWidth());
                    m.setDuration(420);
                }
                imageView.setImageResource(R.drawable.ic_check_white);
                imageView.setImageTintList(ColorStateList.valueOf(((TextView) ((ViewGroup) v).getChildAt(i)).getTextColors().getDefaultColor()));
                if (anim)
                {
                    m.start();
                }
                ((ViewGroup) v).getChildAt(i).setVisibility(View.INVISIBLE);
            }
        }
    }
}
