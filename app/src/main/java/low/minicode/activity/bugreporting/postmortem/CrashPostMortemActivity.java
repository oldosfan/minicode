/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.bugreporting.postmortem;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.ArrayList;

import low.minicode.Library;
import low.minicode.LogReader;
import low.minicode.Profiler;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.wrappers.data.StrongReference;
import low.minicode.wrappers.data.mortem.CrashReport;

public class CrashPostMortemActivity extends ThemedAppCompatActivity
{
    String logcat;
    TextView mLogcat, mStack, mSysInfo;
    ImageButton mDone, mViewStuff, mViewAddit;
    CardView mContent;
    ScrollView mReport;
    ProgressBar mProgressBar;
    EditText mExtraInfo;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d("CrashInspector", "onCreate: debug");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_post_mortem);
        try
        {
            Library.vibrateError();
            ArrayList<Throwable> throwableArrayList = (ArrayList<Throwable>) getIntent().getSerializableExtra("throwables");
            if (throwableArrayList == null)
            {
                finish();
                return;
            }
            StringBuilder stack = new StringBuilder();
            for (Throwable iter : throwableArrayList)
            {
                stack.append(ExceptionUtils.getStackTrace(iter)).append("\n");
            }
            mLogcat = findViewById(R.id.crash_report_logcat);
            mStack = findViewById(R.id.crash_report_stack);
            mSysInfo = findViewById(R.id.crash_report_sysinfo);
            mDone = findViewById(R.id.post_mortem_ok);
            mViewStuff = findViewById(R.id.view_report_content);
            mContent = findViewById(R.id.post_mortem_report_view);
            mProgressBar = findViewById(R.id.crash_report_progressbar);
            mViewAddit = findViewById(R.id.view_crash_report_info_field);
            mExtraInfo = findViewById(R.id.crash_report_additional_information);
            mStack.setTextColor(getResources().getColor(R.color.red));
            mStack.setText(stack.toString());
            mReport = findViewById(R.id.report);
            mViewStuff.setOnClickListener(v ->
            {
                if (mReport.getVisibility() == View.GONE)
                {
                    mReport.setVisibility(View.VISIBLE);
                    mViewStuff.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }
                else
                {
                    mReport.setVisibility(View.GONE);
                    mViewStuff.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
            });
            mSysInfo.setText(getResources().getConfiguration().toString() + "\n" + System.getProperties().toString() + "\n" + Profiler.getInfoAsString());
            mViewAddit.setOnClickListener(v ->
            {
                if (mExtraInfo.getVisibility() == View.GONE)
                {
                    mExtraInfo.setVisibility(View.VISIBLE);
                    mViewAddit.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }
                else
                {
                    mExtraInfo.setVisibility(View.GONE);
                    mViewAddit.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
            });
            mDone.setOnClickListener(v ->
            {
                mProgressBar.setIndeterminate(true);
                mDone.setVisibility(View.GONE);
                CrashReport report = new CrashReport(stack.toString(), logcat, getResources().getConfiguration().toString() + "\n" + System.getProperties().toString() + "\n" + Profiler.getInfoAsString(), mExtraInfo.getText().toString());
                Thread t = new Thread()
                {
                    @Override
                    public void run()
                    {
                        super.run();
                        report.sendBugReport(() -> runOnUiThread(() -> finish()));
                    }
                };
                t.start();
            });
            new GrabLogcatTask(this).execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            finish();
        }
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }

    private static class GrabLogcatTask extends AsyncTask<ProgressBar, Void, String>
    {
        StrongReference<CrashPostMortemActivity> activity = new StrongReference<>();

        public GrabLogcatTask(CrashPostMortemActivity activity)
        {
            this.activity.set(activity);
        }

        @Override
        protected String doInBackground(ProgressBar... progressBars)
        {
            return new LogReader().getLog().toString();
        }

        @Override
        protected void onPostExecute(String s)
        {
            activity.get().mProgressBar.setProgress(0);
            activity.get().mProgressBar.setIndeterminate(false);
            activity.get().logcat = s;
            activity.get().mLogcat.setText(s.length() > 10240 ? s.substring(0, 10240) + "...: truncated to 10240" : s);
            activity.clear();
            super.onPostExecute(s);
        }
    }
}
