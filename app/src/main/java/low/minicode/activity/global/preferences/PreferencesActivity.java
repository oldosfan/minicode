/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.global.preferences;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.Preference;
import low.minicode.PreferenceTypes;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.editor.preferences.EditorSetFontActivity;
import low.minicode.activity.gitlab.integration.GitlabLoginHelperActivity;
import low.minicode.activity.global.libraries.LibraryManagementActivity;
import low.minicode.activity.global.preferences.autocomplete.AddAutocompleteActivity;
import low.minicode.activity.main.MainPageActivity;
import low.minicode.activity.terminal.preferences.TerminalColorsActivity;
import low.minicode.adapter.global.preferences.PreferenceListAdapter;

public class PreferencesActivity extends ThemedAppCompatActivity
{
    RecyclerView recyclerView;
    PreferenceListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
        whitify();
        //noinspection deprecation
        adapter = new PreferenceListAdapter(Arrays.asList(new Preference(getString(R.string.auto_save), getString(R.string.auto_save_files_tell), "auto_save", null, true, PreferenceTypes.TYPE_CHECKBOX, getResources().getDrawable(R.drawable.ic_save_black_24dp)), new Preference(getString(R.string.auto_show_keyboard), getString(R.string.auto_kbd_2), "auto_show_keyboard", null, false, PreferenceTypes.TYPE_CHECKBOX, getResources().getDrawable(R.drawable.ic_keyboard_outline)), new Preference(getString(R.string.auto_check_updates), getString(R.string.will_be_update_notified), "auto_update", null, true, PreferenceTypes.TYPE_CHECKBOX, getResources().getDrawable(R.drawable.ic_system_update_alt_black_24dp)), new Preference(getString(R.string.allow_apps_to_edit_files), getString(R.string.apps_allow_open), "allow_edit", null, false, PreferenceTypes.TYPE_CHECKBOX, getResources().getDrawable(R.drawable.ic_call_merge_black_24dp)), new Preference(getString(R.string.haptic_feedback), getString(R.string.mc_provide_haptic), "haptic", null, true, PreferenceTypes.TYPE_CHECKBOX, Library::vibrateSuccess, getResources().getDrawable(R.drawable.ic_vibration_black_24dp)), new Preference(getString(R.string.night_mode), getString(R.string.dark_theme_will_black), "dark_night", null, false, PreferenceTypes.TYPE_CHECKBOX, () ->
        {
            runOnUiThread(() ->
            {
                MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(this);
                b.setMessage(R.string.will_now_exit);
                b.setCancelable(false);
                b.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                    Intent mStartActivity = new Intent(this.getApplicationContext(), MainPageActivity.class);
                    int mPendingIntentId = 123456;
                    PendingIntent mPendingIntent = PendingIntent.getActivity(this.getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                    AlarmManager mgr = (AlarmManager) this.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                    finishAffinity();
                    System.exit(0);
                });
                b.show();
            });
        }, getResources().getDrawable(R.drawable.ic_weather_night)), new Preference(getString(R.string.terminal_style), getString(R.string.term_use_selected_style), v -> startActivity(new Intent(this, TerminalColorsActivity.class)), getResources().getDrawable(R.drawable.ic_colorize_black_24dp)), new Preference(getString(R.string.manage_custom_libraries), getString(R.string.minicode_will_link_executables_with_given_libs), v -> startActivity(new Intent(this, LibraryManagementActivity.class)), getResources().getDrawable(R.drawable.ic_library_books_black_24dp)), new Preference(getString(R.string.clear_update_cache), getString(R.string.will_remove_avail_update_info), v ->
        {
            getSharedPreferences("low.minicode.updater", MODE_PRIVATE).edit().remove("available_version").remove("available_version_fancy").apply();
            Library.vibrateSuccess();
            Snackbar.make(v, R.string.update_cache_cleared, Snackbar.LENGTH_SHORT).show();
        }, getResources().getDrawable(R.drawable.ic_cached_black_24dp)), new Preference(getString(R.string.login_to_gitlab), getString(R.string.generate_an_oauth2_token), v ->
        {
            startActivity(new Intent(this, GitlabLoginHelperActivity.class));
        }, getResources().getDrawable(R.drawable.ic_gitlab_icon_1_color_white_rgb)), new Preference(getString(R.string.custom_autocomplete_entries), getString(R.string.add_custom_entries_for_autocomplete), v -> startActivity(new Intent(this, AddAutocompleteActivity.class)), getResources().getDrawable(R.drawable.ic_edit_black_24dp)), new Preference(getString(R.string.editor_font), getString(R.string.editor_displayed_in_selected_font), v -> startActivity(new Intent(this, EditorSetFontActivity.class)), getResources().getDrawable(R.drawable.ic_font_download_black_24dp)), new Preference(getString(R.string.check_updates_over), getString(R.string.minicode_will_only_check_update_over), "update_network_override", new String[] {getString(R.string.wifi_only), getString(R.string.wifi_and_mobile_data)}, 0, PreferenceTypes.TYPE_DROPDOWN, getResources().getDrawable(R.drawable.ic_signal_wifi_3_bar_black_24dp)), Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? new Preference(getString(R.string.lang), getString(R.string.override_lang), "language_override", new String[] {"System default/系统默认", "English", "简体中文"}, 0, PreferenceTypes.TYPE_DROPDOWN, () ->
        {
            runOnUiThread(this::recreate);
        }, getResources().getDrawable(R.drawable.ic_translate)) : new Preference(getString(R.string.lang), getString(R.string.old_sys), v ->
        {
        }, getResources().getDrawable(R.drawable.ic_translate)), new Preference(getString(R.string.theme), getString(R.string.displayed_in_theme), "theme", new String[] {getString(R.string.sys_def), getString(R.string.dark), getString(R.string.light), getString(R.string.auto_by_time)}, 0, PreferenceTypes.TYPE_DROPDOWN, () ->
        {
            if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("theme", 0) == 0)
            {
                runOnUiThread(() ->
                {
                    MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(this);
                    b.setMessage(R.string.will_now_exit);
                    b.setCancelable(false);
                    b.setPositiveButton(R.string.ok, (dialog, which) ->
                    {
                        Intent mStartActivity = new Intent(this.getApplicationContext(), MainPageActivity.class);
                        int mPendingIntentId = 123456;
                        PendingIntent mPendingIntent = PendingIntent.getActivity(this.getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) this.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                        finishAffinity();
                        System.exit(0);
                    });
                    b.show();
                });
            }
            else
            {
                recreate();
            }
        }, getResources().getDrawable(R.drawable.ic_theme_light_dark)), new Preference(getString(R.string.upd_check_frequency), getString(R.string.mc_will_check_for_updates_once), "update_interval", new String[] {getString(R.string.per_one), getString(R.string.per_2), getString(R.string.per_4), getString(R.string.per_8)}, 1, PreferenceTypes.TYPE_DROPDOWN, getResources().getDrawable(R.drawable.ic_av_timer_black_24dp)), new Preference(getString(R.string.nav_menu_direction), getString(R.string.nav_menu_expand_direction), "menu_orientation", new String[] {getString(R.string.horiz), getString(R.string.vertical), getString(R.string.auto_screen_orient)}, 2, PreferenceTypes.TYPE_DROPDOWN, getResources().getDrawable(R.drawable.ic_rotate_left_black_24dp)), new Preference(getString(R.string.font_size), getString(R.string.minicode_will_use_font_size), "font_size", 10, 25, 15, getResources().getDrawable(R.drawable.ic_text_fields_black_24dp)), new Preference(getString(R.string.editor_delay), getString(R.string.editor_will_delay), "save_delay", 250, 5000, 1000, getResources().getDrawable(R.drawable.ic_timelapse_black_24dp)), new Preference(getString(R.string.font), getString(R.string.mc_display_selected_font), "font", new String[] {"A", "B"}, 0, PreferenceTypes.TYPE_DROPDOWN, () -> Minicode.theMinicode.restoreFont(), getResources().getDrawable(R.drawable.ic_font_download_black_24dp))));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.settings_recycler);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }
}
