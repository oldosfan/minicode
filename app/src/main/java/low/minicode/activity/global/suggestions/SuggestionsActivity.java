/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.global.suggestions;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.git.ProjectSuggestionsAdapter;

public class SuggestionsActivity extends ThemedAppCompatActivity
{
    private RecyclerView projects, files;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestions);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        projects = findViewById(R.id.suggested_projects);
        files = findViewById(R.id.suggested_files);
        projects.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        projects.setAdapter(new ProjectSuggestionsAdapter());
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    @Override
    protected void onPostResume()
    {
        super.onPostResume();
        if (projects.getAdapter() != null)
        {
            projects.getAdapter().notifyDataSetChanged();
        }
    }
}
