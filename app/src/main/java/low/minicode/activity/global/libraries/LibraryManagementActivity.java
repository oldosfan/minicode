/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.global.libraries;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.TarExtractorTask;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.global.libraries.LibraryListAdapter;

public class LibraryManagementActivity extends ThemedAppCompatActivity
{
    private static int DOCUMENT_REQUEST_CODE = 28745;
    private RecyclerView libraries;
    private FloatingActionButton add;
    private LibraryListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_library_management);
        libraries = findViewById(R.id.custom_libraries_recyclerview);
        add = findViewById(R.id.add_new_library);
        add.setOnClickListener(v ->
        {
            Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            i.setType("*/*");
            startActivityForResult(i, DOCUMENT_REQUEST_CODE);
        });
        libraries.setLayoutManager(new LinearLayoutManager(this));
        libraries.setAdapter(adapter = new LibraryListAdapter());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DOCUMENT_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            InputStream inputStream;
            try
            {
                inputStream = getContentResolver().openInputStream(data.getData());
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
                return;
            }
            if (!data.getData().toString().endsWith(".tar"))
            {
                MaterialAlertDialogBuilder alertDialogBuilder = new MaterialAlertDialogBuilder(this);
                alertDialogBuilder.setTitle(R.string.error);
                alertDialogBuilder.setMessage(R.string.library_bundle_must_be_uncompressed_tar_archive);
                alertDialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                });
                alertDialogBuilder.show();
            }
            else
            {
                MaterialAlertDialogBuilder inputDialog = new MaterialAlertDialogBuilder(this);
                inputDialog.setTitle(R.string.name);
                EditText editField;
                inputDialog.setView((editField = new EditText(this)));
                editField.setSingleLine();
                inputDialog.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                    try
                    {
                        if (editField.getText().toString().trim().isEmpty())
                        {
                            throw new IOException(getString(R.string.we_need_something_here));
                        }
                        MaterialAlertDialogBuilder wait = new MaterialAlertDialogBuilder(this);
                        wait.setMessage(getString(R.string.extracting_library_s, editField.getText()));
                        wait.setCancelable(false);
                        Dialog extractingNotifier = wait.show();
                        TarExtractorTask tarExtractorTask = new TarExtractorTask(inputStream, new File(Library.getCustomLibDir(), "lib" + editField.getText().toString().trim() + ""));
                        Executors.newSingleThreadExecutor().submit(() ->
                        {
                            try
                            {
                                tarExtractorTask.run();
                                extractingNotifier.dismiss();
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                                MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(this);
                                error.setTitle(R.string.error);
                                error.setMessage(e.getLocalizedMessage());
                                error.setPositiveButton(R.string.ok, (dialog2, which2) ->
                                {
                                });
                                runOnUiThread(error::show);
                            }
                        });
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                        MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(this);
                        error.setTitle(R.string.error);
                        error.setMessage(e.getLocalizedMessage());
                        error.setPositiveButton(R.string.ok, (dialog2, which2) ->
                        {
                        });
                        error.show();
                    }
                });
                inputDialog.show();
            }
        }
    }
}