/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.global.preferences.autocomplete;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.global.preferences.SharedPreferencesSetRemoveAdapter;

public class AddAutocompleteActivity extends ThemedAppCompatActivity
{
    SharedPreferencesSetRemoveAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        String[] things = Arrays.copyOf(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getStringSet("auto_complete_additions", new ConcurrentSkipListSet<>()).toArray(), getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getStringSet("auto_complete_additions", new ConcurrentSkipListSet<>()).toArray().length, String[].class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_autocomplete);
        ImageButton addButton = findViewById(R.id.add_custom_autocomplete);
        addButton.setOnClickListener(v ->
        {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(AddAutocompleteActivity.this);
            mBuilder.setTitle(R.string.add_custom_entries_for_autocomplete);
            EditText editTextWiget = new EditText(AddAutocompleteActivity.this);
            mBuilder.setView(editTextWiget);
            mBuilder.setPositiveButton(R.string.add, (dialog, which) ->
            {
                Set<String> set = getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getStringSet("auto_complete_additions", new ConcurrentSkipListSet<>());
                try
                {
                    if (!set.add(editTextWiget.getText().toString()))
                    {
                        Toast.makeText(this, R.string.entry_already_exists, Toast.LENGTH_LONG).show();
                        return;
                    }
                    getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit().remove("auto_complete_additions").apply();
                    getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit().putStringSet("auto_complete_additions", set).apply();
                    adapter.initializeWithNewData(set);
                }
                catch (IllegalArgumentException e)
                {
                    Toast.makeText(this, R.string.entry_already_exists, Toast.LENGTH_LONG).show();
                }
            });
            mBuilder.show();
        });
        RecyclerView recyclerView = findViewById(R.id.custom_autocomplete_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SharedPreferencesSetRemoveAdapter(Objects.requireNonNull(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getStringSet("auto_complete_additions", new ConcurrentSkipListSet<>())));
        recyclerView.setAdapter(adapter);
    }
}
