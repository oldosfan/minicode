/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.git;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.BatchingProgressMonitor;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.eclipse.jgit.lib.StoredConfig;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.git.BranchListAdapter;
import low.minicode.fragments.git.VCSBranchesFragment;
import low.minicode.fragments.git.VCSChangesFragment;
import low.minicode.fragments.git.VCSCommitsFragment;
import low.minicode.fragments.git.VCSServerFragment;
import low.minicode.fragments.git.VCSSettingsFragment;
import low.minicode.fragments.git.VCSSubmodulesFragment;
import low.minicode.fragments.git.VCSTagsFragment;

public class VersionControlActivity extends ThemedAppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    public String projectName;
    public Repository repository;
    private NavigationView navigationView;
    private View push, pull, pushMarker, pullMarker;
    private ProgressBar pushBar, pullBar, pushSpinner, pullSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_control);
        whitify();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle(R.string.error);
            builder.setMessage(R.string.git_needs_nio);
            builder.setPositiveButton(R.string.ok, ((dialog, which) -> finish()));
            builder.show();
            return;
        }
        projectName = getIntent().getStringExtra("projectName");
        Toolbar toolbar = findViewById(R.id.git_toolbar);
        setSupportActionBar(toolbar);
        push = findViewById(R.id.push_to_origin);
        pull = findViewById(R.id.pull_from_origin);
        pushBar = findViewById(R.id.push_origin_progressbar);
        pullBar = findViewById(R.id.pull_origin_progressbar);
        pushSpinner = findViewById(R.id.push_spinner);
        pullSpinner = findViewById(R.id.pull_spinner);
        pushMarker = findViewById(R.id.push_marker);
        pullMarker = findViewById(R.id.pull_marker);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view_vcs);
        navigationView.getHeaderView(0).findViewById(R.id.vcs_menu_back).setOnClickListener(v -> closeDrawer());
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.git_nav_branches);
        try
        {
            Log.d(getClass().getName(), "onCreate: make repo");
            repository = new RepositoryBuilder().setGitDir(new File(getFilesDir(), projectName + "/.git")).readEnvironment().findGitDir().setMustExist(true).build();
            Log.d(getClass().getName(), "onCreate: successfully made repo");
        }
        catch (RepositoryNotFoundException e0)
        {
            try
            {
                repository = new RepositoryBuilder().setGitDir(new File(getFilesDir(), projectName + "/.git")).readEnvironment().findGitDir().build();
                try
                {
                    repository.create();
                }
                catch (IllegalStateException ignored)
                {
                }
            }
            catch (IOException ignored)
            {
            }
        }
        catch (IOException e1)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle(R.string.error);
            builder.setMessage(R.string.git_init_failed);
            builder.setPositiveButton(R.string.ok, ((dialog, which) -> finish()));
            builder.show();
            Log.d(getClass().getName(), "onCreate: repo init failed (0)");
            return;
        }
        Git git = new Git(repository);
        Bundle args = new Bundle();
        args.putString("projectName", projectName);
        args.putString("unused", "unused");
        setTitle(R.string.branches);
        FragmentManager manager = getFragmentManager();
        VCSBranchesFragment branchesFragment = VCSBranchesFragment.newInstance(projectName, "");
        branchesFragment.setArguments(args);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.vcs_fragment_claim, branchesFragment, "vcs_branches_fragment");
        transaction.commit();
        push.setOnClickListener(v ->
        {
            push.setClickable(false);
            pushMarker.setVisibility(View.INVISIBLE);
            pushSpinner.setVisibility(View.VISIBLE);
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            builder.setItems(R.array.push_options, (dialog2, which) ->
            {
                Handler h = new Handler();
                PushCommand pushCommand = git.push();
                if (which == 0)
                {
                    pushCommand.setPushAll();
                }
                else
                {
                    pushCommand.setPushTags();
                }
                Library.transportCredentials(pushCommand, projectName);
                pushCommand.setProgressMonitor(new BatchingProgressMonitor()
                {
                    @Override
                    protected void onUpdate(String taskName, int workCurr)
                    {
                    }

                    @Override
                    protected void onEndTask(String taskName, int workCurr)
                    {
                    }

                    @Override
                    protected void onUpdate(String taskName, int workCurr, int workTotal, int percentDone)
                    {
                        pushBar.post(() -> pushBar.setProgress(percentDone));
                    }

                    @Override
                    protected void onEndTask(String taskName, int workCurr, int workTotal, int percentDone)
                    {
                    }
                });
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        pushCommand.call();
                    }
                    catch (GitAPIException e)
                    {
                        e.printStackTrace();
                        MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(this);
                        error.setTitle(R.string.push_failed);
                        error.setMessage(e.getLocalizedMessage());
                        error.setPositiveButton(R.string.ok, (dialog3, which1) ->
                        {
                        });
                        h.post(error::show);
                    }
                    finally
                    {
                        h.post(() -> pushBar.setProgress(0));
                        h.post(() -> push.setClickable(true));
                        h.post(() ->
                        {
                            pushMarker.setVisibility(View.VISIBLE);
                            pushSpinner.setVisibility(View.INVISIBLE);
                        });
                    }
                });
            });
            builder.setOnCancelListener(dialog ->
            {
                push.setClickable(true);
                pushMarker.setVisibility(View.VISIBLE);
                pushSpinner.setVisibility(View.INVISIBLE);
            });
            builder.show();
        });
        pull.setOnClickListener(v ->
        {
            pull.setClickable(false);
            pullMarker.setVisibility(View.INVISIBLE);
            pullSpinner.setVisibility(View.VISIBLE);
            Handler h = new Handler();
            PullCommand pullCommand = git.pull();
            Library.transportCredentials(pullCommand, projectName);
            if (repository.getConfig().getString("remote", "origin", "fetch") == null)
            {
                StoredConfig c = repository.getConfig();
                c.setString("remote", "origin", "fetch", "+refs/heads/*:refs/remotes/origin/*");
                try
                {
                    c.save();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            pullCommand.setProgressMonitor(new BatchingProgressMonitor()
            {
                @Override
                protected void onUpdate(String taskName, int workCurr)
                {
                }

                @Override
                protected void onEndTask(String taskName, int workCurr)
                {
                }

                @Override
                protected void onUpdate(String taskName, int workCurr, int workTotal, int percentDone)
                {
                    pullBar.post(() -> pullBar.setProgress(percentDone));
                }

                @Override
                protected void onEndTask(String taskName, int workCurr, int workTotal, int percentDone)
                {
                }
            });
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    PullResult result = pullCommand.call();
                    if (result.getMergeResult().getMergeStatus() == MergeResult.MergeStatus.CONFLICTING)
                    {
                        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this);
                        materialAlertDialogBuilder.setTitle(R.string.merge_conflicts);
                        RecyclerView recyclerView = new RecyclerView(this);
                        materialAlertDialogBuilder.setView(recyclerView);
                        recyclerView.setLayoutManager(new LinearLayoutManager(Minicode.context));
                        recyclerView.setAdapter(new BranchListAdapter.MergeConflictSubAdapter(result.getMergeResult()));
                        materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog1, which1) ->
                        {
                        });
                        h.post(materialAlertDialogBuilder::show);
                    }
                }
                catch (GitAPIException e)
                {
                    e.printStackTrace();
                    MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(this);
                    error.setTitle(R.string.pull_failed);
                    error.setMessage(e.getLocalizedMessage());
                    error.setPositiveButton(R.string.ok, (dialog2, which) ->
                    {
                    });
                    h.post(error::show);
                }
                finally
                {
                    h.post(() -> pullBar.setProgress(0));
                    h.post(() -> pull.setClickable(true));
                    h.post(() ->
                    {
                        pullMarker.setVisibility(View.VISIBLE);
                        pullSpinner.setVisibility(View.INVISIBLE);
                    });
                }
            });
        });
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void closeDrawer()
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if (item == navigationView.getCheckedItem())
        {
            return false;
        }
        if (id == R.id.git_nav_branches)
        {
            setTitle(R.string.branches);
            Bundle args = new Bundle();
            args.putString("projectName", projectName);
            args.putString("unused", "unused");
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Fragment f;
            if ((f = manager.findFragmentByTag("vcs_branches_fragment")) == null)
            {
                f = VCSBranchesFragment.newInstance(projectName, "vcs_branches_fragment");
                f.setArguments(args);
            }
            transaction.replace(R.id.vcs_fragment_claim, f);
            transaction.commit();
        }
        else if (id == R.id.git_nav_commits)
        {
            setTitle(R.string.commits);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("projectName", projectName);
            args.putString("unused", "unused");
            Fragment f;
            if ((f = manager.findFragmentByTag("vcs_commits_fragment")) == null)
            {
                f = VCSCommitsFragment.newInstance(projectName, "vcs_commits_fragment");
                f.setArguments(args);
            }
            transaction.replace(R.id.vcs_fragment_claim, f);
            transaction.commit();
        }
        else if (id == R.id.git_nav_changed)
        {
            setTitle(R.string.changed_commits);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("projectName", projectName);
            args.putString("unused", "unused");
            Fragment f;
            if ((f = manager.findFragmentByTag("vcs_changes_fragment")) == null)
            {
                f = VCSChangesFragment.newInstance(projectName, "vcs_changes_fragment");
                f.setArguments(args);
            }
            transaction.replace(R.id.vcs_fragment_claim, f);
            transaction.commit();
        }
        else if (id == R.id.git_nav_tags)
        {
            setTitle(R.string.tags);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("projectName", projectName);
            args.putString("unused", "unused");
            Fragment f;
            if ((f = manager.findFragmentByTag("vcs_tags_fragment")) == null)
            {
                f = VCSTagsFragment.newInstance(projectName, "vcs_tags_fragment");
                f.setArguments(args);
            }
            transaction.replace(R.id.vcs_fragment_claim, f);
            transaction.commit();
        }
        else if (id == R.id.git_nav_submodules)
        {
            setTitle(R.string.submodules);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("projectName", projectName);
            args.putString("unused", "unused");
            Fragment f;
            if ((f = manager.findFragmentByTag("vcs_submodules_fragment")) == null)
            {
                f = VCSSubmodulesFragment.newInstance(projectName, "vcs_submodules_fragment");
                f.setArguments(args);
            }
            transaction.replace(R.id.vcs_fragment_claim, f);
            transaction.commit();
        }
        else if (id == R.id.git_nav_settings)
        {
            setTitle(R.string.settings);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("projectName", projectName);
            args.putString("unused", "unused");
            Fragment f;
            if ((f = manager.findFragmentByTag("vcs_settings_fragment")) == null)
            {
                f = VCSSettingsFragment.newInstance(projectName, "vcs_settings_fragment");
                f.setArguments(args);
            }
            transaction.replace(R.id.vcs_fragment_claim, f);
            transaction.commit();
        }
        else if (id == R.id.git_nav_server)
        {
            setTitle(R.string.server);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            Bundle args = new Bundle();
            args.putString("projectName", projectName);
            args.putString("unused", "unused");
            Fragment f;
            if ((f = manager.findFragmentByTag("vcs_server_fragment")) == null)
            {
                f = VCSServerFragment.newInstance(projectName, "vcs_server_fragment");
                f.setArguments(args);
            }
            transaction.replace(R.id.vcs_fragment_claim, f);
            transaction.commit();
        }
        return true;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }
}
