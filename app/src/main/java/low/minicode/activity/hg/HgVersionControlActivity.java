package low.minicode.activity.hg;

import android.os.Bundle;

import org.tmatesoft.hg.core.HgException;
import org.tmatesoft.hg.core.HgInitCommand;
import org.tmatesoft.hg.core.HgRepositoryNotFoundException;
import org.tmatesoft.hg.repo.HgLookup;
import org.tmatesoft.hg.repo.HgRepository;
import org.tmatesoft.hg.util.CancelledException;

import java.io.File;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class HgVersionControlActivity extends ThemedAppCompatActivity
{
    private File project;
    private HgRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hg_version_control);
        project = (File) getIntent().getSerializableExtra("project");
        if (new File(project, ".git").exists())
        {
            Library.vibrateError();
            Library.displayErrorDialogFromThrowable(this, new Throwable(getString(R.string.project_appears_to_be_git_repo)), true, this::finish);
            return;
        }
        try
        {
            repository = new HgLookup().detect(project);
        }
        catch (HgRepositoryNotFoundException e)
        {
            try
            {
                repository = new HgInitCommand().location(project).execute();
            }
            catch (HgException | CancelledException e1)
            {
                Library.vibrateError();
                Library.displayErrorDialogFromThrowable(this, e1, true, this::finish);
                return;
            }
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return super.onSupportNavigateUp();
    }
}