/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.basic;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import java.util.Locale;

import low.minicode.application.Minicode;
import low.minicode.R;

@SuppressLint("Registered")
public class ThemedAppCompatActivity extends AppCompatActivity
{
    public static final int NIGHT_MODE_AUTO = 0;
    public static final int NIGHT_MODE_DARK = 1;
    public static final int NIGHT_MODE_LIGHT = 2;
    public static final int NIGHT_MODE_TIME = 3;
    public static final int REQUEST_SELF = 935;
    private String currentLanguage;
    private boolean isBlack;
    private int nightMode;

    public static Context updateBaseContextLocale(Context context)
    {
        String language = Minicode.language;
        if (language.isEmpty())
        {
            return context;
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            return updateResourcesLocale(context, locale);
        }
        return updateResourcesLocaleLegacy(context, locale);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResourcesLocale(Context context, Locale locale)
    {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private static Context updateResourcesLocaleLegacy(Context context, Locale locale)
    {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    public static boolean dark(Context c)
    {
        return (c.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        currentLanguage = Minicode.language;
        isBlack = getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getBoolean("dark_night", false);
        nightMode = getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("theme", 0);
        if (nightMode == NIGHT_MODE_DARK)
        {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else if (nightMode == NIGHT_MODE_LIGHT)
        {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        else if (nightMode == NIGHT_MODE_AUTO)
        {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        }
        else if (nightMode == NIGHT_MODE_TIME)
        {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
        }
        try
        {
            int resID = getPackageManager().getActivityInfo(getComponentName(), 0).getThemeResource();
            if (resID == R.style.AppTheme)
            {
                this.setTheme(isDark() && isBlack ? R.style.AppTheme_Black : R.style.AppThemeLOS);
            }
            else if (resID == R.style.AppTheme_NoActionBar)
            {
                this.setTheme(R.style.AppThemeLOS_NoActionBar);
            }
            else if (resID == R.style.FullscreenTheme)
            {
                this.setTheme(R.style.LOSFullscreenTheme);
            }
            else if (resID == R.style.Theme_Transparent_Fullscreen)
            {
                this.setTheme(R.style.Theme_Transparent_LOS);
            }
            else if (resID == R.style.AppTheme_About)
            {
                this.setTheme(R.style.AppTheme_About_LOS);
            }
            else if (resID == R.style.Theme_Transparent_Material)
            {
                this.setTheme(R.style.Theme_Transparent_LOSMaterial);
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);
        if (!shouldNotInterfereWithActionBar())
        {
            View t = getActionBar(getWindow().getDecorView().getRootView());
            if (t == null)
            {
                return;
            }
            t.setBackgroundColor(getResources().getColor(R.color.surface_color));
            whitify();
            if (t instanceof Toolbar)
            {
                ((Toolbar) t).setTitleTextColor(getResources().getColor(R.color.accent_device_default_light));
            }
            else if (t instanceof androidx.appcompat.widget.Toolbar)
            {
                ((androidx.appcompat.widget.Toolbar) t).setTitleTextColor(getResources().getColor(R.color.accent_device_default_light));
            }
        }
    }

    public void whitify()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            getWindow().getDecorView().setSystemUiVisibility(isDark() ? 0 : View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        getWindow().setStatusBarColor(getResources().getColor(!isDark() ? R.color.primary_device_default_light : R.color.primary_device_default_dark));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            if (!isDark())
            {
                getWindow().getDecorView().setSystemUiVisibility(getWindow().getDecorView().getSystemUiVisibility() | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            }
            else
            {
                getWindow().getDecorView().setSystemUiVisibility(getWindow().getDecorView().getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            }
            getWindow().setNavigationBarColor(getColor(!isDark() ? R.color.primary_device_default_light : R.color.primary_device_default_dark));
        }
    }

    public ViewGroup getActionBar(View view)
    {
        if (view instanceof ViewGroup)
        {
            ViewGroup viewGroup = (ViewGroup) view;
            if (viewGroup instanceof androidx.appcompat.widget.Toolbar)
            {
                return viewGroup;
            }
            for (int i = 0; i < viewGroup.getChildCount(); i++)
            {
                ViewGroup actionBar = getActionBar(viewGroup.getChildAt(i));
                if (actionBar != null)
                {
                    return actionBar;
                }
            }
        }
        return null;
    }

    @Override
    public void setContentView(int view)
    {
        super.setContentView(view);
        if (!shouldNotInterfereWithActionBar())
        {
            View t = getActionBar(getWindow().getDecorView().getRootView());
            if (t == null)
            {
                return;
            }
            Log.d(getClass().getName(), "T reached");
            t.setBackgroundColor(getResources().getColor(R.color.surface_color));
            whitify();
            if (t instanceof Toolbar)
            {
                ((Toolbar) t).setTitleTextColor(getResources().getColor(R.color.accent_device_default_light));
            }
            else if (t instanceof androidx.appcompat.widget.Toolbar)
            {
                ((androidx.appcompat.widget.Toolbar) t).setTitleTextColor(getResources().getColor(R.color.accent_device_default_light));
            }
            if (getSupportActionBar() != null)
            {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_blue);
            }
            if (getActionBar() != null)
            {
                getActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_blue);
            }
        }
    }

    public boolean isDark()
    {
        return (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        Context c = updateBaseContextLocale(base);
        super.attachBaseContext(c == null ? base : c);
    }

    @Override
    protected void onPostResume()
    {
        super.onPostResume();
        if (nightMode != getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("theme", 0))
        {
            recreate();
        }
        if (!currentLanguage.equals(Minicode.getLocaleCode(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("language_override", 0))))
        {
            Minicode.language = Minicode.getLocaleCode(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("language_override", 0));
            recreate();
        }
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        if (nightMode != getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("theme", 0))
        {
            new Handler().postDelayed(this::recreate, 10);
        }
    }

    public boolean isBlack()
    {
        return isBlack;
    }

    public void finish(View v)
    {
        finish();
    }

    public @Nullable
    View getContent()
    {
        return getWindow().getDecorView().findViewById(android.R.id.content);
    }

    public boolean shouldNotInterfereWithActionBar()
    {
        return false;
    }

    public abstract class Module
    {
        private Activity parentAcivity;

        protected Module(@Nullable Bundle savedInstanceState, @NonNull Activity parentAcivity)
        {
            this.parentAcivity = parentAcivity;
            onCreate(savedInstanceState);
        }

        public Activity getParentAcivity()
        {
            return parentAcivity;
        }

        protected abstract void onCreate(Bundle savedInstanceState);
    }
}
