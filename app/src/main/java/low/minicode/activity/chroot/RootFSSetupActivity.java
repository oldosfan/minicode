/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.chroot;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewAnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.tukaani.xz.LZMAInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.Downloader;
import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.TarExtractorTask;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;

public class RootFSSetupActivity extends ThemedAppCompatActivity
{
    private Module currentModule;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        currentModule = new ModuleStageA(savedInstanceState, this);
        if (new File(Library.getChrootDir(), ".download_complete").exists())
        {
            Intent i = new Intent(this, NDKTerminalActvitiy.class);
            i.putExtra("binary", Library.getRoot());
            i.putExtra("binaryArray", Library.getLibMountInvocation());
            Log.d(getClass().getName(), "onCreate: " + Arrays.toString(Library.getLibMountInvocation()));
            startActivity(i);
        }
    }

    private class ModuleStageA extends Module
    {
        private MaterialButton nextButton;

        private ModuleStageA(@Nullable Bundle savedInstanceState, @NonNull Activity parentActivity)
        {
            super(savedInstanceState, parentActivity);
        }

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            setContentView(R.layout.activity_root_fssetup);
            if (!Library.hasRoot())
            {
                MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(getApplicationContext());
                error.setCancelable(false);
                error.setTitle(R.string.device_not_rooted);
                error.setMessage(R.string.need_root_for_chroot);
                error.setPositiveButton(R.string.ok, (dialog, which) -> finish());
                return;
            }
            this.nextButton = findViewById(R.id.chroot_next);
            this.nextButton.setOnClickListener(v -> currentModule = new ModuleStageB(savedInstanceState, getParentAcivity(), (int) nextButton.getX(), (int) nextButton.getY()));
        }
    }

    private class ModuleStageB extends Module
    {
        private final int startX;
        private final int startY;
        private MaterialButton cancelButton;
        private TextView downloadUri, downloadProgress;
        private ProgressBar progressBar;
        private Future<Void> downloadTask;

        private ModuleStageB(@Nullable Bundle savedInstanceState, @NonNull Activity parentAcivity, int startX, int startY)
        {
            super(savedInstanceState, parentAcivity);
            this.startX = startX;
            this.startY = startY;
            onCreate2(savedInstanceState);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
        }

        private void onCreate2(Bundle savedInstanceState)
        {
            setContentView(R.layout.activity_root_fssetup_stage2);
            Animator m = ViewAnimationUtils.createCircularReveal(getContent(), startX, startY, 0, (int) Math.hypot(getContent().getWidth(), getContent().getHeight())).setDuration(300);
            m.start();
            this.cancelButton = findViewById(R.id.cancel);
            this.downloadUri = findViewById(R.id.gentoo_uri);
            this.downloadProgress = findViewById(R.id.download_status);
            this.progressBar = findViewById(R.id.download_progress);
            downloadProgress.setText(R.string.waiting_for_response);
            this.cancelButton.setOnClickListener(v ->
            {
                if (downloadTask != null)
                {
                    downloadTask.cancel(true);
                }
                currentModule = new ModuleStageA(savedInstanceState, getParentAcivity());
            });
            this.downloadUri.setText(getString(R.string.retrieving_gentoo_stage3, Library.getGentoo()));
            this.downloadTask = Executors.newSingleThreadExecutor().submit(() ->
            {
                AtomicBoolean flag = new AtomicBoolean(true);
                Downloader d = new Downloader.Builder().setOutputDir(new File(getCacheDir(), "downloads_g")).setProgressMonitor(new Downloader.ProgressMonitor()
                {
                    @Override
                    public void progress(int total, int consumed)
                    {
                        runOnUiThread(() ->
                        {
                            progressBar.setProgress((int) Library.percentage(total, consumed));
                            downloadProgress.setText(getString(R.string.d_s_s, (int) Library.percentage(total, consumed), Library.getBetterFilesize(consumed), Library.getBetterFilesize(total)));
                        });
                    }

                    @Override
                    public void error(@Nullable Throwable t, String humanReadableLocalized)
                    {
                        flag.set(false);
                        Toast.makeText(Minicode.context, humanReadableLocalized, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void complete()
                    {
                    }
                }).setUrl(Library.getGentoo()).build();
                d.run();
                if (!flag.get())
                {
                    return null;
                }
                File f = new File(new File(getCacheDir(), "downloads_g"), Library.getGentoo().substring(Library.getGentoo().lastIndexOf('/') + 1));
                FileInputStream originalInputStream = new FileInputStream(f);
                InputStream inputStream;
                if (Library.getGentoo().endsWith(".xz"))
                {
                    inputStream = new XZCompressorInputStream(originalInputStream);
                }
                else if (Library.getGentoo().endsWith(".bz2"))
                {
                    inputStream = new BZip2CompressorInputStream(originalInputStream);
                }
                else if (Library.getGentoo().endsWith(".gz"))
                {
                    inputStream = new GzipCompressorInputStream(originalInputStream);
                }
                else if (Library.getGentoo().endsWith(".lzma"))
                {
                    inputStream = new LZMAInputStream(originalInputStream);
                }
                else
                {
                    return null;
                }
                TarExtractorTask extractorTask = new TarExtractorTask(inputStream, originalInputStream, new TarExtractorTask.UnpredictableProgressMonitor()
                {
                    @Override
                    public void work(String title, long total, long completed)
                    {
                        runOnUiThread(() -> downloadProgress.setText(title));
                    }

                    @Override
                    public void work2(long total, long completed)
                    {
                        runOnUiThread(() -> progressBar.setProgress((int) Library.percentage(total, completed)));
                    }
                }, () -> new File(Library.getChrootDir(), ".download_complete"), new File(Library.getResDir(), "chroot"));
                extractorTask.run();
                return null;
            });
        }
    }
}
