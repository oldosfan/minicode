/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.dialog;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.navigator.dialog.SimplerFilesAdapter;

public class SaveFileActivity extends ThemedAppCompatActivity
{
    String mProjectName, mName;
    RecyclerView mRecycelerView;
    EditText mNameEntry;
    FloatingActionButton mOK;
    ProgressBar mSaving;
    File mCurrentPosition;
    SimplerFilesAdapter simplerFilesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_file);
        mProjectName = getIntent().getStringExtra("project_name");
        Log.d(getClass().getName(), mProjectName);
        mName = getIntent().getStringExtra("name");
        mCurrentPosition = new File(getFilesDir(), mProjectName + "/" + getIntent().getStringExtra("loc"));
        Log.d(getClass().getName(), mCurrentPosition.getAbsolutePath());
        mSaving = findViewById(R.id.saving);
        mRecycelerView = findViewById(R.id.files_recycler_import);
        mNameEntry = findViewById(R.id.entry_text_save);
        mOK = findViewById(R.id.save);
        mRecycelerView.setLayoutManager(new LinearLayoutManager(this));
        mRecycelerView.setAdapter(simplerFilesAdapter = new SimplerFilesAdapter(this, mCurrentPosition));
        mOK.setOnClickListener(v ->
        {
            if (getIntent().getData() == null)
            {
                return;
            }
            mOK.setImageDrawable(null);
            mSaving.setVisibility(View.VISIBLE);
            mOK.setClickable(false);
            if (!getIntent().getData().getScheme().equals("content"))
            {
                Library.vibrateError();
                Snackbar.make(mNameEntry, R.string.unsupported_scheme_file, Snackbar.LENGTH_LONG).show();
            }
            else
            {
                ContentResolver resolver = getContentResolver();
                InputStream is;
                try
                {
                    is = resolver.openInputStream(getIntent().getData());
                }
                catch (FileNotFoundException e)
                {
                    Library.vibrateError();
                    Snackbar.make(mNameEntry, R.string.file_not_found, Snackbar.LENGTH_LONG).show();
                    mOK.setImageResource(R.drawable.ic_err_24dp);
                    mOK.setSupportImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.background_blue)));
                    mOK.setClickable(false);
                    mSaving.setVisibility(View.INVISIBLE);
                    return;
                }
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    if (new File(mCurrentPosition, mNameEntry.getText().toString()).exists())
                    {
                        runOnUiThread(() ->
                        {
                            Library.vibrateError();
                            Snackbar.make(mOK, R.string.file_already_exists, Snackbar.LENGTH_LONG).show();
                            mOK.setImageResource(R.drawable.ic_err_24dp);
                            mOK.setSupportImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.background_blue)));
                            mOK.setClickable(true);
                            mSaving.setVisibility(View.INVISIBLE);
                        });
                        return;
                    }
                    try
                    {
                        new File(mCurrentPosition, mNameEntry.getText().toString()).createNewFile();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    try
                    {
                        IOUtils.copy(is, new FileOutputStream(new File(mCurrentPosition, mNameEntry.getText().toString())));
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                        runOnUiThread(() ->
                        {
                            Library.vibrateError();
                            mOK.setImageResource(R.drawable.ic_err_24dp);
                            mOK.setSupportImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.background_blue)));
                            mOK.setClickable(true);
                            mSaving.setVisibility(View.INVISIBLE);
                        });
                        return;
                    }
                    runOnUiThread(() ->
                    {
                        mOK.setImageResource(R.drawable.ic_check_white);
                        mOK.setSupportImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.background_blue)));
                        mOK.setClickable(false);
                        mSaving.setVisibility(View.INVISIBLE);
                        setResult(RESULT_OK);
                        finish();
                    });
                });
            }
        });
        if (mProjectName == null || getIntent().getData() == null)
        {
            Log.d(getClass().getName(), "onCreate: Invalid intent! Finishing activity");
            finish();
        }
        if (mName == null)
        {
            mName = "";
        }
        mNameEntry.setText(mName);
        mNameEntry.setSelection(0, mNameEntry.getText().length());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SELF && resultCode == RESULT_OK)
        {
            setResult(resultCode);
            finish();
        }
    }
}
