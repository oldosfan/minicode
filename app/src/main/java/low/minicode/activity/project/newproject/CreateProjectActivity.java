/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.newproject;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class CreateProjectActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_project);
        this.setTitle("New Project");
        FloatingActionButton forwardsButton = findViewById(R.id.forwards);
        // ImageView image = (ImageView) findViewById(R.id.imageView);
        // image.setImageResource(R.drawable.introduction);
        forwardsButton.setCompatElevation(3.0f);
        forwardsButton.setOnClickListener(v ->
        {
            Intent nextIntent = new Intent(CreateProjectActivity.this, NewProjectActivity.class);
            CreateProjectActivity.this.startActivityForResult(nextIntent, 0);
            finish();
        });

    }

}
