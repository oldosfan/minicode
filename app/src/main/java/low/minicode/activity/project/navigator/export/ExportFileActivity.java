/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.export;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class ExportFileActivity extends ThemedAppCompatActivity
{
    static final int SELECT_FILE_DIALOG_ACTIVITY = 1300;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        name = getIntent().getStringExtra("fileName");
        if (name == null)
        {
            finish();
        }
        else
        {
            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_TITLE, new File(name).getName());
            startActivityForResult(intent, SELECT_FILE_DIALOG_ACTIVITY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_FILE_DIALOG_ACTIVITY)
        {
            Log.d("TAG", String.format("onActivityResult: resultcode: %d", resultCode));
            if (resultCode == RESULT_OK)
            {
                try
                {
                    assert data != null;
                    assert data.getData() != null;
                    OutputStream output = getContentResolver().openOutputStream(data.getData());
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setMessage(R.string.exporting_dot);
                    AlertDialog dialog = alertDialog.create();
                    dialog.show();
                    new Thread()
                    {
                        @Override
                        public void run()
                        {
                            super.run();
                            try
                            {
                                FileUtils.copyFile(new File(name), output);
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                            runOnUiThread(() -> finish());
                        }
                    }.start();

                }
                catch (FileNotFoundException e)
                {
                    Log.d("TAG", "onActivityResult: exiting; filenotfoundexception");
                    e.printStackTrace();
                    finish();
                }
            }
            else
            {
                Log.d("TAG", "onActivityResult: exiting; result not ok");
                finish();
            }
        }
        else
        {
            finish();
        }
    }
}
