/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.export;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.wrappers.data.StrongReference;

public class ExportProjectActivity extends AppCompatActivity
{
    static final int SELECT_FILE_DIALOG_ACTIVITY = 100;

    public String makeExportName(String projName)
    {
        return projName + "-export-" + Calendar.getInstance().getTimeInMillis() + "-miniCode.zip";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exporting_files_drop_down);
        String projectName = getIntent().getStringExtra("projectName");
        if (projectName == null)
        {
            finish();
        }
        else
        {
            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_TITLE, makeExportName(projectName));
            startActivityForResult(intent, SELECT_FILE_DIALOG_ACTIVITY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_FILE_DIALOG_ACTIVITY)
        {
            Log.d("TAG", String.format("onActivityResult: resultcode: %d", resultCode));
            if (resultCode == RESULT_OK)
            {
                try
                {
                    OutputStream output = getContentResolver().openOutputStream(data.getData());
                    new ZipMakerTask(findViewById(R.id.extracting_project_textview), findViewById(R.id.extract_success_textview), findViewById(R.id.extract_success_button), findViewById(R.id.export_status), getIntent().getStringExtra("projectName"), output, this).execute();
                }
                catch (FileNotFoundException e)
                {
                    Log.d("TAG", "onActivityResult: exiting; filenotfoundexception");
                    e.printStackTrace();
                    finish();
                }
            }
            else
            {
                Log.d("TAG", "onActivityResult: exiting; result not ok");
                finish();
            }
        }
        else
        {
            finish();
        }
    }

    static class ZipMakerTask extends AsyncTask<Void, Object, Void>
    {
        StrongReference<TextView> extractMessageNotifier = new StrongReference<>();
        StrongReference<TextView> filesExtractedTo = new StrongReference<>();
        StrongReference<Button> okButton = new StrongReference<>();
        StrongReference<ProgressBar> progressBar = new StrongReference<>();
        StrongReference<Activity> activity = new StrongReference<>();
        String projectName;
        String name;
        OutputStream extractOutputStream;

        public ZipMakerTask(TextView extractMessageNotifier, TextView filesExtractedTo, Button okButton, ProgressBar progressBar, String projectName, OutputStream extractOutputStream, Activity activity)
        {
            this.extractMessageNotifier.set(extractMessageNotifier);
            this.filesExtractedTo.set(filesExtractedTo);
            this.okButton.set(okButton);
            this.progressBar.set(progressBar);
            this.projectName = projectName;
            this.progressBar.get().setIndeterminate(true);
            this.filesExtractedTo.get().setText("");
            this.extractOutputStream = extractOutputStream;
            this.activity.set(activity);
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            Log.d("TAG", "doInBackground: export");
            if (new File(Minicode.context.getFilesDir(), this.projectName).listFiles().length < 1)
            {
                this.publishProgress(Minicode.context.getString(R.string.nothing_to_do));
                this.okButton.get().setOnClickListener(va -> activity.get().finish());
                this.cancel(true);
            }
            ZipUtil.pack(new File(Minicode.context.getFilesDir(), this.projectName), extractOutputStream, name ->
            {
                this.publishProgress(name);
                return name;
            });
            return null;
        }

        @Override
        protected void onProgressUpdate(Object... objects)
        {
            this.filesExtractedTo.get().setText((CharSequence) objects[0]);
        }

        @Override
        protected void onPostExecute(Void v)
        {
            this.filesExtractedTo.get().setText(Minicode.context.getString(R.string.project_exported));
            this.okButton.get().setOnClickListener(va -> activity.get().finish());
            this.progressBar.get().setIndeterminate(false);
            this.progressBar.get().setProgress(100);
            this.extractMessageNotifier.get().setVisibility(View.INVISIBLE);
            try
            {
                this.extractOutputStream.flush();
                this.extractOutputStream.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

    }
}
