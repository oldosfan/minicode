/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.newproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.BatchingProgressMonitor;
import org.zeroturnaround.zip.ZipBreakException;
import org.zeroturnaround.zip.ZipException;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class NewProjectActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(super.isDark() ? R.layout.activity_create_project2_variant_dark : R.layout.activity_create_project2);
        ImageButton imageButton = findViewById(R.id.new_project);
        imageButton.setOnClickListener(view ->
        {
            EditText text = findViewById(R.id.nameField);
            if (text.getText().length() < 1)
            {
                Snackbar.make(view, R.string.we_need_something_here, Snackbar.LENGTH_LONG).show();
                return;
            }
            File curDir = this.getFilesDir();
            if (new File(getFilesDir(), text.getText().toString()).exists())
            {
                if (text.getText().toString().equals(".res") || text.getText().toString().equals("man_pages"))
                {
                    Snackbar.make(view, R.string.name_reserved, Snackbar.LENGTH_LONG).show();
                }
                else
                {
                    Snackbar.make(view, R.string.project_already_exists, Snackbar.LENGTH_LONG).show();
                }
                return;
            }
            new File(curDir, text.getText().toString()).mkdirs();
            Library.vibrateSuccess();
            finish();
        });
        MaterialButton importFileButton = findViewById(R.id.import_project_button);
        MaterialButton cloneProjectButton = findViewById(R.id.clone_project_button);
        importFileButton.setOnClickListener(view ->
        {
            EditText text = findViewById(R.id.nameField);
            if (text.getText().length() < 1)
            {
                Snackbar.make(view, R.string.we_need_something_here, Snackbar.LENGTH_LONG).show();
                Library.vibrateError();
                return;
            }
            File curDir = this.getFilesDir();
            if (new File(getFilesDir().getAbsolutePath() + "/" + text.getText()).exists())
            {
                Snackbar.make(view, R.string.project_already_exists, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Library.vibrateError();
                return;
            }
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(intent, 0x0a);
        });
        cloneProjectButton.setOnClickListener(v ->
        {
            Library.askDialogString(false, getString(R.string.clone), null, this, params ->
            {
                AlertDialog dialog = new AlertDialog.Builder(this).setTitle(R.string.cloning).setMessage(R.string.waiting_for_response).setCancelable(false).create();
                dialog.show();
                CloneCommand cloneCommand = new CloneCommand();
                cloneCommand.setCloneAllBranches(true);
                cloneCommand.setURI(params[0]);
                cloneCommand.setDirectory(new File(getFilesDir(), Library.referRepoNameFromUri(params[0])));
                cloneCommand.setProgressMonitor(new BatchingProgressMonitor()
                {
                    @Override
                    protected void onUpdate(String taskName, int workCurr)
                    {
                    }

                    @Override
                    protected void onEndTask(String taskName, int workCurr)
                    {
                    }

                    @Override
                    protected void onUpdate(String taskName, int workCurr, int workTotal, int percentDone)
                    {
                        runOnUiThread(() -> dialog.setTitle(taskName));
                        runOnUiThread(() -> dialog.setMessage(String.format(getString(R.string.percent_complete), percentDone)));
                    }

                    @Override
                    protected void onEndTask(String taskName, int workCurr, int workTotal, int percentDone)
                    {
                    }
                });
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        try
                        {
                            if (new URI(params[0]).getScheme().equals("http") || new URI(params[0]).getScheme().equals("https"))
                            {
                                cloneCommand.setURI(Library.followHTTPRedirects(params[0], true));
                            }
                        }
                        catch (URISyntaxException e)
                        {
                            e.printStackTrace();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                        cloneCommand.call().close();
                        runOnUiThread(dialog::dismiss);
                        finish();
                    }
                    catch (GitAPIException e)
                    {
                        e.printStackTrace();
                        runOnUiThread(dialog::dismiss);
                        AlertDialog.Builder error = new AlertDialog.Builder(this);
                        error.setTitle(R.string.clone_failed);
                        error.setMessage(e.getLocalizedMessage());
                        error.setPositiveButton(R.string.ok, (dialog2, which) ->
                        {
                        });
                        runOnUiThread(error::show);
                    }
                });
                return null;
            });
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent returnedData)
    {
        super.onActivityResult(requestCode, resultCode, returnedData);
        if (requestCode == 0x0a)
        {
            if (resultCode == RESULT_OK && returnedData.getData() != null)
            {
                if (returnedData.getData().getScheme() != null && returnedData.getData().getScheme().equals("content"))
                {
                    InputStream inputStream;
                    try
                    {
                        inputStream = getContentResolver().openInputStream(returnedData.getData());
                    }
                    catch (FileNotFoundException e)
                    {
                        Snackbar.make(findViewById(R.id.new_project), R.string.inval_export, Snackbar.LENGTH_SHORT).show();
                        Library.vibrateError();
                        return;
                    }
                    EditText text = findViewById(R.id.nameField);
                    String name = text.getText().toString();
                    MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
                    builder.setMessage(R.string.importing);
                    Dialog dialog = builder.create();
                    dialog.show();
                    new File(getFilesDir(), name).mkdir();
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            ZipUtil.unpack(inputStream, new File(getFilesDir(), name));
                        }
                        catch (ZipException | ZipBreakException e)
                        {
                            runOnUiThread(() ->
                            {
                                Snackbar.make(findViewById(R.id.new_project), R.string.inval_export, Snackbar.LENGTH_SHORT).show();
                                Library.vibrateError();
                            });
                            return;
                        }
                        finally
                        {
                            runOnUiThread(dialog::dismiss);
                        }
                    });
                    Toast.makeText(this, R.string.project_imported, Toast.LENGTH_LONG).show();
                    Library.vibrateSuccess();
                    finish();
                }
            }
        }
    }

    @Override
    protected void onPause()
    {
        overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_out);
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_out);
        super.onDestroy();
    }
}
