/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.bookmarks;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.concurrent.ConcurrentSkipListSet;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.projects.RedesignedProjectsListAdapter;
import low.minicode.application.Minicode;

public class BookmarkedProjectsActivity extends ThemedAppCompatActivity
{
    private RedesignedProjectsListAdapter projectsListAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarked_projects);
        projectsListAdapter = new RedesignedProjectsListAdapter(this);
        projectsListAdapter.setFileFilterCallback(object -> !Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getStringSet("pinned_projects", new ConcurrentSkipListSet<>()).contains(object.getName()));
        recyclerView = findViewById(R.id.pinned_projects);
        if (projectsListAdapter.getItemCount() < 1)
        {
            findViewById(R.id.no_bookmarked_projects).setVisibility(View.VISIBLE);
        }
        else
        {
            findViewById(R.id.no_bookmarked_projects).setVisibility(View.INVISIBLE);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(projectsListAdapter);
        }
        projectsListAdapter.setStateChangeListener(new RedesignedProjectsListAdapter.StateChangeListener()
        {
            @Override
            public void onRemoveBookmark(@NonNull String project)
            {
                projectsListAdapter.notifyDataSetChanged();
                if (projectsListAdapter.getItemCount() < 1)
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.VISIBLE);
                }
                else
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.INVISIBLE);
                    recyclerView.setLayoutManager(new LinearLayoutManager(BookmarkedProjectsActivity.this));
                    recyclerView.setAdapter(projectsListAdapter);
                }
            }

            @Override
            public void onBookmark(@NonNull String project)
            {
                projectsListAdapter.notifyDataSetChanged();
                if (projectsListAdapter.getItemCount() < 1)
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.VISIBLE);
                }
                else
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.INVISIBLE);
                    recyclerView.setLayoutManager(new LinearLayoutManager(BookmarkedProjectsActivity.this));
                    recyclerView.setAdapter(projectsListAdapter);
                }
            }

            @Override
            public void onUndo(@NonNull String project)
            {
                if (projectsListAdapter.getItemCount() < 1)
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.VISIBLE);
                }
                else
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.INVISIBLE);
                    recyclerView.setLayoutManager(new LinearLayoutManager(BookmarkedProjectsActivity.this));
                    recyclerView.setAdapter(projectsListAdapter);
                }
            }

            @Override
            public void onPin(@Nullable String project)
            {
            }

            @Override
            public void onRemove(@NonNull String project)
            {
                if (projectsListAdapter.getItemCount() < 1)
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.VISIBLE);
                }
                else
                {
                    findViewById(R.id.no_bookmarked_projects).setVisibility(View.INVISIBLE);
                    recyclerView.setLayoutManager(new LinearLayoutManager(BookmarkedProjectsActivity.this));
                    recyclerView.setAdapter(projectsListAdapter);
                }
            }

            @Override
            public void onItemsCounted(int count)
            {
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        onBackPressed();
        return true;
    }
}
