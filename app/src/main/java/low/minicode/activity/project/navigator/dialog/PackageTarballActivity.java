/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class PackageTarballActivity extends ThemedAppCompatActivity
{
    private View packageAsLibrary, packageAsTarball;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_tarball);
        packageAsLibrary = findViewById(R.id.use_as_library);
        packageAsTarball = findViewById(R.id.package_as_regular_tarball);
        file = (File) getIntent().getSerializableExtra("file");
        if (file == null)
        {
            return;
        }
        packageAsTarball.setOnClickListener(v ->
        {
        });
        packageAsLibrary.setOnClickListener(v ->
        {
            if (new File(file.getParent(), file.getName().startsWith("lib") ? file.getName() + ".tar" : "lib" + file.getName() + ".tar").exists())
            {
                Library.vibrateError();
                Snackbar.make(v, getString(R.string.file_s_already_exists, file.getName().startsWith("lib") ? file.getName() + ".tar" : "lib" + file.getName() + ".tar"), Snackbar.LENGTH_LONG).setAction(R.string.delete, v2 -> new File(file.getParent(), file.getName().startsWith("lib") ? file.getName() + ".tar" : "lib" + file.getName() + ".tar").delete()).show();
                return;
            }
            if (!file.isDirectory())
            {
                Library.vibrateError();
                Snackbar.make(v, R.string.must_be_directory, Snackbar.LENGTH_LONG).show();
                return;
            }
            if (!new File(file, "lib").isDirectory() || !new File(file, "include").isDirectory())
            {
                Library.vibrateError();
                Snackbar.make(v, R.string.not_val_lib_must_contain_lib_include_directories, Snackbar.LENGTH_LONG).show();
                return;
            }
            MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(this);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setMessage(getString(R.string.packaging_tarball));
            Dialog d = dialogBuilder.show();
            try
            {
                FileOutputStream outputStream = new FileOutputStream(new File(file.getParent(), file.getName().startsWith("lib") ? file.getName() + ".tar" : "lib" + file.getName() + ".tar"));
                TarArchiveOutputStream tarArchiveOutputStream = new TarArchiveOutputStream(outputStream);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Library.recursiveAddToTar(tarArchiveOutputStream, new File(file, "include"), "");
                        Library.recursiveAddToTar(tarArchiveOutputStream, new File(file, "lib"), "");
                        runOnUiThread(d::dismiss);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    Library.vibrateSuccess();
                    runOnUiThread(d::dismiss);
                    runOnUiThread(this::finish);
                });
            }
            catch (IOException e)
            {
                d.dismiss();
                Library.vibrateError();
                e.printStackTrace();
            }
            return;
        });
        packageAsTarball.setOnClickListener(v ->
        {
            if (new File(file.getParent(), file.getName() + ".tar").exists())
            {
                Library.vibrateError();
                Snackbar.make(v, getString(R.string.file_s_already_exists, file.getName() + ".tar"), Snackbar.LENGTH_LONG).setAction(R.string.delete, v2 -> new File(file.getParent(), file.getName() + ".tar").delete()).show();
                return;
            }
            MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(this);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setMessage(getString(R.string.packaging_tarball));
            Dialog d = dialogBuilder.show();
            try
            {
                FileOutputStream outputStream = new FileOutputStream(new File(file.getParent(), file.getName() + ".tar"));
                TarArchiveOutputStream tarArchiveOutputStream = new TarArchiveOutputStream(outputStream);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Library.recursiveAddToTar(tarArchiveOutputStream, file, "");
                        runOnUiThread(d::dismiss);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    runOnUiThread(d::dismiss);
                    runOnUiThread(this::finish);
                    Library.vibrateSuccess();
                });
            }
            catch (IOException e)
            {
                d.dismiss();
                e.printStackTrace();
                Library.vibrateSuccess();
            }
            return;
        });
    }
}
