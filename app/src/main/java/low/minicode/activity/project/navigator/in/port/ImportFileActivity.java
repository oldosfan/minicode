/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.in.port;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import org.zeroturnaround.zip.commons.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.wrappers.data.StrongReference;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class ImportFileActivity extends ThemedAppCompatActivity
{
    private static final int REQ_CODE = 294;
    private String folderName;

    public String getFolderName()
    {
        return folderName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        folderName = getIntent().getStringExtra("folderName");
        if (folderName == null)
        {
            finish();
        }
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQ_CODE && data != null && data.getData() != null)
        {
            if (data.getData().getScheme().equals("file"))
            {
                Toast.makeText(this, R.string.unsupported_scheme_file, Toast.LENGTH_LONG).show();
                finish();
            }
            new ImportFileTask(this, data.getData(), new File(folderName, Library.getFileName(data.getData(), this))).execute();
        }
        else
        {
            finish();
        }
    }

    private static class ImportFileTask extends AsyncTask<Void, Void, Void>
    {
        private StrongReference<ImportFileActivity> importFileActivity;
        private Uri uri;
        private File outFile;
        private AlertDialog.Builder builder;
        private AlertDialog alertDialog;

        public ImportFileTask(ImportFileActivity importFileActivity, Uri uri, File outFile)
        {
            this.uri = uri;
            this.outFile = outFile;
            this.importFileActivity = new StrongReference<>(importFileActivity);
            builder = new AlertDialog.Builder(importFileActivity).setView(R.layout.importing_file).setTitle(R.string.importing_file).setCancelable(false);
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            alertDialog = builder.show();
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            try
            {
                InputStream inputStream = importFileActivity.get().getContentResolver().openInputStream(uri);
                if (inputStream != null)
                {
                    FileUtils.copy(inputStream, outFile);
                }
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            alertDialog.dismiss();
            importFileActivity.get().finish();
            importFileActivity.clear();
        }
    }
}
