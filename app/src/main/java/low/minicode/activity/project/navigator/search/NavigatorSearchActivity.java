/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.search;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.navigator.search.FindFileAdapter;

public class NavigatorSearchActivity extends ThemedAppCompatActivity
{
    private Toolbar toolbar;
    private EditText searchText;
    private File file;
    private String projectName;
    private FindFileAdapter adapter;
    private RecyclerView results;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator_search);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        toolbar = findViewById(R.id.toolbar);
        searchText = findViewById(R.id.search_edittext);
        results = findViewById(R.id.find_results);
        whitify();
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_blue);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        searchText.requestFocus();
        file = (File) getIntent().getSerializableExtra("loc");
        projectName = getIntent().getStringExtra("project_name");
        adapter = new FindFileAdapter(file, projectName, this);
        results.setLayoutManager(new LinearLayoutManager(this)
        {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state)
            {
                try
                {
                    super.onLayoutChildren(recycler, state);
                }
                catch (IndexOutOfBoundsException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state)
            {
                try
                {
                    return super.scrollVerticallyBy(dy, recycler, state);
                }
                catch (IndexOutOfBoundsException e)
                {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        results.setAdapter(adapter);
        searchText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                adapter.setCallback(s.length() < 1 ? null : object -> object.getName().contains(s));
            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }

    @Override
    protected void onPause()
    {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    public String getProjectName()
    {
        return projectName;
    }
}
