/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.Executors;

import low.minicode.Copy;
import low.minicode.Library;
import low.minicode.ProjectManifest;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.compiler.options.RunOptionsActivity;
import low.minicode.activity.git.VersionControlActivity;
import low.minicode.activity.hg.HgVersionControlActivity;
import low.minicode.activity.project.export.ExportProjectActivity;
import low.minicode.activity.project.navigator.in.port.ImportFileActivity;
import low.minicode.activity.project.navigator.search.NavigatorSearchActivity;
import low.minicode.activity.project.preferences.ProjectSettingsActivity;
import low.minicode.activity.setup.languages.JDKSetupActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;
import low.minicode.adapter.navigator.FilesListAdapter;
import ru.noties.markwon.Markwon;
import ru.noties.markwon.core.CorePlugin;
import ru.noties.markwon.image.ImagesPlugin;

public class NavigatorActivity extends ThemedAppCompatActivity
{
    FloatingActionButton actionMenuButton;
    FloatingActionButton actionSettingsButton;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    FilesListAdapter listAdapter;
    File currentPosition;
    File[] currentPositionFileList;
    public ProjectManifest manifest;
    Toolbar toolbar;
    ImageButton search, git;
    MaterialCardView markwonContainer;
    TextView markwon;
    String lastReadme;
    boolean menuExpanded;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator);
        whitify();
        git = findViewById(R.id.git);
        markwonContainer = findViewById(R.id.markwon_container);
        markwon = findViewById(R.id.readme_md);
        if (getIntent().getExtras() == null)
        {
            return;
        }
        else if (getIntent().getExtras().getString("loc") == null || getIntent().getExtras().getString("projectName") == null)
        {
            return;
        }
        if (!new File(getFilesDir(), getIntent().getExtras().getString("projectName") + "/project_manifest.json").exists())
        {
            try
            {
                new File(getFilesDir(), getIntent().getExtras().getString("projectName") + "/project_manifest.json").createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        toolbar = findViewById(R.id.toolbar);
        search = findViewById(R.id.search);
        setSupportActionBar(toolbar);
        this.manifest = new ProjectManifest(new File(getFilesDir(), getIntent().getExtras().getString("projectName") + "/project_manifest.json"));
        currentPosition = new File(this.getFilesDir() + "/" + getIntent().getExtras().getString("projectName") + "/" + getIntent().getExtras().getString("loc"));
        search.setOnClickListener(v ->
        {
            Intent i = new Intent(this, NavigatorSearchActivity.class);
            i.putExtra("loc", currentPosition);
            i.putExtra("project_name", getIntent().getExtras().getString("projectName"));
            startActivity(i);
        });
        if (!manifest.passHash.isEmpty() && currentPosition.getParent().equals(getFilesDir().getAbsolutePath()))
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Black_NoActionBar);
            builder.setTitle(R.string.project_password_protected);
            builder.setMessage(R.string.project_is_password_protected);
            EditText entryDialog = new EditText(this);
            entryDialog.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
            entryDialog.setHint(R.string.password);
            builder.setView(entryDialog);
            builder.setOnCancelListener(dialog -> finish());
            builder.setPositiveButton(R.string.ok, (dialog, which) ->
            {
                try
                {
                    if (Arrays.toString(MessageDigest.getInstance("MD5").digest(entryDialog.getText().toString().getBytes())).equals(manifest.passHash))
                    {
                        Library.vibrateSuccess();
                        dialog.dismiss();
                    }
                    else
                    {
                        Library.vibrateError();
                        entryDialog.setText("");
                        finish();
                    }
                }
                catch (NoSuchAlgorithmException e)
                {
                    e.printStackTrace();
                    Log.wtf("NavigatorActivity", e.getCause());
                    finish();
                }
            });
            builder.show();
        }
        Log.d("FilePosition", currentPosition.getAbsolutePath());
        if (currentPosition.exists())
        {
            currentPositionFileList = currentPosition.listFiles();
        }
        else
        {
            finish();
            return;
        }
        actionMenuButton = findViewById(R.id.navigator_menu);
        actionSettingsButton = findViewById(R.id.navigator_settings);
        recyclerView = findViewById(R.id.file_view_recycler);
        layoutManager = new LinearLayoutManager(this);
        if (!currentPosition.isDirectory())
        {
            return;
        }
        currentPositionFileList = currentPosition.listFiles();
        listAdapter = new FilesListAdapter(currentPositionFileList, this, getIntent().getExtras().getString("projectName"), getIntent().getExtras().getString("loc"));
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(layoutManager);
        actionSettingsButton.setOnClickListener(v ->
        {
            Intent intentSettings = new Intent(v.getContext(), ProjectSettingsActivity.class);
            intentSettings.putExtra("projectName", getIntent().getExtras().getString("projectName"));
            startActivityForResult(intentSettings, 0);
            toggleMenu();
        });
        final NavigatorActivity navigatorActivityFinal = this;
        String currentPositionLoc = currentPosition.getAbsolutePath();
        actionMenuButton.setOnClickListener(/* v -> alertBuilder.show() */ v -> toggleMenu());
        setTitle(Library.relative(this.getFilesDir(), new File(this.getFilesDir() + "/" + this.getIntent().getExtras().getString("projectName") + "/" + this.getIntent().getExtras().getString("loc"))));
        findViewById(R.id.navigator_new_folder).setOnClickListener(v ->
        {
            toggleMenu();
            final EditText promptText = new EditText(navigatorActivityFinal);
            promptText.setSingleLine(true);
            final AlertDialog.Builder promptDialog = new AlertDialog.Builder(navigatorActivityFinal);
            promptDialog.setTitle(getString(R.string.folder_name));
            promptDialog.setView(promptText);
            promptDialog.setPositiveButton(R.string.ok, (dialog, which) ->
            {
                String loc = promptText.getText().toString();
                if (loc.equals("project_manifest.json"))
                {
                    Snackbar.make(this.getWindow().getDecorView().getRootView(), "Disallowed folder name", Snackbar.LENGTH_LONG).show();
                    return;
                }
                new File(currentPositionLoc + "/" + loc).mkdirs();
                onActivityResult(0, 0, null);
            });
            promptDialog.show();
        });
        findViewById(R.id.navigator_new_file).setOnClickListener(v ->
        {
            toggleMenu();
            EditText promptText = new EditText(navigatorActivityFinal);
            promptText.setSingleLine(true);
            AlertDialog.Builder promptDialog = new AlertDialog.Builder(navigatorActivityFinal);
            promptDialog.setTitle(getString(R.string.file_name));
            promptDialog.setView(promptText);
            promptDialog.setPositiveButton(R.string.ok, (dialog, which) ->
            {
                String loc = promptText.getText().toString();
                try
                {
                    new File(currentPositionLoc + "/" + loc).createNewFile();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                onActivityResult(0, 0, null);
            });
            promptDialog.show();
        });
        Executors.newSingleThreadExecutor().submit(() ->
        {
            while (true)
            {
                File[] files = currentPosition.listFiles();
                if (files == null)
                {
                    return;
                }
                if (!Arrays.equals(files, currentPositionFileList))
                {
                    runOnUiThread(() -> onActivityResult(0, 0, null));
                }
                try
                {
                    if (new File(currentPosition, "README.md").exists())
                    {
                        Markwon markwon2 = Markwon.builder(this).usePlugin(CorePlugin.create()).usePlugin(ImagesPlugin.create(this)).build();
                        String s = new Scanner(new File(currentPosition, "README.md")).useDelimiter("\\A").next();
                        if (!s.equals(lastReadme))
                        {
                            lastReadme = s;
                            Spanned markdown = markwon2.render(markwon2.parse(s));
                            runOnUiThread(() ->
                            {
                                markwon2.setParsedMarkdown(markwon, markdown);
                                markwonContainer.setVisibility(View.VISIBLE);
                            });
                        }
                    }
                    else if (new File(currentPosition, "README").exists())
                    {
                        String s = new Scanner(new File(currentPosition, "README")).useDelimiter("\\A").next();
                        if (!s.equals(lastReadme))
                        {
                            lastReadme = s;
                            runOnUiThread(() ->
                            {
                                markwon.setText(s);
                                markwonContainer.setVisibility(View.VISIBLE);
                            });
                        }
                    }
                    else if (markwonContainer.getVisibility() != View.GONE)
                    {
                        runOnUiThread(() -> markwonContainer.setVisibility(View.GONE));
                    }
                    Thread.sleep(420);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }
            }
        });
        findViewById(R.id.navigator_export_project).setOnClickListener(v ->
        {
            toggleMenu();
            Intent i2 = new Intent(NavigatorActivity.this, ExportProjectActivity.class);
            i2.putExtra("projectName", getIntent().getExtras().getString("projectName"));
            this.startActivityForResult(i2, 0);
        });
        findViewById(R.id.navigator_run).setOnClickListener(v ->
        {
            Intent i2 = new Intent(this, RunOptionsActivity.class);
            i2.putExtra("projectName", getIntent().getStringExtra("projectName"));
            startActivityForResult(i2, 0);
            toggleMenu();
        });
        findViewById(R.id.navigator_import).setOnClickListener(v ->
        {
            Intent i2 = new Intent(this, ImportFileActivity.class);
            i2.putExtra("folderName", currentPositionLoc);
            toggleMenu();
            startActivityForResult(i2, 0);
        });
        git.setOnClickListener(v ->
        {
            Intent i = new Intent(this, VersionControlActivity.class);
            i.putExtra("projectName", getIntent().getStringExtra("projectName"));
            startActivityForResult(i, 0);
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        this.currentPositionFileList = currentPosition.listFiles();
        this.listAdapter.notifyDataSetChanged();
        if (!new File(getFilesDir(), getIntent().getExtras().getString("projectName") + "/project_manifest.json").exists())
        {
            try
            {
                new File(getFilesDir(), getIntent().getExtras().getString("projectName") + "/project_manifest.json").createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        this.manifest = new ProjectManifest(new File(getFilesDir(), getIntent().getExtras().getString("projectName") + "/project_manifest.json"));
    }

    protected void toggleMenu()
    {
        FloatingActionButton[] expandFabs = {findViewById(R.id.navigator_new_folder), findViewById(R.id.navigator_new_file), findViewById(R.id.navigator_export_project), findViewById(R.id.navigator_import), findViewById(R.id.navigator_run), findViewById(R.id.navigator_settings)};
        if (!menuExpanded)
        {
            menuExpanded = true;
            int i = 1;
            for (FloatingActionButton floatingActionButton : expandFabs)
            {
                floatingActionButton.setVisibility(View.VISIBLE);
                floatingActionButton.animate().setListener(null);
                if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("menu_orientation", 2) == 1)
                {
                    floatingActionButton.animate().translationY(-(getResources().getDimension(R.dimen.standard_65) * i));
                }
                else if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("menu_orientation", 2) == 0)
                {
                    floatingActionButton.animate().translationX(-(getResources().getDimension(R.dimen.standard_65) * i));
                }
                else if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("menu_orientation", 2) == 2)
                {
                    switch (getResources().getConfiguration().orientation)
                    {
                        case (Configuration.ORIENTATION_PORTRAIT):
                        {
                            floatingActionButton.animate().translationY(-(getResources().getDimension(R.dimen.standard_65) * i));
                            break;
                        }
                        case (Configuration.ORIENTATION_LANDSCAPE):
                        {
                            floatingActionButton.animate().translationX(-(getResources().getDimension(R.dimen.standard_65) * i));
                            break;
                        }
                        default:
                        {
                            floatingActionButton.animate().translationX(-(getResources().getDimension(R.dimen.standard_65) * i));
                            break;
                        }

                    }
                }
                ++i;
            }
        }
        else
        {
            menuExpanded = false;
            for (FloatingActionButton floatingActionButton : expandFabs)
            {
                if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("menu_orientation", 1) == 1)
                {
                    floatingActionButton.animate().translationY(0).translationX(0).setListener(new Animator.AnimatorListener()
                    {
                        @Override
                        public void onAnimationStart(Animator animation)
                        {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            floatingActionButton.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation)
                        {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation)
                        {
                        }
                    });
                }
                else if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("menu_orientation", 1) == 0)
                {
                    floatingActionButton.animate().translationX(0).setListener(new Animator.AnimatorListener()
                    {
                        @Override
                        public void onAnimationStart(Animator animation)
                        {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            floatingActionButton.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation)
                        {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation)
                        {
                        }
                    });
                }
                else if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("menu_orientation", 1) == 2)
                {
                    floatingActionButton.animate().translationY(0).translationX(0).setListener(new Animator.AnimatorListener()
                    {
                        @Override
                        public void onAnimationStart(Animator animation)
                        {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            floatingActionButton.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation)
                        {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation)
                        {
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig)
    {
        if (menuExpanded)
        {
            toggleMenu();
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        menu.add(R.string.git);
        menu.add(android.R.string.paste);
        menu.add(R.string.python_3_interpreter);
        menu.add(R.string.mercurial);
        menu.getItem(0).setOnMenuItemClickListener(item ->
        {
            Intent i = new Intent(this, VersionControlActivity.class);
            i.putExtra("projectName", getIntent().getStringExtra("projectName"));
            startActivityForResult(i, 0);
            return true;
        });
        menu.getItem(1).setOnMenuItemClickListener(item ->
        {
            if (Copy.currentEntry == null)
            {
                Toast.makeText(this, R.string.nothing_to_copy, Toast.LENGTH_SHORT).show();
                return false;
            }
            AlertDialog.Builder copy = new AlertDialog.Builder(this);
            copy.setMessage(getString(R.string.copying_file_ft, Library.relative(getFilesDir(), Copy.currentEntry.getFile()), Library.relative(getFilesDir(), currentPosition)));
            AlertDialog d = copy.create();
            d.setCancelable(false);
            d.show();
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    Copy.currentEntry.copyTo(currentPosition);
                    runOnUiThread(() -> this.onActivityResult(0, 0, null));
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                catch (Copy.NotDirectoryException2 e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    runOnUiThread(d::dismiss);
                }
            });
            return true;
        });
        menu.getItem(2).setOnMenuItemClickListener(item ->
        {
            if (!Library.getPython3Prefix().exists())
            {
                AlertDialog.Builder error = new AlertDialog.Builder(this);
                error.setTitle(R.string.no_python3);
                error.setMessage(R.string.please_extract_lang_pack);
                error.setPositiveButton(R.string.ok, (dialog, which) ->
                {
                });
                error.setNeutralButton(R.string.extract, (dialog, which) -> startActivity(new Intent(this, JDKSetupActivity.class)));
                error.show();
            }
            else
            {
                Intent i = new Intent(this, NDKTerminalActvitiy.class);
                i.putExtra("binary", Library.getPython3Executable().getAbsolutePath());
                i.putExtra("cwd", new File(getFilesDir(), getIntent().getStringExtra("projectName")).getAbsolutePath());
                startActivityForResult(i, 0);
            }
            return true;
        });
        menu.getItem(3).setOnMenuItemClickListener(item ->
        {
            Intent i = new Intent(this, HgVersionControlActivity.class);
            i.putExtra("project", new File(getFilesDir(), getIntent().getStringExtra("projectName")));
            startActivityForResult(i, 0);
            return true;
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }
}
