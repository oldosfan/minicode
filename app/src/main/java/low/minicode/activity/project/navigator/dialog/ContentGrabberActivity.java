/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class ContentGrabberActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getIntent().getData() == null)
        {
            Log.d(getClass().getName(), "onCreate: *invalid intent*");
        }
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.select_project);
        File[] files = getFilesDir().listFiles();
        ArrayList<String> s = new ArrayList<>();
        for (File iter : files)
        {
            if (!iter.getName().equals("man_pages") && !iter.getName().equals(".res"))
            {
                s.add(iter.getName());
            }
        }
        builder.setItems(Arrays.copyOf(s.toArray(), s.size(), CharSequence[].class), (dialog, which) ->
        {
            Intent i = new Intent(this, SaveFileActivity.class);
            String name = Library.getFileName(getIntent().getData(), this);
            i.putExtra("name", name);
            i.putExtra("loc", "/");
            i.putExtra("project_name", s.get(which));
            i.setData(getIntent().getData()).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(i);
            finish();
        });
        builder.setOnDismissListener(dialog -> finish());
        builder.show();
    }
}
