/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.navigator.dialog.SimplerFilesAdapter;

public class LocalSaveFileActivity extends ThemedAppCompatActivity
{
    String mProjectName, mName;
    RecyclerView mRecycelerView;
    EditText mNameEntry;
    FloatingActionButton mOK;
    ProgressBar mSaving;
    File mCurrentPosition;
    SimplerFilesAdapter simplerFilesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_file);
        mProjectName = getIntent().getStringExtra("project_name");
        Log.d(getClass().getName(), mProjectName);
        mName = getIntent().getStringExtra("name");
        mCurrentPosition = new File(getFilesDir(), mProjectName + "/" + getIntent().getStringExtra("loc"));
        Log.d(getClass().getName(), mCurrentPosition.getAbsolutePath());
        mSaving = findViewById(R.id.saving);
        mRecycelerView = findViewById(R.id.files_recycler_import);
        mNameEntry = findViewById(R.id.entry_text_save);
        mOK = findViewById(R.id.save);
        mRecycelerView.setLayoutManager(new LinearLayoutManager(this));
        mRecycelerView.setAdapter(simplerFilesAdapter = new SimplerFilesAdapter(this, mCurrentPosition)
        {
            @Override
            public void onBindViewHolder(@NonNull ViewHolder holder, int position)
            {
                super.onBindViewHolder(holder, position);
                holder.itemView.setOnClickListener(v ->
                {
                    Intent i = getIntent();
                    i.putExtra("loc", getIntent().getStringExtra("loc") + File.separator + files.get(position).getName());
                    startActivityForResult(i, ThemedAppCompatActivity.REQUEST_SELF);
                });
            }
        });
        mOK.setOnClickListener(v ->
        {
            Intent i = new Intent();
            i.putExtra("result", Library.relative(new File(getFilesDir(), mProjectName), new File(mCurrentPosition, mNameEntry.getText().toString())));
            setResult(RESULT_OK, i);
            finish();
        });
        mNameEntry.setText(mName);
        mNameEntry.setSelection(0, mNameEntry.getText().length());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SELF && resultCode == RESULT_OK)
        {
            setResult(resultCode, data);
            finish();
        }
    }
}
