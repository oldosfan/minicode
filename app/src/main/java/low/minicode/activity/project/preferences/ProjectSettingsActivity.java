/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.preferences;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import low.minicode.Library;
import low.minicode.ProjectManifest;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.project.navigator.dialog.NavigatorReturnPathActivity;
import low.minicode.adapter.versatile.ArrayListAdapter;

public class ProjectSettingsActivity extends ThemedAppCompatActivity
{
    String projectName;
    File manifestFile;
    ProjectManifest manifest;
    RecyclerView currentCompileFiles;
    ArrayListAdapter adapter;
    CheckBox box, shared;

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_settings);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        box = findViewById(R.id.use_gmake2);
        shared = findViewById(R.id.shared_library);
        if (bundle == null)
        {
            return;
        }
        projectName = bundle.getString("projectName");
        if (projectName == null)
        {
            finish();
            return;
        }
        else
        {
            setTitle(getString(R.string.preferences_colon, projectName));
        }
        this.manifestFile = new File(this.getFilesDir(), projectName + "/" + "project_manifest.json");
        this.manifest = new ProjectManifest(manifestFile);
        FloatingActionButton saveButton = findViewById(R.id.save_build_preferences);
        MaterialButton addFileButton = findViewById(R.id.add_files_button);
        MaterialButton removeFileButton = findViewById(R.id.remove_files_button);
        this.currentCompileFiles = findViewById(R.id.current_compile_files);
        ArrayList<String> items = new ArrayList<String>();
        for (int i = 0; i < manifest.compileFiles.size(); ++i)
        {
            items.add(Library.relative(new File(getFilesDir(), projectName), new File(manifest.compileFiles.get(i))));
        }
        adapter = new ArrayListAdapter(items);
        this.currentCompileFiles.setLayoutManager(new LinearLayoutManager(this));
        this.currentCompileFiles.setAdapter(adapter);
        this.shared.setChecked(manifest.isShared);
        addFileButton.setOnClickListener(v ->
        {
            Intent i = new Intent(v.getContext(), NavigatorReturnPathActivity.class);
            i.putExtra("projectName", projectName);
            i.putExtra("loc", "/");
            this.startActivityForResult(i, 0);
        });
        removeFileButton.setOnClickListener(v ->
        {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            String[] items12 = new String[manifest.compileFiles.size()];
            for (int i = 0; i < manifest.compileFiles.size(); ++i)
            {
                items12[i] = Library.relative(new File(getFilesDir(), projectName), new File(manifest.compileFiles.get(i)));
            }
            builder.setTitle(R.string.remove_file);
            builder.setItems(items12, (dialog, which) ->
            {
                manifest.removeFromFilesList(which);
                manifest.writeToFile(manifestFile.getAbsolutePath());
                ArrayList<String> items1 = new ArrayList<>();
                for (int i = 0; i < manifest.compileFiles.size(); ++i)
                {
                    if (manifest.compileFiles.get(i).startsWith("/"))
                    {
                        items1.add(Library.relative(new File(getFilesDir(), projectName), new File(manifest.compileFiles.get(i))));
                    }
                    else
                    {
                        items1.add(manifest.compileFiles.get(i));
                    }
                }
                adapter.initializeWithNewData(items1);
            });
            builder.show();
        });
        box.setEnabled(true);
        box.setChecked(manifest.useGmake);
        box.setOnCheckedChangeListener((v, val) ->
        {
            manifest.useGmake = val;
            if (!new File(manifestFile.getParent(), "Makefile").exists() && val)
            {
                try
                {
                    Library.writeFileAsBytes(new File(manifestFile.getParent(), "Makefile").getAbsolutePath(), Library.makefileMessage.getBytes());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
        final EditText ldFlags, ccFlags, pass;
        final boolean[] isChangedPass = new boolean[1];
        ldFlags = findViewById(R.id.linker_flags);
        ccFlags = findViewById(R.id.build_flags);
        pass = findViewById(R.id.project_password);
        ldFlags.setText(manifest.linkerFlags);
        ccFlags.setText(manifest.compilerFlags);
        pass.setText("");
        pass.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                isChangedPass[0] = true;
                pass.setHint(R.string.password);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                isChangedPass[0] = true;
                pass.setHint(R.string.password);
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                isChangedPass[0] = true;
                pass.setHint(R.string.password);
            }
        });
        if (!manifest.passHash.isEmpty())
        {
            pass.setHint(R.string.preset_pass);
        }
        final ProjectSettingsActivity act = this;
        findViewById(R.id.remove_run_argument).setOnClickListener(v ->
        {
            MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(this);
            b.setTitle("Remove argument");
            String[] arr = new String[this.manifest.args.size()];
            manifest.args.toArray(arr);
            b.setItems(arr, (dialog, which) ->
            {
                this.manifest.removeFromArgsArray(which);
                this.manifest.writeToFile(this.manifestFile.getAbsolutePath());
            });
            b.show();
        });
        findViewById(R.id.add_run_argument).setOnClickListener(v ->
        {
            MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(this);
            b.setTitle("Add argument");
            EditText t = new EditText(this);
            t.setPadding(15, 15, 15, 15);
            b.setView(t);
            b.setPositiveButton(android.R.string.ok, (dialog, which) ->
            {
                if (t.getText().toString().length() > 1)
                {
                    this.manifest.putOnArgsArray(t.getText().toString());
                    this.manifest.writeToFile(this.manifestFile.getAbsolutePath());
                }
            });
            b.show();
        });
        saveButton.setOnClickListener(v ->
        {
            ((FloatingActionButton) v).setImageResource(R.drawable.ic_more_horiz_white_24dp);
            try
            {
                manifest.linkerFlags = ldFlags.getText().toString();
                manifest.compilerFlags = ccFlags.getText().toString();
                manifest.isShared = shared.isChecked();
                if (pass.getText().toString().isEmpty() && isChangedPass[0])
                {
                    manifest.passHash = "";
                }
                else if (isChangedPass[0])
                {
                    manifest.passHash = Arrays.toString(MessageDigest.getInstance("MD5").digest(pass.getText().toString().getBytes()));
                }
                manifest.writeToFile(manifestFile.getAbsolutePath());
                ((FloatingActionButton) v).setImageResource(R.drawable.ic_check_white);
                Handler handler = new Handler();
                handler.postDelayed(() -> ((FloatingActionButton) v).setImageResource(R.drawable.ic_save_black_24dp), 2000);
                return;
            }
            catch (ProjectManifest.ManifestException e)
            {
                ((FloatingActionButton) v).setImageResource(R.drawable.ic_err_24dp);
                v.setBackgroundColor(getResources().getColor(R.color.red));
                return;
            }
            catch (NoSuchAlgorithmException e)
            {
                e.printStackTrace();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null)
        {
            return;
        }
        else if (resultCode == RESULT_CANCELED)
        {
        }
        else if (data.getData() != null)
        {
            manifest.putOnFilesList(Library.relative(new File(getFilesDir(), projectName), new File(data.getDataString())));
            manifest.writeToFile(manifestFile.getAbsolutePath());
            manifest = new ProjectManifest(manifestFile);
            ArrayList<String> items = new ArrayList<>();
            for (int i = 0; i < manifest.compileFiles.size(); ++i)
            {
                if (manifest.compileFiles.get(i).startsWith("/"))
                {
                    items.add(Library.relative(new File(getFilesDir(), projectName), new File(manifest.compileFiles.get(i))));
                }
                else
                {
                    items.add(manifest.compileFiles.get(i));
                }
            }
            adapter.initializeWithNewData(items);
        }
    }
}
