/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.project.navigator.dialog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.navigator.dialog.FilesListReturnAdapter;

public class NavigatorReturnPathActivity extends ThemedAppCompatActivity
{
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    FilesListReturnAdapter listAdapter;
    File currentPosition;
    File[] currentPositionFileList;
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getIntent().getExtras() == null)
        {
            return;
        }
        else if (getIntent().getExtras().getString("loc") == null || getIntent().getExtras().getString("projectName") == null)
        {
            return;
        }
        currentPosition = new File(this.getFilesDir() + "/" + getIntent().getExtras().getString("projectName") + "/" + getIntent().getExtras().getString("loc"));
        Log.d("FilePosition", currentPosition.getAbsolutePath());
        if (currentPosition.exists())
        {
            currentPositionFileList = currentPosition.listFiles();
        }
        else
        {
            finish();
            return;
        }
        setContentView(R.layout.activity_floating_navigator);
        recyclerView = findViewById(R.id.file_view_recycler);
        layoutManager = new LinearLayoutManager(this);
        if (!currentPosition.isDirectory())
        {
            return;
        }
        currentPositionFileList = currentPosition.listFiles();
        listAdapter = new FilesListReturnAdapter(currentPositionFileList, this, getIntent().getExtras().getString("projectName"), getIntent().getExtras().getString("loc"), currentPosition);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        currentPositionFileList = currentPosition.listFiles();
        listAdapter.initializeWithNewData(currentPositionFileList);
        if (data != null && data.getData() != null && !data.getData().toString().isEmpty())
        {
            this.gotPath(data.getData().toString());
        }
    }

    public void gotPath(String path)
    {
        this.path = path;
        Intent data = new Intent();
        data.setData(Uri.parse(path));
        setResult(RESULT_OK, data);
        finish();
    }
}
