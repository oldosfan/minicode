/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.setup.resources;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;

import com.google.android.material.button.MaterialButton;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.lzma.LZMACompressorInputStream;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.N;
import low.minicode.R;
import low.minicode.wrappers.data.StrongReference;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.credits.licenses.FreeSoftwareLicensesActivity;

import static android.view.View.VISIBLE;

public class UnpackingAssetsActivity extends ThemedAppCompatActivity
{
    static final Integer NOTIFICATION_EXTRACT_PROGRESS = 0;
    AssetManager am;
    MaterialButton done;
    int easterBunnyHunt = 0;

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unpacking_assets);
        this.done = findViewById(R.id.button_extract_ok);
        this.done.setText(R.string.continuem);
        this.findViewById(R.id.progress).setVisibility(View.GONE);
        findViewById(R.id.button_licenses).setOnClickListener(v ->
        {
            Intent i = new Intent(UnpackingAssetsActivity.this, FreeSoftwareLicensesActivity.class);
            this.startActivity(i);
        });
        ((TextView) findViewById(R.id.current_unpacked_file)).setText(R.string.welcometominicode);
        this.done.setOnClickListener(v ->
        {
            findViewById(R.id.button_licenses).setEnabled(false);
            this.done.setOnClickListener((v2) -> UnpackingAssetsActivity.this.finish());
            this.done.setEnabled(false);
            if (this.getIntent().getExtras().getString("assetPath") == null)
            {
                finish();
                return;
            }
            this.am = this.getAssets();
            String arg = this.getIntent().getExtras().getString("assetPath");
            InputStream is, is2, is3, is4, is5;
            AssetManager.AssetInputStream orig1;
            AssetManager.AssetInputStream orig2;
            AssetManager.AssetInputStream orig3;
            AssetManager.AssetInputStream orig4;
            AssetManager.AssetInputStream orig5;
            try
            {
                orig1 = (AssetManager.AssetInputStream) this.am.open(arg);
                orig2 = (AssetManager.AssetInputStream) this.am.open("manual.tar.lzma");
                orig3 = (AssetManager.AssetInputStream) this.am.open(String.format("gmake.%s", arg));
                orig4 = (AssetManager.AssetInputStream) this.am.open(String.format("file.%s", arg));
                orig5 = (AssetManager.AssetInputStream) this.am.open(String.format("git.%s", arg));
                is = new LZMACompressorInputStream(orig1);
                is2 = new LZMACompressorInputStream(orig2);
                is3 = new LZMACompressorInputStream(orig3);
                is4 = new LZMACompressorInputStream(orig4);
                is5 = new LZMACompressorInputStream(orig5);
            }
            catch (IOException e)
            {
                finish();
                return;
            }
            catch (OutOfMemoryError e)
            {
                finishAffinity();
                return;
            }
            this.setTitle("Extracting assets");
            this.findViewById(R.id.progress).setVisibility(VISIBLE);
            new TarExtracterTask(orig1, findViewById(R.id.progress), findViewById(R.id.current_unpacked_file), is, this, ".res", arg, "First run", () ->
            {
                new TarExtracterTask(orig2, findViewById(R.id.progress), findViewById(R.id.current_unpacked_file), is2, this, "man_pages", "manual.tar.lzma", "Extract manual", () ->
                {
                    new TarExtracterTask(orig3, findViewById(R.id.progress), findViewById(R.id.current_unpacked_file), is3, this, ".res/gmake", String.format("gmake.%s", arg), "Extract GNU make", () ->
                    {
                        new TarExtracterTask(orig4, findViewById(R.id.progress), findViewById(R.id.current_unpacked_file), is4, this, ".res/file", String.format("file.%s", arg), "Extract File", () ->
                        {
                            new TarExtracterTask(orig5, findViewById(R.id.progress), findViewById(R.id.current_unpacked_file), is5, this, ".res/git", String.format("git.%s", arg), "Extract Git", () ->
                            {
                                this.done.setEnabled(true);
                                try
                                {
                                    new File(getFilesDir(), ".res/mc_extract_done").createNewFile();
                                }
                                catch (IOException e)
                                {
                                    throw new RuntimeException(e);
                                }
                            }).execute();
                        }).execute();
                    }).execute();
                }).execute();
            }).execute();
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBackPressed()
    {
        ++easterBunnyHunt;
        if (easterBunnyHunt > 4)
        {
            new AlertDialog.Builder(this).show().dismiss();
            ((TextView) findViewById(R.id.current_unpacked_file)).setText("NCC-1701\n" + "\"No stupid A, B, C or D\"");
            if (easterBunnyHunt > 8)
            {
                ((TextView) findViewById(R.id.current_unpacked_file)).setText("Engage");
            }
            if (easterBunnyHunt > 10)
            {
                ((TextView) findViewById(R.id.current_unpacked_file)).setText("Fire phasers");
            }
            if (easterBunnyHunt > 20)
            {
                ((TextView) findViewById(R.id.current_unpacked_file)).setText("Let's try a continuous pulse");
            }
            if (easterBunnyHunt > 40)
            {
                ((TextView) findViewById(R.id.current_unpacked_file)).setText("...");
                ((TextView) findViewById(R.id.bcya)).setText("By continuing, you agree to the terms of the Federation Charter\n(this is legally void) ");
            }
            if (easterBunnyHunt > ((180 | 29) | 120034))
            {
                ((TextView) findViewById(R.id.current_unpacked_file)).setText("I am Locotus... of Borg.");
                ((TextView) findViewById(R.id.bcya)).setText("");
            }
        }
        return;
    }

    static class TarExtracterTask extends AsyncTask<Void, Object, Void>
    {
        Integer count;
        StrongReference<ProgressBar> progressBar = new StrongReference<>();
        StrongReference<TextView> textView = new StrongReference<>();
        TarArchiveInputStream tarArchiveInputStream;
        StrongReference<UnpackingAssetsActivity> a = new StrongReference<>();
        InputStream ti;
        String dirPrefix;
        String n;
        Notification.Builder notification;
        Boolean notificationSafe;
        NotificationManager manager;
        NotificationChannel channel;
        @SuppressWarnings("deprecation")
        String name;
        File file;
        Runnable onCompleteHandler;
        AssetManager.AssetInputStream origStream;

        TarExtracterTask(AssetManager.AssetInputStream origStream, ProgressBar pbar, TextView tv, InputStream ti, UnpackingAssetsActivity a, String dirPrefix, String astName, String name, Runnable onCompleteHandler)
        {
            Log.d("Start", String.valueOf(this.count));
            this.n = astName;
            this.onCompleteHandler = onCompleteHandler;
            this.progressBar.set(pbar);
            this.textView.set(tv);
            this.a.set(a);
            this.ti = ti;
            this.name = name;
            this.dirPrefix = dirPrefix;
            this.origStream = origStream;
            this.manager = (NotificationManager) Minicode.context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        @SuppressLint("WrongConstant")
        @Override
        protected void onPreExecute()
        {
            this.progressBar.get().setVisibility(VISIBLE);
            Notification.Builder notificationBuilder = new Notification.Builder(Minicode.context);
            notificationBuilder.setContentTitle(name);
            notificationBuilder.setProgress(100, 0, false);
            notificationBuilder.setOngoing(true);
            notificationBuilder.setSmallIcon(R.drawable.ic_firstrun_black_24dp);
            notificationBuilder.setColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                this.channel = new NotificationChannel("assetschan", "Notifications (general)", NotificationManager.IMPORTANCE_MAX);
                this.channel.setSound(null, null);
                notificationBuilder.setChannelId(this.channel.getId());
                manager.createNotificationChannel(channel);
            }
            this.notification = notificationBuilder;
            this.notificationSafe = true;
            manager.notify(NOTIFICATION_EXTRACT_PROGRESS, notification.build());
        }

        @Override
        protected void onProgressUpdate(Object... args)
        {
            Integer i = (Integer) args[0];
            String s = (String) args[1];
            this.textView.get().setText(s);
            this.progressBar.get().setProgress(i);
            if (args.length >= 3)
            {
                if ((Boolean) args[2])
                {
                    this.progressBar.get().setIndeterminate(true);
                }
                else
                {
                    this.progressBar.get().setIndeterminate(false);
                }
            }
            else
            {
                this.progressBar.get().setIndeterminate(false);
            }
            if (notificationSafe && args.length >= 3 && (Boolean) args[2])
            {
                this.notification.setProgress(100, i, true).setContentText(s);
                manager.notify(NOTIFICATION_EXTRACT_PROGRESS, this.notification.build());
            }
            else if (notificationSafe)
            {
                this.notification.setProgress(100, i, false).setContentText(s);
                manager.notify(NOTIFICATION_EXTRACT_PROGRESS, this.notification.build());
            }
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            Process.setThreadPriority(Process.THREAD_PRIORITY_FOREGROUND);
            TarArchiveEntry entry;
            int amount = 0;
            int total = 0;
            try
            {
                total = this.origStream.available();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            try
            {
                publishProgress(0, Minicode.context.getString(R.string.removing_old_assets), true);
                Library.deleteDirectoryRecursively(new File(Minicode.context.getFilesDir(), dirPrefix));
                this.tarArchiveInputStream = new TarArchiveInputStream(this.ti);
                while ((entry = this.tarArchiveInputStream.getNextTarEntry()) != null)
                {
                    ++amount;
                    String symname = null;
                    if (entry.isSymbolicLink())
                    {
                        symname = entry.getLinkName();
                    }
                    Log.d("total", String.valueOf(total));
                    this.publishProgress((int) Library.percentage(total, total - origStream.available()), entry.getName(), total == 0);
                    Log.d("remaining", ((Double) ((double) total - origStream.available())).toString());
                    if (entry.isDirectory())
                    {
                        continue;
                    }
                    Log.d("Extract", entry.getName());
                    int BUFFER = 1024;
                    File f = new File(this.a.get().getFilesDir() + File.separator + this.dirPrefix, entry.getName());
                    //noinspection ResultOfMethodCallIgnored
                    f.delete();
                    if (!f.getParentFile().exists() && !f.getParentFile().isDirectory())
                    {
                        if (!f.getParentFile().mkdirs())
                        {
                            throw new RuntimeException("Failed to create directory");
                        }
                    }
                    if (!f.getParentFile().isDirectory())
                    {
                        throw new RuntimeException("Unexpected file behavior: parent *not* a directory");
                    }
                    if (!f.exists() && !f.isDirectory())
                    {
                        if (!f.createNewFile())
                        {
                            throw new RuntimeException("Failed to create file");
                        }
                    }
                    if (symname == null)
                    {
                        BufferedOutputStream of = new BufferedOutputStream(new FileOutputStream(f));
                        byte[] buf = new byte[BUFFER];
                        int fread;
                        while ((fread = tarArchiveInputStream.read(buf)) > 0)
                        {
                            of.write(buf, 0, fread);
                        }
                        of.flush();
                        of.close();
                        N.chmod(f.getAbsolutePath(), entry.getMode());
                    }
                    else
                    {
                        if (f.exists())
                        {
                            if (!f.delete())
                            {
                                throw new RuntimeException("Couldn't delete file to make way for symbolic link");
                            }
                        }
                        Log.d("symlinker", String.format("%s <- %s", symname, f.getAbsolutePath()));
                        N.symlink(symname, f.getAbsolutePath());
                    }
                }
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void u)
        {
            Library.storeVersionToSharedPrefs(a.get());
            try
            {
                tarArchiveInputStream.close();
                File f = new File(this.a.get().getFilesDir() + File.separator + this.dirPrefix, ".extract_complete");
                f.createNewFile();
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            this.a.get().setTitle("Assets extracted");
            this.a.get().findViewById(R.id.progress).setVisibility(View.GONE);
            this.textView.get().setText(Minicode.context.getString(R.string.wedone));
            this.a.get().findViewById(R.id.button_licenses).setEnabled(true);
            this.manager.cancel(null, NOTIFICATION_EXTRACT_PROGRESS);
            NotificationCompat.Builder completeBuilder;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                completeBuilder = new NotificationCompat.Builder(this.a.get(), this.channel.getId());
            }
            else
            {
                //noinspection deprecation
                completeBuilder = new NotificationCompat.Builder(this.a.get());
            }
            completeBuilder.setContentTitle("Task complete");
            completeBuilder.setContentText(this.name);
            completeBuilder.setSmallIcon(R.drawable.ic_check_white);
            completeBuilder.setColor(this.a.get().getResources().getColor(R.color.colorAccent));
            this.manager.notify(name.hashCode(), completeBuilder.build());
            if (onCompleteHandler != null)
            {
                onCompleteHandler.run();
            }
            return;
        }
    }
}
