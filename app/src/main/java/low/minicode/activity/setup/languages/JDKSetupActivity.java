/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.setup.languages;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import org.tukaani.xz.LZMAInputStream;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.TarExtractorTask;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class JDKSetupActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jdksetup);
        whitify();
        findViewById(R.id.set_up_lang_packs).setOnClickListener(v ->
        {
            boolean java = ((CheckBox) findViewById(R.id.java)).isChecked();
            boolean python2 = ((CheckBox) findViewById(R.id.py_2)).isChecked();
            boolean python3 = ((CheckBox) findViewById(R.id.py_3)).isChecked();
            @SuppressLint("CutPasteId") View v2 = getLayoutInflater().inflate(R.layout.activity_jdksetup_in_progress, null);
            setContentView(v2);
            Animator m = ViewAnimationUtils.createCircularReveal(v2, getWindow().getDecorView().getWidth() / 2, getWindow().getDecorView().getHeight() / 2, 0, (int) Math.hypot(getWindow().getDecorView().getWidth(), getWindow().getDecorView().getHeight())).setDuration(300);
            m.start();
            ProgressBar bar = v2.findViewById(R.id.extract_language_packs_progress);
            findViewById(R.id.cancel).setOnClickListener(v3 -> setContentView(R.layout.activity_jdksetup));
            AssetManager.AssetInputStream py3;
            try
            {
                py3 = (AssetManager.AssetInputStream) getAssets().open(String.format("python3.%s.tar.lzma", Library.getArchName()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
                recreate();
                return;
            }
            TarExtractorTask e;
            try
            {
                e = new TarExtractorTask(new LZMAInputStream(new BufferedInputStream(py3)), py3, new TarExtractorTask.UnpredictableProgressMonitor()
                {
                    @Override
                    public void work(String title, long total, long completed)
                    {
                    }

                    @Override
                    public void work2(long total, long completed)
                    {
                        bar.setProgress((int) Library.percentage(total, completed));
                    }
                }, this::finish, new File(getFilesDir(), ".res/python3"));
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        e.run();
                    }
                    catch (IOException e1)
                    {
                        e1.printStackTrace();
                    }
                });
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
                recreate();
                return;
            }
        });
    }

}
