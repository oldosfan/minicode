/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.compiler;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.Library;
import low.minicode.N;
import low.minicode.ProjectManifest;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.compiler.errors.ViewErrorsActivity;
import low.minicode.application.Minicode;
import low.minicode.wrappers.data.StrongReference;
import low.minicode.wrappers.data.error.Error;
import low.minicode.wrappers.data.parcelable.BasicParcelable;

public class CompileActivity extends ThemedAppCompatActivity implements Serializable
{
    String manifestPath;
    String projectName;
    CompileTask task;
    Toolbar toolbar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compile_new);
        manifestPath = getIntent().getStringExtra("manifestPath");
        projectName = getIntent().getStringExtra("projectName");
        if (manifestPath == null || projectName == null)
        {
            Toast.makeText(this, "Oi!", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
        progressBar = findViewById(R.id.progress);
        progressBar.setProgress(0);
        (task = new CompileTask(this, findViewById(R.id.compile_status))).execute();
    }

    @Override
    public void onBackPressed()
    {
        if (task != null && task.complete)
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }

    private static class CompileTask extends AsyncTask<Void, Object, Void>
    {
        final StrongReference<TextView> tf = new StrongReference<>();
        final StrongReference<CompileActivity> a = new StrongReference<>();
        Pattern compileErrorPattern;
        boolean complete;

        @SuppressLint("WrongConstant")
        private CompileTask(CompileActivity a, TextView tf)
        {
            this.tf.set(tf);
            this.a.set(a);
            this.compileErrorPattern = Pattern.compile("(.*\\.cp?p?x?x?c?):(\\d+):(\\d+):(.+):(.*)");
        }

        @SuppressLint("StringFormatInvalid")
        @Override
        protected Void doInBackground(Void... voids)
        {
            StringBuilder eStreamText = new StringBuilder();
            ProjectManifest projectManifest = new ProjectManifest(new File(a.get().manifestPath));
            List<String[]> todo = Library.compilerStrings(projectManifest.compileFiles, projectManifest.linkerFlags, projectManifest.compilerFlags, projectManifest.isShared, a.get().projectName);
            new File(a.get().getFilesDir(), a.get().projectName + (this.a.get().getIntent().getStringExtra("runFile") == null ? "/out/" : String.format("/out-%s/", this.a.get().getIntent().getStringExtra("runFile")))).mkdirs();
            InputStream errorStream;
            InputStream normalStream;
            Future<Integer> future;
            for (String[] process : todo)
            {
                if (todo.indexOf(process) >= projectManifest.compileFiles.size())
                {
                    a.get().runOnUiThread(() ->
                    {
                        a.get().toolbar.setTitle(R.string.linking);
                        a.get().progressBar.setProgress((int) Library.percentage(todo.size() - 1, todo.indexOf(process)));
                        ((TextView) a.get().findViewById(R.id.compile_status)).append(String.format("\n> %s\n", a.get().getString(R.string.linking)));
                    });
                }
                else
                {
                    a.get().runOnUiThread(() ->
                    {
                        a.get().toolbar.setTitle(Library.relative(new File(a.get().getFilesDir(), a.get().projectName), new File(projectManifest.compileFiles.get(todo.indexOf(process)))));
                        a.get().progressBar.setProgress((int) Library.percentage(todo.size() - 1, todo.indexOf(process)));
                        ((TextView) a.get().findViewById(R.id.compile_status)).append(String.format("\n> %s\n", Library.relative(new File(a.get().manifestPath).getParentFile(), new File(projectManifest.compileFiles.get(todo.indexOf(process))))));
                    });
                }
                Process currentProcess;
                try
                {
                    ProcessBuilder pb = new ProcessBuilder(process);
                    Library.deleteDirectoryRecursively(new File(Library.getPrefixDir(), ".temps"));
                    Map<String, String> mp = pb.environment();
                    mp.put("LD_LIBRARY_PATH", Minicode.context.getApplicationInfo().nativeLibraryDir + ":" + Library.getPrefixDir() + "/lib");
                    mp.put("TMP", Library.getTempDir().getAbsolutePath());
                    Library.ensureTempDir();
                    currentProcess = pb.start();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                errorStream = currentProcess.getErrorStream();
                normalStream = currentProcess.getInputStream();
                future = Executors.newSingleThreadExecutor().submit(() ->
                {
                    currentProcess.waitFor();
                    return currentProcess.exitValue();
                });
                while (true)
                {
                    try
                    {
                        if (errorStream != null && errorStream.available() > 0)
                        {
                            byte[] b = new byte[errorStream.available()];
                            errorStream.read(b);
                            eStreamText.append(new String(b));
                            a.get().runOnUiThread(() -> ((TextView) a.get().findViewById(R.id.compile_status)).append(new String(b)));
                        }
                        if (normalStream != null && normalStream.available() > 0)
                        {
                            byte[] b = new byte[normalStream.available()];
                            normalStream.read(b);
                            eStreamText.append(new String(b));
                            a.get().runOnUiThread(() -> ((TextView) a.get().findViewById(R.id.compile_status)).append(new String(b)));
                        }
                        if (future != null && future.isDone())
                        {
                            if (future.get() != 0)
                            {
                                int start = ((TextView) a.get().findViewById(R.id.compile_status)).getText().length();
                                a.get().runOnUiThread(() -> ((TextView) a.get().findViewById(R.id.compile_status)).append("\n\nErrors/warnings found: \n"));
                                String eStream = eStreamText.toString();
                                ArrayList<String> ab = new ArrayList<>();
                                Matcher m = compileErrorPattern.matcher(eStream);
                                while (m.find())
                                {
                                    ab.add(eStream.substring(m.start(), m.end()));
                                    Log.d("match", eStream.substring(m.start(), m.end()));
                                }
                                ArrayList<Error> errors = new ArrayList<>();
                                for (String iter : ab)
                                {
                                    Matcher m2 = compileErrorPattern.matcher(iter);
                                    if (m2.matches())
                                    {
                                        Error err = new Error();
                                        err.file = m2.group(1);
                                        err.line = Integer.valueOf(m2.group(2));
                                        err.word = Integer.valueOf(m2.group(3));
                                        err.type = m2.group(4);
                                        err.desc = m2.group(5);
                                        errors.add(err);
                                        a.get().runOnUiThread(() -> ((TextView) a.get().findViewById(R.id.compile_status)).append(err.toString() + "\n"));
                                    }
                                }
                                MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(a.get());
                                Library.vibrateError();
                                b.setMessage(String.format(this.a.get().getString(R.string.compilefailed), future.get()));
                                b.setPositiveButton(this.a.get().getString(R.string.view_errors), ((dialog, which) ->
                                {
                                    Intent i = new Intent(this.a.get().getApplicationContext(), ViewErrorsActivity.class);
                                    BasicParcelable<ArrayList<Error>> e = new BasicParcelable<>(errors);
                                    i.putExtra("errors", errors);
                                    this.a.get().startActivityForResult(i, 0);
                                }));
                                a.get().runOnUiThread(b::show);
                                a.get().runOnUiThread(Library::vibrateError);
                                return null;
                            }
                            break;
                        }
                    }
                    catch (IOException | InterruptedException | ExecutionException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
            }
            try
            {
                FileUtils.copyFile(new File(Library.getTempDir(), projectManifest.isShared ? a.get().projectName + ".so" : a.get().projectName), new File(new File(a.get().getFilesDir(), a.get().projectName), projectManifest.isShared ? a.get().projectName + ".so" : a.get().projectName));
                N.chmod(new File(new File(a.get().getFilesDir(), a.get().projectName), projectManifest.isShared ? a.get().projectName + ".so" : a.get().projectName).getAbsolutePath(), 0755);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(a.get());
            Library.vibrateSuccess();
            b.setMessage(R.string.compilesucessful);
            b.setPositiveButton(R.string.ok, ((dialog, which) -> a.get().finish()));
            a.get().runOnUiThread(b::show);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            this.complete = true;
        }
    }
}
