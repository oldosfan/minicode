/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.compiler.options;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import low.minicode.Library;
import low.minicode.ProjectManifest;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.compiler.CompileActivity;
import low.minicode.activity.compiler.MakeActivity;
import low.minicode.activity.project.navigator.dialog.NavigatorReturnPathActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;

public class RunOptionsActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        String projectName = getIntent().getStringExtra("projectName");
        if (projectName == null)
        {
            finish();
            return;
        }
        setContentView(R.layout.activity_run_options);
        File projectRoot = new File(getFilesDir(), projectName);
        File projectManifest = new File(projectRoot, "project_manifest.json");
        if (!projectManifest.exists())
        {
            try
            {
                projectManifest.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        ProjectManifest m = new ProjectManifest(projectManifest);
        findViewById(R.id.new_shell).setOnClickListener(v ->
        {
            Intent i = new Intent(v.getContext(), NDKTerminalActvitiy.class);
            i.putExtra("binary", "/system/bin/sh");
            i.putExtra("binaryArray", new String[] {"-sh"});
            startActivityForResult(i, 0);
        });
        findViewById(R.id.new_shell).setLongClickable(true);
        StringBuilder svb = new StringBuilder();
        svb.append("-sh").append(" ");
        findViewById(R.id.new_shell).setOnLongClickListener(v ->
        {
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.setTitle(R.string.additional_args);
            EditText e = new EditText(this);
            b.setPositiveButton(R.string.ok, (dialog, which) ->
            {
                svb.append(e.getText().toString());
                String arg = svb.toString();
                String[] splitargs = arg.split(" ");
                Intent i = new Intent(v.getContext(), NDKTerminalActvitiy.class);
                i.putExtra("binary", "/system/bin/sh");
                i.putExtra("binaryArray", splitargs);
                startActivityForResult(i, 0);
            });
            b.setView(e);
            b.show();
            return true;
        });
        if (m.useGmake)
        {
            ((FloatingActionButton) findViewById(R.id.compile)).setImageResource(R.drawable.ic_make_24dp);
            findViewById(R.id.compile).setOnClickListener(v ->
            {
                String makeTarget = "minicode";
                if (!(new File(getFilesDir(), projectName + "/Makefile").exists() && new File(getFilesDir(), projectName + "/Makefile").isFile()))
                {
                    Snackbar.make(this.getWindow().getDecorView(), R.string.no_valid_makefile, Snackbar.LENGTH_LONG).show();
                }
                String invocation = Library.getGmakeInvocation(this) + " " + makeTarget;
                Intent i = new Intent(RunOptionsActivity.this, MakeActivity.class);
                i.putExtra("makeInvocation", invocation);
                i.putExtra("projectName", projectName);
                i.putExtra("manifestPath", projectManifest.getAbsolutePath());
                startActivityForResult(i, 0);
            });
        }
        else
        {
            findViewById(R.id.compile).setOnClickListener(v ->
            {
                if (m.compileFiles.size() < 1 && getIntent().getStringExtra("runFile") == null)
                {
                    Snackbar.make(this.getWindow().getDecorView(), R.string.nothing_to_do, Snackbar.LENGTH_LONG).show();
                }
                else
                {
                    Intent i = new Intent(RunOptionsActivity.this, CompileActivity.class);
                    i.putExtra("manifestPath", m.manifestLocation.getAbsolutePath());
                    i.putExtra("projectName", projectName);
                    if (getIntent().getStringExtra("runFile") != null)
                    {
                        i.putExtra("runFile", getIntent().getStringExtra("runFile"));
                    }
                    RunOptionsActivity.this.startActivityForResult(i, 10);
                }
            });
        }
        findViewById(R.id.run_elf).setOnClickListener(v ->
        {
            Intent i = new Intent(RunOptionsActivity.this, NavigatorReturnPathActivity.class);
            i.putExtra("projectName", projectName);
            i.putExtra("loc", "/");
            startActivityForResult(i, 15);
        });
    }

    @Override
    public void onActivityResult(int r, int c, Intent d)
    {
        super.onActivityResult(r, c, d);
        if (r != 15)
        {
            finish();
        }
        else
        {
            if (c == RESULT_CANCELED)
            {
                return;
            }
            else if (d != null)
            {
                File projectRoot = new File(getFilesDir(), getIntent().getStringExtra("projectName"));
                File projectManifest = new File(projectRoot, "project_manifest.json");
                if (!projectManifest.exists())
                {
                    try
                    {
                        projectManifest.createNewFile();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                ProjectManifest m = new ProjectManifest(projectManifest);
                Intent i = new Intent(RunOptionsActivity.this, NDKTerminalActvitiy.class);
                ArrayList<String> a = (ArrayList<String>) m.args;
                a.add(0, d.getData().toString());
                String[] arr = new String[a.size()];
                if (arr.length > 0)
                {
                    a.toArray(arr);
                }
                i.putExtra("binary", d.getData().toString());
                if (arr.length > 0)
                {
                    i.putExtra("binaryArray", arr);
                }
                startActivityForResult(i, 0);
            }
        }
    }
}
