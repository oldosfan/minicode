/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.compiler;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.Library;
import low.minicode.ProjectManifest;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.compiler.errors.ViewErrorsActivity;
import low.minicode.application.Minicode;
import low.minicode.wrappers.data.error.Error;

public class MakeActivity extends ThemedAppCompatActivity implements Serializable
{
    String manifestPath;
    String projectName;
    MakeTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compile);
        if (super.isDark())
        {
            findViewById(R.id.compile_scrollview).setBackgroundColor(getResources().getColor(R.color.black));
            findViewById(R.id.compile_status).setBackgroundColor(getResources().getColor(R.color.black));
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        manifestPath = getIntent().getStringExtra("manifestPath");
        projectName = getIntent().getStringExtra("projectName");
        if (manifestPath == null || projectName == null)
        {
            Toast.makeText(this, "Oi!", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        (task = new MakeTask(this, findViewById(R.id.compile_status))).execute();

    }

    @Override
    public void onBackPressed()
    {
        if (task != null && task.complete)
        {
            super.onBackPressed();
        }
    }

    class MakeTask extends AsyncTask<Void, Object, Void>
    {
        final Integer NOTIFICATION_ID = 2;
        EditText tf;
        MakeActivity a;
        Pattern compileErrorPattern;
        NotificationManager nm;
        NotificationChannel notificationChannel;
        NotificationCompat.Builder builder;
        boolean complete;

        @SuppressLint("WrongConstant")
        MakeTask(MakeActivity a, EditText tf)
        {
            this.tf = tf;
            this.a = a;
            this.compileErrorPattern = Pattern.compile("(.*\\.cp?p?x?x?c?):(\\d+):(\\d+):(.+):(.*)");
            this.nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                this.notificationChannel = new NotificationChannel("mktask", "GNU Make", NotificationManager.IMPORTANCE_LOW);
                this.nm.createNotificationChannel(notificationChannel);
            }
            this.builder = new NotificationCompat.Builder(this.a, "mktask");
            this.builder.setContentTitle("Make");
            this.builder.setContentText(String.format(getString(R.string.building_stuff), a.projectName));
            this.builder.setColorized(true);
            this.builder.setColor(a.getResources().getColor(R.color.accent_device_default_700));
            this.builder.setSmallIcon(R.drawable.ic_make_24dp);
            this.builder.setProgress(Integer.MAX_VALUE, 0, true);
            this.nm.notify(NOTIFICATION_ID, this.builder.build());
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            StringBuilder eStreamText = new StringBuilder();
            ProjectManifest mam = new ProjectManifest(new File(a.manifestPath));
            String todo = getIntent().getStringExtra("makeInvocation");
            //noinspection ResultOfMethodCallIgnored
            Process currentProcess;
            try
            {
                ProcessBuilder pb = new ProcessBuilder("/system/bin/sh", "-c", todo);
                Library.deleteDirectoryRecursively(new File(Library.getPrefixDir(), ".temps"));
                new File(Library.getPrefixDir(), ".temps").mkdirs();
                pb.directory(new File(getFilesDir(), a.projectName));
                Map<String, String> mp = pb.environment();
                mp.put("LD_LIBRARY_PATH", Minicode.context.getApplicationInfo().nativeLibraryDir + ":" + Library.getPrefixDir() + "/lib");
                currentProcess = pb.start();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            InputStream errorStream = currentProcess.getErrorStream();
            InputStream normalStream = currentProcess.getInputStream();
            Future<Integer> future = Executors.newSingleThreadExecutor().submit(() ->
            {
                currentProcess.waitFor();
                return currentProcess.exitValue();
            });
            while (true)
            {
                try
                {
                    if (errorStream.available() > 0)
                    {
                        int start = ((EditText) a.findViewById(R.id.compile_status)).getText().length();
                        byte[] b = new byte[errorStream.available()];
                        errorStream.read(b);
                        eStreamText.append(new String(b));
                        a.runOnUiThread(() -> ((EditText) a.findViewById(R.id.compile_status)).append(new String(b)));
                        a.runOnUiThread(() -> ((EditText) a.findViewById(R.id.compile_status)).getText().setSpan(new ForegroundColorSpan(getResources().getColor(R.color.red)), start, ((EditText) a.findViewById(R.id.compile_status)).getText().length(), 0));
                    }
                    if (normalStream.available() > 0)
                    {
                        byte[] b = new byte[normalStream.available()];
                        normalStream.read(b);
                        eStreamText.append(new String(b));
                        a.runOnUiThread(() -> ((EditText) a.findViewById(R.id.compile_status)).append(new String(b)));
                    }
                    if (future.isDone())
                    {
                        if (future.get() != 0)
                        {
                            a.runOnUiThread(() -> ((EditText) a.findViewById(R.id.compile_status)).append("\n\nErrors/warnings found: \n"));
                            String eStream = eStreamText.toString();
                            ArrayList<String> ab = new ArrayList<>();
                            Matcher m = compileErrorPattern.matcher(eStream);
                            while (m.find())
                            {
                                ab.add(eStream.substring(m.start(), m.end()));
                                Log.d("match", eStream.substring(m.start(), m.end()));
                            }
                            ArrayList<Error> errors = new ArrayList<>();
                            for (String iter : ab)
                            {
                                Matcher m2 = compileErrorPattern.matcher(iter);
                                if (m2.matches())
                                {
                                    Error err = new Error();
                                    err.file = m2.group(1);
                                    err.line = Integer.valueOf(m2.group(2));
                                    err.word = Integer.valueOf(m2.group(3));
                                    err.type = m2.group(4);
                                    err.desc = m2.group(5);
                                    errors.add(err);
                                    a.runOnUiThread(() -> ((EditText) a.findViewById(R.id.compile_status)).append(err.toString() + "\n"));
                                }
                            }
                            AlertDialog.Builder b = new AlertDialog.Builder(a);
                            b.setMessage(String.format(getString(R.string.compilefailed), future.get()));
                            b.setPositiveButton(getString(R.string.view_errors), ((dialog, which) ->
                            {
                                Intent i = new Intent(this.a.getApplicationContext(), ViewErrorsActivity.class);
                                i.putExtra("errors", errors);
                                startActivityForResult(i, 0);
                            }));
                            a.runOnUiThread(b::show);
                            Library.vibrateError();
                            a.runOnUiThread(() ->
                            {
                                this.builder.setContentTitle("Make failed");
                                this.builder.setColor(a.getResources().getColor(R.color.red));
                                this.builder.setProgress(100, 0, false);
                                this.nm.notify(NOTIFICATION_ID, this.builder.build());
                            });
                        }
                        else
                        {
                            Library.vibrateSuccess();
                            AlertDialog.Builder b = new AlertDialog.Builder(a);
                            b.setMessage(String.format(getString(R.string.compilesucessful), future.get()));
                            b.setPositiveButton(R.string.ok, ((dialog, which) -> a.finish()));
                            a.runOnUiThread(b::show);
                            a.runOnUiThread(() ->
                            {
                                this.builder.setContentTitle("Make done");
                                this.builder.setColor(a.getResources().getColor(R.color.success));
                                this.builder.setProgress(100, 100, false);
                                this.nm.notify(NOTIFICATION_ID, this.builder.build());
                            });
                        }
                        break;
                    }
                }
                catch (IOException | InterruptedException | ExecutionException e)
                {
                    throw new RuntimeException(e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            this.complete = true;
        }
    }
}
