/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.updates;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nullable;

import low.minicode.BuildConfig;
import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.wrappers.data.StrongReference;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class UpdatesActivity extends ThemedAppCompatActivity
{
    static final boolean UPDATE_MANAGER_TEST = true;
    final AtomicReference<ArrayList<DownloaderTask>> tasks = new AtomicReference<>();
    RecyclerView recyclerView;
    UpdateAdapter adapter;
    UpdateIndexerTask updateIndexerTask;

    @SuppressLint({"DefaultLocale", "WrongConstant"})
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updates);
        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.updates_swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(() ->
        {
            if (updateIndexerTask != null)
            {
                updateIndexerTask.cancel(true);
            }
            (updateIndexerTask = new UpdateIndexerTask(this, adapter)).execute();
        });
        tasks.set(new ArrayList<>());
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.setTitle(R.string.more_priv);
            b.setMessage(R.string.need_more_priv);
            b.setPositiveButton(R.string.ok, (dialog, which) -> finish());
            b.setCancelable(false);
        }
        recyclerView = findViewById(R.id.recycler_updates);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        if (BuildConfig.DEBUG & !UPDATE_MANAGER_TEST)
        {
            try
            {
                ((TextView) findViewById(R.id.current_ver)).setText(String.format("%s (%d) [ DEBUG ]", getPackageManager().getPackageInfo(getPackageName(), 0).versionName, getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
                ((TextView) findViewById(R.id.current_ver)).setTextColor(getResources().getColor(R.color.red));
            }
            catch (PackageManager.NameNotFoundException e)
            {
                e.printStackTrace();
            }
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.setTitle(R.string.debug_build);
            b.setMessage(R.string.no_debug_updates);
            b.setPositiveButton(R.string.ok, (dialog, which) -> finish());
            b.setOnCancelListener((ignored) -> finish());
            b.show();
            return;
        }
        adapter = new UpdateAdapter(new ArrayList<>());
        (updateIndexerTask = new UpdateIndexerTask(this, adapter)).execute();
        recyclerView.setAdapter(adapter);
        try
        {
            ((TextView) findViewById(R.id.current_ver)).setText(String.format("%s (%d)", getPackageManager().getPackageInfo(getPackageName(), 0).versionName, getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed()
    {
        if (tasks.get() != null)
        {
            for (DownloaderTask t : tasks.get())
            {
                t.cancel(true);
            }
        }
        super.onBackPressed();
    }

    public interface OnDownloadErrorHandler
    {
        void run(Exception machineFriendlyException, String humanFriendlyMessage);
    }

    public static class Update
    {
        public String updateDesc, fancyVersion, downloadUri;
        public Long versionCode;

        public Update(String updateDesc, String fancyVersion, Long versionCode, String downloadUri)
        {
            this.updateDesc = updateDesc;
            this.fancyVersion = fancyVersion;
            this.versionCode = versionCode;
            this.downloadUri = downloadUri;
        }
    }

    public static class DownloaderTask extends AsyncTask<Void, Object, Void>
    {
        String outFile, inputUrl;
        StrongReference<ProgressBar> progressBar = new StrongReference<>();
        Notification.Builder notificationBuilder;
        NotificationChannel channel;
        NotificationManager notificationManager;
        Integer downloadTextResID;
        Runnable onCompleteHandler;
        OnDownloadErrorHandler downloadErrorHandler;
        Boolean isError = false;
        Exception exception;
        String friendlyError;

        @SuppressLint("WrongConstant")
        public DownloaderTask(Context c, String inputUrl, String outputDir, @Nullable ProgressBar progressBar, Integer downloadTextResID, Runnable onCompleteHandler, OnDownloadErrorHandler downloadErrorHandler)
        {
            this.outFile = outputDir;
            this.inputUrl = inputUrl;
            this.progressBar.set(progressBar);
            this.notificationBuilder = new Notification.Builder(c);
            this.notificationManager = (NotificationManager) Minicode.context.getSystemService(NOTIFICATION_SERVICE);
            this.downloadTextResID = downloadTextResID;
            this.onCompleteHandler = onCompleteHandler;
            this.downloadErrorHandler = downloadErrorHandler;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                this.channel = new NotificationChannel("updates2", "Download updates", NotificationManager.IMPORTANCE_MAX);
                this.channel.setSound(null, null);
                this.notificationManager.createNotificationChannel(this.channel);
            }
        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
            this.notificationManager.cancel(inputUrl.hashCode());
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            try
            {
                URL url = new URL(inputUrl);
                URLConnection c = url.openConnection();
                InputStream i = url.openStream();
                FileOutputStream outFile = new FileOutputStream(this.outFile);
                int total = c.getContentLength();
                int current = 0;
                int read;
                int lastCurrent = 0;
                byte[] buffer = new byte[1024];
                while ((read = i.read(buffer)) > 0)
                {
                    current += read;
                    outFile.write(buffer, 0, read);
                    if (current - lastCurrent > 2048)
                    {
                        lastCurrent = current;
                        publishProgress(current, total);
                    }
                }
                outFile.flush();
                outFile.close();
            }
            catch (MalformedURLException e)
            {
                isError = true;
                exception = e;
                friendlyError = Minicode.context.getString(R.string.invalid_update_data);
                return null;
            }
            catch (IOException e)
            {
                isError = true;
                exception = e;
                friendlyError = Minicode.context.getString(R.string.failed_update);
                return null;
            }
            return null;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            if (progressBar != null)
            {
                progressBar.get().setVisibility(View.VISIBLE);
                progressBar.get().setProgress(0);
            }
            notificationBuilder.setSmallIcon(R.drawable.ic_system_update_alt_black_24dp);
            notificationBuilder.setColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                notificationBuilder.setColorized(true);
                notificationBuilder.setChannelId(channel.getId());
            }
            notificationBuilder.setContentTitle(Minicode.context.getString(downloadTextResID));
            notificationBuilder.setContentText(Minicode.context.getString(R.string.waiting_for_response));
            notificationBuilder.setProgress(100, 0, false);
            notificationManager.notify(inputUrl.hashCode(), notificationBuilder.build());
        }

        @SuppressLint("DefaultLocale")
        @Override
        protected void onProgressUpdate(Object... values)
        {
            Integer i = (Integer) values[0];
            Integer m = (Integer) values[1];
            if (progressBar != null)
            {
                this.progressBar.get().setMax(m);
                this.progressBar.get().setIndeterminate(m == -1);
                this.progressBar.get().setProgress(i);
            }
            this.notificationBuilder.setContentText(String.format("%s/%s", Library.getBetterFilesize(i), Library.getBetterFilesize(m)));
            this.notificationBuilder.setProgress(m, i, m == -1);
            this.notificationManager.notify(inputUrl.hashCode(), notificationBuilder.build());
        }

        @Override
        protected void onPostExecute(Void ignored)
        {
            if (isError)
            {
                this.downloadErrorHandler.run(exception, friendlyError);
                this.notificationManager.cancel(inputUrl.hashCode());
                this.notificationBuilder.setProgress(0, 0, false);
                this.notificationBuilder.setContentTitle(Minicode.context.getString(R.string.dl_fail));
                this.notificationManager.notify((inputUrl.hashCode() + 1) | 224 ^ 22, this.notificationBuilder.build());
                return;
            }
            this.notificationManager.cancel(inputUrl.hashCode());
            onCompleteHandler.run();
        }
    }

    public class UpdateAdapter extends RecyclerView.Adapter<UpdateAdapter.ViewHolder>
    {
        private Update[] content;

        public UpdateAdapter(List<Update> s)
        {
            ArrayList<Update> where = new ArrayList<>(s);
            Collections.reverse(where);
            this.content = new Update[where.size()];
            if (content.length < 1)
            {
                return;
            }
            where.toArray(content);
        }

        public void initializeWithNewData(List<Update> s)
        {
            ArrayList<Update> where = new ArrayList<>(s);
            Collections.reverse(where);
            this.content = new Update[where.size()];
            where.toArray(this.content);
            this.notifyDataSetChanged();
            return;
        }

        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i)
        {
            Log.d("Step", "Step: onCreateViewHolder");
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.basic_update, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder vh, int i)
        {
            Log.d("Step", "Step: onBindViewHolder");
            Update result = this.content[i];
            vh.mVersionText.setText(String.format("%s (%d)", result.fancyVersion, result.versionCode));
            vh.mDescText.setText(result.updateDesc);
            vh.mInstallButton.setClickable(false);
            vh.mInstallButton.setEnabled(false);
            vh.mInstallButton.setVisibility(View.GONE);
            vh.mInstallButton.setOnClickListener(v ->
            {
                Log.d("event", "onClick - vh.mInstallBtn");
                Intent promptInstall = new Intent(Intent.ACTION_VIEW).setDataAndType(Uri.parse(String.format("file://%s", new File(getExternalFilesDirs("media")[0], String.format(".res/%s-%d.apk", result.fancyVersion, result.versionCode)).getAbsolutePath())), "application/vnd.android.package-archive");
                promptInstall.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Minicode.context.startActivity(promptInstall);
            });
            vh.mDownloadButton.setOnClickListener(v ->
            {
                vh.mDownloadButton.setEnabled(false);
                new File(getExternalFilesDirs("media")[0], ".res").mkdirs();
                DownloaderTask downloaderTask = new DownloaderTask(v.getContext(), result.downloadUri, new File(v.getContext().getExternalFilesDir("media"), String.format(".res/%s-%d.apk", result.fancyVersion, result.versionCode)).getAbsolutePath(), vh.mProgressBar, R.string.downloading_update, () ->
                {
                    vh.mInstallButton.setVisibility(View.VISIBLE);
                    vh.mInstallButton.setClickable(true);
                    vh.mInstallButton.setEnabled(true);
                }, (machineFriendlyException, humanFriendlyMessage) ->
                {
                    machineFriendlyException.printStackTrace();
                    vh.mDownloadButton.setEnabled(true);
                    vh.mProgressBar.setIndeterminate(false);
                    vh.mProgressBar.setProgress(0);
                });
                tasks.get().add(downloaderTask);
                downloaderTask.execute();
            });
        }

        @Override
        public int getItemCount()
        {
            if (this.content.length < 1)
            {
                findViewById(R.id.up_to_date_layout).setVisibility(View.VISIBLE);
            }
            else
            {
                findViewById(R.id.up_to_date_layout).setVisibility(View.GONE);
            }
            return this.content.length;
        }

        public class ViewHolder extends RecyclerView.ViewHolder
        {
            public TextView mDescText, mVersionText;
            public Button mDownloadButton, mInstallButton;
            public ProgressBar mProgressBar;

            public ViewHolder(View v)
            {
                super(v);
                mDescText = v.findViewById(R.id.update_desc);
                mVersionText = v.findViewById(R.id.update_version);
                mDownloadButton = v.findViewById(R.id.download_update_button);
                mInstallButton = v.findViewById(R.id.install_update_button);
                mProgressBar = v.findViewById(R.id.update_download_progress);
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class UpdateIndexerTask extends AsyncTask<Void, Object, ArrayList<Update>>
    {
        static final int UPDATE_NOTIFICATION_ID = 242798;
        Activity c;
        AlertDialog.Builder b;
        Dialog d;
        UpdateAdapter adapter;

        UpdateIndexerTask(Activity c, UpdateAdapter adapter)
        {
            this.c = c;
            this.adapter = adapter;
        }

        @Override
        protected ArrayList<Update> doInBackground(Void... voids)
        {
            ArrayList<Update> results = new ArrayList<>();
            try
            {
                String text;
                InputStream uriInputStream;
                URLConnection connection = new URL(Minicode.updateUrl).openConnection();
                connection.connect();
                uriInputStream = connection.getInputStream();
                text = IOUtils.toString(uriInputStream, Charset.forName("utf-8"));
                JSONObject j = new JSONObject(text);
                JSONArray a = j.getJSONArray("updates");
                ArrayList<JSONObject> arrays = new ArrayList<>();
                int size = a.length();
                for (int i = 0; i < size; ++i)
                {
                    arrays.add(a.getJSONObject(i));
                }
                for (JSONObject array : arrays)
                {
                    String desc = array.getString("updateDescription");
                    String versionFancy = array.getString("versionFancy");
                    Long versionCode = array.getLong("versionCode");
                    String versionURL = array.getString("versionURL");
                    if (versionCode > getPackageManager().getPackageInfo(getPackageName(), 0).versionCode)
                    {
                        results.add(new Update(desc, versionFancy, versionCode, versionURL));
                    }
                }
                return results;
            }
            catch (IOException e)
            {
                e.printStackTrace();
                c.runOnUiThread(() -> Toast.makeText(c, R.string.failed_update, Toast.LENGTH_LONG).show());
                return null;
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                c.runOnUiThread(() -> Toast.makeText(c, getString(R.string.invalid_update_data), Toast.LENGTH_LONG).show());
                return null;
            }
            catch (PackageManager.NameNotFoundException e)
            {
                e.printStackTrace();
                c.runOnUiThread(() -> Toast.makeText(c, getString(R.string.invalid_update_data), Toast.LENGTH_LONG).show());
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Object... values)
        {
            super.onProgressUpdate(values);
        }

        @SuppressLint("WrongConstant")
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            SwipeRefreshLayout swipeRefreshLayout = c.findViewById(R.id.updates_swiperefresh);
            swipeRefreshLayout.setRefreshing(true);
            NotificationChannel nc;
            NotificationCompat.Builder builder;
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                nc = new NotificationChannel("mc_updates", getString(R.string.updates), NotificationManager.IMPORTANCE_LOW);
                notificationManager.createNotificationChannel(nc);
                builder = new NotificationCompat.Builder(c, nc.getId());
            }
            else
            {
                builder = new NotificationCompat.Builder(c);
            }
            builder.setSmallIcon(R.drawable.ic_system_update_alt_black_24dp);
            builder.setColor(getResources().getColor(R.color.accent_device_default_700));
            builder.setColorized(true);
            builder.setContentTitle(getString(R.string.retrieving_updates));
            builder.setProgress(0, 0, true);
            notificationManager.notify(UPDATE_NOTIFICATION_ID, builder.build());
        }

        @Override
        protected void onPostExecute(ArrayList<Update> aVoid)
        {
            super.onPostExecute(aVoid);
            SwipeRefreshLayout swipeRefreshLayout = c.findViewById(R.id.updates_swiperefresh);
            swipeRefreshLayout.setRefreshing(false);
            if (aVoid != null)
            {
                this.adapter.initializeWithNewData(aVoid);
            }
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(UPDATE_NOTIFICATION_ID);
        }
    }

}
