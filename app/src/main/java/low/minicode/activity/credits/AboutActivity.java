/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.credits;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;

import low.minicode.BuildConfig;
import low.minicode.Library;
import low.minicode.N;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.credits.licenses.FreeSoftwareLicensesActivity;
import low.minicode.activity.updates.UpdatesActivity;
import low.minicode.adapter.git.ItemListAdapter;
import low.minicode.application.Minicode;
import low.minicode.wrappers.data.adapteritem.TitleBodyClickableItem;

public class AboutActivity extends ThemedAppCompatActivity
{
    RecyclerView mRecyclerView;
    ItemListAdapter itemListAdapter;
    Boolean canGoBack;

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        canGoBack = true;
        ArrayList<TitleBodyClickableItem> items = new ArrayList<>();
        setContentView(R.layout.activity_about);
        try
        {
            @SuppressLint("DefaultLocale") TitleBodyClickableItem itemVersion = new TitleBodyClickableItem(getString(R.string.version), String.format("%s (%d)", getPackageManager().getPackageInfo(getPackageName(), 0).versionName, getPackageManager().getPackageInfo(getPackageName(), 0).versionCode), null);
            items.add(itemVersion);
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
        whitify();
        items.add(new TitleBodyClickableItem(getString(R.string.asset_version), String.valueOf(Minicode.assetVersion), null));
        items.add(new TitleBodyClickableItem(getString(R.string.maintainer), Minicode.maintainer, null));
        items.add(new TitleBodyClickableItem(getString(R.string.copyright), Minicode.copyright, v ->
        {
            Intent i = new Intent(this, FreeSoftwareLicensesActivity.class);
            i.putExtra("isGrey", true).putExtra("manual_location", "https://gitlab.com/oldosfan/minicode/raw/protect-git-experimentation/COPYING.md");
            startActivity(i);
        }));
        items.add(new TitleBodyClickableItem(getString(R.string.supported_languages), Arrays.toString(Minicode.supportedLanguages), null));
        items.add(new TitleBodyClickableItem(getString(R.string.partially_supported_languages), Arrays.toString(Minicode.partiallySupportedLanguages), null));
        items.add(new TitleBodyClickableItem(getString(R.string.debug), BuildConfig.DEBUG ? getString(R.string.yes) : getString(R.string.no), null));
        items.add(new TitleBodyClickableItem(getString(R.string.architecture), Library.getArchName(), null));
        items.add(new TitleBodyClickableItem(getString(R.string.ext_strg_w), ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED ? getString(R.string.yes) : getString(R.string.need_storage_perm), null, ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED ? R.drawable.ic_check_white : R.drawable.ic_err_24dp));
        if (getSharedPreferences("low.minicode.updater", MODE_PRIVATE).getLong("available_version", 0) <= BuildConfig.VERSION_CODE)
        {
            items.add(new TitleBodyClickableItem(getString(R.string.updates), getString(R.string.inst_updates), v -> startActivity(new Intent(this, UpdatesActivity.class)), R.drawable.ic_keyboard_arrow_right_black_24dp));
        }
        else
        {
            items.add(new TitleBodyClickableItem(getString(R.string.updates), String.format(getString(R.string.minicode_xxx_now_avaliable), getSharedPreferences("low.minicode.updater", MODE_PRIVATE).getString("available_version_fancy", "unknown")), v -> startActivity(new Intent(this, UpdatesActivity.class)), R.drawable.ic_keyboard_arrow_right_black_24dp));
        }
        items.add(new TitleBodyClickableItem(getString(R.string.licenses), getString(R.string.fsl), v -> startActivity(new Intent(this, FreeSoftwareLicensesActivity.class).putExtra("isGrey", true)), R.drawable.ic_keyboard_arrow_right_black_24dp));
        if (BuildConfig.DEBUG)
        {
            items.add(new TitleBodyClickableItem("Crash", "", v ->
            {
                throw new RuntimeException("debug crash");
            }));
            items.add(new TitleBodyClickableItem("Native crash", "", v ->
            {
                N.null_deref();
            }));
        }
        mRecyclerView = findViewById(R.id.about_recycler);
        itemListAdapter = new ItemListAdapter(items);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(itemListAdapter);
    }

    @Override
    public void onBackPressed()
    {
        if (canGoBack)
        {
            super.onBackPressed();
        }
    }

}
