/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.credits.licenses;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.webkit.WebView;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class FreeSoftwareLicensesActivity extends ThemedAppCompatActivity
{
    private WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_htmllicense);
        wv = findViewById(R.id.license);
        wv.setBackgroundColor(Color.TRANSPARENT);
        if (getIntent().getStringExtra("manual_location") == null)
        {
            wv.loadUrl("file:///android_asset/license.html");
        }
        else
        {
            wv.loadUrl(getIntent().getStringExtra("manual_location"));
            findViewById(R.id.html_licence_text).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.primary_device_default_light)));
        }
        if (getIntent().getBooleanExtra("isGrey", false))
        {
            findViewById(R.id.html_licence_text).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.holo_gray_bright_deep)));
        }
    }

    @Override
    public void onBackPressed()
    {
        if (wv.canGoBack())
        {
            wv.goBack();
            return;
        }
        super.onBackPressed();
    }
}
