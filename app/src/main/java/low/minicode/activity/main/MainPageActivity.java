/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.io.File;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.chroot.RootFSSetupActivity;
import low.minicode.activity.credits.AboutActivity;
import low.minicode.activity.gitlab.integration.GitlabIntegrationActivity;
import low.minicode.activity.global.preferences.PreferencesActivity;
import low.minicode.activity.global.suggestions.SuggestionsActivity;
import low.minicode.activity.manual.man.ManualActivity;
import low.minicode.activity.manual.python.PydocActivity;
import low.minicode.activity.project.bookmarks.BookmarkedProjectsActivity;
import low.minicode.activity.project.newproject.NewProjectActivity;
import low.minicode.activity.setup.languages.JDKSetupActivity;
import low.minicode.activity.setup.resources.UnpackingAssetsActivity;
import low.minicode.adapter.git.ProjectSuggestionsAdapter;
import low.minicode.adapter.projects.RedesignedProjectsListAdapter;

public class MainPageActivity extends ThemedAppCompatActivity
{
    private Toolbar toolbar;
    private ExtendedFloatingActionButton add;
    private RecyclerView list, suggestions;
    private RedesignedProjectsListAdapter redesignedProjectsListAdapter;
    private ProjectSuggestionsAdapter suggestionsAdapter;
    private ImageButton showSuggestions, gitLab;
    private View suggestionsContainer;
    private SearchView searchView;
    private CardView barContainer;
    private Handler searchHandler;
    private TextView searchTextView;
    private View noProjectsEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        HandlerThread tmp = new HandlerThread("");
        tmp.start();
        whitify();
        gitLab = findViewById(R.id.gitlab);
        gitLab.setOnClickListener(v -> startActivity(new Intent(this, GitlabIntegrationActivity.class)));
        searchHandler = new Handler(tmp.getLooper());
        showSuggestions = findViewById(R.id.show_suggestions);
        toolbar = findViewById(R.id.toolbar);
        barContainer = findViewById(R.id.search_start);
        noProjectsEmptyView = findViewById(R.id.no_projects_empty_view);
        searchView = findViewById(R.id.search);
        add = findViewById(R.id.add);
        list = findViewById(R.id.my_projects);
        suggestions = findViewById(R.id.suggestions_recycler);
        redesignedProjectsListAdapter = new RedesignedProjectsListAdapter(this);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(redesignedProjectsListAdapter);
        suggestionsContainer = findViewById(R.id.suggestions_container);
        searchTextView = findViewById(R.id.search_text_textview);
        suggestionsAdapter = new ProjectSuggestionsAdapter();
        suggestions.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        suggestions.setAdapter(suggestionsAdapter);
        showSuggestions.setOnClickListener(v ->
        {
            showSuggestions.animate().rotation(suggestionsContainer.getVisibility() == View.VISIBLE ? 0 : 180).start();
            suggestionsContainer.setVisibility(suggestionsContainer.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            getSharedPreferences("low.minicode.main_page", MODE_PRIVATE).edit().putBoolean("visible", suggestionsContainer.getVisibility() == View.VISIBLE).apply();
        });
        if (getSharedPreferences("low.minicode.main_page", MODE_PRIVATE).getBoolean("visible", false))
        {
            showSuggestions.performClick();
        }
        redesignedProjectsListAdapter.setStateChangeListener(new RedesignedProjectsListAdapter.StateChangeListener()
        {
            @Override
            public void onRemoveBookmark(@NonNull String project)
            {
            }

            @Override
            public void onBookmark(@NonNull String project)
            {
            }

            @Override
            public void onUndo(@NonNull String project)
            {
            }

            @Override
            public void onPin(@Nullable String project)
            {
            }

            @Override
            public void onRemove(@NonNull String project)
            {
            }

            @Override
            public void onItemsCounted(int count)
            {
                if (count < 1)
                {
                    noProjectsEmptyView.setVisibility(View.VISIBLE);
                }
                else
                {
                    noProjectsEmptyView.setVisibility(View.GONE);
                }
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                searchHandler.removeCallbacksAndMessages(null);
                searchHandler.postDelayed(() ->
                {
                    redesignedProjectsListAdapter.setFileFilterCallback(object -> !object.getName().contains(newText));
                    runOnUiThread(redesignedProjectsListAdapter::notifyDataSetChanged);
                }, 200);
                return true;
            }
        });
        searchView.setOnSearchClickListener(v ->
        {
            searchTextView.setVisibility(View.GONE);
            searchView.onActionViewExpanded();
        });
        searchView.setOnCloseListener(() ->
        {
            redesignedProjectsListAdapter.setFileFilterCallback(null);
            redesignedProjectsListAdapter.notifyDataSetChanged();
            searchView.onActionViewCollapsed();
            searchTextView.setVisibility(View.VISIBLE);
            return true;
        });
        searchTextView.setOnClickListener(v ->
        {
            searchTextView.setVisibility(View.GONE);
            searchView.onActionViewExpanded();
        });
        setSupportActionBar(toolbar);
        add.setOnClickListener(v -> startActivity(new Intent(this, NewProjectActivity.class)));
        if (!new File(this.getFilesDir(), ".res/mc_extract_done").exists() || Library.wereAppAssetsUpdated(this))
        {
            Intent i = new Intent(this, UnpackingAssetsActivity.class);
            i.putExtra("assetPath", Library.getArchName() + ".tar.lzma");
            i.putExtra("updated", Library.wereAppAssetsUpdated(this));
            this.startActivityForResult(i, 0);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        redesignedProjectsListAdapter.notifyDataSetChanged();
        whitify();
        suggestionsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.about_mc, menu);
        menu.getItem(0).setOnMenuItemClickListener(item ->
        {
            startActivity(new Intent(this, PreferencesActivity.class));
            return true;
        });
        menu.getItem(1).setOnMenuItemClickListener(item ->
        {
            startActivityForResult(new Intent(this, BookmarkedProjectsActivity.class), 0);
            return true;
        });
        menu.getItem(2).setOnMenuItemClickListener(item ->
        {
            startActivity(new Intent(this, AboutActivity.class));
            return true;
        });
        menu.getItem(3).setOnMenuItemClickListener(item ->
        {
            startActivity(new Intent(this, ManualActivity.class));
            return true;
        });
        menu.getItem(4).setOnMenuItemClickListener(item ->
        {
            if (!Library.getPython3Prefix().exists())
            {
                AlertDialog.Builder error = new AlertDialog.Builder(this);
                error.setTitle(R.string.no_python3);
                error.setMessage(R.string.please_extract_lang_pack);
                error.setPositiveButton(R.string.dismiss, (dialog, which) ->
                {
                });
                error.setNeutralButton(R.string.extract, (dialog, which) -> startActivity(new Intent(this, JDKSetupActivity.class)));
                error.show();
            }
            else
            {
                startActivity(new Intent(this, PydocActivity.class));
            }
            return true;
        });
        menu.getItem(5).setOnMenuItemClickListener(item ->
        {
            if (!Library.hasRoot())
            {
                Library.vibrateError();
            }
            else
            {
                startActivity(new Intent(this, RootFSSetupActivity.class));
            }
            return true;
        });
        menu.getItem(6).setOnMenuItemClickListener(item ->
        {
            startActivity(new Intent(this, SuggestionsActivity.class));
            return true;
        });
        return true;
    }

    @Override
    public void onBackPressed()
    {
        if (searchView.isIconified())
        {
            super.onBackPressed();
        }
        else
        {
            searchView.onActionViewCollapsed();
            searchTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy()
    {
        searchHandler.getLooper().quitSafely();
        super.onDestroy();
    }
}
