/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.projects;

import android.os.Bundle;
import android.widget.TextView;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class GitlabEmptyProjectActivity extends ThemedAppCompatActivity
{
    private TextView gitUrl, httpUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_empty_project);
        whitify();
        gitUrl = findViewById(R.id.clone_url);
        httpUrl = findViewById(R.id.clone_url_http);
        if (getIntent().getStringExtra("clone_url") == null || getIntent().getStringExtra("clone_url_http") == null)
        {
            Library.vibrateError();
            finish();
        }
        httpUrl.setText(getIntent().getStringExtra("clone_url_http"));
        gitUrl.setText(getIntent().getStringExtra("clone_url"));
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }
}
