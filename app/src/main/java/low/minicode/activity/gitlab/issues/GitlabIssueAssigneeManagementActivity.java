/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.issues;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Assignee;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.issues.GitlabIssueAssigneeListAdapter;

public class GitlabIssueAssigneeManagementActivity extends ThemedAppCompatActivity
{
    private RecyclerView recyclerView;
    private Issue issue;
    private Project project;
    private Toolbar toolbar;
    private ProgressBar loading;
    private LinearLayout layout;
    private GitLabApi api;
    private GitlabIssueAssigneeListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_issue_assignee_management);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        recyclerView = findViewById(R.id.assignees);
        toolbar = findViewById(R.id.toolbar);
        loading = findViewById(R.id.loading);
        layout = findViewById(R.id.issue_assignees_layout);
        setSupportActionBar(toolbar);
        int iid = getIntent().getIntExtra("issue_id", -1);
        int projectId = getIntent().getIntExtra("project_id", -1);
        String token = getIntent().getStringExtra("token");
        if (iid == -1 || projectId == -1 || token == null)
        {
            finish();
            return;
        }
        layout.setVisibility(View.INVISIBLE);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token);
            try
            {
                do
                {
                    project = api.getProjectApi().getProject(projectId);
                }
                while (project == null);
                do
                {
                    issue = api.getIssuesApi().getIssue(project, iid);
                }
                while (issue == null);
                adapter = new GitlabIssueAssigneeListAdapter(issue, api);
                adapter.setOnStateChangeListener(() ->
                {
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            updateMenu();
                        }
                        catch (GitLabApiException e)
                        {
                            e.printStackTrace();
                        }
                    });
                });
                adapter.refresh();
                updateMenu();
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                finish();
                return;
            }
            runOnUiThread(() -> initialize(savedInstanceState));
        });
    }

    @Override
    protected void onPause()
    {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        super.onDestroy();
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }

    private void initialize(Bundle savedInstanceState)
    {
        loading.setVisibility(View.GONE);
        layout.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    private void updateMenu() throws GitLabApiException
    {
        Log.d(getClass().getName(), "Updating assignee menu");
        AtomicReference<Menu> menu = new AtomicReference<>();
        AtomicReference<Menu> subMenu = new AtomicReference<>();
        runOnUiThread(() ->
        {
            menu.set(toolbar.getMenu());
            subMenu.set(menu.get().findItem(R.id.add) != null && menu.get().findItem(R.id.add).hasSubMenu() ? menu.get().findItem(R.id.add).getSubMenu() : menu.get().addSubMenu(Menu.NONE, R.id.add, Menu.NONE, R.string.collaborators));
            subMenu.get().clear();
            menu.get().getItem(0).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.get().getItem(0).setIcon(R.drawable.ic_add_blue_24dp);
        });
        while (menu.get() == null || subMenu.get() == null)
        {
        }
        subMenu.get().clear();
        Log.d(getClass().getName(), "Retrieving members");
        List<Member> collaborators = api.getProjectApi().getAllMembers(project);
        List<User> usableCollaborators = new ArrayList<>();
        Log.d(getClass().getName(), "Iterating to find valid members");
        memberIter:
        for (Member member : collaborators)
        {
            Log.d(getClass().getName(), "Processing member: " + member.getUsername());
            for (Assignee assignee : adapter.getAssignees())
            {
                Log.d(getClass().getName(), " *        Processing assignee: " + assignee.getUsername());
                if (member.getUsername().equals(assignee.getUsername()))
                {
                    Log.d(getClass().getName(), " *         *        Member already assigned: " + assignee.getUsername());
                    continue memberIter;
                }
            }
            Log.d(getClass().getName(), " *   Adding member: " + member.getUsername());
            usableCollaborators.add(api.getUserApi().getUser(member.getUsername()));
        }
        for (int i = 0; i < usableCollaborators.size(); ++i)
        {
            int finalI = i;
            runOnUiThread(() ->
            {
                subMenu.get().add(Menu.NONE, finalI << 4 | 0b1111, Menu.NONE, usableCollaborators.get(finalI).getName());
                subMenu.get().findItem(finalI << 4 | 0b1111).setOnMenuItemClickListener(menuItem ->
                {
                    recyclerView.setVisibility(View.GONE);
                    loading.setVisibility(View.VISIBLE);
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        List<Integer> assigneeIds = new ArrayList<>();
                        for (Assignee iter : issue.getAssignees())
                        {
                            assigneeIds.add(iter.getId());
                        }
                        assigneeIds.add(usableCollaborators.get(finalI).getId());
                        try
                        {
                            api.getIssuesApi().updateIssue(project, issue.getIid(), null, null, null, assigneeIds, null, null, null, null, null);
                            runOnUiThread(this::recreate);
                        }
                        catch (GitLabApiException e)
                        {
                            e.printStackTrace();
                            runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, true));
                        }
                    });
                    return true;
                });
            });
        }
    }
}
