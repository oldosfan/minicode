package low.minicode.activity.gitlab.issues.milestones;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.issues.milestones.GitlabMilestoneListAdapter;

public class GitlabMilestonesActivity extends ThemedAppCompatActivity
{
    private RecyclerView recyclerView;
    private Project project;
    private GitLabApi api;
    private GitlabMilestoneListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        if (getIntent().getStringExtra("serialized_project") == null || getIntent().getStringExtra("token") == null)
        {
            Library.vibrateError();
            finish();
            return;
        }
        project = new Gson().fromJson(getIntent().getStringExtra("serialized_project"), Project.class);
        api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getIntent().getStringExtra("token"));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                (adapter = new GitlabMilestoneListAdapter(api, project)).refresh();
                runOnUiThread(() -> initialize(savedInstanceState));
            }
            catch (GitLabApiException e)
            {
                Library.vibrateError();
                Library.displayErrorDialogFromThrowable(this, e, true, this::finish);
                return;
            }
        });
    }

    private void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_gitlab_milestones);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return super.onSupportNavigateUp();
    }
}
