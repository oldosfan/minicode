/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.mergerequests;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.GitlabMergeRequestAdapter;

public class GitlabMergeRequestsActivity extends ThemedAppCompatActivity
{
    private Project project;
    private GitLabApi api;
    private GitlabMergeRequestAdapter adapter;
    private RecyclerView view;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int projectId;
        String token;
        projectId = getIntent().getIntExtra("project_id", -1);
        token = getIntent().getStringExtra("token");
        if (projectId == -1 || token == null)
        {
            Library.vibrateError();
            return;
        }
        Executors.newSingleThreadExecutor().submit(() ->
        {
            api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token);
            try
            {
                project = api.getProjectApi().getProject(projectId);
                adapter = new GitlabMergeRequestAdapter(api, project);
                adapter.refresh();
                runOnUiThread(() -> initalize(savedInstanceState));
            }
            catch (GitLabApiException | NullPointerException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                runOnUiThread(this::finish);
                return;
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return super.onSupportNavigateUp();
    }

    public void initalize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_gitlab_merge_requests);
        view = findViewById(R.id.recycler_view);
        view.setLayoutManager(new LinearLayoutManager(this));
        view.setAdapter(adapter);
        adapter.notifyItemChanged(0, adapter.getItemCount());
    }
}
