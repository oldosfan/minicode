/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.integration;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.User;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class GitlabLoginHelperActivity extends ThemedAppCompatActivity
{
    private EditText password;
    private EditText username;
    private MaterialButton button;
    private TextView error;
    private ProgressBar indeterminateindicator;
    private ImageView loggedInAvatar;

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("gitlab_token", null) == null)
        {
            setContentView(R.layout.activity_gitlab_login_helper);
            password = findViewById(R.id.password);
            username = findViewById(R.id.username);
            button = findViewById(R.id.login);
            indeterminateindicator = findViewById(R.id.login_indicator);
            error = findViewById(R.id.error);
            button.setOnClickListener(v ->
            {
                if (password.getText().toString().isEmpty() || username.getText().toString().trim().isEmpty())
                {
                    Library.vibrateError();
                    error.setText(R.string.username_or_password_empty);
                    error.setVisibility(View.VISIBLE);
                    return;
                }
                else
                {
                    error.setVisibility(View.GONE);
                    button.setVisibility(View.INVISIBLE);
                    button.setClickable(false);
                    indeterminateindicator.setVisibility(View.VISIBLE);
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            GitLabApi api = GitLabApi.oauth2Login("https://gitlab.com", username.getText().toString().trim(), password.getText().toString());
                            getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit().putString("gitlab_token", api.getAuthToken()).apply();
                            Library.vibrateSuccess();
                            recreate();
                        }
                        catch (GitLabApiException e)
                        {
                            e.printStackTrace();
                            runOnUiThread(() ->
                            {
                                error.setText(e.getMessage());
                                error.setVisibility(View.VISIBLE);
                                button.setClickable(true);
                                button.setVisibility(View.VISIBLE);
                                indeterminateindicator.setVisibility(View.INVISIBLE);
                            });
                        }
                    });
                }
            });
        }
        else
        {
            setContentView(R.layout.activity_gitlab_login_helper_logged_in);
            TextView username;
            username = findViewById(R.id.username);
            button = findViewById(R.id.login);
            loggedInAvatar = findViewById(R.id.logged_in_user_avatar);
            Executors.newSingleThreadExecutor().submit(() ->
            {
                GitLabApi api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("gitlab_token", null));
                try
                {
                    User user = api.getUserApi().getCurrentUser();
                    username.setText(user.getName());
                    if (user.getAvatarUrl() != null)
                    {
                        Bitmap bitmap = BitmapFactory.decodeStream(new URL(user.getAvatarUrl()).openStream());
                        runOnUiThread(() -> loggedInAvatar.setImageBitmap(bitmap));
                    }
                }
                catch (GitLabApiException | IOException e)
                {
                    getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit().remove("gitlab_token").commit();
                    runOnUiThread(this::recreate);
                }
            });
            button.setOnClickListener(v ->
            {
                getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).edit().remove("gitlab_token").commit();
                recreate();
            });
        }
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }
}
