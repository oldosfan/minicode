package low.minicode.activity.gitlab.tags;

import android.os.Bundle;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class GitlabTagsActivity extends ThemedAppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_tags);
    }
}
