/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.mergerequests;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.fragments.gitlab.mergerequests.GitlabMergeRequestDiscussionFragment;
import low.minicode.fragments.gitlab.mergerequests.GitlabMergeRequestSummaryFragment;

public class GitlabMergeRequestSummaryActivitiy extends ThemedAppCompatActivity
{
    private GitLabApi api;
    private Project project;
    private MergeRequest request;
    private ViewPager fragmentPager;
    private BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            int projectId, mergeRequestIid;
            String token;
            token = getIntent().getStringExtra("token");
            projectId = getIntent().getIntExtra("project_id", -1);
            mergeRequestIid = getIntent().getIntExtra("merge_request_iid", -1);
            if (projectId == -1 || mergeRequestIid == -1 || token == null)
            {
                Library.vibrateError();
                runOnUiThread(this::finish);
                return;
            }
            api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token);
            try
            {
                do
                {
                    project = api.getProjectApi().getProject(projectId);
                }
                while (project == null);
                do
                {
                    request = api.getMergeRequestApi().getMergeRequest(project, mergeRequestIid);
                }
                while (request == null);
                runOnUiThread(() -> initialize(savedInstanceState));

            }
            catch (GitLabApiException e)
            {
                runOnUiThread(() ->
                {
                    Library.vibrateError();
                    Library.displayErrorDialogFromThrowable(this, e, true, this::finish);
                });
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_merge_request_summary);
        setTitle(R.string.summary);
        navigationView = findViewById(R.id.merge_request_navigation);
        fragmentPager = findViewById(R.id.fragment_transaction_pager);
        fragmentPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager())
        {
            @NonNull
            @Override
            public Fragment getItem(int position)
            {
                return position == 0 ? new GitlabMergeRequestSummaryFragment(project, request, api) : new GitlabMergeRequestDiscussionFragment(project, request, api);
            }

            @Override
            public int getCount()
            {
                return 2;
            }
        });
        fragmentPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                setTitle(position == 0 ? R.string.summary : R.string.discussion);
                navigationView.setSelectedItemId(position == 0 ? R.id.information : R.id.discussion);
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        navigationView.setOnNavigationItemSelectedListener(item ->
        {
            fragmentPager.setCurrentItem(item.getItemId() == R.id.information ? 0 : 1);
            return true;
        });
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return super.onSupportNavigateUp();
    }
}
