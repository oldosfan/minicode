package low.minicode.activity.gitlab.commits;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Project;

import java.io.IOException;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.fragments.gitlab.commits.discussion.GitlabCommitDiscussionFragment;
import low.minicode.fragments.gitlab.commits.summary.GitlabCommitSummaryFragment;

public class GitlabCommitSummaryActivity extends ThemedAppCompatActivity
{
    private GitLabApi api;
    private Project project;
    private Commit commit;
    private ViewPager pager;
    private BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_commit_summary);
        if (!getIntent().hasExtra("serialized_commit") || !getIntent().hasExtra("serialized_project") || !getIntent().hasExtra("token"))
        {
            Library.vibrateError();
            finish();
            return;
        }
        ObjectMapper mapper = new ObjectMapper();
        api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getIntent().getStringExtra("token"));
        try
        {
            project = mapper.readValue(getIntent().getStringExtra("serialized_project"), Project.class);
            commit = mapper.readValue(getIntent().getStringExtra("serialized_commit"), Commit.class);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Library.vibrateError();
            Library.displayErrorDialogFromThrowable(this, e, true, this::finish);
            return;
        }
        setTitle(commit.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pager = findViewById(R.id.commit_summary_view_pager);
        navigationView = findViewById(R.id.commit_summary_nav_view);
        navigationView.setOnNavigationItemSelectedListener(item ->
        {
            pager.setCurrentItem(item.getOrder());
            return true;
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                navigationView.setSelectedItemId(navigationView.getMenu().getItem(position).getItemId());
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });
        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager())
        {
            @NonNull
            @Override
            public Fragment getItem(int position)
            {
                return position == 0 ? new GitlabCommitSummaryFragment(project, commit, api) : new GitlabCommitDiscussionFragment(project, commit, api);
            }

            @Override
            public int getCount()
            {
                return 2;
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return super.onSupportNavigateUp();
    }
}
