/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.issues;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.chip.Chip;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import ru.noties.markwon.Markwon;

public class NewGitlabIssueActivity extends ThemedAppCompatActivity
{
    private Project project;
    private GitLabApi api;
    private TextInputEditText editText, title;
    private Chip preview;
    private String rawBody;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        String token;
        int projectId;
        if ((token = getIntent().getStringExtra("token")) == null || (projectId = getIntent().getIntExtra("project_id", -1)) == -1)
        {
            finish();
            return;
        }
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                project = (api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token)).getProjectApi().getProject(projectId);
                runOnUiThread(() -> initialize(savedInstanceState));
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                finish();
                return;
            }
        });
    }

    private void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_new_gitlab_issue);
        editText = findViewById(R.id.issue_body);
        preview = findViewById(R.id.preview);
        title = findViewById(R.id.issue_name);
        preview.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked)
            {
                rawBody = editText.getText() != null ? editText.getText().toString() : "";
                Markwon.create(this).setMarkdown(editText, rawBody);
            }
            else
            {
                editText.setText(rawBody == null ? "" : rawBody);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        int SUBMIT_ISSUE = 1;
        MenuItem menuItem = menu.add(Menu.NONE, 1, Menu.NONE, R.string.submit);
        menuItem.setIcon(R.drawable.ic_menu_send);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menuItem.setOnMenuItemClickListener(menuItem2 ->
        {
            if (editText == null || title == null)
            {
                return false;
            }
            String bodyText = editText.getText().toString();
            String titleText = title.getText().toString();
            if (titleText.trim().isEmpty())
            {
                Library.vibrateError();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    for (int iter : Library.range(1, 4))
                    {
                        runOnUiThread(() -> title.requestFocus());
                        try
                        {
                            Thread.sleep(100);
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                        runOnUiThread(() -> title.clearFocus());
                        try
                        {
                            Thread.sleep(100);
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    runOnUiThread(() -> title.requestFocus());
                });
                Snackbar.make(editText, R.string.we_need_something_here, Snackbar.LENGTH_LONG).show();
            }
            else if (bodyText.isEmpty())
            {
                Library.vibrateError();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    for (int iter : Library.range(1, 4))
                    {
                        runOnUiThread(() -> editText.requestFocus());
                        try
                        {
                            Thread.sleep(100);
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                        runOnUiThread(() -> editText.clearFocus());
                        try
                        {
                            Thread.sleep(100);
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    runOnUiThread(() -> editText.requestFocus());
                });
                Snackbar.make(editText, R.string.we_need_something_here, Snackbar.LENGTH_LONG).show();
            }
            else
            {
                menuItem.setEnabled(false);
                Drawable iconGrey = menuItem.getIcon().getConstantState().newDrawable().mutate();
                iconGrey.setTint(getResources().getColor(R.color.grey_400));
                menuItem.setIcon(iconGrey);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        api.getIssuesApi().createIssue(project, titleText, bodyText);
                        runOnUiThread(() -> finish());
                    }
                    catch (GitLabApiException e)
                    {
                        Library.vibrateError();
                        runOnUiThread(() ->
                        {
                            Library.displayErrorDialogFromThrowable(this, e, true);
                            menuItem.setIcon(R.drawable.ic_menu_send);
                            menuItem.setEnabled(true);
                        });
                        e.printStackTrace();
                    }
                });
            }
            return true;
        });
        return super.onCreateOptionsMenu(menu);
    }
}
