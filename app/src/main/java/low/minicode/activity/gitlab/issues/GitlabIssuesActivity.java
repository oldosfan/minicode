/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.issues;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.issues.GitlabIssuesListAdapter;

public class GitlabIssuesActivity extends ThemedAppCompatActivity
{
    private final static int REQUEST_CREATE = 1;
    private Project project;
    private RecyclerView view;
    private GitLabApi api;
    private GitlabIssuesListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        if (!getIntent().hasExtra("token"))
        {
            finish();
            return;
        }
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                project = (api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getIntent().getStringExtra("token"))).getProjectApi().getProject(getIntent().getIntExtra("project_id", 0));
                adapter = new GitlabIssuesListAdapter(api, project, getIntent().getIntExtra("milestone_id", -1) == -1 ? null : getIntent().getIntExtra("milestone_id", -1));
                adapter.refresh();
            }
            catch (GitLabApiException e)
            {
                Library.vibrateError();
                return;
            }
            runOnUiThread(() -> this.initialize(savedInstanceState));
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuItem item = menu.add(R.string.add);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setIcon(R.drawable.ic_add_blue_24dp);
        item.setOnMenuItemClickListener(menuItem ->
        {
            Intent i = new Intent(this, NewGitlabIssueActivity.class);
            i.putExtra("project_id", project.getId());
            i.putExtra("token", api.getAuthToken());
            startActivityForResult(i, REQUEST_CREATE);
            return true;
        });
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                adapter.refresh();
            }
            catch (GitLabApiException e)
            {
                Library.vibrateError();
                return;
            }
            runOnUiThread(() -> adapter.notifyDataSetChanged());
        });
    }

    protected void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_gitlab_issues);
        view = findViewById(R.id.gitlab_issues);
        view.setLayoutManager(new LinearLayoutManager(this));
        view.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
