/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.integration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.gitlab.projects.GitlabProjectSummaryActivity;

public class GitlabIntentRedirectActivity extends ThemedAppCompatActivity
{
    private final static int REQUEST_GITLAB_LOGIN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_intent_redirect);
        if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("gitlab_token", null) == null)
        {
            Library.vibrateError();
            Intent i = new Intent(this, GitlabLoginHelperActivity.class);
            startActivityForResult(i, REQUEST_GITLAB_LOGIN);
        }
        else
        {
            onActivityResult(REQUEST_GITLAB_LOGIN, 0, null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = getIntent().getData();
        GitLabApi api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("gitlab_token", null));
        if (uri == null)
        {
            Library.vibrateError();
            Toast.makeText(this, R.string.invalid_intent, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        else if (uri.getPathSegments().size() == 2)
        {
            Executors.newSingleThreadExecutor().submit(() ->
            {
                Log.d(getClass().getName(), "onActivityResult: this appears to be a project URI. Parsing URI to try to find the project");
                try
                {
                    Project project = api.getProjectApi().getProject(uri.getPathSegments().get(0), uri.getPathSegments().get(1));
                    Log.d(getClass().getName(), "onActivityResult: found project: " + new Gson().toJson(project));
                    Intent i = new Intent(this, GitlabProjectSummaryActivity.class);
                    i.putExtra("project", project.getPathWithNamespace());
                    i.putExtra("apiKey", getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("gitlab_token", null));
                    runOnUiThread(() -> startActivity(i));
                    finish();
                }
                catch (GitLabApiException e)
                {
                    Log.e(getClass().getName(), "onActivityResult: getProject threw a GitLabApiException: " + new Gson().toJson(e));
                    e.printStackTrace();
                    runOnUiThread(() ->
                    {
                        Library.vibrateError();
                        Library.displayErrorDialogFromThrowable(this, e, true, this::finish);
                    });
                }
            });
        }
        else
        {
            Library.vibrateError();
            Toast.makeText(this, R.string.link_not_yet_supported_by_minicode_gitlab_integration, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }
}
