/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.navigation;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.navigator.GitlabFilesAdapter;

public class GitlabNavigatorActivity extends ThemedAppCompatActivity
{
    private RecyclerView recyclerView;
    private ProgressBar loadingIndicator;
    private Project project;
    private String ref, loc;
    private GitLabApi api;
    private GitlabFilesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_navigator);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.files);
        loadingIndicator = findViewById(R.id.loading_indicator);
        recyclerView.setVisibility(View.INVISIBLE);
        int projectId = getIntent().getIntExtra("project_id", -1);
        String token = getIntent().getStringExtra("token");
        ref = getIntent().getStringExtra("ref");
        loc = getIntent().getStringExtra("loc");
        if (projectId == -1 || token == null || token.isEmpty() || ref == null || ref.isEmpty() || loc == null)
        {
            finish();
            return;
        }
        api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token);
        setTitle(loc);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                project = api.getProjectApi().getProject(projectId);
            }
            catch (GitLabApiException e)
            {
                Library.vibrateError();
                finish();
            }
            runOnUiThread(() -> initialize(savedInstanceState));
        });
    }

    protected void initialize(Bundle savedInstanceState)
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter = new GitlabFilesAdapter(loc, project, api, ref));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                adapter.refresh();
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
            runOnUiThread(() ->
            {
                loadingIndicator.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
            });
        });
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }
}
