/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.secrets;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.secrets.GitlabSSHKeyListAdapter;

public class GitlabSSHKeyManagementActivity extends ThemedAppCompatActivity
{
    private GitLabApi api;
    private GitlabSSHKeyListAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            adapter = new GitlabSSHKeyListAdapter(api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getIntent().getStringExtra("token")));
            try
            {
                adapter.refresh();
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                finish();
                return;
            }
            runOnUiThread(() -> initialize(savedInstanceState));
        });
    }

    private void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_gitlab_sshkey_management);
        recyclerView = findViewById(R.id.ssh_keys);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.new_key, menu);
        menu.getItem(0).setOnMenuItemClickListener(menuItem ->
        {
            MaterialAlertDialogBuilder selectAction = new MaterialAlertDialogBuilder(this);
            selectAction.setItems(new CharSequence[] {getString(R.string.add_minicode_key), getString(R.string.manually_add_key)}, (dialog, which) ->
            {
                if (which == 0)
                {
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            api.getUserApi().addSshKey("miniCode-" + Minicode.uuid.toString(), getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getString("ssh_public_key", null));
                            adapter.refresh();
                            runOnUiThread(adapter::notifyDataSetChanged);
                        }
                        catch (GitLabApiException e)
                        {
                            e.printStackTrace();
                            Library.vibrateError();
                            runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, true));
                        }
                    });
                }
                else
                {
                    Library.askDialogString(true, getString(R.string.name), null, this, params ->
                    {
                        String name = params[0];
                        Library.askDialogString(true, getString(R.string.key), null, this, params2 ->
                        {
                            String key = params2[0];
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                try
                                {
                                    api.getUserApi().addSshKey(name, key);
                                    adapter.refresh();
                                    runOnUiThread(adapter::notifyDataSetChanged);
                                }
                                catch (GitLabApiException e)
                                {
                                    e.printStackTrace();
                                    Library.vibrateError();
                                    runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, true));
                                }
                            });
                            return null;
                        });
                        return null;
                    });
                }
            });
            selectAction.show();
            return true;
        });
        return true;
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }
}
