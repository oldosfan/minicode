/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.ci;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Project;

import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.ci.pager.GitlabCIFragmentPagerAdapter;

public class GitlabContinualIntegrationActivity extends ThemedAppCompatActivity
{
    private Project project;
    private GitLabApi api;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_continual_integration);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        whitify();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().getStringExtra("project_serialized") == null || getIntent().getStringExtra("token") == null)
        {
            finish();
            return;
        }
        else
        {
            project = new Gson().fromJson(getIntent().getStringExtra("project_serialized"), Project.class);
            api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getIntent().getStringExtra("token"));
        }
        tabLayout = findViewById(R.id.ci_switcher);
        viewPager = findViewById(R.id.ci_fragment_placeholder);
        viewPager.setAdapter(new GitlabCIFragmentPagerAdapter(getSupportFragmentManager(), api, project));
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return super.onSupportNavigateUp();
    }
}
