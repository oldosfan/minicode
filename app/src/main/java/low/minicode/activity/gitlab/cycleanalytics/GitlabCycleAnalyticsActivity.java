package low.minicode.activity.gitlab.cycleanalytics;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Project;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.cycleanalytics.pager2.GitlabCycleAnalyticsInformationPanelAdapter;

public class GitlabCycleAnalyticsActivity extends ThemedAppCompatActivity
{
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager recentsInformation;
    private ViewPager2 analyticsInformation;
    private NavigationView analyticsNavigationView;
    private GitLabApi api;
    private Project project;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_cycle_analytics);
        whitify();
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_blue);
        toolbar.setNavigationOnClickListener(this::finish);
        tabLayout = findViewById(R.id.cycle_analytics_tabs);
        recentsInformation = findViewById(R.id.cycle_analytics_pager);
        analyticsInformation = findViewById(R.id.cycle_analytics_information);
        analyticsNavigationView = findViewById(R.id.cycle_analytics_navigator);
        String serializedProject = getIntent().getStringExtra("serialized_project");
        String token = getIntent().getStringExtra("token");
        if (serializedProject == null || token == null)
        {
            finish();
            return;
        }
        project = new Gson().fromJson(serializedProject, Project.class);
        api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token);
        initialize(savedInstanceState);
    }

    public void initialize(Bundle savedInstanceState)
    {
        Executors.newSingleThreadExecutor().submit(() ->
        {
            String rawCycleJsonData;
            JSONObject object;
            JSONArray summary;
            try
            {
                rawCycleJsonData = IOUtils.toString(new URL(project.getWebUrl() + "/cycle_analytics.json"), Charset.defaultCharset());
            }
            catch (IOException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, true, () -> finish()));
                return;
            }
            try
            {
                object = new JSONObject(rawCycleJsonData);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, true, () -> finish()));
                return;
            }
            try
            {
                summary = object.getJSONArray("summary");
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, true, () -> finish()));
                return;
            }
            recentsInformation.post(() ->
            {
                recentsInformation.setAdapter(new PagerAdapter()
                {
                    @Override
                    public int getCount()
                    {
                        return summary.length();
                    }

                    @Override
                    public CharSequence getPageTitle(int position)
                    {
                        return position == 0 ? getString(R.string.new_issues) : (position == 1 ? getString(R.string.commits) : getString(R.string.deploys));
                    }

                    @Override
                    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object)
                    {
                        container.removeView((View) object);
                    }

                    @NonNull
                    @Override
                    public Object instantiateItem(@NonNull ViewGroup container, int position)
                    {
                        View v;
                        container.addView(v = LayoutInflater.from(GitlabCycleAnalyticsActivity.this).inflate(R.layout.basic_cycleanalytics_header_view, container, false));
                        ((TextView) v.findViewById(R.id.cycle_analytics_header_what)).setText(position == 1 ? getString(R.string.commits) : (position == 0 ? getString(R.string.new_issues) : getString(R.string.deploys)));
                        try
                        {
                            ((TextView) v.findViewById(R.id.cycle_analytics_header_amount)).setText(String.valueOf(summary.getJSONObject(position).getInt("value")));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Library.vibrateError();
                        }
                        return v;
                    }

                    @Override
                    public boolean isViewFromObject(@NonNull View view, @NonNull Object object)
                    {
                        return object.equals(view);
                    }
                });
                tabLayout.setupWithViewPager(recentsInformation);
                GitlabCycleAnalyticsInformationPanelAdapter adapter;
                analyticsInformation.setAdapter(adapter = new GitlabCycleAnalyticsInformationPanelAdapter(object));
                adapter.notifyDataSetChanged();
                analyticsInformation.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback()
                {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
                    {
                        super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                    }

                    @Override
                    public void onPageSelected(int position)
                    {
                        super.onPageSelected(position);
                        analyticsNavigationView.setCheckedItem(analyticsNavigationView.getMenu().getItem(position));
                    }

                    @Override
                    public void onPageScrollStateChanged(int state)
                    {
                        super.onPageScrollStateChanged(state);
                    }
                });
                analyticsNavigationView.setCheckedItem(R.id.issues);
                analyticsNavigationView.setNavigationItemSelectedListener(item ->
                {
                    switch (item.getItemId())
                    {
                        case R.id.issues:
                            analyticsInformation.setCurrentItem(0);
                            break;
                        case R.id.plan:
                            analyticsInformation.setCurrentItem(1);
                            break;
                        case R.id.code:
                            analyticsInformation.setCurrentItem(2);
                            break;
                        case R.id.test:
                            analyticsInformation.setCurrentItem(3);
                            break;
                        case R.id.review:
                            analyticsInformation.setCurrentItem(4);
                            break;
                        case R.id.staging:
                            analyticsInformation.setCurrentItem(5);
                            break;
                        case R.id.production:
                            analyticsInformation.setCurrentItem(6);
                            break;
                    }
                    analyticsNavigationView.setCheckedItem(item);
                    return true;
                });
            });
        });
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }
}
