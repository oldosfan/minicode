/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.issues;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AwardEmoji;
import org.gitlab4j.api.models.Discussion;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.Milestone;
import org.gitlab4j.api.models.Note;
import org.gitlab4j.api.models.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.GitlabNestedDiscussionAdapter;
import ru.noties.markwon.Markwon;
import ru.noties.markwon.core.CorePlugin;
import ru.noties.markwon.image.ImagesPlugin;

public class GitlabIssueSummaryActivity extends ThemedAppCompatActivity
{
    private static final int REQ_EDIT_ASSIGNEES = 1;
    private Toolbar toolbar;
    private CardView upvote, downvote;
    private ImageView assignee;
    private TextView markwon, status, author, upvotes, downvotes, no, assignees;
    private GitLabApi api;
    private Project project;
    private Issue issue;
    private Runnable issuesRunnable;
    private RecyclerView discussions;
    private GitlabNestedDiscussionAdapter adapter;
    private ImageButton editAssignees;
    private MaterialButton milestones;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        whitify();
        if (getIntent().getStringExtra("token") == null || getIntent().getIntExtra("project_id", -1) < 0 || getIntent().getIntExtra("issue_id", -1) < 0)
        {
            Library.vibrateError();
            return;
        }
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, getIntent().getStringExtra("token"));
                project = api.getProjectApi().getProject(getIntent().getIntExtra("project_id", 0));
                issue = api.getIssuesApi().getIssue(project.getId(), getIntent().getIntExtra("issue_id", -1));
            }
            catch (GitLabApiException | NullPointerException e)
            {
                Library.vibrateError();
                e.printStackTrace();
                finish();
            }
            runOnUiThread(() -> initialize(savedInstanceState));
        });
    }

    protected void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_gitlab_issue_summary);
        toolbar = findViewById(R.id.toolbar);
        upvote = findViewById(R.id.thumbs_up);
        downvote = findViewById(R.id.thumbs_down);
        upvotes = findViewById(R.id.thumbs_up_counter);
        downvotes = findViewById(R.id.thumbs_down_counter);
        assignee = findViewById(R.id.assignee_avatar);
        markwon = findViewById(R.id.issue_readme);
        status = findViewById(R.id.issue_status);
        author = findViewById(R.id.issue_author);
        editAssignees = findViewById(R.id.edit_assignees);
        assignees = findViewById(R.id.assignees);
        discussions = findViewById(R.id.issue_discussions);
        milestones = findViewById(R.id.select_milestones);
        no = findViewById(R.id.issue_no);
        toolbar.setTitle(issue.getTitle());
        setSupportActionBar(toolbar);
        author.setText(issue.getAuthor() != null ? issue.getAuthor().getName() : "");
        no.setText("#" + issue.getIid());
        editAssignees.setOnClickListener(v ->
        {
            Intent i = new Intent(this, GitlabIssueAssigneeManagementActivity.class);
            i.putExtra("token", api.getAuthToken());
            i.putExtra("project_id", project.getId());
            i.putExtra("issue_id", issue.getIid());
            startActivityForResult(i, REQ_EDIT_ASSIGNEES);
        });
        status.setText(issue.getState() == Constants.IssueState.CLOSED ? R.string.closed : R.string.open);
        status.setTextColor(issue.getState() == Constants.IssueState.CLOSED ? getResources().getColor(R.color.md_red_700) : getResources().getColor(R.color.md_green_700));
        status.setOnClickListener(v ->
        {
            if (issue.getState() == Constants.IssueState.CLOSED)
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        issue = api.getIssuesApi().updateIssue(project, issue.getIid(), null, null, null, null, null, null, Constants.StateEvent.REOPEN, null, null);
                        runOnUiThread(() ->
                        {
                            status.setText(issue.getState() == Constants.IssueState.CLOSED ? R.string.closed : R.string.open);
                            status.setTextColor(issue.getState() == Constants.IssueState.CLOSED ? getResources().getColor(R.color.md_red_700) : getResources().getColor(R.color.md_green_700));
                        });
                        try
                        {
                            List<Discussion> discussions = api.getDiscussionsApi().getIssueDiscussions(project, issue.getIid());
                            runOnUiThread(() -> GitlabIssueSummaryActivity.this.discussions.setAdapter(adapter = new GitlabNestedDiscussionAdapter(api, project, discussions)));
                        }
                        catch (GitLabApiException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                    }
                });
            }
            else
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        issue = api.getIssuesApi().closeIssue(project, issue.getIid());
                        runOnUiThread(() ->
                        {
                            status.setText(issue.getState() == Constants.IssueState.CLOSED ? R.string.closed : R.string.open);
                            status.setTextColor(issue.getState() == Constants.IssueState.CLOSED ? getResources().getColor(R.color.md_red_700) : getResources().getColor(R.color.md_green_700));
                        });
                        List<Discussion> discussions = api.getDiscussionsApi().getIssueDiscussions(project, issue.getIid());
                        runOnUiThread(() -> GitlabIssueSummaryActivity.this.discussions.setAdapter(adapter = new GitlabNestedDiscussionAdapter(api, project, discussions)));
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                    }
                });
            }
        });
        if (issue.getAssignees().size() < 1)
        {
            assignee.setVisibility(View.GONE);
            assignees.setVisibility(View.GONE);
        }
        else
        {
            StringBuilder builder = new StringBuilder();
            builder.append(getString(R.string.assigned_to_c));
            if (issue.getAssignees().size() == 1)
            {
                builder.append(" ").append(issue.getAssignee().getName());
            }
            else if (issue.getAssignees().size() > 1)
            {
                builder.append(" ").append(issue.getAssignee().getName()).append(getString(R.string.comma_and_d_more, issue.getAssignees().size() - 1));
            }
            else
            {
                builder.append(" ").append(getString(R.string.no_assignee));
            }
            assignees.setText(builder);
        }
        issuesRunnable = () ->
        {
            int upvotes = 0;
            int downvotes = 0;
            int upid = 0;
            int downid = 0;
            boolean downvoted = false;
            boolean upvoted = false;
            try
            {
                List<AwardEmoji> awardEmojis = api.getAwardEmojiApi().getIssueAwardEmojis(project.getId(), issue.getIid());
                for (AwardEmoji awardEmoji : awardEmojis)
                {
                    if (awardEmoji.getName().equals("thumbsup"))
                    {
                        ++upvotes;
                        if (awardEmoji.getUser().getName().equals(api.getUserApi().getCurrentUser().getName()))
                        {
                            upvoted = true;
                            upid = awardEmoji.getId();
                        }
                    }
                    else if (awardEmoji.getName().equals("thumbsdown"))
                    {
                        ++downvotes;
                        if (awardEmoji.getUser().getName().equals(api.getUserApi().getCurrentUser().getName()))
                        {
                            downvoted = true;
                            downid = awardEmoji.getId();
                        }
                    }
                }
                int finalUpvotes = upvotes;
                int finalDownvotes = downvotes;
                boolean finalUpvoted = upvoted;
                boolean finalDownvoted = downvoted;
                int finalDownid = downid;
                int finalUpid = upid;
                runOnUiThread(() ->
                {
                    GitlabIssueSummaryActivity.this.upvotes.setText(String.valueOf(finalUpvotes));
                    GitlabIssueSummaryActivity.this.downvotes.setText(String.valueOf(finalDownvotes));
                    upvote.setOnClickListener(v ->
                    {
                        if (finalUpvoted)
                        {
                            upvote.setOnClickListener(null);
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                try
                                {
                                    api.getAwardEmojiApi().deleteIssueAwardEmoji(project.getId(), issue.getIid(), finalUpid);
                                    reinitializeAwardEmojis();
                                }
                                catch (GitLabApiException e)
                                {
                                    e.printStackTrace();
                                }
                            });
                        }
                        else
                        {
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                try
                                {
                                    api.getAwardEmojiApi().addIssueAwardEmoji(project.getId(), issue.getIid(), "thumbsup");
                                }
                                catch (GitLabApiException e)
                                {
                                    e.printStackTrace();
                                }
                                reinitializeAwardEmojis();
                            });

                        }
                    });
                    downvote.setOnClickListener(v ->
                    {
                        if (finalDownvoted)
                        {
                            downvote.setOnClickListener(null);
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                try
                                {
                                    api.getAwardEmojiApi().deleteIssueAwardEmoji(project.getId(), issue.getIid(), finalDownid);
                                    reinitializeAwardEmojis();
                                }
                                catch (GitLabApiException e)
                                {
                                    e.printStackTrace();
                                }
                            });
                        }
                        else
                        {
                            try
                            {
                                api.getAwardEmojiApi().addIssueAwardEmoji(project.getId(), issue.getIid(), "thumbsdown");
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                            reinitializeAwardEmojis();
                        }
                    });
                });
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        };
        reinitializeAwardEmojis();
        Markwon markwon2 = Markwon.builder(this).usePlugin(CorePlugin.create()).usePlugin(ImagesPlugin.create(this)).build();
        Spanned formatted = markwon2.toMarkdown(issue.getDescription());
        markwon2.setParsedMarkdown(markwon, formatted);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                List<Discussion> discussions = api.getDiscussionsApi().getIssueDiscussions(project, issue.getIid());
                runOnUiThread(() ->
                {
                    GitlabIssueSummaryActivity.this.discussions.setLayoutManager(new LinearLayoutManager(this));
                    GitlabIssueSummaryActivity.this.discussions.setAdapter(adapter = new GitlabNestedDiscussionAdapter(api, project, discussions));
                    adapter.setHandler(new GitlabNestedDiscussionAdapter.ReplyDeleteHandler()
                    {
                        @Override
                        public void reply(Note note, Context c, String newContent)
                        {
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                try
                                {
                                    api.getNotesApi().createIssueNote(project, issue.getIid(), newContent);
                                    List<Discussion> discussions = api.getDiscussionsApi().getIssueDiscussions(project, issue.getIid());
                                    runOnUiThread(() -> GitlabIssueSummaryActivity.this.discussions.setAdapter(adapter = new GitlabNestedDiscussionAdapter(api, project, discussions)));
                                }
                                catch (GitLabApiException e)
                                {
                                    e.printStackTrace();
                                }
                            });
                        }

                        @Override
                        public void delete(Note note, Context c)
                        {
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                try
                                {
                                    api.getNotesApi().deleteIssueNote(project, issue.getIid(), note.getId());
                                    List<Discussion> discussions = api.getDiscussionsApi().getIssueDiscussions(project, issue.getIid());
                                    runOnUiThread(() -> GitlabIssueSummaryActivity.this.discussions.setAdapter(adapter = new GitlabNestedDiscussionAdapter(api, project, discussions)));
                                }
                                catch (GitLabApiException e)
                                {
                                    e.printStackTrace();
                                }
                            });
                        }

                        @Override
                        public void edit(Note note, Context c, String newContent)
                        {
                        }
                    });
                });
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
        Executors.newSingleThreadExecutor().submit(() ->
        {
            List<String> wrappers = new ArrayList<>();
            try
            {
                List<Milestone> milestones = api.getMilestonesApi().getMilestones(project);
                for (Milestone iter : milestones)
                {
                    wrappers.add(iter.getTitle());
                }
                wrappers.add(getString(R.string.none));
                runOnUiThread(() ->
                {
                    GitlabIssueSummaryActivity.this.milestones.setOnClickListener(view ->
                    {
                        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this);
                        materialAlertDialogBuilder.setTitle(R.string.select_milestone);
                        int i = 0;
                        if (issue.getMilestone() != null)
                        {
                            for (Milestone iter : milestones)
                            {
                                if (iter.getIid().equals(issue.getMilestone().getIid()))
                                {
                                    break;
                                }
                                ++i;
                            }
                        }
                        else
                        {
                            i = wrappers.size() - 1;
                        }
                        materialAlertDialogBuilder.setSingleChoiceItems(Arrays.copyOf(wrappers.toArray(), wrappers.size(), String[].class), i, (dialog, which) ->
                        {
                            Executors.newSingleThreadExecutor().submit(() ->
                            {
                                try
                                {
                                    issue = api.getIssuesApi().updateIssue(project, issue.getIid(), null, null, null, null, which < milestones.size() ? milestones.get(which).getId() : 0, null, null, null, null);
                                }
                                catch (GitLabApiException e)
                                {
                                    Library.vibrateError();
                                    runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, false, dialog::cancel));
                                }
                            });
                        });
                        materialAlertDialogBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
                        {
                        });
                        materialAlertDialogBuilder.show();
                    });
                });
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                issue = api.getIssuesApi().getIssue(project, issue.getIid());
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
            runOnUiThread(() ->
            {
                if (requestCode == REQ_EDIT_ASSIGNEES)
                {
                    if (issue.getAssignees().size() < 1)
                    {
                        assignee.setVisibility(View.GONE);
                        assignees.setVisibility(View.GONE);
                    }
                    else
                    {
                        assignee.setVisibility(View.VISIBLE);
                        assignees.setVisibility(View.VISIBLE);
                        StringBuilder builder = new StringBuilder();
                        builder.append(getString(R.string.assigned_to_c));
                        if (issue.getAssignees().size() == 1)
                        {
                            builder.append(" ").append(issue.getAssignee().getName());
                        }
                        else if (issue.getAssignees().size() > 1)
                        {
                            builder.append(" ").append(issue.getAssignee().getName()).append(getString(R.string.comma_and_d_more, issue.getAssignees().size() - 1));
                        }
                        else
                        {
                            builder.append(" ").append(getString(R.string.no_assignee));
                        }
                        assignees.setText(builder);
                    }
                }
            });
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuItem item = menu.add(R.string.new_note);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item.setIcon(R.drawable.ic_menu_send);
        item.setOnMenuItemClickListener(item1 ->
        {
            Library.askDialogString(true, getString(R.string.new_note), null, this, params ->
            {
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        api.getNotesApi().createIssueNote(project, issue.getIid(), params[0]);
                        List<Discussion> discussions = api.getDiscussionsApi().getIssueDiscussions(project, issue.getIid());
                        runOnUiThread(() -> GitlabIssueSummaryActivity.this.discussions.setAdapter(adapter = new GitlabNestedDiscussionAdapter(api, project, discussions)));
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                    }
                });
                return null;
            });
            return true;
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void reinitializeAwardEmojis()
    {
        runOnUiThread(() -> Executors.newSingleThreadExecutor().submit(issuesRunnable));
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }

}
