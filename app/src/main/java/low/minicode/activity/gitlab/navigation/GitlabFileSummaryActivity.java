/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.navigation;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.TypedValue;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.CommitAction;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.RepositoryFile;
import org.gitlab4j.api.models.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.gitlab.commits.GitlabCommitsAcitivity;
import low.minicode.activity.project.navigator.dialog.LocalSaveFileActivity;
import low.minicode.adapter.gitlab.commits.GitlabCommitsListAdapter;
import low.minicode.application.Minicode;

public class GitlabFileSummaryActivity extends ThemedAppCompatActivity
{
    private static final int REQUEST_CODE_GET_LOCATION = 493;
    private TextView mime, name, size, commitMessage, path;
    private EditText text;
    private ImageButton download, save;
    private Project project;
    private RepositoryFile file;
    private GitLabApi api;
    private String downloadProject;
    private String ref;
    private GitlabCommitsListAdapter.ViewHolder latestCommit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int projectId;
        String loc;
        String token;
        projectId = getIntent().getIntExtra("project_id", -1);
        ref = getIntent().getStringExtra("ref");
        loc = getIntent().getStringExtra("loc");
        token = getIntent().getStringExtra("token");
        if (projectId < 0 || ref == null || loc == null || token == null)
        {
            finish();
            return;
        }
        Executors.newSingleThreadExecutor().submit(() ->
        {
            api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token);
            try
            {
                project = api.getProjectApi().getProject(projectId);
                file = api.getRepositoryFileApi().getFile(projectId, loc, ref);
                runOnUiThread(() -> this.initialize(savedInstanceState));
            }
            catch (GitLabApiException e)
            {
                Library.vibrateError();
                finish();
            }
        });
    }

    protected void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_gitlab_file_summary);
        setTitle(file.getFileName());
        mime = findViewById(R.id.file_mime);
        name = findViewById(R.id.file_name);
        size = findViewById(R.id.file_size);
        commitMessage = findViewById(R.id.commit_message);
        text = findViewById(R.id.modify_file);
        download = findViewById(R.id.download_file);
        save = findViewById(R.id.upload_file);
        path = findViewById(R.id.file_path);
        name.setText(file.getFileName());
        mime.setText(MimeTypeMap.getSingleton().getMimeTypeFromExtension(FilenameUtils.getExtension(file.getFileName())) == null ? "application/octet-stream" : MimeTypeMap.getSingleton().getMimeTypeFromExtension(FilenameUtils.getExtension(file.getFileName())));
        size.setText(Library.getBetterFilesize(file.getSize()));
        path.setText(getIntent().getStringExtra("loc").length() > 35 ? "..." + getIntent().getStringExtra("loc").substring(getIntent().getStringExtra("loc").length() - 35) : getIntent().getStringExtra("loc"));
        text.setTypeface(Minicode.theMinicode.getEditorTypeface());
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt("font_size", 15));
        latestCommit = new GitlabCommitsListAdapter.ViewHolder(getContent());
        download.setOnClickListener(v ->
        {
            Intent i = new Intent(this, LocalSaveFileActivity.class);
            i.putExtra("loc", "/");
            i.putExtra("name", file.getFileName());
            MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(this);
            dialogBuilder.setTitle(R.string.select_project);
            File[] f = Library.filterOut(File.class, getFilesDir().listFiles(), object -> object.getName().equals(".res") || object.getName().equals("man_pages"));
            ArrayList<String> strings = new ArrayList<>();
            for (File iter : f)
            {
                strings.add(iter.getName());
            }
            dialogBuilder.setItems(Arrays.copyOf(strings.toArray(), strings.size(), String[].class), (dialog, which) ->
            {
                i.putExtra("project_name", strings.get(which));
                downloadProject = strings.get(which);
                startActivityForResult(i, REQUEST_CODE_GET_LOCATION);
            });
            dialogBuilder.show();
        });
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                InputStream is = api.getRepositoryFileApi().getRawFile(project, getIntent().getStringExtra("ref"), file.getFilePath());
                String f = IOUtils.toString(is);
                text.post(() -> text.setText(f));
            }
            catch (GitLabApiException | IOException e)
            {
                e.printStackTrace();
            }

        });
        save.setOnClickListener(v ->
        {
            if (commitMessage.getText().length() > 0)
            {
                MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this);
                materialAlertDialogBuilder.setMessage(R.string.committing);
                materialAlertDialogBuilder.setCancelable(false);
                Dialog d = materialAlertDialogBuilder.show();
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    List<CommitAction> actions = new ArrayList<>();
                    CommitAction action = new CommitAction();
                    action.setAction(CommitAction.Action.UPDATE);
                    action.setContent(text.getText().toString());
                    action.setFilePath(file.getFilePath());
                    actions.add(action);
                    try
                    {
                        User user = api.getUserApi().getCurrentUser();
                        api.getCommitsApi().createCommit(project.getId(), ref, commitMessage.getText().toString(), ref, user.getPublicEmail(), user.getUsername(), actions);
                        Library.vibrateSuccess();
                        d.dismiss();
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                        Library.vibrateError();
                        MaterialAlertDialogBuilder errorBuilder = new MaterialAlertDialogBuilder(this);
                        errorBuilder.setTitle(R.string.error);
                        errorBuilder.setMessage(e.getMessage());
                        errorBuilder.setPositiveButton(R.string.ok, (dialog, which) ->
                        {
                        });
                        runOnUiThread(() ->
                        {
                            d.dismiss();
                            errorBuilder.show();
                        });
                    }
                });
            }
        });
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                Commit commit = api.getCommitsApi().getCommits(project.getId(), file.getRef(), null, null, file.getFilePath(), 1).page(1).get(0);
                latestCommit.itemView.post(() ->
                {
                    latestCommit.description.setText(commit.getTitle());
                    latestCommit.hash.setText(commit.getId());
                    latestCommit.user.setText(String.format("%s [%s]", commit.getAuthorName(), commit.getCommitterEmail()));
                    findViewById(R.id.basic_gitlab_commit).setOnClickListener(v ->
                    {
                        Intent i = new Intent(this, GitlabCommitsAcitivity.class);
                        i.putExtra("file_path", file.getFilePath());
                        i.putExtra("project_id", project.getId());
                        i.putExtra("token", api.getAuthToken());
                        i.putExtra("ref", file.getRef());
                        startActivity(i);
                    });
                });
                User user = api.getUserApi().getUser(commit.getAuthorName());
                if (user != null && user.getAvatarUrl() != null)
                {
                    Bitmap bitmap = BitmapFactory.decodeStream(new URL(user.getAvatarUrl()).openStream());
                    latestCommit.avatar.post(() -> latestCommit.avatar.setImageBitmap(bitmap));
                }
            }
            catch (GitLabApiException | IOException e)
            {
                e.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GET_LOCATION && resultCode == RESULT_OK && data != null)
        {
            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this);
            materialAlertDialogBuilder.setMessage(R.string.downloading);
            materialAlertDialogBuilder.setCancelable(false);
            Dialog d = materialAlertDialogBuilder.show();
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    File f = new File(new File(getFilesDir(), downloadProject), data.getStringExtra("result"));
                    if (f.exists())
                    {
                        throw new IOException(getString(R.string.file_already_exists));
                    }
                    InputStream is = api.getRepositoryFileApi().getRawFile(project, getIntent().getStringExtra("ref"), file.getFilePath());
                    IOUtils.copy(is, new FileOutputStream(f));
                    runOnUiThread(d::dismiss);
                }
                catch (IOException | GitLabApiException e)
                {
                    MaterialAlertDialogBuilder error = new MaterialAlertDialogBuilder(this);
                    error.setTitle(R.string.error);
                    error.setMessage(e.getLocalizedMessage());
                    error.setPositiveButton(R.string.ok, (dialog, which) ->
                    {
                    });
                    runOnUiThread(d::dismiss);
                    runOnUiThread(error::show);
                }
            });
        }
    }
}
