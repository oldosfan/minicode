/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.projects;

import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.gitlab.ci.GitlabContinualIntegrationActivity;
import low.minicode.activity.gitlab.commits.GitlabCommitsAcitivity;
import low.minicode.activity.gitlab.cycleanalytics.GitlabCycleAnalyticsActivity;
import low.minicode.activity.gitlab.issues.GitlabIssuesActivity;
import low.minicode.activity.gitlab.issues.milestones.GitlabMilestonesActivity;
import low.minicode.activity.gitlab.mergerequests.GitlabMergeRequestsActivity;
import low.minicode.activity.project.newproject.NewProjectActivity;
import low.minicode.adapter.gitlab.navigator.GitlabFilesAdapter;
import low.minicode.application.Minicode;
import low.minicode.wrappers.data.StrongReference;
import low.minicode.wrappers.gitlab.branch.BranchStringWrapper;
import ru.noties.markwon.Markwon;
import ru.noties.markwon.core.CorePlugin;
import ru.noties.markwon.image.ImagesPlugin;

@SuppressWarnings("unchecked")
public class GitlabProjectSummaryActivity extends ThemedAppCompatActivity
{
    private Toolbar toolbar;
    private Project project;
    private ProgressBar progressBar;
    private String apiToken;
    private GitLabApi api;
    private User creator;
    private Spinner spinner;
    private TextView author, description, lastModified, lastCommitName, lastCommitAuthor, lastCommitShasum;
    private ImageView lastCommitBitmap;
    private RecyclerView files;
    private Branch branch;
    private List<Branch> branchList;
    private List<Commit> commitList;
    private GitlabFilesAdapter filesAdapter;
    private MaterialCardView latestCommits;
    private TextView readme;

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_project_summary);
        whitify();
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        if (getIntent().getStringExtra("project") == null || (apiToken = getIntent().getStringExtra("apiKey")) == null)
        {
            return;
        }
        @Nullable String branchName = getIntent().getStringExtra("branch");
        String projectPath = getIntent().getStringExtra("project");
        api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, apiToken);
        readme = findViewById(R.id.markwon);
        progressBar = findViewById(R.id.loading_cycle);
        toolbar.setNavigationOnClickListener(v -> finish());
        lastCommitAuthor = findViewById(R.id.latest_commit_by);
        lastCommitName = findViewById(R.id.latest_commit_title);
        lastCommitShasum = findViewById(R.id.latest_commit_shasum);
        lastCommitBitmap = findViewById(R.id.latest_commit_person);
        latestCommits = findViewById(R.id.latest_commit);
        author = findViewById(R.id.author);
        description = findViewById(R.id.description);
        lastModified = findViewById(R.id.last_updated);
        spinner = findViewById(R.id.branches);
        files = findViewById(R.id.files);
        files.setLayoutManager(new LinearLayoutManager(this));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                Log.d(getClass().getName(), "Loading project");
                project = api.getProjectApi().getProject(projectPath);
                Log.d(getClass().getName(), "loading other things");
                AtomicInteger inc = new AtomicInteger(0);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Log.d(getClass().getName(), "loading creator");
                        creator = api.getUserApi().getUser(project.getCreatorId());
                        Log.d(getClass().getName(), "found creator");
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                        Library.vibrateError();
                        finish();
                    }
                    inc.set(inc.get() + 1);
                });
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Log.d(getClass().getName(), "loading branchList");
                        branchList = api.getRepositoryApi().getBranches(project.getId());
                        Log.d(getClass().getName(), "loaded branchList");
                        if (branch == null)
                        {
                            try
                            {
                                Log.d(getClass().getName(), "loading default/selected branch");
                                do
                                {
                                    if (branchName == null)
                                    {
                                        Log.d(getClass().getName(), "branchName is null; loading default branch");
                                        branch = api.getRepositoryApi().getBranch(project, project.getDefaultBranch());
                                    }
                                    else
                                    {
                                        Log.d(getClass().getName(), "branchName is explicitly defined! loading specified branch " + branchName);
                                        branch = api.getRepositoryApi().getBranch(project, branchName);
                                        if (branch == null)
                                        {
                                            Library.vibrateError();
                                            runOnUiThread(this::finish);
                                        }
                                    }
                                }
                                while (branch == null);
                                Log.d(getClass().getName(), "loaded branch");
                            }
                            catch (GitLabApiException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        if (branch != null)
                        {
                            Log.d(getClass().getName(), "loading commits");
                            do
                            {
                                Pager<Commit> pager = api.getCommitsApi().getCommits(project.getId(), branch.getName(), null, null, 5);
                                commitList = pager.hasNext() ? pager.next() : new ArrayList<>();
                                if (commitList == null)
                                {
                                    Log.w(getClass().getName(), "commits *null*");
                                }
                            }
                            while (commitList == null);
                            Log.d(getClass().getName(), "loaded commits");
                        }
                        inc.set(inc.get() + 1);
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                        Library.vibrateError();
                        finish();
                    }
                });
                while (inc.get() < 2)
                {
                }
                runOnUiThread(() ->
                {
                    progressBar.setVisibility(View.GONE);
                    initialize(savedInstanceState);
                });
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
                Library.vibrateError();
                finish();
            }
        });
    }

    protected void initialize(Bundle savedInstanceState)
    {
        if (branch == null)
        {
            Intent i = new Intent(this, GitlabEmptyProjectActivity.class);
            i.putExtra("clone_url", project.getSshUrlToRepo());
            i.putExtra("clone_url_http", project.getHttpUrlToRepo());
            startActivity(i);
            finish();
            return;
        }
        StrongReference<Boolean> valid = new StrongReference<>(true);
        getSupportActionBar().setTitle(project.getNameWithNamespace());
        toolbar.setTitle(project.getNameWithNamespace());
        setTitle(project.getNameWithNamespace());
        author.setText(creator.getName());
        description.setText(project.getDescription());
        lastModified.setText(getString(R.string.last_updated_s, project.getLastActivityAt().toString()));
        List<BranchStringWrapper> wrappers = new ArrayList<>();
        for (Branch branch : branchList)
        {
            wrappers.add(new BranchStringWrapper(branch));
        }
        ArrayAdapter<BranchStringWrapper> arrayAdapter = new ArrayAdapter<BranchStringWrapper>(this, android.R.layout.simple_spinner_dropdown_item, Arrays.copyOf(wrappers.toArray(), wrappers.size(), BranchStringWrapper[].class))
        {
            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                return tv;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
            {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                return tv;
            }
        };
        if (commitList != null && !commitList.isEmpty())
        {
            Commit latest = commitList.get(0);
            lastCommitName.setText(latest.getTitle());
            lastCommitAuthor.setText(latest.getAuthorName());
            lastCommitShasum.setText(latest.getShortId());
            Executors.newSingleThreadExecutor().submit(() ->
            {
                User author = null;
                try
                {
                    Log.d(getClass().getName(), "loading author");
                    author = api.getUserApi().getUser(latest.getAuthorName());
                    Log.d(getClass().getName(), "author found");
                }
                catch (GitLabApiException e)
                {
                    e.printStackTrace();
                }
                if (latest.getAuthor() != null)
                {
                    Log.d(getClass().getName(), latest.getAuthor().getAvatarUrl());
                }
                else
                {
                    Log.w(getClass().getName(), "commit " + latest.getShortId() + " author is null!");
                }
                try
                {
                    if (author != null)
                    {
                        Bitmap bitmap = BitmapFactory.decodeStream(new URL(author.getAvatarUrl()).openConnection().getInputStream());
                        if (bitmap != null && valid.get())
                        {
                            runOnUiThread(() -> lastCommitBitmap.setImageBitmap(bitmap));
                        }
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            });
        }
        spinner.setAdapter(arrayAdapter);
        final boolean[] flag = {true};
        spinner.setSelection(branchList.indexOf(branch), true);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (!flag[0] && branch != branchList.get(position))
                {
                    branch = branchList.get(position);
                    progressBar.setVisibility(View.VISIBLE);
                    valid.set(false);
                    Executors.newSingleThreadExecutor().submit(() ->
                    {
                        try
                        {
                            Pager<Commit> pager = api.getCommitsApi().getCommits(project.getId(), branch.getName(), null, null, 5);
                            commitList = pager.hasNext() ? pager.next() : new ArrayList<>();
                        }
                        catch (GitLabApiException e)
                        {
                            e.printStackTrace();
                            Library.vibrateError();
                            finish();
                        }
                        runOnUiThread(() ->
                        {
                            progressBar.setVisibility(View.GONE);
                            initialize(savedInstanceState);
                        });
                    });
                }
                else
                {
                    flag[0] = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            }
        });
        files.setAdapter(filesAdapter = new GitlabFilesAdapter("/", project, api, branch == null ? null : branch.getName()));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                filesAdapter.refresh();
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
            runOnUiThread(() -> filesAdapter.notifyDataSetChanged());
            try
            {
                InputStream file = api.getRepositoryFileApi().getRawFile(project.getId(), branch.getName(), "/README.md");
                if (file != null)
                {
                    String markdownText = IOUtils.toString(file);
                    Markwon markwonInstance;
                    Spanned markdownSpanned = (markwonInstance = Markwon.builder(this).usePlugin(CorePlugin.create()).usePlugin(ImagesPlugin.create(this)).build()).toMarkdown(markdownText);
                    runOnUiThread(() -> markwonInstance.setParsedMarkdown(readme, markdownSpanned));
                }
            }
            catch (GitLabApiException | IOException e)
            {
                e.printStackTrace();
            }
        });
        latestCommits.setOnClickListener(v ->
        {
            Intent i = new Intent(this, GitlabCommitsAcitivity.class);
            i.putExtra("project_id", project.getId());
            i.putExtra("token", api.getAuthToken());
            i.putExtra("ref", branch.getName());
            startActivity(i);
        });
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.project_summary, menu);
        menu.findItem(R.id.show_in_browser).setOnMenuItemClickListener(menuItem ->
        {
            if (project != null)
            {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse(project.getHttpUrlToRepo());
                i.setData(uri);
                startActivity(i);
                return true;
            }
            return false;
        });
        menu.findItem(R.id.clone).setOnMenuItemClickListener(menuItem ->
        {
            if (project != null)
            {
                Toast.makeText(this, R.string.copied_to_clipboard, Toast.LENGTH_LONG).show();
                ((ClipboardManager) getSystemService(CLIPBOARD_SERVICE)).setText(project.getSshUrlToRepo());
                startActivity(new Intent(this, NewProjectActivity.class));
                return true;
            }
            return false;
        });
        menu.findItem(R.id.issues).setOnMenuItemClickListener(menuItem ->
        {
            if (api != null && project != null)
            {
                Intent i = new Intent(this, GitlabIssuesActivity.class);
                i.putExtra("project_id", project.getId());
                i.putExtra("token", api.getAuthToken());
                startActivity(i);
            }
            return true;
        });
        Drawable lightBlueMRIcon = getDrawable(R.drawable.ic_call_merge_black_24dp).getConstantState().newDrawable().mutate();
        lightBlueMRIcon.setTint(getResources().getColor(R.color.accent_device_default_light));
        menu.findItem(R.id.merge_requests).setIcon(lightBlueMRIcon);
        menu.findItem(R.id.merge_requests).setOnMenuItemClickListener(menuItem ->
        {
            if (api != null && project != null)
            {
                Intent i = new Intent(this, GitlabMergeRequestsActivity.class);
                i.putExtra("project_id", project.getId());
                i.putExtra("token", api.getAuthToken());
                startActivity(i);
            }
            return true;
        });
        Drawable lightBlueCIIcon = getDrawable(R.drawable.ic_timelapse_black_24dp).getConstantState().newDrawable().mutate();
        lightBlueCIIcon.setTint(getResources().getColor(R.color.accent_device_default_light));
        menu.findItem(R.id.ci_cd).setIcon(lightBlueCIIcon);
        menu.findItem(R.id.ci_cd).setOnMenuItemClickListener(menuItem ->
        {
            if (api != null && project != null)
            {
                Intent i = new Intent(this, GitlabContinualIntegrationActivity.class);
                i.putExtra("project_serialized", new Gson().toJson(project));
                i.putExtra("token", api.getAuthToken());
                startActivity(i);
            }
            return true;
        });
        menu.findItem(R.id.milestones).setOnMenuItemClickListener(menuItem ->
        {
            if (api != null && project != null)
            {
                Intent i = new Intent(this, GitlabMilestonesActivity.class);
                i.putExtra("serialized_project", new Gson().toJson(project));
                i.putExtra("token", api.getAuthToken());
                startActivity(i);
            }
            return true;
        });
        menu.findItem(R.id.cycle_analytics).setOnMenuItemClickListener(menuItem ->
        {
            if (api != null && project != null)
            {
                Intent i = new Intent(this, GitlabCycleAnalyticsActivity.class);
                i.putExtra("serialized_project", new Gson().toJson(project));
                i.putExtra("token", api.getAuthToken());
                startActivity(i);
            }
            return true;
        });
        return true;
    }

}
