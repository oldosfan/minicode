/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.integration;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.gitlab.projects.GitlabProjectSummaryActivity;
import low.minicode.activity.gitlab.secrets.GitlabSSHKeyManagementActivity;
import low.minicode.adapter.gitlab.GitlabProjectsListAdapter;

public class GitlabIntegrationActivity extends ThemedAppCompatActivity
{
    private ImageButton directlyOpen;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private GitlabProjectsListAdapter adapter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_integration);
        whitify();
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.gitlab_colon_projects);
        recyclerView = findViewById(R.id.gitlab_projects);
        swipeRefreshLayout = findViewById(R.id.refresh);
        directlyOpen = findViewById(R.id.directly_open_project);
        setSupportActionBar(toolbar);
        if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("gitlab_token", "").isEmpty())
        {
            Library.vibrateError();
            Toast.makeText(this, R.string.please_login_to_gitlab, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter((adapter = new GitlabProjectsListAdapter(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("gitlab_token", ""))));
        swipeRefreshLayout.setRefreshing(true);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                Log.d(getClass().getName(), "Starting gitlab refresh");
                adapter.refresh();
                Log.d(getClass().getName(), "Gitlab refresh completed");
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
            runOnUiThread(() -> adapter.notifyDataSetChanged());
            runOnUiThread(() -> swipeRefreshLayout.setRefreshing(false));
        });
        swipeRefreshLayout.setOnRefreshListener(() ->
        {
            Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    Log.d(getClass().getName(), "Starting gitlab refresh");
                    adapter.refresh();
                    Log.d(getClass().getName(), "Gitlab refresh completed");
                }
                catch (GitLabApiException e)
                {
                    e.printStackTrace();
                }
                runOnUiThread(() -> adapter.notifyDataSetChanged());
                runOnUiThread(() -> swipeRefreshLayout.setRefreshing(false));
            });
        });
        directlyOpen.setOnClickListener(v ->
        {
            Library.askDialogString(true, getString(R.string.name), null, this, params ->
            {
                Log.d(getClass().getName(), "project manual resolve started");
                runOnUiThread(() -> Executors.newSingleThreadExecutor().submit(() -> resolveProject(params[0])));
                return null;
            });
        });
    }

    private void resolveProject(String name)
    {
        try
        {
            Log.d(getClass().getName(), "resolving project " + name);
            Project p;
            if ((p = adapter.getApi().getProjectApi().getProject(name)) != null)
            {
                Log.d(getClass().getName(), "resolved project " + name);
                Intent i = new Intent(this, GitlabProjectSummaryActivity.class);
                i.putExtra("project", p.getPathWithNamespace());
                i.putExtra("apiKey", adapter.getApi().getAuthToken());
                runOnUiThread(() -> startActivity(i));
            }
            else
            {
                Log.d(getClass().getName(), "there is no project named " + name);
            }
        }
        catch (GitLabApiException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.gitlab_integration, menu);
        menu.getItem(0).setOnMenuItemClickListener(menuItem ->
        {
            Library.askDialogString(true, getString(R.string.name), null, this, params ->
            {
                swipeRefreshLayout.setRefreshing(true);
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        adapter.getApi().getProjectApi().createProject(params[0]);
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                        Library.vibrateError();
                        runOnUiThread(() -> Library.displayErrorDialogFromThrowable(this, e, true));
                    }
                    try
                    {
                        Log.d(getClass().getName(), "Starting gitlab refresh");
                        adapter.refresh();
                        Log.d(getClass().getName(), "Gitlab refresh completed");
                    }
                    catch (GitLabApiException e)
                    {
                        e.printStackTrace();
                    }
                    runOnUiThread(() -> adapter.notifyDataSetChanged());
                    runOnUiThread(() -> swipeRefreshLayout.setRefreshing(false));
                });
                return null;
            });
            return true;
        });
        menu.getItem(1).setOnMenuItemClickListener(menuItem ->
        {
            Intent i = new Intent(this, GitlabSSHKeyManagementActivity.class);
            i.putExtra("token", adapter.getApi().getAuthToken());
            startActivity(i);
            return true;
        });
        return true;
    }

    @Override
    public boolean shouldNotInterfereWithActionBar()
    {
        return true;
    }
}
