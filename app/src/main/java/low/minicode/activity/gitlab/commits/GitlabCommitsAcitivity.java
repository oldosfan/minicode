/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.gitlab.commits;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;

import java.util.concurrent.Executors;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.adapter.gitlab.commits.GitlabCommitsListAdapter;

public class GitlabCommitsAcitivity extends ThemedAppCompatActivity
{
    private RecyclerView recyclerView;
    private Project project;
    private String ref;
    private GitLabApi api;
    private GitlabCommitsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTitle(R.string.commits);
        setContentView(R.layout.layout_loading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int projectId = getIntent().getIntExtra("project_id", -1);
        String token = getIntent().getStringExtra("token");
        ref = getIntent().getStringExtra("ref");
        if (projectId == -1 || token == null || token.isEmpty() || ref == null || ref.isEmpty())
        {
            finish();
        }
        api = new GitLabApi(GitLabApi.ApiVersion.V4, "https://gitlab.com", Constants.TokenType.OAUTH2_ACCESS, token);
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                project = api.getProjectApi().getProject(projectId);
            }
            catch (GitLabApiException e)
            {
                Library.vibrateError();
                finish();
            }
            runOnUiThread(() -> initialize(savedInstanceState));
        });
    }

    protected void initialize(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_gitlab_commits_activity);
        recyclerView = findViewById(R.id.commits);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter = new GitlabCommitsListAdapter(api, project, ref, getIntent().getStringExtra("file_path")));
        Executors.newSingleThreadExecutor().submit(() ->
        {
            try
            {
                adapter.refresh();
            }
            catch (GitLabApiException e)
            {
                e.printStackTrace();
            }
            runOnUiThread(() -> adapter.notifyDataSetChanged());
        });
    }
}
