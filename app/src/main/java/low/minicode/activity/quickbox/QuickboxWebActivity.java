/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.quickbox;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toolbar;

import androidx.core.content.FileProvider;

import org.apache.commons.io.input.CharSequenceReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import jsyntaxpane.Token;
import jsyntaxpane.TokenType;
import jsyntaxpane.lexers.DefaultJFlexLexer;
import jsyntaxpane.lexers.JavaScriptLexer;
import jsyntaxpane.lexers.XHTMLLexer;
import low.minicode.wrappers.data.DualDataStructure;
import low.minicode.wrappers.data.DualDataStructure2;
import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.web.FloatingWebActivity;

public class QuickboxWebActivity extends ThemedAppCompatActivity
{
    private Toolbar toolbar;
    private MultiAutoCompleteTextView multiAutoCompleteTextView, js;
    private XHTMLIndexer indexer;
    private JavascriptIndexer indexer2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quickbox_web);
        multiAutoCompleteTextView = findViewById(R.id.edit);
        js = findViewById(R.id.javascript_edit);
        toolbar = findViewById(R.id.toolbar_quickbox);
        multiAutoCompleteTextView.setTypeface(Typeface.MONOSPACE);
        multiAutoCompleteTextView.requestFocus();
        js.setTypeface(Typeface.MONOSPACE);
        js.requestFocus();
        toolbar.setNavigationOnClickListener(v ->
        {
            Executors.newSingleThreadExecutor().submit(() ->
            {
                File quack = new File(getCacheDir(), "quickbox.html");
                try
                {
                    Library.writeFileAsBytes(quack.getAbsolutePath(), multiAutoCompleteTextView.getText().toString().getBytes());
                    Intent i = new Intent(this, FloatingWebActivity.class);
                    i.setData(FileProvider.getUriForFile(this, "low.minicode.authority", quack));
                    i.putExtra("javascript_extra", js.getText().toString());
                    runOnUiThread(() -> startActivity(i));
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            });
        });
        multiAutoCompleteTextView.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (indexer != null)
                {
                    indexer.interrupt();
                }
                indexer = new XHTMLIndexer(multiAutoCompleteTextView, s.toString());
                indexer.start();
                getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).edit().putString("quickbox_htmltext", s.toString()).apply();
            }
        });
        js.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (indexer2 != null)
                {
                    indexer2.interrupt();
                }
                indexer2 = new JavascriptIndexer(js, s.toString());
                indexer2.start();
                getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).edit().putString("quickbox_jstext", s.toString()).apply();
            }
        });
        multiAutoCompleteTextView.setText(getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).getString("quickbox_htmltext", ""));
        js.setText(getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).getString("quickbox_jstext", ""));
    }

    private static class XHTMLIndexer extends Thread
    {
        private final EditText editText;
        private String text;
        private List<DualDataStructure2<DualDataStructure<Integer>, Integer>> stash = new ArrayList<>();

        public XHTMLIndexer(EditText editText, String text)
        {
            super("XHTMLIndexer");
            this.editText = editText;
            this.text = text;
        }

        @Override
        public void run()
        {
            super.run();
            DefaultJFlexLexer lexer = new XHTMLLexer(new CharSequenceReader(text));
            Token token;
            try
            {
                while ((token = lexer.yylex()) != null)
                {
                    String sub = text.substring(token.start, token.end());
                    if (token.type == TokenType.COMMENT || token.type == TokenType.COMMENT2)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.darker_gray)));
                    }
                    else if (token.type == TokenType.STRING)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.tab_indicator_text)));
                    }
                    else if (token.type == TokenType.STRING2)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    }
                    else if (token.type == TokenType.NUMBER)
                    {
                        if (sub.matches("0(x[\\dabcdefABCDEF]+)|(b[01]+)"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_dark)));
                        }
                        else if (sub.equals("0"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.false_color)));
                        }
                        else if (sub.equals("1"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.true_color)));
                        }
                        else
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_700)));
                        }
                    }
                    else if (sub.equals("return") && token.type == TokenType.KEYWORD)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    }
                    else
                    {
                        if (token.type == TokenType.TYPE || token.type == TokenType.TYPE2 || token.type == TokenType.TYPE3)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_green_900)));
                        }
                        else if (token.type == TokenType.KEYWORD)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_orange_900)));
                        }
                        else if (token.type == TokenType.KEYWORD2)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.gplus_color_3)));
                        }
                        else if (token.type == TokenType.ERROR)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_red_600)));
                        }
                        else if (token.type != TokenType.OPERATOR)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_deep_purple_500)));
                        }
                    }
                }
                for (DualDataStructure2<DualDataStructure<Integer>, Integer> structure : stash)
                {
                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(structure.getB());
                    editText.post(() ->
                    {
                        try
                        {
                            editText.getEditableText().setSpan(foregroundColorSpan, structure.getA().getA(), structure.getA().getB(), 0);
                        }
                        catch (IndexOutOfBoundsException e)
                        {
                        }
                    });
                }
            }
            catch (IOException e)
            {
            }
        }
    }

    private static class JavascriptIndexer extends Thread
    {
        private final EditText editText;
        private String text;
        private List<DualDataStructure2<DualDataStructure<Integer>, Integer>> stash = new ArrayList<>();

        public JavascriptIndexer(EditText editText, String text)
        {
            super("JavascriptIndexer");
            this.editText = editText;
            this.text = text;
        }

        @Override
        public void run()
        {
            super.run();
            DefaultJFlexLexer lexer = new JavaScriptLexer(new CharSequenceReader(text));
            Token token;
            try
            {
                while ((token = lexer.yylex()) != null)
                {
                    String sub = text.substring(token.start, token.end());
                    if (token.type == TokenType.COMMENT || token.type == TokenType.COMMENT2)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.darker_gray)));
                    }
                    else if (token.type == TokenType.STRING)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.tab_indicator_text)));
                    }
                    else if (token.type == TokenType.STRING2)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    }
                    else if (token.type == TokenType.NUMBER)
                    {
                        if (sub.matches("0(x[\\dabcdefABCDEF]+)|(b[01]+)"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_dark)));
                        }
                        else if (sub.equals("0"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.false_color)));
                        }
                        else if (sub.equals("1"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.true_color)));
                        }
                        else
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_700)));
                        }
                    }
                    else if (sub.equals("return") && token.type == TokenType.KEYWORD)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    }
                    else
                    {
                        if (token.type == TokenType.TYPE || token.type == TokenType.TYPE2 || token.type == TokenType.TYPE3)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_green_900)));
                        }
                        else if (token.type == TokenType.KEYWORD)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_orange_900)));
                        }
                        else if (token.type == TokenType.KEYWORD2)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.gplus_color_3)));
                        }
                        else if (token.type == TokenType.ERROR)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_red_600)));
                        }
                        else if (token.type != TokenType.OPERATOR)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_deep_purple_500)));
                        }
                    }
                }
                for (DualDataStructure2<DualDataStructure<Integer>, Integer> structure : stash)
                {
                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(structure.getB());
                    editText.post(() ->
                    {
                        try
                        {
                            editText.getEditableText().setSpan(foregroundColorSpan, structure.getA().getA(), structure.getA().getB(), 0);
                        }
                        catch (IndexOutOfBoundsException e)
                        {
                        }
                    });
                }
            }
            catch (IOException e)
            {
            }
        }
    }
}
