/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.quickbox;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Switch;
import android.widget.Toolbar;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.io.input.CharSequenceReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;

import jsyntaxpane.Token;
import jsyntaxpane.TokenType;
import jsyntaxpane.lexers.CLexer;
import jsyntaxpane.lexers.CppLexer;
import jsyntaxpane.lexers.DefaultJFlexLexer;
import low.minicode.wrappers.data.DualDataStructure;
import low.minicode.wrappers.data.DualDataStructure2;
import low.minicode.Library;
import low.minicode.application.Minicode;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;

public class QuickboxActivity extends ThemedAppCompatActivity
{
    private Toolbar toolbar;
    private MultiAutoCompleteTextView multiAutoCompleteTextView;
    private Switch cPlusPlus;
    private boolean cpp;
    private Indexer indexer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quickbox);
        cPlusPlus = findViewById(R.id.cpp);
        multiAutoCompleteTextView = findViewById(R.id.edit);
        toolbar = findViewById(R.id.toolbar_quickbox);
        multiAutoCompleteTextView.setTypeface(Typeface.MONOSPACE);
        multiAutoCompleteTextView.requestFocus();
        cpp = getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).getBoolean("quickbox_cpp", false);
        if (getIntent().getData() != null)
        {
            Uri contentUri = getIntent().getData();
            String mimeType = getIntent().getType();
            if (mimeType != null && contentUri != null)
            {
                String name = Library.getFileName(contentUri, this);
                toolbar.setTitle(name);
                if (mimeType.equals("text/x-c++src"))
                {
                    cpp = true;
                }
                Executors.newSingleThreadExecutor().submit(() ->
                {
                    try
                    {
                        Scanner scanner = new Scanner(getContentResolver().openInputStream(contentUri));
                        String s = scanner.useDelimiter("\\A").next();
                        multiAutoCompleteTextView.post(() -> multiAutoCompleteTextView.setText(s));
                    }
                    catch (FileNotFoundException e)
                    {
                        e.printStackTrace();
                    }
                });
            }
        }
        cPlusPlus.setChecked(cpp);
        toolbar.setNavigationOnClickListener(v ->
        {
            MaterialAlertDialogBuilder compiling = new MaterialAlertDialogBuilder(this);
            compiling.setMessage(R.string.compiling);
            Dialog d = compiling.show();
            d.setCancelable(false);
            Executors.newSingleThreadExecutor().submit(() ->
            {
                File output = new File(getCacheDir(), "quickbox_executable");
                File quack = new File(getCacheDir(), "quickbox." + (cpp ? "cc" : "c"));
                try
                {
                    Log.d(getClass().getName(), Library.compiler(output, quack));
                    Library.writeFileAsBytes(quack.getAbsolutePath(), multiAutoCompleteTextView.getText().toString().getBytes());
                    Process p = Runtime.getRuntime().exec(new String[] {"/system/bin/sh", "-c", Library.compiler(output, quack)}, new String[] {String.format("LD_LIBRARY_PATH=%s:%s", Library.getPrefixDir() + "/lib", getApplicationInfo().nativeLibraryDir)}, getCacheDir());
                    Library.logProcess(p, "clang");
                    p.waitFor();
                    if (p.exitValue() != 0)
                    {
                        throw new IOException("");
                    }
                    d.dismiss();
                    Intent i = new Intent(this, NDKTerminalActvitiy.class);
                    i.putExtra("binary", output.getAbsolutePath());
                    i.putExtra("pwd", getCacheDir().getAbsolutePath());
                    runOnUiThread(() -> startActivity(i));
                }
                catch (IOException | InterruptedException e)
                {
                    e.printStackTrace();
                    d.dismiss();
                    runOnUiThread(() -> Snackbar.make(multiAutoCompleteTextView, R.string.compile_failed, Snackbar.LENGTH_LONG).show());
                }
            });
        });
        cPlusPlus.setOnCheckedChangeListener((v, val) ->
        {
            cpp = val;
            if (indexer != null)
            {
                indexer.interrupt();
            }
            indexer = new Indexer(val, multiAutoCompleteTextView, multiAutoCompleteTextView.getText().toString());
            indexer.start();
            getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).edit().putBoolean("quickbox_cpp", cpp).apply();
        });
        multiAutoCompleteTextView.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (indexer != null)
                {
                    indexer.interrupt();
                }
                indexer = new Indexer(cpp, multiAutoCompleteTextView, s.toString());
                indexer.start();
                getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).edit().putString("quickbox_text", s.toString()).putBoolean("quickbox_cpp", cpp).apply();
            }
        });
        multiAutoCompleteTextView.setText(getSharedPreferences("low.minicode.quickbox", MODE_PRIVATE).getString("quickbox_text", ""));
    }

    private static class Indexer extends Thread
    {
        private final boolean cpp;
        private final EditText editText;
        private String text;
        private List<DualDataStructure2<DualDataStructure<Integer>, Integer>> stash = new ArrayList<>();

        public Indexer(boolean cpp, EditText editText, String text)
        {
            super("Indexer");
            this.cpp = cpp;
            this.editText = editText;
            this.text = text;
        }

        @Override
        public void run()
        {
            super.run();
            DefaultJFlexLexer lexer = (cpp ? new CppLexer(new CharSequenceReader(text)) : new CLexer(new CharSequenceReader(text)));
            Token token;
            try
            {
                while ((token = lexer.yylex()) != null)
                {
                    String sub = text.substring(token.start, token.end());
                    if (token.type == TokenType.COMMENT || token.type == TokenType.COMMENT2)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.darker_gray)));
                    }
                    else if (token.type == TokenType.STRING)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.tab_indicator_text)));
                    }
                    else if (token.type == TokenType.STRING2)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    }
                    else if (token.type == TokenType.NUMBER)
                    {
                        if (sub.matches("0(x[\\dabcdefABCDEF]+)|(b[01]+)"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_dark)));
                        }
                        else if (sub.equals("0"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.false_color)));
                        }
                        else if (sub.equals("1"))
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.true_color)));
                        }
                        else
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_700)));
                        }
                    }
                    else if (sub.equals("return") && token.type == TokenType.KEYWORD)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                    }
                    else
                    {
                        if (token.type == TokenType.TYPE || token.type == TokenType.TYPE2 || token.type == TokenType.TYPE3)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_green_900)));
                        }
                        else if (token.type == TokenType.KEYWORD)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_orange_900)));
                        }
                        else if (token.type == TokenType.KEYWORD2)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.gplus_color_3)));
                        }
                        else if (token.type == TokenType.ERROR)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_red_600)));
                        }
                        else if (token.type != TokenType.OPERATOR)
                        {
                            stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_deep_purple_500)));
                        }
                    }
                }
                for (DualDataStructure2<DualDataStructure<Integer>, Integer> structure : stash)
                {
                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(structure.getB());
                    editText.post(() ->
                    {
                        try
                        {
                            editText.getEditableText().setSpan(foregroundColorSpan, structure.getA().getA(), structure.getA().getB(), 0);
                        }
                        catch (IndexOutOfBoundsException e)
                        {
                        }
                    });
                }
            }
            catch (IOException e)
            {
            }
        }
    }
}
