/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.activity.web;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;

public class FloatingWebActivity extends ThemedAppCompatActivity
{
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floating_web);
        setActionBar(findViewById(R.id.toolbar));
        ((WebView) findViewById(R.id.webview)).getSettings().setDomStorageEnabled(true);
        ((WebView) findViewById(R.id.webview)).getSettings().setJavaScriptEnabled(true);
        ((WebView) findViewById(R.id.webview)).loadUrl(getIntent().getDataString());
        ((WebView) findViewById(R.id.webview)).setWebChromeClient(new WebChromeClient());
        findViewById(R.id.navigate).setOnClickListener(v -> ((WebView) findViewById(R.id.webview)).loadUrl(Uri.parse(Uri.decode(((EditText) findViewById(R.id.uri)).getText().toString())).toString()));
        ((WebView) findViewById(R.id.webview)).setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                setTitle(view.getTitle());
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)
            {
                String temp = request.getUrl().toString();
                if (temp.isEmpty())
                {
                }
                else
                {
                    ((EditText) findViewById(R.id.uri)).setText(temp);
                }
                return false;
            }
        });
        if (getIntent().getStringExtra("javascript_extra") != null)
        {
            Library.injectScriptFile(findViewById(R.id.webview), getIntent().getStringExtra("javascript_extra"));
        }
    }
}
