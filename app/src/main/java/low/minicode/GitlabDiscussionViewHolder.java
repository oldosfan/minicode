/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GitlabDiscussionViewHolder extends RecyclerView.ViewHolder
{
    public static final int TYPE_DISCUSSION = 0;
    public static final int TYPE_ACTION = 1;
    public TextView content, author;
    public ImageButton reply, remove;
    public ImageView authorAvatar;

    public GitlabDiscussionViewHolder(@NonNull View itemView)
    {
        super(itemView);
        content = itemView.findViewById(R.id.discussion);
        reply = itemView.findViewById(R.id.reply_to_discussion);
        remove = itemView.findViewById(R.id.remove_discussion);
        author = itemView.findViewById(R.id.discussion_author);
        authorAvatar = itemView.findViewById(R.id.discussion_author_avatar);

    }

    public GitlabDiscussionViewHolder(@NonNull ViewGroup parentView, int type)
    {
        this(LayoutInflater.from(parentView.getContext()).inflate(type == TYPE_ACTION ? R.layout.basic_discussion_absolute : R.layout.basic_gitlab_discussion, parentView, false));
    }
}
