/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.util.Log;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TarExtractorTask
{
    private InputStream stream;
    private InputStream progressStream;
    private UnpredictableProgressMonitor monitor;
    private Runnable onCompleteListener;
    private File directory;

    public TarExtractorTask(InputStream stream, InputStream progressStream, UnpredictableProgressMonitor monitor, Runnable onCompleteListener, File directory)
    {
        this.stream = stream;
        this.progressStream = progressStream;
        this.monitor = monitor;
        this.onCompleteListener = onCompleteListener;
        this.directory = directory;
    }

    public TarExtractorTask(InputStream stream, Runnable onCompleteListener, File directory)
    {
        this.stream = stream;
        this.onCompleteListener = onCompleteListener;
        this.directory = directory;
    }

    public TarExtractorTask(InputStream stream, File directory)
    {
        this.stream = stream;
        this.directory = directory;
        this.onCompleteListener = () ->
        {
        };
    }

    private void _run() throws IOException
    {
        if (!directory.exists())
        {
            if (!directory.mkdirs())
            {
                throw new IOException("failed to create directory");
            }
        }
        else
        {
            if (!directory.isDirectory())
            {
                throw new IOException("directory must be directory");
            }
            Library.intactDeleteDirectoryContentsRecursively(directory);
        }
        TarArchiveInputStream archiveInputStream = new TarArchiveInputStream(stream);
        Integer total = null;
        if (progressStream != null)
        {
            total = progressStream.available();
            monitor.work2(total, 0);
        }
        TarArchiveEntry entry;
        while ((entry = archiveInputStream.getNextTarEntry()) != null)
        {
            if (monitor != null)
            {
                if (entry.isDirectory() || entry.isSymbolicLink())
                {
                    Log.d(getClass().getName(), String.format("_run: work (directory/symlink): " + entry.getName()));
                    monitor.work(entry.getName(), -1, 0);
                }
                else
                {
                    monitor.work(entry.getName(), archiveInputStream.getCurrentEntry().getRealSize(), 0);
                }
            }
            File f = new File(directory, entry.getName());
            if (entry.isDirectory())
            {
                f.mkdirs();
            }
            else if (entry.isSymbolicLink())
            {
                if (N.symlink(entry.getLinkName(), f.getAbsolutePath()) != 0)
                {
                    throw new IOException("symbolic link failed");
                }
            }
            else
            {
                FileOutputStream outputStream = new FileOutputStream(f);
                byte[] buffer = new byte[512];
                int read;
                long total_read = 0;
                if (entry.getRealSize() < 1)
                {
                    continue;
                }
                while ((read = archiveInputStream.read(buffer, 0, buffer.length)) > 0)
                {
                    total_read += read;
                    outputStream.write(buffer, 0, read);
                    if (monitor != null)
                    {
                        monitor.work(entry.getName(), entry.getRealSize(), read);
                        if (progressStream != null)
                        {
                            monitor.work2(total, total - progressStream.available());
                        }
                    }
                }
                N.chmod(f.getAbsolutePath(), entry.getMode());
            }
        }
        onCompleteListener.run();
    }

    public void run() throws IOException
    {
        _run();
    }

    public interface UnpredictableProgressMonitor
    {
        void work(String title, long total, long completed);

        void work2(long total, long completed);
    }

}
