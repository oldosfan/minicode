/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.widget.files.factory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.widget.files.reciever.OpenFileReciever;

public class PinnedFilesRemoteViewFactory implements RemoteViewsService.RemoteViewsFactory
{
    private List<String> files = new ArrayList<>();
    private Context c;

    public PinnedFilesRemoteViewFactory(Context c)
    {
        this.c = c;
    }

    @Override
    public void onCreate()
    {
        files.clear();
        Set<String> tmp = c.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>());
        for (String iter : tmp)
        {
            if (new File(c.getFilesDir(), iter).exists())
            {
                files.add(iter);
            }
        }
        Log.d(getClass().getName() + "@" + hashCode(), "onCreate: factory start");
    }

    @Override
    public void onDataSetChanged()
    {
        files.clear();
        Set<String> tmp = c.getSharedPreferences("low.minicode.pinned_files", Context.MODE_PRIVATE).getStringSet("pinned_files", new ConcurrentSkipListSet<>());
        for (String iter : tmp)
        {
            if (new File(c.getFilesDir(), iter).exists())
            {
                files.add(iter);
            }
        }
        Log.d(getClass().getName() + "@" + hashCode(), "onCreate: factory changed");
    }

    @Override
    public void onDestroy()
    {
        Log.d(getClass().getName() + "@" + hashCode(), "onCreate: factory destroyed");
    }

    @Override
    public int getCount()
    {
        return files.size();
    }

    @Override
    public RemoteViews getViewAt(int position)
    {
        RemoteViews rv = new RemoteViews(c.getPackageName(), R.layout.basic_found_file);
        rv.setTextViewText(R.id.file_name, new File(files.get((int) getItemId(position))).getName());
        String projectName = files.get((int) getItemId(position)).split("/")[0];
        rv.setTextViewText(R.id.file_path, c.getString(R.string.s_s, projectName, Library.relative(new File(c.getFilesDir(), projectName), new File(c.getFilesDir(), files.get((int) getItemId(position))))));
        Intent i = new Intent();
        Bundle extras = new Bundle();
        extras.putString(OpenFileReciever.EXTRA_FILE, new File(c.getFilesDir(), files.get((int) getItemId(position))).getAbsolutePath());
        i.putExtras(extras);
        i.setData(Uri.parse(i.toUri(Intent.URI_INTENT_SCHEME)));
        rv.setOnClickFillInIntent(R.id.found_file_layout, i);
        return rv;
    }

    @Override
    public RemoteViews getLoadingView()
    {
        return null;
    }

    @Override
    public int getViewTypeCount()
    {
        return 1;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }
}
