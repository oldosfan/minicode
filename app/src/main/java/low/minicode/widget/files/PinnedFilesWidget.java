/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.widget.files;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import low.minicode.widget.files.service.PinnedFilesRemoteViewsService;
import low.minicode.R;
import low.minicode.widget.files.reciever.OpenFileReciever;

public class PinnedFilesWidget extends AppWidgetProvider
{
    public static final String UPDATE_ACTION = "android.appwidget.action.APPWIDGET_UPDATE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
    {
        for (int appWidgetId : appWidgetIds)
        {
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.pinned_files_widget);
            Intent remoteServiceIntent = new Intent(context, PinnedFilesRemoteViewsService.class);
            views.setRemoteAdapter(R.id.pinned_files, remoteServiceIntent);
            remoteServiceIntent.setData(Uri.parse(remoteServiceIntent.toUri(Intent.URI_INTENT_SCHEME)));
            Intent pendingIntentTemplate = new Intent(context, OpenFileReciever.class);
            pendingIntentTemplate.setAction(OpenFileReciever.OPEN_CONTENT);
            pendingIntentTemplate.setData(Uri.parse(pendingIntentTemplate.toUri(Intent.URI_INTENT_SCHEME)));
            views.setPendingIntentTemplate(R.id.pinned_files, PendingIntent.getBroadcast(context, 0, pendingIntentTemplate, PendingIntent.FLAG_UPDATE_CURRENT));
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds)
    {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onDisabled(Context context)
    {
        super.onDisabled(context);
    }

    @Override
    public void onEnabled(Context context)
    {
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction() != null && intent.getAction().equals(UPDATE_ACTION))
        {
            Log.d(getClass().getName() + "@" + hashCode(), "onReceive: got UPDATE_ACTION");
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int[] widgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, getClass()));
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetIds, R.id.pinned_files);
        }
        super.onReceive(context, intent);
    }
}

