/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.widget.files.reciever;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.File;

import low.minicode.Library;
import low.minicode.activity.editor.EditorActivity;
import low.minicode.activity.project.navigator.NavigatorActivity;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;

public class OpenFileReciever extends BroadcastReceiver
{
    public static final String OPEN_CONTENT = "low.minicode.OPEN_CONTENT";
    public static final String EXTRA_FILE = "low.minicode.file";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction() != null && intent.getAction().equals(OPEN_CONTENT))
        {
            Log.d(getClass().getName() + "@" + hashCode(), "onReceive: got OPEN_CONTENT");
            File f = new File(intent.getStringExtra(EXTRA_FILE));
            Intent i;
            if (f.isDirectory())
            {
                i = new Intent(context, NavigatorActivity.class);
                i.putExtra("projectName", Library.relative(context.getFilesDir(), f).split("/")[0]);
                i.putExtra("loc", Library.relative(context.getFilesDir(), f).substring(Library.relative(context.getFilesDir(), f).indexOf("/")));
            }
            else if (f.canExecute())
            {
                i = new Intent(context, NDKTerminalActvitiy.class);
                i.putExtra("binary", f.getAbsolutePath());
            }
            else
            {
                i = new Intent(context, EditorActivity.class);
                i.putExtra("project_name", Library.relative(context.getFilesDir(), f).split("/")[0]);
                i.putExtra("file", f);
                i.putExtra("line", 0);
            }
            try
            {
                PendingIntent.getActivity(context, 0, i, 0).send();
            }
            catch (PendingIntent.CanceledException e)
            {
                e.printStackTrace();
            }
        }
    }
}
