package low.minicode.global.broadcast;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import low.minicode.R;
import low.minicode.activity.bugreporting.postmortem.CrashPostMortemActivity;

public class CrashNotificationReciever extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d(getClass().getName(), "Broadcast recieved: " + intent.getAction());
        Intent mortemService = new Intent(context, CrashPostMortemActivity.class);
        mortemService.putExtras(intent.getExtras());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel("mortem", context.getString(R.string.crash_notifications), NotificationManager.IMPORTANCE_HIGH);
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
        }
        Notification compat = new NotificationCompat.Builder(context, "mortem").setContentTitle(context.getString(R.string.minicode_crashed)).setContentText(context.getString(R.string.tap_to_report)).setSmallIcon(R.drawable.ic_warning_black_24dp).setColorized(true).setColor(context.getResources().getColor(R.color.accent_device_default_light)).setContentIntent(PendingIntent.getActivity(context, 0, mortemService, PendingIntent.FLAG_ONE_SHOT)).build();
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify("crash_report".hashCode(), compat);
    }
}
