/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.global.server.registry;

import java.util.HashMap;
import java.util.Map;

import low.minicode.wrappers.data.DualDataStructure2;
import low.minicode.wrappers.server.FTPServer;
import low.minicode.wrappers.server.GitServer;

public final class ServerRegistry
{
    public static final Map<String, FTPServer> ftpRegistry = new HashMap<>();
    public static final Map<String, GitServer> gitRegistry = new HashMap<>();
    public static DualDataStructure2<Process, Integer> pydocServer;
}
