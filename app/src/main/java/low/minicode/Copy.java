/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.os.Build;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.NotDirectoryException;

public final class Copy
{
    public static Copy currentEntry;
    private String name, project;
    private File file;

    public Copy(File file)
    {
        this.name = name;
        this.project = project;
        this.file = file;
    }

    public void copyTo(File directory) throws IOException, NotDirectoryException2
    {
        if (!file.isDirectory())
        {
            if (!directory.exists())
            {
                throw new FileNotFoundException(String.format("directory %s must exist", directory.getName()));
            }
            if (!directory.isDirectory())
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    throw new NotDirectoryException(directory.getAbsolutePath());
                }
                else
                {
                    throw new NotDirectoryException2(directory);
                }
            }
            File out = new File(directory, file.getName());
            if (out.exists())
            {
                throw new FileExistsException(out.getAbsolutePath());
            }
            else
            {
                out.createNewFile();
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            FileOutputStream fileOutputStream = new FileOutputStream(out);
            try
            {
                IOUtils.copy(fileInputStream, fileOutputStream);
            }
            finally
            {
                fileInputStream.close();
                fileOutputStream.close();
            }
        }
        else
        {
            if (!directory.exists())
            {
                throw new FileNotFoundException(String.format("directory %s must exist", directory.getName()));
            }
            if (!directory.isDirectory())
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    throw new NotDirectoryException(directory.getAbsolutePath());
                }
                else
                {
                    throw new NotDirectoryException2(directory);
                }
            }
            File out = new File(directory, file.getName());
            if (out.exists())
            {
                throw new FileExistsException(out.getAbsolutePath());
            }
            else
            {
                out.mkdirs();
            }
            FileUtils.copyDirectory(file, out);
        }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public File getFile()
    {
        return file;
    }

    public void setFile(File file)
    {
        this.file = file;
    }

    public String getProject()
    {
        return project;
    }

    public void setProject(String project)
    {
        this.project = project;
    }

    public static class NotDirectoryException2 extends Exception
    {
        private File f;

        private NotDirectoryException2(File f)
        {
        }

        @Override
        public String getMessage()
        {
            return String.format("Not a directory: %s", f.getAbsolutePath());
        }
    }
}
