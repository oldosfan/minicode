/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.terminal.task;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import low.minicode.Library;
import low.minicode.R;
import low.minicode.activity.terminal.emulator.NDKTerminalActvitiy;
import low.minicode.application.Minicode;
import low.minicode.jna.JNAHelper;
import low.minicode.terminal.view.TerminalView;
import low.minicode.wrappers.data.DualDataStructure;
import low.minicode.wrappers.data.StrongReference;
import low.minicode.wrappers.terminal.NativePty;
import vt110.AnsiControlSequence;
import vt110.AnsiControlSequenceListener;
import vt110.AnsiControlSequenceParser;
import vt110.SgrColor;

public class AsyncTerminalTask extends AsyncTask<Void, Object, Void>
{
    private static final Object ESC_LOCK_1 = new Object();
    StrongReference<TerminalView> inventory = new StrongReference<>();
    NativePty tty;
    Integer pid;
    StrongReference<NDKTerminalActvitiy> act = new StrongReference<>();
    WaitHandlerTask subTask;
    AnsiControlSequence sequence;
    Integer foreground, background;
    DualDataStructure<Integer> savedCursor;
    boolean bold;

    public AsyncTerminalTask(TerminalView terminal, NativePty tty, Integer pid, NDKTerminalActvitiy act)
    {
        this.inventory.set(terminal);
        this.tty = tty;
        this.pid = pid;
        this.act.set(act);
        System.gc();
    }

    @Override
    protected void onPreExecute()
    {
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        char ch;
        System.gc();
        (subTask = new WaitHandlerTask(this, pid, act.get())).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        Log.d("CrashDbg", "here6");
        while (true)
        {
            long val;
            //noinspection StatementWithEmptyBody
            while ((val = JNAHelper.INSTANCE.pty_remaining(tty).longValue()) == 0)
            {
            }
            String res = JNAHelper.INSTANCE.pty_read(this.tty, new JNAHelper.SizeT(val));
            if (res == null)
            {
                break;
            }
            Log.d(getClass().getName(), "Bytes: " + Arrays.toString(res.getBytes()));
            AnsiControlSequenceParser parser = new AnsiControlSequenceParser(new AnsiControlSequenceListener()
            {
                @Override
                public synchronized void parsedControlSequence(AnsiControlSequence seq)
                {
                    sequence = seq;
                    Log.d(getClass().getName(), "Escape: [" + seq.getCommand() + "]: " + Arrays.toString(seq.getParameters()));
                    if (seq.getCommand() == 'm')
                    {
                        SgrColor.GatewayColor color = SgrColor.getColors(seq.getParameters());
                        synchronized (ESC_LOCK_1)
                        {
                            if (color.getBackground() == -1)
                            {
                                background = null;
                            }
                            else
                            {
                                background = color.getBackground();
                            }
                            if (color.getForeground() == -1)
                            {
                                foreground = null;
                            }
                            else
                            {
                                foreground = color.getForeground();
                            }
                            bold = color.isForegroundBold();
                        }
                    }
                    else if (seq.getCommand() == 'K')
                    {
                        if (seq.getParameters().length == 0)
                        {
                            AtomicBoolean flip = new AtomicBoolean(false);
                            inventory.get().post(() ->
                            {
                                inventory.get().eraseToEndOfLine(inventory.get().getCursorPosition());
                                flip.set(true);
                            });
                            while (!flip.get())
                            {
                            }
                        }
                        else
                        {
                            try
                            {
                                int i = Integer.valueOf(seq.getParameters()[0]);
                                switch (i)
                                {
                                    case 1:
                                    {
                                        AtomicBoolean flip = new AtomicBoolean(false);
                                        inventory.get().post(() ->
                                        {
                                            inventory.get().eraseToStartOfLine(inventory.get().getCursorPosition());
                                            flip.set(true);
                                        });
                                        while (!flip.get())
                                        {
                                        }
                                        break;
                                    }
                                    case 2:
                                    {
                                        AtomicBoolean flip = new AtomicBoolean(false);
                                        inventory.get().post(() ->
                                        {
                                            inventory.get().eraseLine(inventory.get().getCursorPosition());
                                            flip.set(true);
                                        });
                                        while (!flip.get())
                                        {
                                        }
                                        break;
                                    }
                                }
                            }
                            catch (NumberFormatException e)
                            {
                            }
                        }
                    }
                    else if (seq.getCommand() == 'J')
                    {
                        if (seq.getParameters().length == 1)
                        {
                            try
                            {
                                int select = Integer.valueOf(seq.getParameters()[0]);
                                switch (select)
                                {
                                    case 1:
                                    {
                                        /* inventory.get().post(() ->
                                        {
                                            int currentVisFirst = inventory.get().getOffsetForPosition(0, inventory.get().getHeight() - act.get().findViewById(R.id.term_scroll).getHeight());
                                            Log.d(getClass().getName(), String.valueOf(currentVisFirst));
                                            inventory.get().setText(inventory.get().getText().toString().substring(currentVisFirst));
                                        }); */
                                        break;
                                    }
                                    case 2:
                                    {
                                        AtomicBoolean flip = new AtomicBoolean(false);
                                        inventory.get().post(() ->
                                        {
                                            inventory.get().clear(background == null ? -1 : background, foreground == null ? -1 : foreground, false);
                                            flip.set(true);
                                        });
                                        while (!flip.get())
                                        {
                                        }
                                        break;
                                    }
                                }
                            }
                            catch (NumberFormatException e)
                            {
                                publishProgress("Warning: invalid ANSI escape code \'J\'.", null, null, false);
                            }
                        }
                    }
                    else if (seq.getCommand() == 'H' | seq.getCommand() == 'f')
                    {
                        if (seq.getParameters().length < 1)
                        {
                            AtomicBoolean flip = new AtomicBoolean(false);
                            inventory.get().post(() ->
                            {
                                inventory.get().setCursorPositionWithOffset(0, 0);
                                flip.set(true);
                            });
                            while (!flip.get())
                            {
                            }
                        }
                        else if (seq.getParameters().length == 2)
                        {
                            try
                            {
                                AtomicBoolean flip = new AtomicBoolean(false);
                                inventory.get().post(() ->
                                {
                                    inventory.get().setCursorPositionWithOffset(Integer.parseInt(seq.getParameters()[1]), Integer.parseInt(seq.getParameters()[0]));
                                    flip.set(true);
                                });
                                while (!flip.get())
                                {
                                }
                            }
                            catch (NumberFormatException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (seq.getCommand() == 'A' && !seq.isSingle())
                    {
                        try
                        {
                            if (seq.getParameters().length == 1)
                            {
                                AtomicBoolean flip = new AtomicBoolean(false);
                                inventory.get().post(() ->
                                {
                                    inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() - Integer.valueOf(seq.getParameters()[0])).getA(), inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() - Integer.valueOf(seq.getParameters()[0])).getB());
                                    flip.set(true);
                                });
                                while (!flip.get())
                                {
                                }
                            }
                            else
                            {
                                inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() - 1).getA(), inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() - 1).getB());
                            }
                        }
                        catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else if (seq.getCommand() == 'B' && !seq.isSingle())
                    {
                        try
                        {
                            if (seq.getParameters().length == 1)
                            {
                                AtomicBoolean flip = new AtomicBoolean(false);
                                inventory.get().post(() ->
                                {
                                    inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() + Integer.valueOf(seq.getParameters()[0])).getA(), inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() + Integer.valueOf(seq.getParameters()[0])).getB());
                                    flip.set(true);
                                });
                                while (!flip.get())
                                {
                                }
                            }
                            else
                            {
                                inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() + 1).getA(), inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() + 1).getB());
                            }
                        }
                        catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else if (seq.getCommand() == 'C' && !seq.isSingle())
                    {
                        try
                        {
                            if (seq.getParameters().length == 1)
                            {
                                AtomicBoolean flip = new AtomicBoolean(false);
                                inventory.get().post(() ->
                                {
                                    inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() + Integer.valueOf(seq.getParameters()[0])).getA(), inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() + Integer.valueOf(seq.getParameters()[0])).getB());
                                    flip.set(true);
                                });
                                while (!flip.get())
                                {
                                }
                            }
                            else
                            {
                                inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() + 1).getA(), inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() + 1).getB());
                            }
                        }
                        catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else if (seq.getCommand() == 'D' && !seq.isSingle())
                    {
                        try
                        {
                            if (seq.getParameters().length == 1)
                            {
                                AtomicBoolean flip = new AtomicBoolean(false);
                                inventory.get().post(() ->
                                {
                                    inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() - Integer.valueOf(seq.getParameters()[0])).getA(), inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() - Integer.valueOf(seq.getParameters()[0])).getB());
                                    flip.set(true);
                                });
                                while (!flip.get())
                                {
                                }
                            }
                            else
                            {
                                inventory.get().setCursorPositionWithOffset(inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() - 1).getA(), inventory.get().getCursorPosition().setA(inventory.get().getCursorPosition().getA() - 1).getB());
                            }
                        }
                        catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else if (seq.getCommand() == 'l')
                    {
                        inventory.get().setLineWrap(false);
                    }
                    else if (seq.getCommand() == 'h')
                    {
                        inventory.get().setLineWrap(true);
                    }
                    else if (seq.getCommand() == 's')
                    {
                        DualDataStructure<Integer> temp = inventory.get().getCursorPosition();
                        if (inventory.get().getFirstVisibleOffset() != -1)
                        {
                            temp.setB(temp.getB() - inventory.get().getFirstVisibleOffset());
                        }
                        savedCursor = temp;
                    }
                    else if (seq.getCommand() == 'u')
                    {
                        if (savedCursor != null)
                        {
                            inventory.get().setCursorPositionWithOffset(savedCursor.getA(), savedCursor.getB());
                        }
                    }
                    else if (seq.getCommand() == 'q')
                    {
                        if (seq.getParameters().length == 1)
                        {
                            switch (seq.getParameters()[0])
                            {
                                case "0":
                                {
                                    act.get().runOnUiThread(() ->
                                    {
                                        act.get().led1.setVisibility(View.INVISIBLE);
                                        act.get().led2.setVisibility(View.INVISIBLE);
                                        act.get().led3.setVisibility(View.INVISIBLE);
                                        act.get().led4.setVisibility(View.INVISIBLE);
                                    });
                                    break;
                                }
                                case "1":
                                {
                                    act.get().runOnUiThread(() -> act.get().led1.setVisibility(View.VISIBLE));
                                    break;
                                }
                                case "2":
                                {
                                    act.get().runOnUiThread(() -> act.get().led2.setVisibility(View.VISIBLE));
                                    break;
                                }
                                case "3":
                                {
                                    act.get().runOnUiThread(() -> act.get().led3.setVisibility(View.VISIBLE));
                                    break;
                                }
                                case "4":
                                {
                                    act.get().runOnUiThread(() -> act.get().led4.setVisibility(View.VISIBLE));
                                    break;
                                }
                            }
                        }
                    }
                    else if (seq.getCommand() == 'n' && !seq.isSingle())
                    {
                        if (seq.getParameters().length == 1 && seq.getParameters()[0].equals("7"))
                        {
                            JNAHelper.INSTANCE.pty_write(tty, new JNAHelper.SizeT(4), new byte[] {27, '[', '0', 'n'});
                        }
                    }
                    else if (seq.isSingle())
                    {
                        switch (seq.getCommand())
                        {
                            case 'D':
                            {
                                inventory.get().getCellGrid().removeRow(0);
                                inventory.get().getCursorPosition().setB(inventory.get().getCursorPosition().getB() - 1);
                                inventory.get().post(inventory.get()::requestLayout);
                                // ((ScrollView) inventory.get().getParent()).scrollTo(0, ((Math.round((float) ((ScrollView) inventory.get().getParent()).getScrollY() / inventory.get().getTextSize() + inventory.get().getTextPadding()) + 1) * (inventory.get().getTextSize() + inventory.get().getTextPadding())));
                                break;
                            }
                            case 'M':
                            {
                                inventory.get().getCellGrid().addRow();
                                inventory.get().post(inventory.get()::requestLayout);
                                // ((ScrollView) inventory.get().getParent()).scrollTo(0, ((Math.round((float) ((ScrollView) inventory.get().getParent()).getScrollY() / inventory.get().getTextSize() + inventory.get().getTextPadding()) - 1) * (inventory.get().getTextSize() + inventory.get().getTextPadding())));
                                break;
                            }
                        }
                    }
                }

                @Override
                public synchronized void parsedString(String str)
                {
                    if (str.charAt(0) == 7 && str.length() == 1)
                    {
                        return;
                    }
                    publishProgress(str, background, foreground, bold);
                }
            });
            parser.parse(res);
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected synchronized void onProgressUpdate(Object... values)
    {
        Integer background = (Integer) values[1];
        Integer foreground = (Integer) values[2];
        Library.appendColoredText(this.inventory.get(), (String) values[0], foreground == null ? this.inventory.get().getTextColor() : foreground, background == null ? Color.TRANSPARENT : background, (Boolean) values[3]);
    }

    static class WaitHandlerTask extends AsyncTask<Void, Object, Integer>
    {
        AsyncTerminalTask t;
        Integer pid;
        StrongReference<NDKTerminalActvitiy> act = new StrongReference<>();

        WaitHandlerTask(AsyncTerminalTask t, Integer pid, NDKTerminalActvitiy act)
        {
            this.t = t;
            this.pid = pid;
            this.act.set(act);
        }

        @Override
        protected Integer doInBackground(Void... voids)
        {
            while (true)
            {
                JNAHelper.ProcessResult result = JNAHelper.INSTANCE.wait_process_result();
                if (result.pid == this.pid)
                {
                    return result.exit_code;
                }
            }
        }

        @Override
        protected void onPostExecute(Integer integer)
        {
            new Handler().postDelayed(() -> act.get().runOnUiThread(() -> t.inventory.get().append(String.format(Minicode.context.getString(R.string.exit_code_d), integer), new TerminalView.CellState(-1, -1, false))), 500);
        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
        }
    }
}
