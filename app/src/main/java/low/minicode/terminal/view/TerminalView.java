/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.terminal.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import low.minicode.R;
import low.minicode.application.Minicode;
import low.minicode.wrappers.data.DualDataStructure;

import static low.minicode.Library.NEWLINE;

public class TerminalView extends View
{
    private static final int BACKGROUND = Color.TRANSPARENT;
    private int textSize;
    private int textPadding;
    private int availableWordsPerLine;
    private int visibleLines;
    private int firstVisibleOffset;
    private int textColor;
    private int layoutBackground;
    private int layoutForeground;
    private boolean layoutBold;
    private boolean lineWrap;
    private boolean layoutFlip;
    private DualDataStructure<Integer> scroll;
    private OnResizeHandler onResizeHandler;
    private Typeface typeface;
    private DualDataStructure<Integer> cursorPosition;
    private Grid<Cell> cellGrid;
    private DrawBuffer drawBuffer;
    private EchoQueryHandler echoQueryHandler;

    public TerminalView(Context context)
    {
        super(context);
        this.lineWrap = true;
        this.cursorPosition = new DualDataStructure<>(0, 0);
        this.visibleLines = 20;
        this.firstVisibleOffset = -1;
        this.textColor = Color.WHITE;
        this.textPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 2, getResources().getDisplayMetrics());
        this.textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt("font_size", 15), getResources().getDisplayMetrics());
        if (getParent() != null && getParent() instanceof ViewGroup)
        {
            this.visibleLines = (int) Math.floor(((ViewGroup) getParent()).getHeight() / ((double) (textSize + textPadding)));
        }
        else
        {
            this.visibleLines = 20;
        }
        this.drawBuffer = new DrawBuffer();
    }

    public TerminalView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        this.lineWrap = true;
        this.cursorPosition = new DualDataStructure<>(0, 0);
        this.visibleLines = 20;
        this.firstVisibleOffset = -1;
        this.textColor = Color.WHITE;
        this.textPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 0, getResources().getDisplayMetrics());
        this.textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt("font_size", 15), getResources().getDisplayMetrics());
        if (getParent() != null && getParent() instanceof ViewGroup)
        {
            this.visibleLines = (int) Math.floor(((ViewGroup) getParent()).getHeight() / ((double) (textSize + textPadding)));
        }
        this.drawBuffer = new DrawBuffer();
    }

    public TerminalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        this.lineWrap = true;
        this.cursorPosition = new DualDataStructure<>(0, 0);
        this.visibleLines = 20;
        this.firstVisibleOffset = -1;
        this.textColor = Color.WHITE;
        this.textPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 0, getResources().getDisplayMetrics());
        this.textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt("font_size", 15), getResources().getDisplayMetrics());
        if (getParent() != null && getParent() instanceof ViewGroup)
        {
            this.visibleLines = (int) Math.floor(((ViewGroup) getParent()).getHeight() / ((double) (textSize + textPadding)));
        }
        this.drawBuffer = new DrawBuffer();
    }

    public TerminalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.lineWrap = true;
        this.cursorPosition = new DualDataStructure<>(0, 0);
        this.visibleLines = 20;
        this.firstVisibleOffset = -1;
        this.textColor = Color.WHITE;
        this.textPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 0, getResources().getDisplayMetrics());
        this.textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, Minicode.context.getSharedPreferences("low.minicode.preferences", Context.MODE_PRIVATE).getInt("font_size", 15), getResources().getDisplayMetrics());
        if (getParent() != null && getParent() instanceof ViewGroup)
        {
            this.visibleLines = (int) Math.floor(((ViewGroup) getParent()).getHeight() / ((double) (textSize + textPadding)));
        }
        this.drawBuffer = new DrawBuffer();
    }

    public void setLineWrap(boolean lineWrap)
    {
        this.lineWrap = lineWrap;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);
        if (changed)
        {
            if (onResizeHandler != null)
            {
                onResizeHandler.onResize(resizeTerminalFromParentView((View) getParent()), availableWordsPerLine);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int targetWidth, targetHeight;
        boolean scroll = false;
        if (getParent() != null && getParent() instanceof ScrollView && ((ScrollView) getParent()).getScrollY() + ((ScrollView) getParent()).getHeight() >= getHeight() - 150)
        {
            scroll = true;
        }
        if (widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST)
        {
            targetWidth = widthSize;
            availableWordsPerLine = (int) Math.floor((double) widthSize / (double) (textSize + textPadding));
        }
        else
        {
            throw new NullPointerException("Width must be constrained");
        }
        if (cellGrid == null)
        {
            this.cellGrid = new Grid<>(Cell.class, availableWordsPerLine);
        }
        if (heightMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.AT_MOST)
        {
            throw new NullPointerException("Height cannot be constrained");
        }
        else
        {
            targetHeight = (cellGrid.getHeight() + 1) * (textSize + textPadding);
        }
        if (onResizeHandler != null)
        {
            onResizeHandler.onResize(resizeTerminalFromParentView((View) getParent()), availableWordsPerLine);
        }
        setMeasuredDimension(targetWidth, targetHeight);
        if (layoutFlip)
        {
            cellGrid.addRowUntil(visibleLines);
            for (int y = 0; y < visibleLines; ++y)
            {
                for (int x = 0; x < cellGrid.getWidth(); ++x)
                {
                    cellGrid.set(y, x, new Cell(layoutForeground, layoutBackground, layoutBold, ' '));
                }
            }
            layoutFlip = false;
        }
        if (scroll)
        {
            ((ScrollView) getParent()).post(() -> ((ScrollView) getParent()).fullScroll(ScrollView.FOCUS_DOWN));
        }
    }

    public int getTextColor()
    {
        return textColor;
    }

    public void setTextColor(int textColor)
    {
        this.textColor = textColor;
        this.drawBuffer.textPaint.setColor(textColor);
        invalidate();
    }

    public void append(String string, CellState cellState)
    {
        boolean layoutInvalid = false;
        if (cursorPosition.getB() < 0)
        {
            cursorPosition.setB(0);
        }
        if (cursorPosition.getA() < 0)
        {
            cursorPosition.setA(0);
        }
        for (char ch : string.toCharArray())
        {
            if (ch == NEWLINE)
            {
                cursorPosition.setA(0);
                cursorPosition.setB(cursorPosition.getB() + 1);
                if (cursorPosition.getB() >= cellGrid.getHeight())
                {
                    cellGrid.addRowUntil(cursorPosition.getB() + 1);
                    layoutInvalid = true;
                }
                if (visibleLines >= cellGrid.getHeight())
                {
                    firstVisibleOffset = -1;
                }
                else
                {
                    firstVisibleOffset = cellGrid.getHeight() - visibleLines;
                }
            }
            else if (ch == 13)
            {
                cursorPosition.setA(0);
                layoutInvalid = false;
            }
            else if (ch == 8)
            {
                pop(cursorPosition);
                layoutInvalid = true;
            }
            else
            {
                if (cursorPosition.getB() >= cellGrid.getHeight())
                {
                    cellGrid.addRowUntil(cursorPosition.getB() + 1);
                    layoutInvalid = true;
                }
                if (cursorPosition.getA() >= cellGrid.getWidth())
                {
                    if (!lineWrap)
                    {
                        layoutInvalid = false;
                        cursorPosition.setA(cursorPosition.getA() + 1);
                    }
                    else
                    {
                        layoutInvalid = true;
                        cursorPosition.setA(0);
                        cursorPosition.setB(cursorPosition.getB() + 1);
                        if (visibleLines >= cellGrid.getHeight())
                        {
                            firstVisibleOffset = -1;
                        }
                        else
                        {
                            firstVisibleOffset = cellGrid.getHeight() - visibleLines;
                        }
                        if (cursorPosition.getB() >= cellGrid.getHeight())
                        {
                            cellGrid.addRowUntil(cursorPosition.getB() + 1);
                        }
                        cellGrid.set(cursorPosition.getB(), cursorPosition.getA(), new Cell(cellState, ch));
                        cursorPosition.setA(cursorPosition.getA() + 1);
                    }
                }
                else
                {
                    cellGrid.set(cursorPosition.getB(), cursorPosition.getA(), new Cell(cellState, ch));
                    cursorPosition.setA(cursorPosition.getA() + 1);
                }
            }
        }
        if (cellGrid.getHeight() > 450)
        {
            cursorPosition.setB(cursorPosition.getB() - (cellGrid.getHeight() - 450));
            cellGrid = cellGrid.subGrid(cellGrid.getHeight() - 450, cellGrid.getHeight());
            layoutInvalid = true;
        }
        if (layoutInvalid)
        {
            requestLayout();
        }
        else
        {
            invalidate();
        }
    }

    public Grid<Cell> getCellGrid()
    {
        return cellGrid;
    }

    public int getFirstVisibleOffset()
    {
        return firstVisibleOffset;
    }

    public int getTextSize()
    {
        return textSize;
    }

    public void setTextSize(int textSize)
    {
        this.textSize = textSize;
        drawBuffer.getTextPaint().setTextSize(textSize);
        requestLayout();
    }

    public int getTextPadding()
    {
        return textPadding;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        boolean textColorChanged, transColorChanged, typefaceChanged, cursorFaceChanged;
        for (int y = 0; y <= cellGrid.getHeight() - 1; ++y)
        {
            int yOff = (y + 1) * (textSize + textPadding);
            for (int x = 0; x <= cellGrid.getWidth() - 1; ++x)
            {
                textColorChanged = false;
                transColorChanged = false;
                typefaceChanged = false;
                cursorFaceChanged = false;
                int xOff = x * (textSize + textPadding);
                Cell c = cellGrid.get(y, x);
                if (x == cursorPosition.getA() && y == cursorPosition.getB())
                {
                    if (echoQueryHandler != null && echoQueryHandler.isEcho())
                    {
                        drawBuffer.getCursorPaint().setStyle(Paint.Style.FILL);
                        cursorFaceChanged = true;
                    }
                    canvas.drawRect(xOff, yOff - (textSize + textPadding), xOff + (textSize + textPadding), yOff + 3, drawBuffer.getCursorPaint());
                    if (cursorFaceChanged)
                    {
                        drawBuffer.getCursorPaint().setStyle(Paint.Style.STROKE);
                    }
                }
                if (c == null)
                {
                    continue;
                }
                if (c.colorFg != -1)
                {
                    drawBuffer.getTextPaint().setColor(c.getColorFg());
                    textColorChanged = true;
                }
                if (c.fgBold)
                {
                    typefaceChanged = true;
                    drawBuffer.getTextPaint().setTypeface(drawBuffer.typefaceBold);
                }
                if (c.colorBg != -1)
                {
                    drawBuffer.getTransPaint().setColor(c.getColorBg());
                    transColorChanged = true;
                    canvas.drawRect(xOff, yOff - (textSize + textPadding), xOff + (textSize + textPadding), yOff + 3, drawBuffer.getTransPaint());
                }
                if (c.getCh() != ' ')
                {
                    canvas.drawText(String.valueOf(c.getCh()), xOff + textPadding, yOff + textPadding, drawBuffer.getTextPaint());
                }
                if (typefaceChanged)
                {
                    drawBuffer.getTextPaint().setTypeface(Minicode.theMinicode.getEditorTypeface());
                }
                if (transColorChanged)
                {
                    drawBuffer.getTransPaint().setColor(Color.TRANSPARENT);
                }
                if (textColorChanged)
                {
                    drawBuffer.getTextPaint().setColor(textColor);
                }
            }
        }
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, @Nullable Rect previouslyFocusedRect)
    {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        invalidate();
    }

    public void pop(DualDataStructure<Integer> cursorPosition)
    {
        if (cursorPosition.getA() == 0)
        {
            cellGrid.removeRow(cursorPosition.getB());
            cursorPosition.setB(cursorPosition.getB() - 1);
            cursorPosition.setA(cellGrid.getWidth() - 1);
        }
        else
        {
            try
            {
                cellGrid.set(cursorPosition.getB(), cursorPosition.getA(), null);
            }
            catch (IndexOutOfBoundsException e)
            {
            }
            cursorPosition.setA(cursorPosition.getA() - 1);
        }
        if (cellGrid.getHeight() > 450)
        {
            cellGrid = cellGrid.subGrid(4, cellGrid.getHeight());
            cursorPosition.setB(cursorPosition.getB() - 3);
        }
        invalidate();
    }

    public void eraseDown(DualDataStructure<Integer> cursorPosition)
    {
        for (int i = cursorPosition.getB(); i < cellGrid.getHeight(); ++i)
        {
            cellGrid.removeRow(i);
        }
    }

    public void eraseUp(DualDataStructure<Integer> cursorPosition)
    {
        for (int i = cursorPosition.getB(); i >= 0; --i)
        {
            cellGrid.removeRow(i);
            cursorPosition.setB(cursorPosition.getB() - 1);
        }
    }

    public void eraseToEndOfLine(DualDataStructure<Integer> cursorPosition)
    {
        for (int i = cursorPosition.getA(); i < cellGrid.getWidth(); ++i)
        {
            cellGrid.set(cursorPosition.getB(), i, null);
        }
        requestLayout();
    }

    public void eraseToStartOfLine(DualDataStructure<Integer> cursorPosition)
    {
        while (cursorPosition.getA() > 0)
        {
            pop(cursorPosition);
        }
        requestLayout();
    }

    public void eraseLine(DualDataStructure<Integer> cursorPosition)
    {
        for (int i = 0; i < cellGrid.getWidth(); ++i)
        {
            cellGrid.set(cursorPosition.getB(), cursorPosition.getA(), null);
        }
    }

    public void clear(int background, int foreground, boolean bold)
    {
        cellGrid = null;
        firstVisibleOffset = -1;
        setCursorPosition(0, 0);
        if (background != -1 || foreground != -1 || bold)
        {
            layoutFlip = true;
            layoutForeground = foreground;
            layoutBackground = background;
            layoutBold = bold;
        }
        requestLayout();
    }

    public void resizeY(int y)
    {
        visibleLines = y;
    }

    public int resizeTerminalFromParentView(View parent)
    {
        return (visibleLines = (int) Math.floor(parent.getHeight() / ((double) (textSize + textPadding))));
    }

    public void clampWidth()
    {
        requestLayout();
    }

    public DualDataStructure<Integer> getCursorPosition()
    {
        return cursorPosition;
    }

    public void setCursorPosition(DualDataStructure<Integer> cursorPosition)
    {
        this.cursorPosition = cursorPosition;
        post(this::requestLayout);
    }

    public void setCursorPositionWithOffset(int x, int y)
    {
        if (firstVisibleOffset == -1)
        {
            setCursorPosition(x, y);
        }
        else
        {
            setCursorPosition(x, firstVisibleOffset + y);
        }
    }

    public void setCursorPosition(int x, int y)
    {
        this.cursorPosition.setA(x).setB(y);
        post(this::requestLayout);
    }

    public void setOnResizeHandler(OnResizeHandler onResizeHandler)
    {
        this.onResizeHandler = onResizeHandler;
    }

    public void setEchoQueryHandler(EchoQueryHandler echoQueryHandler)
    {
        this.echoQueryHandler = echoQueryHandler;
    }

    public interface OnResizeHandler
    {
        void onResize(int visibleLines, int horizontal);
    }

    public interface EchoQueryHandler
    {
        boolean isEcho();
    }

    public static class CellState
    {
        int colorFg;
        int colorBg;
        boolean fgBold;

        public CellState(int colorFg, int colorBg, boolean fgBold)
        {
            this.colorFg = colorFg;
            this.colorBg = colorBg;
            this.fgBold = fgBold;
        }

        public CellState(CellState toCopy)
        {
            colorFg = toCopy.getColorFg();
            colorBg = toCopy.getColorBg();
            fgBold = toCopy.isFgBold();
        }

        public int getColorFg()
        {
            return colorFg;
        }

        public int getColorBg()
        {
            return colorBg;
        }

        public boolean isFgBold()
        {
            return fgBold;
        }

    }

    public static class Cell extends CellState
    {
        char ch;

        public Cell(int colorFg, int colorBg, boolean fgBold, char ch)
        {
            super(colorFg, colorBg, fgBold);
            this.ch = ch;
        }

        public Cell(CellState state, char ch)
        {
            super(state);
            this.ch = ch;
        }

        public char getCh()
        {
            return ch;
        }

        @NonNull
        @Override
        public String toString()
        {
            return String.valueOf(ch);
        }
    }

    public static class Grid<C>
    {
        private List<C[]> gridRepresentation;
        private Class<C> clazz;
        private int width;

        @SuppressWarnings("unchecked")
        public Grid(Class<C> clazz, int width)
        {
            this.clazz = clazz;
            this.width = width;
            this.gridRepresentation = new ArrayList<>();
            ((ArrayList<C[]>) this.gridRepresentation).ensureCapacity(1024 * 1024);
            this.gridRepresentation.add((C[]) Array.newInstance(clazz, width));
        }

        private Grid(Class<C> clazz, List<C[]> gridRepresentation, int width)
        {
            this.clazz = clazz;
            this.width = width;
            this.gridRepresentation = gridRepresentation;
        }

        public Grid<C> subGrid(int start, int end)
        {
            return new Grid<>(clazz, gridRepresentation.subList(start, end), width);
        }

        public void set(int row, int column, C item) throws IndexOutOfBoundsException
        {
            gridRepresentation.get(row)[column] = item;
        }

        public C get(int row, int column) throws IndexOutOfBoundsException
        {
            return gridRepresentation.get(row)[column];
        }

        public void eraseToEndOfRow(int start, int row)
        {
            for (int i = start; i <= getWidth(); ++i)
            {
                gridRepresentation.get(row)[i] = null;
            }
        }

        public void removeRow(int loc)
        {
            gridRepresentation.remove(loc);
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return gridRepresentation.size();
        }

        @SuppressWarnings("unchecked")
        public void addRow()
        {
            gridRepresentation.add((C[]) Array.newInstance(clazz, width));
        }

        public void addRowUntil(int amt)
        {
            while (getHeight() < amt)
            {
                addRow();
            }
        }

        @NonNull
        @Override
        public String toString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (C[] row : gridRepresentation)
            {
                for (C cell : row)
                {
                    stringBuilder.append(cell != null ? cell.toString() : " ");
                }
                stringBuilder.append(NEWLINE);
            }
            return stringBuilder.toString();
        }
    }

    private class DrawBuffer
    {
        private Canvas c;
        private Paint transPaint;
        private Paint textPaint;
        private Paint cursorPaint;
        private Typeface typefaceBold = Typeface.create(Minicode.theMinicode.getEditorTypeface(), Typeface.BOLD);

        private DrawBuffer()
        {
            c = new Canvas();
            transPaint = new Paint();
            textPaint = new Paint();
            cursorPaint = new Paint();
            textPaint.setColor(textColor);
            textPaint.setTypeface(Minicode.theMinicode.getEditorTypeface());
            transPaint.setColor(Color.TRANSPARENT);
            textPaint.setTextSize(textSize);
            cursorPaint.setColor(getResources().getColor(R.color.md_grey_700));
            cursorPaint.setStyle(Paint.Style.STROKE);
            textPaint.setAntiAlias(true);
            textPaint.setSubpixelText(true);
        }

        public Canvas getC()
        {
            return c;
        }

        public Paint getTransPaint()
        {
            return transPaint;
        }

        public Paint getTextPaint()
        {
            return textPaint;
        }

        public Paint getCursorPaint()
        {
            return cursorPaint;
        }
    }
}
