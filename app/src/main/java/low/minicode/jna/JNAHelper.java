/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.jna;

import com.sun.jna.IntegerType;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

import low.minicode.wrappers.terminal.NativePty;

public interface JNAHelper extends Library
{
    JNAHelper INSTANCE = Native.loadLibrary("N2", JNAHelper.class);

    NativePty open_pty(short cols, short lines);

    int resize_pty(WindowSize size, NativePty tty);

    SSizeT pty_remaining(NativePty tty);

    int exec_pty(NativePty tty, String exec, String[] argv, String[] envp, String cwd);

    String pty_read(NativePty tty, SizeT amount);

    int pty_setutf8(NativePty tty);

    SSizeT pty_write(NativePty tty, SizeT amount, byte[] data);

    ProcessResult wait_process_result();

    void set_tty_erase(byte value, NativePty tty);

    int pty_is_echo(NativePty tty);

    class SizeT extends IntegerType
    {
        public SizeT()
        {
            this(0);
        }

        public SizeT(long value)
        {
            super(Native.SIZE_T_SIZE, value);
        }
    }

    class SSizeT extends IntegerType
    {
        public SSizeT()
        {
            this(0);
        }

        public SSizeT(long value)
        {
            super(Native.LONG_SIZE, value);
        }
    }

    class WindowSize extends Structure
    {
        public short ws_row;
        public short ws_col;
        public short ws_xpixel;
        public short ws_ypixel;

        public WindowSize(short ws_row, short ws_col)
        {
            this.ws_row = ws_row;
            this.ws_col = ws_col;
            this.ws_xpixel = 0;
            this.ws_ypixel = 0;
        }

        public WindowSize(short ws_row, short ws_col, short ws_xpixel, short ws_ypixel)
        {
            this.ws_row = ws_row;
            this.ws_col = ws_col;
            this.ws_xpixel = ws_xpixel;
            this.ws_ypixel = ws_ypixel;
        }

        @Override
        protected List<String> getFieldOrder()
        {
            return Arrays.asList("ws_row", "ws_col", "ws_xpixel", "ws_ypixel");
        }
    }

    class ProcessResult extends Structure
    {
        public int pid;
        public int exit_code;

        @Override
        protected List<String> getFieldOrder()
        {
            return Arrays.asList("pid", "exit_code");
        }
    }
}
