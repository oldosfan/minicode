/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.jna;

import com.sun.jna.Library;
import com.sun.jna.Native;

import low.minicode.wrappers.terminal.NativePty;

public interface PtyLibrary extends Library
{
    int O_RDONLY = 00000000;
    int O_WRONLY = 00000001;
    int O_RDWR = 00000002;
    PtyLibrary INSTANCE = Native.load("P", PtyLibrary.class);

    static PtyLibrary makeInstance(String libname)
    {
        return Native.load(libname, PtyLibrary.class);
    }

    NativePty mptyncl(int cols, int lines, int _ptflags_, int _ptflags_slave_);

    byte rpty(NativePty _tty_);

    int wpty(NativePty _tty_, byte _c_);

    int wstrpty(NativePty _tty_, String _str_);

    int execpp(NativePty _tty_, String _command_, String[] _args_, String _ld_lib_path_);

    void closepty(NativePty _tty_);

    interface LibC extends Library
    {
        int WNOHANG = 0x00000001;

        static LibC makeInstance()
        {
            return Native.load("c", LibC.class);
        }

        int kill(int pid, int sig);

        int waitpid(int pid, int stat_loc, int options);

        int wait(int stat_loc);
    }
}
