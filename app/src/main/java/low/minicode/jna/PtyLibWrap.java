/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.jna;

import low.minicode.wrappers.terminal.NativePty;

public class PtyLibWrap
{
    static
    {
        System.loadLibrary("P");
    }

    public static native byte readDesPty(int des);

    public static native long writeDes(int des, byte dat);

    public static native int kill(int pid, int sig);

    public static native int waitp();

    public static native int waitNoHang();

    public static native NativePty makeNewPty(int cols, int lines, int flags_master, int flags_slave);
}
