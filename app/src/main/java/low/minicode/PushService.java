/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.UUID;

import low.minicode.activity.updates.UpdatesActivity;
import low.minicode.application.Minicode;

import static android.net.ConnectivityManager.TYPE_ETHERNET;
import static android.net.ConnectivityManager.TYPE_WIFI;

public class PushService extends IntentService
{
    public PushService(String name)
    {
        super(name);
    }

    public PushService()
    {
        super("PushService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent)
    {
        UpdateCheckThread updateCheckThread = new UpdateCheckThread();
        updateCheckThread.start();
        try
        {
            updateCheckThread.join();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    class UpdateCheckThread extends Thread
    {
        public ArrayList<UpdatesActivity.Update> getResults()
        {
            ArrayList<UpdatesActivity.Update> results = new ArrayList<>();
            try
            {
                String text;
                InputStream uriInputStream;
                URLConnection connection = new URL(Minicode.updateUrl).openConnection();
                connection.connect();
                uriInputStream = connection.getInputStream();
                text = IOUtils.toString(uriInputStream, Charset.forName("utf-8"));
                JSONObject j = new JSONObject(text);
                JSONArray a = j.getJSONArray("updates");
                ArrayList<JSONObject> arrays = new ArrayList<>();
                int size = a.length();
                for (int i = 0; i < size; ++i)
                {
                    arrays.add(a.getJSONObject(i));
                }
                for (JSONObject array : arrays)
                {
                    String desc = array.getString("updateDescription");
                    String versionFancy = array.getString("versionFancy");
                    Long versionCode = array.getLong("versionCode");
                    String versionURL = array.getString("versionURL");
                    if (versionCode > getPackageManager().getPackageInfo(getPackageName(), 0).versionCode)
                    {
                        results.add(new UpdatesActivity.Update(desc, versionFancy, versionCode, versionURL));
                    }
                }
                return results;
            }
            catch (IOException e)
            {
                return null;
            }
            catch (JSONException e)
            {
                return null;
            }
            catch (PackageManager.NameNotFoundException e)
            {
                return null;
            }
        }

        public String hasKnownBadThings()
        {
            try
            {
                String text;
                InputStream uriInputStream;
                URLConnection connection = new URL(Minicode.updateUrl).openConnection();
                connection.connect();
                uriInputStream = connection.getInputStream();
                text = IOUtils.toString(uriInputStream, Charset.forName("utf-8"));
                JSONObject j = new JSONObject(text);
                JSONArray a = j.getJSONArray("badThings");
                ArrayList<JSONObject> arrays = new ArrayList<>();
                int size = a.length();
                for (int i = 0; i < size; ++i)
                {
                    arrays.add(a.getJSONObject(i));
                }
                for (JSONObject array : arrays)
                {
                    if (array.getLong("versionCode") == BuildConfig.VERSION_CODE)
                    {
                        return array.getString("badThingDescription");
                    }
                }
                return null;
            }
            catch (IOException e)
            {
                return null;
            }
            catch (JSONException e)
            {
                return null;
            }
        }

        public boolean canCheckUpdates()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager) Minicode.context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            {
                Log.d("BackgroundAutoUpdater", String.format("canCheckUpdates: bucket is %d", ((UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE)).getAppStandbyBucket()));
                if (((UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE)).getAppStandbyBucket() > UsageStatsManager.STANDBY_BUCKET_WORKING_SET)
                {
                    Log.d("BackgroundAutoUpdater", "canCheckUpdates: no; respecting bucket less than working set");
                    return false;
                }
            }
            if (connectivityManager.getActiveNetworkInfo() == null || ((PowerManager) getSystemService(Context.POWER_SERVICE)).isPowerSaveMode())
            {
                Log.d("BackgroundAutoUpdater", "canCheckUpdates: no; power save mode or network info null");
                return false;
            }
            if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("update_network_override", 0) == 0)
            {
                return connectivityManager.getActiveNetworkInfo().getType() == TYPE_WIFI || connectivityManager.getActiveNetworkInfo().getType() == TYPE_ETHERNET;
            }
            else
            {
                return getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("update_network_override", 0) == 1;
            }
        }

        @SuppressLint({"WrongConstant", "ApplySharedPref"})
        @Override
        public void run()
        {
            super.run();
            while (true)
            {
                double time = Math.pow(2, getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("update_interval", 1)) * 60;
                Log.d("BackgroundAutoUpdater", "Checking for updates");
                if (canCheckUpdates() && getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getBoolean("auto_update", true))
                {
                    ArrayList<UpdatesActivity.Update> updates = getResults();
                    if (updates == null)
                    {
                        try
                        {
                            sleep((long) (time * 1000));
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                        continue;
                    }
                    if (updates.size() > 0)
                    {
                        Log.d("BackgroundAutoUpdater", "Updates found");
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        NotificationChannel notificationChannel;
                        NotificationCompat.Builder notificationBuilder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            notificationChannel = new NotificationChannel("update_check_mc_auto", "Auto-updates", NotificationManager.IMPORTANCE_DEFAULT);
                            notificationManager.createNotificationChannel(notificationChannel);
                            notificationBuilder = new NotificationCompat.Builder(Minicode.context, notificationChannel.getId());
                        }
                        else
                        {
                            notificationBuilder = new NotificationCompat.Builder(Minicode.context);
                        }
                        notificationBuilder.setContentTitle(getString(R.string.updates_available));
                        notificationBuilder.setContentText(String.format(getString(R.string.minicode_xxx_now_avaliable), updates.get(updates.size() - 1).fancyVersion));
                        notificationBuilder.setSmallIcon(R.drawable.ic_system_update_alt_black_24dp);
                        PendingIntent contentIntent = PendingIntent.getActivity(Minicode.context, 0, new Intent(Minicode.context, UpdatesActivity.class), 0);
                        notificationBuilder.setColor(Minicode.context.getResources().getColor(R.color.accent_device_default_700));
                        notificationBuilder.setColorized(true);
                        notificationBuilder.setContentIntent(contentIntent);
                        getSharedPreferences("low.minicode.updater", MODE_PRIVATE).edit().putString("available_version_fancy", updates.get(updates.size() - 1).fancyVersion).putLong("available_version", updates.get(updates.size() - 1).versionCode).putLong("stats_expire", System.currentTimeMillis() + (60 * 25) * 1000).commit();
                        notificationBuilder.setAutoCancel(true);
                        if (getSharedPreferences("low.minicode.updater", MODE_PRIVATE).getLong("last_check", 0) < updates.get(updates.size() - 1).versionCode)
                        {
                            Library.vibrateError();
                            notificationManager.notify(Minicode.uuid.hashCode(), notificationBuilder.build());
                            getSharedPreferences("low.minicode.updater", MODE_PRIVATE).edit().putLong("last_check", updates.get(updates.size() - 1).versionCode).commit();
                        }
                    }
                    String badThings = hasKnownBadThings();
                    if (badThings != null)
                    {
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        NotificationChannel notificationChannel;
                        NotificationCompat.Builder notificationBuilder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            notificationChannel = new NotificationChannel("bad_things", "Security", NotificationManager.IMPORTANCE_MAX);
                            notificationManager.createNotificationChannel(notificationChannel);
                            notificationBuilder = new NotificationCompat.Builder(Minicode.context, notificationChannel.getId());
                        }
                        else
                        {
                            notificationBuilder = new NotificationCompat.Builder(Minicode.context);
                        }
                        PendingIntent contentIntent = PendingIntent.getActivity(Minicode.context, 0, new Intent(Minicode.context, UpdatesActivity.class), 0);
                        notificationBuilder.setAutoCancel(true);
                        notificationBuilder.setOngoing(true);
                        notificationBuilder.setSmallIcon(R.drawable.ic_warning_black_24dp);
                        notificationBuilder.setColorized(true);
                        notificationBuilder.setContentTitle(getString(R.string.minicode_critical_bug));
                        notificationBuilder.setContentText(badThings);
                        notificationBuilder.setContentIntent(contentIntent);
                        notificationManager.notify(Minicode.uuid.hashCode() & UUID.randomUUID().hashCode(), notificationBuilder.build());
                        Library.vibrateError();
                    }
                }
                try
                {
                    sleep((long) (time * 1000));
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
