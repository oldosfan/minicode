/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.data;

public class DualDataStructure2<T, Tb>
{
    private T a;
    private Tb b;

    public DualDataStructure2(T a, Tb b)
    {
        this.a = a;
        this.b = b;
    }

    public T getA()
    {
        return a;
    }

    public DualDataStructure2<T, Tb> setA(T a)
    {
        this.a = a;
        return this;
    }

    public Tb getB()
    {
        return b;
    }

    public DualDataStructure2<T, Tb> setB(Tb b)
    {
        this.b = b;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (getClass().isInstance(o))
        {
            if (((DualDataStructure2<T, Tb>) o).a == a || ((DualDataStructure2<T, Tb>) o).b == b)
            {
                return true;
            }
            else if (((DualDataStructure2<T, Tb>) o).a == null | ((DualDataStructure2<T, Tb>) o).b == null)
            {
                return false;
            }
            else if (a.equals(((DualDataStructure2<T, Tb>) o).a) || b.equals(((DualDataStructure2<T, Tb>) o).a))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
