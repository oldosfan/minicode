/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.data.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Node<T>
{
    private ArrayList<Node<T>> children;
    private T t;

    public Node(T t)
    {
        this.children = new ArrayList<>();
        this.t = t;
    }

    public void addAll(Collection<Node<T>> collection)
    {
        children.addAll(collection);
    }

    public void addAllObjects(Collection<T> collection)
    {
        for (T iter : collection)
        {
            children.add(new Node<>(iter));
        }
    }

    public void pullFrom(Node<T> t)
    {
        children.addAll(t.children);
    }

    public List<Node<T>> getChildren()
    {
        return children;
    }

    public T getObject()
    {
        return t;
    }

    public void add(Node<T> tNode)
    {
        children.add(tNode);
    }

    public void add(T t)
    {
        children.add(new Node<>(t));
    }

    public Node<T> findNodeByObject(T object)
    {
        for (Node<T> tNode : children)
        {
            if (tNode.getObject().equals(object))
            {
                return tNode;
            }
        }
        return null;
    }
}
