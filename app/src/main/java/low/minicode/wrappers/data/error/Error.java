/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.data.error;

import android.annotation.SuppressLint;

import java.io.Serializable;

public class Error implements Serializable
{
    public String type, desc, file;
    public Integer line, word;

    @SuppressLint("DefaultLocale")
    @Override
    public String toString()
    {
        return String.format("%s: at line %d (word %d):\n> %s: %s", file, line, word, type, desc);
    }
}