/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.data;

public class DualDataStructure<T>
{
    private T a;
    private T b;

    public DualDataStructure(T a, T b)
    {
        this.a = a;
        this.b = b;
    }

    public T getA()
    {
        return a;
    }

    public DualDataStructure<T> setA(T a)
    {
        this.a = a;
        return this;
    }

    public T getB()
    {
        return b;
    }

    public DualDataStructure<T> setB(T b)
    {
        this.b = b;
        return this;
    }
}
