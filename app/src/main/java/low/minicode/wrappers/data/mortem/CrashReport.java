/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.data.mortem;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import low.minicode.BuildConfig;
import low.minicode.application.Minicode;

public class CrashReport
{
    private String stackTrace, logcat, info, step;

    public CrashReport(String stackTrace, String logcat, String info, String step)
    {
        this.stackTrace = stackTrace;
        this.logcat = logcat;
        this.info = info;
        this.step = step;
    }

    public String getStackTrace()
    {
        return stackTrace;
    }

    public String getLogcat()
    {
        return logcat;
    }

    public String getInfo()
    {
        return info;
    }

    public void sendBugReport() throws IOException
    {
        String postURL = Minicode.bugReportEndpoint;
        HttpPost post = new HttpPost(postURL);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("logcat", logcat));
        params.add(new BasicNameValuePair("clientVersion", String.valueOf(BuildConfig.VERSION_CODE)));
        params.add(new BasicNameValuePair("uuid", Minicode.uuid.toString()));
        params.add(new BasicNameValuePair("stack", stackTrace));
        params.add(new BasicNameValuePair("info", info));
        params.add(new BasicNameValuePair("step", step));
        UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, "UTF-8");
        post.setEntity(ent);
        HttpClient client = new DefaultHttpClient();
        client.execute(post);
    }

    public void sendBugReport(Runnable onDoneHandler)
    {
        try
        {
            sendBugReport();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        onDoneHandler.run();
    }

    public String getStep()
    {
        return step;
    }
}
