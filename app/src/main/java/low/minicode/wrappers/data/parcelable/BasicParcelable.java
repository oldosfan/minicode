/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.data.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class BasicParcelable<T> implements Parcelable
{
    public static final Creator<BasicParcelable> CREATOR = new Creator<BasicParcelable>()
    {
        @Override
        public BasicParcelable createFromParcel(Parcel in)
        {
            return new BasicParcelable(in);
        }

        @Override
        public BasicParcelable[] newArray(int size)
        {
            return new BasicParcelable[size];
        }
    };
    T object;

    public BasicParcelable(T in)
    {
        object = in;
    }

    protected BasicParcelable(Parcel in)
    {
        //noinspection unchecked
        object = (T) Objects.requireNonNull(in.readArray(Object.class.getClassLoader()))[0];
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeArray(new Object[] {object});
    }
}
