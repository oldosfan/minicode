/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.server;

import android.content.Context;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.Daemon;
import org.eclipse.jgit.transport.DaemonClient;
import org.eclipse.jgit.transport.resolver.RepositoryResolver;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;

public class GitServer
{
    private final String projectName;
    private final Integer port;
    private final Context context;
    private Daemon d;

    public GitServer(String projectName, Integer port, Context context)
    {
        this.projectName = projectName;
        this.port = port;
        this.context = context;
        this.run();
    }

    public void run()
    {
        d = new Daemon(new InetSocketAddress(port));
        d.setRepositoryResolver(new RepositoryResolverImplementation(projectName, context));
        d.getService("git-receive-pack").setEnabled(true);
        try
        {
            d.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public Integer getPort()
    {
        return port;
    }

    public Daemon getDaemon()
    {
        return d;
    }

    private static final class RepositoryResolverImplementation implements RepositoryResolver<DaemonClient>
    {
        private String projectName;
        private Context c;

        private RepositoryResolverImplementation(String projectName, Context c)
        {
            this.projectName = projectName;
            this.c = c;
        }

        @Override
        public Repository open(DaemonClient client, String name) throws RepositoryNotFoundException
        {
            if (!name.equals(projectName))
            {
                throw new RepositoryNotFoundException(name);
            }
            try
            {
                return Git.open(new File(c.getFilesDir(), name)).getRepository();
            }
            catch (IOException e)
            {
                throw new RepositoryNotFoundException("");
            }
        }
    }
}
