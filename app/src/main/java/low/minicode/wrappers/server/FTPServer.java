/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.server;

import android.content.Context;
import android.util.Log;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.filesystem.nativefs.NativeFileSystemFactory;
import org.apache.ftpserver.filesystem.nativefs.impl.NativeFileSystemView;
import org.apache.ftpserver.filesystem.nativefs.impl.NativeFtpFile;
import org.apache.ftpserver.ftplet.Authentication;
import org.apache.ftpserver.ftplet.AuthenticationFailedException;
import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpFile;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.AnonymousAuthentication;
import org.apache.ftpserver.usermanager.ClearTextPasswordEncryptor;
import org.apache.ftpserver.usermanager.PasswordEncryptor;
import org.apache.ftpserver.usermanager.UserManagerFactory;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.apache.ftpserver.usermanager.impl.AbstractUserManager;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class FTPServer
{
    private final String projectName;
    private final String password;
    private final Integer port;
    private final Context context;
    private FtpServer ftpServer;

    public FTPServer(String projectName, String password, Integer port, Context context)
    {
        this.projectName = projectName;
        this.password = password;
        this.port = port;
        this.context = context;
        this.run();
    }

    public void run()
    {
        FtpServerFactory serverFactory = new FtpServerFactory();
        ListenerFactory factory = new ListenerFactory();
        factory.setPort(port);
        serverFactory.addListener("default", factory.createListener());
        UserManagerFactory userManagerFactory = new TestUserManagerFactory(password);
        serverFactory.setUserManager(userManagerFactory.createUserManager());
        serverFactory.setFileSystem(new NativeFileSystemFactory()
        {
            @Override
            public FileSystemView createFileSystemView(User user) throws FtpException
            {
                return new NativeFileSystemView(user, false)
                {
                    @Override
                    public FtpFile getHomeDirectory()
                    {
                        try
                        {
                            Constructor<NativeFtpFile> ftpFileConstructor = NativeFtpFile.class.getConstructor(String.class, File.class, User.class);
                            ftpFileConstructor.setAccessible(true);
                            return ftpFileConstructor.newInstance(new File(context.getFilesDir(), projectName).getAbsolutePath(), new File(context.getFilesDir(), projectName), user);
                        }
                        catch (NoSuchMethodException e)
                        {
                            e.printStackTrace();
                            return super.getHomeDirectory();
                        }
                        catch (IllegalAccessException e)
                        {
                            e.printStackTrace();
                            return super.getHomeDirectory();
                        }
                        catch (InstantiationException e)
                        {
                            e.printStackTrace();
                            return super.getHomeDirectory();
                        }
                        catch (InvocationTargetException e)
                        {
                            e.printStackTrace();
                            return super.getHomeDirectory();
                        }
                    }
                };
            }
        });
        ftpServer = serverFactory.createServer();
        try
        {
            ftpServer.start();
        }
        catch (FtpException e)
        {
            e.printStackTrace();
        }
    }

    public FtpServer getFtpServer()
    {
        return ftpServer;
    }

    public Integer getPort()
    {
        return port;
    }

    private class TestUserManagerFactory implements UserManagerFactory
    {
        private String password;

        public TestUserManagerFactory(String password)
        {
            this.password = password;
        }

        @Override
        public UserManager createUserManager()
        {
            return new TestUserManager("miniCode", new ClearTextPasswordEncryptor(), password);
        }
    }

    private class TestUserManager extends AbstractUserManager
    {
        private BaseUser testUser;
        private BaseUser anonUser;

        public TestUserManager(String adminName, PasswordEncryptor passwordEncryptor, String password)
        {
            super(adminName, passwordEncryptor);
            testUser = new BaseUser();
            testUser.setAuthorities(Arrays.asList(new ConcurrentLoginPermission(10, 3), new WritePermission()));
            testUser.setEnabled(true);
            testUser.setHomeDirectory(new File(context.getFilesDir(), projectName).getAbsolutePath());
            testUser.setMaxIdleTime(10000);
            testUser.setName(adminName);
            testUser.setPassword(password);
            anonUser = new BaseUser(testUser);
            anonUser.setName("anonymous");
            anonUser.setHomeDirectory(new File(context.getFilesDir(), projectName).getAbsolutePath());
        }

        @Override
        public User getUserByName(String username) throws FtpException
        {
            if (testUser.getName().equals(username))
            {
                return testUser;
            }
            else if (anonUser.getName().equals(username))
            {
                return anonUser;
            }
            return null;
        }

        @Override
        public String[] getAllUserNames() throws FtpException
        {
            return new String[] {testUser.getName(), anonUser.getName()};
        }

        @Override
        public void delete(String username) throws FtpException
        {
        }

        @Override
        public void save(User user) throws FtpException
        {
        }

        @Override
        public boolean doesExist(String username) throws FtpException
        {
            return testUser.getName().equals(username) || anonUser.getName().equals(username);
        }

        @Override
        public User authenticate(Authentication authentication) throws AuthenticationFailedException
        {
            if (UsernamePasswordAuthentication.class.isAssignableFrom(authentication.getClass()))
            {
                UsernamePasswordAuthentication upAuth = (UsernamePasswordAuthentication) authentication;
                Log.d(getClass().getName(), String.format("Auth: %s:%s | %s:%s", upAuth.getUsername(), testUser.getName(), testUser.getPassword(), upAuth.getPassword()));
                if (testUser.getName().equals(upAuth.getUsername()) && testUser.getPassword().equals(upAuth.getPassword()))
                {
                    return testUser;
                }
                if (anonUser.getName().equals(upAuth.getUsername()))
                {
                    return anonUser;
                }
            }
            else if (AnonymousAuthentication.class.isAssignableFrom(authentication.getClass()))
            {
                return anonUser;
            }
            return null;
        }
    }
}
