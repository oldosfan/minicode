package low.minicode.wrappers.gitlab.milestone;

import androidx.annotation.NonNull;

import org.gitlab4j.api.models.Milestone;

public class MilestoneStringWrapper
{
    private Milestone milestone;

    public MilestoneStringWrapper(Milestone milestone)
    {
        this.milestone = milestone;
    }

    @NonNull
    @Override
    public String toString()
    {
        return milestone.getTitle();
    }

    public Milestone getMilestone()
    {
        return milestone;
    }
}
