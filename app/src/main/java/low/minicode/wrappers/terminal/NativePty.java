/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.wrappers.terminal;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class NativePty extends Structure
{
    public int master;
    public int slave;
    public int cols;
    public int lines;
    public String slave_path;

    public NativePty(int master, int slave, int cols, int lines, String slave_path)
    {
        this.master = master;
        this.slave = slave;
        this.cols = cols;
        this.slave_path = slave_path;
    }

    public NativePty()
    {
    }

    @Override
    protected List getFieldOrder()
    {
        return Arrays.asList("master", "slave", "cols", "lines", "slave_path");
    }
}