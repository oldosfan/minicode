/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.application.Minicode;

public class FileInfoViewer
{
    private String result;
    private File file;

    public FileInfoViewer(File file)
    {
        this.result = Minicode.context.getString(R.string.no_result);
        this.file = file;
        try
        {
            Process p = Runtime.getRuntime().exec(Library.getFileInvocation(file.getAbsolutePath()), new String[] {String.format("LD_LIBRARY_PATH=%s", Library.getFileLibPath())});
            Future<Integer> future = Executors.newSingleThreadExecutor().submit(() ->
            {
                try
                {
                    p.waitFor();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                    return -1;
                }
                return p.exitValue();
            });
            InputStream is = p.getInputStream();
            StringBuilder tmpRes = new StringBuilder();
            do
            {
                int avail = 0;
                byte[] b = new byte[0];
                if ((avail = is.available()) > 0)
                {
                    b = new byte[avail];
                    is.read(b);
                }
                tmpRes.append(new String(b));
            }
            while (!future.isDone());
            String res = tmpRes.toString();
            Log.d("result", res);
            if (res.matches(".+:\\s+.*\\n"))
            {
                Pattern pattern = Pattern.compile(".+:\\s+(.*)\\n");
                Matcher m = pattern.matcher(res);
                if (m.find())
                {
                    result = m.group(1);
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            result = Minicode.context.getString(R.string.no_file_info);
        }
    }

    public File getFile()
    {
        return file;
    }

    public String getResult()
    {
        return result;
    }
}
