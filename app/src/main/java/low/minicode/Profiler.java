/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.os.Build;

import java.util.Hashtable;

public class Profiler
{
    public static String getInfoAsString()
    {
        Hashtable<String, String> hashtable = new Hashtable<>();
        hashtable.put("SDK", String.valueOf(Build.VERSION.SDK_INT));
        hashtable.put("Release", String.valueOf(Build.VERSION.RELEASE));
        hashtable.put("Hardware", Build.HARDWARE);
        hashtable.put("Manufacturer", Build.MANUFACTURER);
        hashtable.put("Display", Build.DISPLAY);
        hashtable.put("Model", Build.MODEL);
        hashtable.put("CPU-ABI", Build.CPU_ABI);
        hashtable.put("Detected CPU", Library.getArchName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            hashtable.put("Security patch", Build.VERSION.SECURITY_PATCH);
        }
        hashtable.put("Type", Build.TYPE);
        return hashtable.toString();
    }
}
