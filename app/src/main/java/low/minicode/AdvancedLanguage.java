/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

public enum AdvancedLanguage
{
    C(1), CC(1 << 1), M(1 << 2), MM(1 << 3), JS(1 << 8), JAVA(1 << 9), SHELL(1 << 10), PYTHON3(1 << 11), PYTHON2(1 >> 11), HTML(1 << 12), SCALA(1 << 14), REGEX(1 << 15), LUA(1 << 16), LFLEX(1 << 25), PLIST(1 << 26), XML(1 << 30), SQL(1 >> 31), MARKDOWN(1 >> 27);
    private long mask;

    AdvancedLanguage(long mask)
    {
        this.mask = mask;
    }

    public long getMask()
    {
        return mask;
    }
}
