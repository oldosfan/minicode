/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class Downloader
{
    private final ProgressMonitor m;
    private final String url;
    private final File outputDir;
    private final boolean treatOutputDirAsExact;

    private Downloader(ProgressMonitor m, String url, File outputDir, boolean treatOutputDirAsExact)
    {
        this.m = m;
        this.url = url;
        this.outputDir = outputDir;
        this.treatOutputDirAsExact = treatOutputDirAsExact;
    }

    public void _run() throws IOException
    {
        if (!treatOutputDirAsExact)
        {
            outputDir.mkdirs();
        }
        URLConnection connection = new URL(url).openConnection();
        InputStream stream = connection.getInputStream();
        String name = connection.getHeaderField("Content-Disposition");
        if (name == null)
        {
            name = url.substring(url.lastIndexOf('/') + 1);
        }
        FileOutputStream fileOutputStream = treatOutputDirAsExact ? new FileOutputStream(outputDir) : new FileOutputStream(new File(outputDir, name));
        int length = connection.getContentLength();
        int totalRead = 0;
        int read;
        byte[] buffer = new byte[2048];
        while ((read = stream.read(buffer)) > 0)
        {
            totalRead += read;
            fileOutputStream.write(buffer, 0, read);
            m.progress(length, totalRead);
        }
        m.progress(length, totalRead);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    public void run()
    {
        try
        {
            _run();
        }
        catch (IOException e)
        {
            m.error(e, e.getLocalizedMessage());
        }
    }

    public interface ProgressMonitor
    {
        void progress(int total, int consumed);

        void error(@Nullable Throwable t, String humanReadableLocalized);

        void complete();
    }

    public static class Builder
    {
        private ProgressMonitor m;
        private String url;
        private File outputDir;
        private boolean treatOutputDirAsExact;

        public Builder setProgressMonitor(ProgressMonitor m)
        {
            this.m = m;
            return this;
        }

        public Builder setUrl(String url)
        {
            this.url = url;
            return this;
        }

        public Builder setOutputDir(File outputDir)
        {
            this.outputDir = outputDir;
            return this;
        }

        public Builder setTreatOutputDirAsExact(boolean treatOutputDirAsExact)
        {
            this.treatOutputDirAsExact = treatOutputDirAsExact;
            return this;
        }

        public Downloader build()
        {
            return new Downloader(m, url, outputDir, treatOutputDirAsExact);
        }
    }
}
