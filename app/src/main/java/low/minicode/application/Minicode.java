/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode.application;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;

import com.google.common.collect.ImmutableList;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import low.minicode.Fonts;
import low.minicode.PushService;
import low.minicode.R;
import low.minicode.activity.basic.ThemedAppCompatActivity;
import low.minicode.activity.project.navigator.NavigatorActivity;
import low.minicode.global.broadcast.CrashNotificationReciever;

public class Minicode extends Application
{
    public static final String maintainer = "oldosfan";
    public static final String copyright = "Copyright © 2018-2019 oldosfan. All rights reserved.";
    public static final String bugReportEndpoint = "http://www.oldosfan.com/minicode/acra.php";
    public static final int assetVersion = 201;
    public static final String updateUrl = "http://www.oldosfan.com/minicode/updates.json";
    public static final String[] supportedLanguages = {"C", "C++", "Python-3"};
    public static final String[] partiallySupportedLanguages = {"Objective-C", "Objective-C++"};
    public static UUID uuid;
    public static Map<String, Typeface> originalFonts;
    public static Context context;
    public static Handler globalHandler;
    public static String language = "";
    public static Minicode theMinicode;

    @SuppressWarnings("unchecked")
    public Minicode()
    {
        super();
        try
        {
            Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
            staticField.setAccessible(true);
            originalFonts = (Map<String, Typeface>) staticField.get(null);
        }
        catch (NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    private static Context updateLang(Context context, String language)
    {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.setLocale(locale);
        context = context.createConfigurationContext(config);
        return context;
    }

    public static String getLocaleCode(int localParams)
    {
        switch (localParams)
        {
            case (0):
                return Locale.getDefault().getLanguage();
            case (1):
                return Locale.UK.getLanguage();
            case (2):
                return Locale.CHINA.getLanguage();
            default:
                return Locale.getDefault().getLanguage();
        }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        if (getSharedPreferences("low.minicode.identity", MODE_PRIVATE).getString("uuid", null) == null)
        {
            uuid = UUID.randomUUID();
            getSharedPreferences("low.minicode.identity", MODE_PRIVATE).edit().putString("uuid", uuid.toString()).apply();
        }
        else
        {
            uuid = UUID.fromString(getSharedPreferences("low.minicode.identity", MODE_PRIVATE).getString("uuid", UUID.randomUUID().toString()));
        }
        if (getSharedPreferences("low.minicode.mortem_service", MODE_PRIVATE).getString("serialized_throwable", null) != null)
        {
            Intent intent = new Intent(this, CrashNotificationReciever.class);
            String response = getSharedPreferences("low.minicode.mortem_service", MODE_PRIVATE).getString("serialized_throwable", null);
            String[] byteValues = response.substring(1, response.length() - 1).split(",");
            byte[] bytes = new byte[byteValues.length];
            for (int i = 0; i < bytes.length; ++i)
            {
                bytes[i] = Byte.decode(byteValues[i].trim());
            }
            try
            {
                ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
                intent.putExtra("throwables", new ArrayList<>(Collections.singletonList((Throwable) inputStream.readObject())));
                intent.setAction("low.minicode.DEFAULT_ACTION");
                sendBroadcast(intent);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
            getSharedPreferences("low.minicode.mortem_service", MODE_PRIVATE).edit().remove("serialized_throwable").commit();
        }
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) ->
        {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try
            {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(throwable);
                getSharedPreferences("low.minicode.mortem_service", MODE_PRIVATE).edit().putString("serialized_throwable", Arrays.toString(outputStream.toByteArray())).commit();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            System.exit(0);
        });
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        context = this.getApplicationContext();
        globalHandler = new Handler();
        updateShortcuts();
        theMinicode = this;
        new ServiceStarter().start();
        restoreFont();
        // new WakeLockThread(this.getApplicationContext()).start();
    }

    public void restoreFont()
    {
        try
        {
            if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("font", 0) == 0)
            {
                FontOverrideHelper.restoreFonts();
            }
            else
            {
                setDefaultFont();
            }
        }
        catch (NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }

    public void setDefaultFont()
    {
        try
        {
            Field DEFAULT = Typeface.class.getDeclaredField("DEFAULT");
            DEFAULT.setAccessible(true);
            DEFAULT.set(null, Fonts.REGULAR);
            Field DEFAULT_BOLD = Typeface.class.getDeclaredField("DEFAULT_BOLD");
            DEFAULT_BOLD.setAccessible(true);
            DEFAULT_BOLD.set(null, Fonts.BOLD);
            Field SDEFAULT = Typeface.class.getDeclaredField("SERIF");
            SDEFAULT.setAccessible(true);
            SDEFAULT.set(null, Fonts.REGULAR);
            Field SDEFAULT_BOLD = Typeface.class.getDeclaredField("SANS_SERIF");
            SDEFAULT_BOLD.setAccessible(true);
            SDEFAULT_BOLD.set(null, Fonts.REGULAR);
            Field sDefaults = Typeface.class.getDeclaredField("sDefaults");
            sDefaults.setAccessible(true);
            sDefaults.set(null, new Typeface[] {Fonts.REGULAR, Fonts.BOLD, Fonts.ITALIC, Fonts.BOLD_ITALIC});
            FontOverrideHelper.replaceFont("sans-serif", Fonts.REGULAR);
            FontOverrideHelper.replaceFont("sans-serif-condensed", Fonts.REGULAR);
            FontOverrideHelper.replaceFont("sans-serif-condensed-light", Fonts.REGULAR);
            FontOverrideHelper.replaceFont("sans-serif-thin", Fonts.REGULAR);
            FontOverrideHelper.replaceFont("sans-serif-medium", Fonts.REGULAR);
            FontOverrideHelper.replaceFont("sans-serif-condensed-bold", Fonts.BOLD);
            FontOverrideHelper.replaceFont("sans-serif-condensed-medium", Fonts.BOLD);
            FontOverrideHelper.replaceFont("sans-serif-monospace", Fonts.BOLD);
            FontOverrideHelper.replaceFont("sans-serif-black", Fonts.BOLD);
            FontOverrideHelper.replaceFont("serif", Fonts.ITALIC);
            FontOverrideHelper.replaceFont("SANS_SERIF", Fonts.BOLD_ITALIC);
        }
        catch (NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    }

    public void updateShortcuts()
    {
        if (getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("pinned", null) != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1)
        {
            createShorcut(getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getString("pinned", null));
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1)
        {
            ShortcutManager shortcutManager = getSystemService(ShortcutManager.class);
            shortcutManager.removeAllDynamicShortcuts();
        }
    }

    @TargetApi(25)
    private void createShorcut(String pinned)
    {
        ShortcutManager shortcutManager = getSystemService(ShortcutManager.class);
        Intent intent1 = new Intent(getApplicationContext(), NavigatorActivity.class);
        intent1.setAction(Intent.ACTION_VIEW);
        intent1.putExtra("projectName", pinned);
        intent1.putExtra("loc", "/");
        ShortcutInfo shortcut1 = new ShortcutInfo.Builder(this, String.format("pinned_%s", pinned)).setIntent(intent1).setLongLabel(pinned).setShortLabel(pinned).setIcon(Icon.createWithResource(this, R.drawable.ic_pin_24dp)).build();
        shortcutManager.setDynamicShortcuts(ImmutableList.of(shortcut1));
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        language = base.getSharedPreferences("low.minicode.preferences", MODE_PRIVATE).getInt("language_override", 0) != 0 ? getLocaleCode(base.getSharedPreferences("low" + ".minicode.preferences", MODE_PRIVATE).getInt("language_override", 0)) : "";
        super.attachBaseContext(ThemedAppCompatActivity.updateBaseContextLocale(base));
    }

    private boolean isServiceRunning(Class<?> serviceClass)
    {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (serviceClass.getName().equals(service.service.getClassName()))
            {
                return true;
            }
        }
        return false;
    }

    public Typeface getEditorTypeface()
    {
        try
        {
            if (!new File(getFilesDir(), ".res/fonts/active.ttf").exists())
            {
                return Typeface.createFromAsset(getAssets(), "fonts/MesloLGM-Regular.ttf");
            }
            else
            {
                File ttf = new File(getFilesDir(), ".res/fonts/active.ttf");
                return Typeface.createFromFile(ttf);
            }
        }
        catch (RuntimeException e)
        {
            return Typeface.MONOSPACE;
        }
    }

    public static final class FontOverrideHelper
    {
        @SuppressWarnings("unchecked")
        public static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface)
        {
            try
            {
                Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                Log.d("FontOverrideHelper", staticField.get(null).toString());
                HashMap<String, Typeface> map = new HashMap<>((Map<String, Typeface>) staticField.get(null));
                map.put(staticTypefaceFieldName, newTypeface);
                staticField.set(null, map);
            }
            catch (NoSuchFieldException e)
            {
                e.printStackTrace();
            }
            catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
            catch (ClassCastException e)
            {
                e.printStackTrace();
            }
        }

        public static void restoreFonts() throws NoSuchFieldException, IllegalAccessException
        {
            Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
            staticField.setAccessible(true);
            staticField.set(null, originalFonts);
        }
    }

    class ServiceStarter extends Thread
    {
        @Override
        public void run()
        {
            ArrayList<Class<? extends Service>> services = new ArrayList<>();
            services.add(PushService.class);
            while (true)
            {
                try
                {
                    Log.v("ServiceStarter", "run: starting service(s)");
                    for (Class<? extends Service> service : services)
                    {
                        startService(new Intent(Minicode.context, service));
                    }
                    Log.v("ServiceStarter", "run: service(s) started");
                    break;
                }
                catch (IllegalStateException e)
                {
                    try
                    {
                        Log.v("ServiceStarter", "run: pausing 240ms before trying again");
                        sleep(240);
                    }
                    catch (InterruptedException e1)
                    {
                        e1.printStackTrace();
                    }
                }
            }
            Log.v("ServiceStarter", "run: entering passive monitor mode");
            while (true)
            {
                Log.v("ServiceStarter", "run: new cycle");
                try
                {
                    sleep(500);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                for (Class<? extends Service> service : services)
                {
                    if (!isServiceRunning(service))
                    {
                        while (true)
                        {
                            try
                            {
                                Log.v("ServiceStarter", String.format("run: Service %s stopped..." + " restarting", service.getCanonicalName()));
                                startService(new Intent(Minicode.context, service));
                                break;
                            }
                            catch (IllegalStateException e)
                            {
                                try
                                {
                                    Log.v("ServiceStarter", "run: pausing 240ms before trying " + "again");
                                    sleep(240);
                                }
                                catch (InterruptedException e1)
                                {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
