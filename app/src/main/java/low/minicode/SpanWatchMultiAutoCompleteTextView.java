/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.MultiAutoCompleteTextView;

import java.util.ArrayList;

@SuppressLint("AppCompatCustomView")
public class SpanWatchMultiAutoCompleteTextView extends MultiAutoCompleteTextView
{
    private ArrayList<SelectionHandler> selectionHandlers;
    private ArrayList<SelectionHandler> enterHandler;

    public SpanWatchMultiAutoCompleteTextView(Context context)
    {
        super(context);
        selectionHandlers = new ArrayList<>();
    }

    public SpanWatchMultiAutoCompleteTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        selectionHandlers = new ArrayList<>();
    }

    public SpanWatchMultiAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        selectionHandlers = new ArrayList<>();
    }

    public SpanWatchMultiAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        selectionHandlers = new ArrayList<>();
    }

    public void addSelectionHandler(SelectionHandler spanWatcher)
    {
        selectionHandlers.add(spanWatcher);
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd)
    {
        if (selectionHandlers == null)
        {
            selectionHandlers = new ArrayList<>();
        }
        for (SelectionHandler watcher : selectionHandlers)
        {
            watcher.onSelectionChanged(selStart, selEnd, getText().toString());
        }
        super.onSelectionChanged(selStart, selEnd);
    }

    public interface SelectionHandler
    {
        void onSelectionChanged(int selStart, int selEnd, String text);
    }
}
