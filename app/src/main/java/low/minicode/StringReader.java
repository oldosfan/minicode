/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import org.apache.commons.io.input.CharSequenceReader;

public class StringReader extends CharSequenceReader
{
    private int loc = 0;
    private String source;

    public StringReader(String source)
    {
        super(source);
        this.source = source;
    }

    @Override
    public int read(char[] cbuf, int off, int len)
    {
        int result = super.read(cbuf, off, len);
        loc += result;
        return result;
    }

    @Override
    public void close()
    {
    }

    public int getLoc()
    {
        return loc;
    }

    public String getSource()
    {
        return source;
    }
}
