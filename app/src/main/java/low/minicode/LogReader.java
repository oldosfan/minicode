/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class LogReader
{
    private final String TAG = getClass().getCanonicalName();
    private final String processId = Integer.toString(android.os.Process.myPid());

    public StringBuilder getLog()
    {
        StringBuilder builder = new StringBuilder();
        try
        {
            String[] command = new String[] {"logcat", "-d", "-v", "y"};
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                builder.append(line).append("\n");
            }
        }
        catch (IOException ex)
        {
            Log.e(TAG, "getLog failed", ex);
        }
        return builder;
    }
}
