/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.graphics.Color;
import android.text.Editable;
import android.text.style.BackgroundColorSpan;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import low.minicode.wrappers.data.StrongReference;

import static android.graphics.Color.TRANSPARENT;

public class FindTask extends Thread
{
    private final StrongReference<EditText> editText;
    private int flags;
    private List<Selection> stash;
    private Pattern p;

    public FindTask(int flags, EditText editText, Pattern p)
    {
        this.flags = flags;
        this.editText = new StrongReference<>();
        this.editText.set(editText);
        this.stash = new ArrayList<>();
        this.p = p;
    }

    @Override
    public void run()
    {
        editText.get().getEditableText().setSpan(new BackgroundColorSpan(TRANSPARENT), 0, editText.get().getText().length(), 0);
        if (p.pattern().isEmpty())
        {
            return;
        }
        Matcher m = p.matcher(editText.get().getText());
        while (m.find())
        {
            if (isInterrupted())
            {
                return;
            }
            stash.add(new Selection(m.start(), m.end()));
            if (stash.size() > 2)
            {
                synchronized (editText)
                {
                    try
                    {
                        Editable d = editText.get().getEditableText();
                        for (Selection s : stash)
                        {
                            d.setSpan(new BackgroundColorSpan(Color.YELLOW), s.getStart(), s.getEnd(), 0);
                        }
                    }
                    catch (IndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                    }
                    stash.clear();
                }
            }
        }
        if (isInterrupted())
        {
            return;
        }
        synchronized (editText)
        {
            try
            {
                Editable d = editText.get().getEditableText();
                for (Selection s : stash)
                {
                    d.setSpan(new BackgroundColorSpan(Color.YELLOW), s.getStart(), s.getEnd(), 0);
                }
            }
            catch (IndexOutOfBoundsException e)
            {
                e.printStackTrace();
            }
            stash.clear();
        }
    }
}
