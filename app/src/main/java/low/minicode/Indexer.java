/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.text.style.ForegroundColorSpan;
import android.widget.EditText;

import org.apache.commons.io.input.CharSequenceReader;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import jsyntaxpane.Token;
import jsyntaxpane.TokenType;
import jsyntaxpane.lexers.BashLexer;
import jsyntaxpane.lexers.CLexer;
import jsyntaxpane.lexers.CppLexer;
import jsyntaxpane.lexers.DefaultJFlexLexer;
import jsyntaxpane.lexers.JavaLexer;
import jsyntaxpane.lexers.JavaScriptLexer;
import jsyntaxpane.lexers.LuaLexer;
import jsyntaxpane.lexers.PropertiesLexer;
import jsyntaxpane.lexers.PythonLexer;
import jsyntaxpane.lexers.ScalaLexer;
import jsyntaxpane.lexers.SqlLexer;
import jsyntaxpane.lexers.XHTMLLexer;
import low.minicode.application.Minicode;
import low.minicode.wrappers.data.DualDataStructure;
import low.minicode.wrappers.data.DualDataStructure2;
import low.minicode.wrappers.data.StrongReference;

public class Indexer extends Thread
{
    private final StrongReference<EditText> editText;
    private String text;
    private AdvancedLanguage language;
    private List<DualDataStructure2<DualDataStructure<Integer>, Integer>> stash;

    public Indexer(EditText editText, String text, AdvancedLanguage language)
    {
        super("Indexer");
        this.editText = new StrongReference<>();
        this.editText.set(editText);
        this.text = text;
        this.language = language;
        this.stash = new ArrayList<>();
    }

    public Indexer(EditText editText, String text, AdvancedLanguage language, List<DualDataStructure2<DualDataStructure<Integer>, Integer>> stash)
    {
        this.editText = new StrongReference<>(editText);
        this.text = text;
        this.language = language;
        this.stash = stash;
    }

    @Override
    public void run()
    {
        super.run();
        if (isInterrupted())
        {
            return;
        }
        DefaultJFlexLexer lexer;
        if (language != null)
        {
            switch (language)
            {
                case C:
                case M:
                    lexer = new CLexer(new CharSequenceReader(text));
                    break;
                case CC:
                case MM:
                    lexer = new CppLexer(new CharSequenceReader(text));
                    break;
                case JS:
                    lexer = new JavaScriptLexer(new CharSequenceReader(text));
                    break;
                case HTML:
                    lexer = new XHTMLLexer(new CharSequenceReader(text));
                    break;
                case JAVA:
                    lexer = new JavaLexer(new CharSequenceReader(text));
                    break;
                case SHELL:
                    lexer = new BashLexer(new CharSequenceReader(text));
                    break;
                case PYTHON3:
                    lexer = new PythonLexer(new CharSequenceReader(text));
                    break;
                case LUA:
                    lexer = new LuaLexer(new CharSequenceReader(text));
                    break;
                case SQL:
                    lexer = new SqlLexer(new CharSequenceReader(text));
                    break;
                case XML:
                    lexer = new XHTMLLexer(new CharSequenceReader(text));
                    break;
                case LFLEX:
                    lexer = new XHTMLLexer(new CharSequenceReader(text));
                    break;
                case SCALA:
                    lexer = new ScalaLexer(new CharSequenceReader(text));
                    break;
                case PLIST:
                    lexer = new PropertiesLexer(new CharSequenceReader(text));
                    break;
                default:
                    lexer = new DefaultJFlexLexer()
                    {
                        @Override
                        public void yyreset(Reader reader)
                        {
                        }

                        @Override
                        public Token yylex() throws IOException
                        {
                            return null;
                        }

                        @Override
                        public char yycharat(int pos)
                        {
                            return 0;
                        }

                        @Override
                        public int yylength()
                        {
                            return 0;
                        }

                        @Override
                        public String yytext()
                        {
                            return null;
                        }

                        @Override
                        public int yychar()
                        {
                            return 0;
                        }
                    };
                    break;
            }
        }
        else
        {
            lexer = new DefaultJFlexLexer()
            {
                @Override
                public void yyreset(Reader reader)
                {
                }

                @Override
                public Token yylex() throws IOException
                {
                    return null;
                }

                @Override
                public char yycharat(int pos)
                {
                    return 0;
                }

                @Override
                public int yylength()
                {
                    return 0;
                }

                @Override
                public String yytext()
                {
                    return null;
                }

                @Override
                public int yychar()
                {
                    return 0;
                }
            };
        }
        if (isInterrupted())
        {
            return;
        }
        Token token;
        try
        {
            while ((token = lexer.yylex()) != null)
            {
                String sub = text.substring(token.start, token.end());
                if (token.type == TokenType.COMMENT || token.type == TokenType.COMMENT2)
                {
                    stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.darker_gray)));
                }
                else if (token.type == TokenType.STRING)
                {
                    stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(android.R.color.tab_indicator_text)));
                }
                else if (token.type == TokenType.STRING2)
                {
                    stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                }
                else if (token.type == TokenType.NUMBER)
                {
                    if (sub.matches("0(x[\\dabcdefABCDEF]+)|(b[01]+)"))
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_dark)));
                    }
                    else
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.accent_device_default_700)));
                    }
                }
                else if (sub.equals("return") && token.type == TokenType.KEYWORD)
                {
                    stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.holo_green_light)));
                }
                else
                {
                    if (token.type == TokenType.TYPE || token.type == TokenType.TYPE2 || token.type == TokenType.TYPE3)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_green_900)));
                    }
                    else if (token.type == TokenType.KEYWORD)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_orange_900)));
                    }
                    else if (token.type == TokenType.KEYWORD2)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.gplus_color_3)));
                    }
                    else if (token.type == TokenType.ERROR)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_red_600)));
                    }
                    else if (token.type == TokenType.OPERATOR)
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), editText.get().getTextColors().getDefaultColor()));
                    }
                    else
                    {
                        stash.add(new DualDataStructure2<>(new DualDataStructure<>(token.start, token.end()), Minicode.context.getResources().getColor(R.color.md_deep_purple_500)));
                    }
                }
                if (stash.size() > 10)
                {
                    if (isInterrupted())
                    {
                        return;
                    }
                    AtomicBoolean atomicBoolean = new AtomicBoolean(false);
                    editText.get().post(() ->
                    {
                        for (DualDataStructure2<DualDataStructure<Integer>, Integer> structure : stash)
                        {
                            if (structure == null)
                            {
                                return;
                            }
                            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(structure.getB());
                            try
                            {
                                editText.get().getEditableText().setSpan(foregroundColorSpan, structure.getA().getA(), structure.getA().getB(), 0);
                            }
                            catch (IndexOutOfBoundsException e)
                            {
                            }
                        }
                        stash.clear();
                        atomicBoolean.set(true);
                    });
                    while (!atomicBoolean.get())
                    {
                        if (isInterrupted())
                        {
                            return;
                        }
                    }
                }
            }
            if (isInterrupted())
            {
                return;
            }
            AtomicBoolean b = new AtomicBoolean(false);
            editText.get().post(() ->
            {
                for (DualDataStructure2<DualDataStructure<Integer>, Integer> structure : stash)
                {
                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(structure.getB());
                    try
                    {
                        editText.get().getEditableText().setSpan(foregroundColorSpan, structure.getA().getA(), structure.getA().getB(), 0);
                    }
                    catch (IndexOutOfBoundsException e)
                    {
                    }
                }
                stash.clear();
                b.set(true);
            });
            while (!b.get())
            {
            }
        }
        catch (IOException e)
        {
        }
    }
}