/*
 * Copyright (c) 2018 - 2019 oldosfan.
 *
 * miniCode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * miniCode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package low.minicode;

import android.graphics.drawable.Drawable;
import android.view.View;

public class Preference
{
    private String bigHeader;
    private String smallHeader;
    private String preferenceName;
    private String[] items;
    private Object defaultValue;
    private PreferenceTypes type;
    private View.OnClickListener onClickListener;
    private Runnable onChangeListener;
    private Drawable drawable;
    private Integer seekBarMax, seekBarMin;

    public Preference(String bigHeader, String smallHeader, String preferenceName, String[] items, Object defaultValue, PreferenceTypes type)
    {
        if (type == PreferenceTypes.TYPE_CLICK || type == PreferenceTypes.TYPE_SLIDE)
        {
            throw new BadTypeException("Type must not be TYPE_CLICK. Use the other constructor");
        }
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.preferenceName = preferenceName;
        this.items = items;
        this.defaultValue = defaultValue;
        this.type = type;
    }

    public Preference(String bigHeader, String smallHeader, String preferenceName, String[] items, Object defaultValue, PreferenceTypes type, Drawable drawable)
    {
        if (type == PreferenceTypes.TYPE_CLICK || type == PreferenceTypes.TYPE_SLIDE)
        {
            throw new BadTypeException("Type must not be TYPE_CLICK. Use the other constructor");
        }
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.preferenceName = preferenceName;
        this.items = items;
        this.defaultValue = defaultValue;
        this.type = type;
        this.drawable = drawable;
    }

    public Preference(String bigHeader, String smallHeader, String preferenceName, String[] items, Object defaultValue, PreferenceTypes type, Runnable onChangeListener)
    {
        if (type == PreferenceTypes.TYPE_CLICK || type == PreferenceTypes.TYPE_SLIDE)
        {
            throw new BadTypeException("Type must not be TYPE_CLICK. Use the other constructor");
        }
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.preferenceName = preferenceName;
        this.items = items;
        this.defaultValue = defaultValue;
        this.type = type;
        this.onChangeListener = onChangeListener;
    }

    public Preference(String bigHeader, String smallHeader, String preferenceName, String[] items, Object defaultValue, PreferenceTypes type, Runnable onChangeListener, Drawable drawable)
    {
        if (type == PreferenceTypes.TYPE_CLICK || type == PreferenceTypes.TYPE_SLIDE)
        {
            throw new BadTypeException("Type must not be TYPE_CLICK. Use the other constructor");
        }
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.preferenceName = preferenceName;
        this.items = items;
        this.defaultValue = defaultValue;
        this.type = type;
        this.onChangeListener = onChangeListener;
        this.drawable = drawable;
    }

    public Preference(String bigHeader, String smallHeader)
    {
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.onClickListener = onClickListener;
        this.type = PreferenceTypes.TYPE_CLICK;
    }

    public Preference(String bigHeader, String smallHeader, View.OnClickListener onClickListener, Drawable drawable)
    {
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.onClickListener = onClickListener;
        this.type = PreferenceTypes.TYPE_CLICK;
        this.drawable = drawable;
    }

    public Preference(String bigHeader, String smallHeader, String preferenceName, int min, int max, int defaultValue)
    {
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.defaultValue = defaultValue;
        this.type = PreferenceTypes.TYPE_SLIDE;
        this.seekBarMax = max;
        this.preferenceName = preferenceName;
        this.seekBarMin = min;
    }

    public Preference(String bigHeader, String smallHeader, String preferenceName, int min, int max, int defaultValue, Drawable drawable)
    {
        this.bigHeader = bigHeader;
        this.smallHeader = smallHeader;
        this.defaultValue = defaultValue;
        this.type = PreferenceTypes.TYPE_SLIDE;
        this.drawable = drawable;
        this.seekBarMax = max;
        this.preferenceName = preferenceName;
        this.seekBarMin = min;
    }

    public Integer getSeekBarMax()
    {
        return seekBarMax;
    }

    public Integer getSeekBarMin()
    {
        return seekBarMin;
    }

    public String getSmallHeader()
    {
        return smallHeader;
    }

    public String getPreferenceName()
    {
        return preferenceName;
    }

    public PreferenceTypes getType()
    {
        return type;
    }

    public String getBigHeader()
    {
        return bigHeader;
    }

    public String[] getItems()
    {
        return items;
    }

    public Object getDefaultValue()
    {
        return defaultValue;
    }

    public View.OnClickListener getOnClickListener()
    {
        return onClickListener;
    }

    public Runnable getOnChangeListener()
    {
        return onChangeListener;
    }

    public Drawable getDrawable()
    {
        return drawable;
    }

    public class BadTypeException extends RuntimeException
    {
        public BadTypeException(String e)
        {
            super(e);
        }
    }

}
