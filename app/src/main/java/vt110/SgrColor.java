/*
 * Copyright (c) 2009-2011 Graham Edgecombe.
 * Copyright (c) 2019 Po Lu
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package vt110;

import android.graphics.Color;

import androidx.annotation.Size;

/**
 * Contains colors used by the SGR ANSI escape sequence.
 *
 * @author Graham Edgecombe
 */
public final class SgrColor
{
    /**
     * An array of normal intensity colors.
     */
    public static final int[] COLOR_NORMAL = new int[] {Color.rgb(0, 0, 0),
                                                        Color.rgb(128, 0, 0),
                                                        Color.rgb(0, 128, 0),
                                                        Color.rgb(128, 128, 0),
                                                        Color.rgb(0, 0, 128),
                                                        Color.rgb(128, 0, 128),
                                                        Color.rgb(0, 128, 128),
                                                        Color.rgb(192, 192, 192)};
    /**
     * An array of bright intensity colors.
     */
    public static final int[] COLOR_BRIGHT = new int[] {Color.rgb(128, 128, 128),
                                                        Color.rgb(255, 0, 0),
                                                        Color.rgb(0, 255, 0),
                                                        Color.rgb(255, 255, 0),
                                                        Color.rgb(0, 0, 255),
                                                        Color.rgb(0, 0, 255),
                                                        Color.rgb(255, 0, 255),
                                                        Color.rgb(0, 255, 255),
                                                        Color.rgb(255, 255, 255)};
    /**
     * Default private constructor to prevent instantiation.
     */
    private SgrColor()
    {
    }

    public static GatewayColor getColors(@Size(min = 2, max = 2) String[] parameters)
    {
        int foregroundColor = -1, backgroundColor = -1;
        boolean foregroundBold = false, backgroundBold = false;
        boolean fGBright = false, bGBright = false;
        for (String parameter : parameters)
        {
            if (parameter.equals("0"))
            {
                foregroundColor = -1;
                backgroundColor = -1;
                backgroundBold = false;
                foregroundBold = false;
            }
            else if (parameter.equals("2"))
            {
                backgroundBold = true;
                foregroundBold = true;
            }
            else if (parameter.equals("22"))
            {
                backgroundBold = false;
                foregroundBold = false;
            }
            else if ((parameter.startsWith("3") || parameter.startsWith("4")) && parameter.length() == 2)
            {
                int color = Integer.parseInt(parameter.substring(1));

                if (color > COLOR_NORMAL.length - 1)
                {
                    break;
                }

                if (parameter.startsWith("3"))
                {
                    foregroundColor = color;
                }
                else if (parameter.startsWith("4"))
                {
                    backgroundColor = color;
                }
            }
            else if ((parameter.startsWith("9") && parameter.length() == 2) || (parameter.startsWith("10") && parameter.length() == 3))
            {
                int color = Integer.parseInt(parameter.substring(1));

                if (color > COLOR_BRIGHT.length - 1)
                {
                    break;
                }

                if (parameter.startsWith("9"))
                {
                    fGBright = true;
                    foregroundColor = color;
                }
                else if (parameter.startsWith("10"))
                {
                    bGBright = true;
                    backgroundColor = color;
                }
            }
        }
        return new GatewayColor(foregroundColor != -1 ? (!fGBright ? COLOR_NORMAL[foregroundColor] : COLOR_BRIGHT[foregroundColor]) : -1, backgroundColor != -1 ? (!bGBright ? COLOR_NORMAL[backgroundColor] : COLOR_BRIGHT[backgroundColor]) : -1, foregroundBold, backgroundBold);
    }

    public final static class GatewayColor
    {
        private int foreground, background;
        private boolean foregroundBold, backgroundBold;

        public GatewayColor(int foreground, int background, boolean foregroundBold, boolean backgroundBold)
        {
            this.foreground = foreground;
            this.background = background;
            this.foregroundBold = foregroundBold;
            this.backgroundBold = backgroundBold;
        }

        public int getForeground()
        {
            return foreground;
        }

        public int getBackground()
        {
            return background;
        }

        public boolean isForegroundBold()
        {
            return foregroundBold;
        }

        public boolean isBackgroundBold()
        {
            return backgroundBold;
        }
    }

}

