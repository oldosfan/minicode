#include <sys/stat.h>
#include <jni.h>
#include <zconf.h>

JNIEXPORT jint JNICALL
Java_low_minicode_N_chmod(JNIEnv *env, jclass type, jstring file_, jint mode)
{
    const char *file = (*env)->GetStringUTFChars(env, file_, 0);
    return chmod(file, (mode_t) mode);
}

JNIEXPORT jint JNICALL
Java_low_minicode_N_symlink(JNIEnv * env, jclass type, jstring a_, jstring b_)
{
    const char * a = (* env)->GetStringUTFChars(env, a_, 0);
    const char * b = (* env)->GetStringUTFChars(env, b_, 0);

    return symlink(a, b);
}

JNIEXPORT jint JNICALL
Java_low_minicode_N_null_1deref(JNIEnv * env, jclass type)
{
    char d = (* (char *) 0);
    return (int) d;
}