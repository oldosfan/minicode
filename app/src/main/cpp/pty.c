#include <stdlib.h>
#include <termios.h>

#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/unistd.h>
#include <sys/wait.h>

#include "pty.h"

typedef struct
{
    int     master;
    int     slave;
    int     cols;
    int     lines;
    char *  slave_path;
} pty;

typedef struct
{
    pid_t   pid;
    int     exit_code;
} process_result;

pty * open_pty(unsigned short cols, unsigned short lines)
{
    pty * result = malloc(sizeof(pty));
    result->master = posix_openpt(O_RDWR);
    if ((!result->master) || (grantpt(result->master) == -1)
        || (unlockpt(result->master) == -1))
        return NULL;
    result->slave_path = ptsname(result->master);
    if ((!result->slave_path) || ((result->slave = open(result->slave_path, O_RDWR)) == -1)) // NOLINT(android-cloexec-open)
        return NULL;
    result->cols = cols;
    result->lines = lines;
    
    struct winsize * size = malloc(sizeof(struct winsize));
    size->ws_col = cols;
    size->ws_row = lines;
    
    if (ioctl(result->slave, TIOCGWINSZ, size) == -1)
        return NULL;

    tcdrain(result->master);
    
    free(size);
    
    return result;
}

int resize_pty(struct winsize size, pty * tty)
{
    if (ioctl(tty->slave, TIOCGWINSZ, &size))
        return -1;

    return 0;
}

int exec_pty(pty * tty, char * exec, char ** argv, char ** envp, char * cwd)
{
    int pid;
    if (!(pid = fork()))
    {
        close(tty->master);
        setsid();
        
        dup2(tty->slave, 0);
        dup2(tty->slave, 1);
        dup2(tty->slave, 2);
        
        if (ioctl(tty->slave, TIOCSCTTY, 0))
            perror("ioctl");
        
        if (envp)
            for (; * envp; ++envp)
                putenv(* envp);
        if (cwd)
            if (chdir(cwd))
                perror("chdir");

        execvp(exec, argv);
        perror("execvp");
        
        exit(1);
        
        return -1;
    }
    else
    {
        return pid;
    }
}

ssize_t pty_remaining(pty * tty)
{
    int response;
    ssize_t result = 0;
    
    response = ioctl(tty->master, FIONREAD, &result);

    if (response != 0)
        return -1;
    
    return result;
}

int pty_setutf8(pty * tty)
{
    struct termios tios;
    tcgetattr(tty->slave, &tios);
    tios.c_iflag |= IUTF8;
    tios.c_iflag &= ~(IXON | IXOFF);
    tcsetattr(tty->slave, TCSANOW, &tios);
    return 0;
}

char * pty_read(pty * tty, size_t amount)
{
    char * buf = malloc(amount + 1);
    buf[amount] = 0;
    if (read(tty->master, buf, amount) < 0)
        return NULL;
    return buf;
}

ssize_t pty_write(pty * tty, size_t amount, char * data)
{
    return write(tty->master, data, amount);
}

process_result * wait_process_result()
{
    process_result * result = malloc(sizeof(process_result));
    int tmp_result;

    result->pid = wait(&tmp_result);

    if (WIFEXITED(tmp_result))
        result->exit_code = WEXITSTATUS(tmp_result);
    else
        result->exit_code = -1;

    return result;
}

void set_tty_erase(cc_t value, pty * tty)
{
    struct termios * terminfo = malloc(sizeof(struct termios));
    tcgetattr(tty->slave, terminfo);
    terminfo->c_cc[VERASE] = value;
    tcsetattr(tty->slave, 0, terminfo);
    tcgetattr(tty->master, terminfo);
    terminfo->c_cc[VERASE] = value;
    tcsetattr(tty->master, 0, terminfo);
}

int pty_is_echo(pty * tty)
{
    struct termios * term = malloc(sizeof(struct termios));
    tcgetattr(tty->master, term);
    return term->c_lflag & ECHO;
}