#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>

#include <jni.h>

#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <sys/wait.h>

#include "mpty.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"

void hacf(char *reason)
{
    perror(reason);
    exit(0);
}


/* !doc!
 * Returns a single instance of a pesudo-terminal as defined in "mpty.h".
 * May return null
 */
pty * mptyncl(int cols, int lines, int _ptflags_, int _ptflags_slave_)
{
    pty *pty = malloc(sizeof(pty));
    pty->master = posix_openpt(_ptflags_);
    if (!pty->master)
        return NULL;
    else if (grantpt(pty->master) == -1 || unlockpt(pty->master) == -1)
        return NULL;
    else
    {
        pty->slave_path = ptsname(pty->master);
        if (pty->slave_path == NULL)
            return NULL;
        pty->slave = open(pty->slave_path, _ptflags_slave_);
        if (!pty->slave_path)
            return NULL;
    }
    pty->lines = lines;
    pty->cols = cols;
    return pty;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wint-conversion"
/* !doc!
 * Returns a character, -1 when EOF. (this depends on the pty flags specified)
 */

char rpty(pty * _tty_)
{
    // signal(SIGSEGV, restore_buf_rpty);
    if (_tty_ == NULL)
    {
        printf("_tty_ null");
    }
    char * buf = malloc(sizeof(char));
    if (buf == NULL)
    {
        return -4;
    }
    if (_tty_ == NULL)
    {
        perror("null pointer deref _tty_");
        hacf("null pointer deref _tty_");
        return 0;
    }
    if (read(_tty_->master, buf, 1) < 1)
    {
        return -1;
    }
    return buf[0];
}
#pragma clang diagnostic pop

/* !doc!
 * Write 1 character to the pty _tty_
 */

int wpty(pty * _tty_, char _c_)
{
    return (int) write(_tty_->master, &_c_, 1);
}

/* !doc!
 * Write a string literal/char pointer to pty master
 */

int wstrpty(pty * _tty_, char *_str_)
{
    return (int) write(_tty_->master, _str_, strlen(_str_));
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCDFAInspection"

/* !doc!
 * Execute stuff in a pty
 */

int execpp(pty * _tty_, char * _command_, char ** _args_, char * _ld_lib_path_)
{
    if (_command_ == NULL || _args_ == NULL)
    {
        perror("null");
        hacf("null");
    }
    pid_t pid = fork();
    if (!pid)
    {
        close(_tty_->master);
        setsid();

        putenv(_ld_lib_path_);

        if (ioctl(_tty_->slave, TIOCSCTTY, NULL) == -1)
        {
            perror("ioctl");
            hacf("ioctl");
        }

        dup2(_tty_->slave, 0);
        dup2(_tty_->slave, 1);
        dup2(_tty_->slave, 2);

        close(_tty_->slave);
        int i = execvp(_command_, _args_);
        perror("execvp");
        dprintf(1, "error executing %s", _command_);
        exit(i);
        return i;
    }
    else
    {
        return pid;
    }
}

#pragma clang diagnostic pop

void closepty(pty * _tty_)
{
    close(_tty_->slave);
    close(_tty_->master);
    return;
}

JNIEXPORT jbyte JNICALL

Java_low_minicode_jna_PtyLibWrap_readDesPty(JNIEnv *env, jclass type, jint filedes)
{
    pty * tty = malloc(sizeof(pty));
    tty->master = filedes;
    return rpty(tty);
}

JNIEXPORT jlong JNICALL

Java_low_minicode_jna_PtyLibWrap_writeDes(JNIEnv *env, jclass type, jint filedes,
                                          jbyte byte)
{
    return write(filedes, &byte, 1);
}

JNIEXPORT jint JNICALL

Java_low_minicode_jna_PtyLibWrap_kill(JNIEnv *env, jclass type, jint pid, jint sig)
{
    return kill(pid, sig);
}

JNIEXPORT jint JNICALL

Java_low_minicode_jna_PtyLibWrap_waitp(JNIEnv *env, jclass type)
{
    return wait(NULL);
}

JNIEXPORT jobject JNICALL

Java_low_minicode_jna_PtyLibWrap_makeNewPty(JNIEnv *env, jclass type, jint cols,
                                            jint lines,
                                            jint flags_master, jint flags_slave)
{
    pty *ref = mptyncl(cols, lines, flags_master, flags_slave);
    jclass class = (*env)->FindClass(env, "low/minicode/NativePty");
    // int master, int slave, int cols, int lines, String slave_path
    jstring jstring1 = (*env)->NewStringUTF(env, ref->slave_path);
    jmethodID constructor = (*env)->GetMethodID(env, class, "<init>", "(IIIILjava/lang/String;)V");
    jobject object = (*env)->NewObject(env, class, constructor, ref->master, ref->slave, ref->cols,
                                       ref->lines, jstring1);
    return object;
}

JNIEXPORT jint JNICALL

Java_low_minicode_wrappers_terminalPtyLibWrap_waitNoHang(JNIEnv *env, jclass type)
{
    return waitpid(-1, NULL, WNOHANG);
}

// int execpp(pty * _tty_, char * _command_, char ** _args_, char * _ld_lib_path_);

#pragma clang diagnostic pop