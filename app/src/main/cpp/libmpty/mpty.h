/*
 * Copyright © 2018 Po Lu
 * See the COPYING document
 */

typedef struct
{
    int master;
    int slave;
    int cols;
    int lines;
    char * slave_path;
} pty;
