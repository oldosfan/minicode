#include <sys/mount.h>
#include <sys/errno.h>

#include <zconf.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <wait.h>

int bind(char * source, char * target)
{
    return mount(source, target, NULL, MS_BIND, NULL);
}

int unbind(char * location)
{
    return umount(location);
}

int main(int argc, char ** argv)
{
#define PROC argv[1]
#define SYS argv[2]
#define DEV argv[3]
#define TMP argv[4]
#define TARGET argv[5]
    if (getuid() != 0)
    {
        puts("must be root");
        return EPERM;
    }
    if (argc != 6)
    {
        printf("usage: %s <proc> <sys> <dev> <tmp> <target>\n", argv[0]);
        return EINVAL;
    }

    char * tmp_proc = malloc(strlen(TARGET) + 6);
    strcpy(tmp_proc, TARGET);
    strcat(tmp_proc, "/proc");

    char * tmp_tmp = malloc(strlen(TARGET) + 4);
    strcpy(tmp_tmp, TARGET);
    strcat(tmp_tmp, "/tmp");

    char * tmp_dev = malloc(strlen(TARGET) + 4);
    strcpy(tmp_dev, TARGET);
    strcat(tmp_dev, "/dev");

    char * tmp_sys = malloc(strlen(TARGET) + 4);
    strcpy(tmp_sys, TARGET);
    strcat(tmp_sys, "/sys");

    if (mount("proc", tmp_proc, "proc", 0, NULL))
    {
        perror("mount (proc)");
        return errno;
    }

    if (mount("sysfs", tmp_sys, "sysfs", 0, NULL))
    {
        perror("mount (sys)");
        if (unbind(tmp_proc))
            perror("umount");
        return errno;
    }

    if (bind(DEV, tmp_dev))
    {
        perror("mount (dev)");
        if (unbind(tmp_proc) || unbind(tmp_sys))
            perror("umount");
        return errno;
    }

    if (bind(TMP, tmp_tmp))
    {
        perror("mount (tmp)");
        if (unbind(tmp_proc) || unbind(tmp_sys) || unbind(tmp_dev))
            perror("umount");
        return errno;
    }

    pid_t pid = fork();
    if (!pid)
    {
        chroot(TARGET);
        chdir("/");
        execl("/bin/sh", "-sh");
        perror("/bin/sh");
        exit(errno);
    }
    else
    {
        int result = 0;
        waitpid(pid, &result, 0);

        if (unbind(tmp_proc) || unbind(tmp_sys) || unbind(tmp_dev) || unbind(tmp_tmp))
            perror("umount");

        if (WIFEXITED(result))
            return WEXITSTATUS(result);
        else if (WIFSIGNALED(result))
            return WTERMSIG(result);
        else if (WIFSTOPPED(result))
            return WSTOPSIG(result);
        else
            return 256;
    }
#undef PROC
#undef SYS
#undef DEV
#undef TMP
#undef TARGET
}