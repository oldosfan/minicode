#include <jni.h>

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <math.h>
#include <string.h>

#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <sys/wait.h>

#include <android/log.h>

#include <linux/errno.h>
#include <errno.h>

#include "term_handler.h"


unsigned int numdig(unsigned int a)
{
    return a == 0 ? 1 : (unsigned int) log10(a) + 1;
}


char *num_to_char_array(unsigned int i)
{
    char *r = malloc(numdig(i) + 2);
    snprintf(r, numdig(i), "%d", i);
    return r;
}

char *nstrcat(char *a, char *b)
{
    ssize_t lm = strlen(a) + strlen(b);
    return strncat(a, b, lm);
}

ptty *create_new_ptty(int cols, int lines)
{
    ptty *a = malloc(sizeof(ptty));
    a->fd_master = posix_openpt(O_RDWR);
    if (a->fd_master == -1)
        return NULL;
    else if ((grantpt(a->fd_master) || unlockpt(a->fd_master)) == -1)
        return NULL;
    else
    {
        a->slave_path = ptsname(a->fd_master);
        a->fd_slave = open(a->slave_path, O_RDWR);
    }
    if (a->fd_slave < 0)
        return NULL;

    a->cols = cols;
    a->lines = lines;
    __android_log_write(ANDROID_LOG_DEBUG, "create_new_pty()<native>m",
                        num_to_char_array((unsigned) a->fd_master));
    __android_log_write(ANDROID_LOG_DEBUG, "create_new_pty()<native>s",
                        num_to_char_array((unsigned) a->fd_slave));
    return a;
}

void set_utf8_mode(ptty *tty)
{
    struct termios ptymaster, ptyslave;
    tcgetattr(tty->fd_master, &ptymaster);
    tcgetattr(tty->fd_slave, &ptyslave);
    ptymaster.c_iflag |= IUTF8;
    ptyslave.c_iflag |= IUTF8;
}

pid_t create_process_on_slave(ptty *tty_ref, const char *args[], const char *prog)
{
    pid_t pid = fork();
    __android_log_write(ANDROID_LOG_DEBUG, "call", "create_process_on_slave (!native) called");
    // char * l = malloc(numdig(tty_ref->lines) + 1);
    // char * c = malloc(numdig(tty_ref->cols) + 1);
    // snprintf(l, numdig(tty_ref->lines), "%d", tty_ref->lines);
    // snprintf(c, numdig(tty_ref->cols), "%d", tty_ref->cols);
    // set_utf8_mode(tty_ref);
    if (pid == 0)
    {
        __android_log_write(ANDROID_LOG_DEBUG, "call",
                            "create_process_on_slave <inside fork? > setting $LINES and $COLS");
        // setenv("LINES", l, 1);
        // setenv("COLS", c, 1);
        __android_log_write(ANDROID_LOG_DEBUG, "call", "setting ttys");
        dup2(tty_ref->fd_slave, 0);
        dup2(tty_ref->fd_slave, 1);
        dup2(tty_ref->fd_slave, 2);
        __android_log_write(ANDROID_LOG_DEBUG, "call", "complete");
        return execvp(prog, args);
    } else
    {
        return pid;
    }
}

/*
 * This function is guaranteed to return a pointer to a *single* *empty-terminated* character, -1 if EOF, or
 * errno for non-blocking calls.
 * (ch[0])
 */

char *ptymread(ptty *ref)
{
    char *ch = malloc(1);
    int o = fcntl(ref->fd_master, F_GETFL, 0);
    fcntl(ref->fd_master, F_SETFL, o | O_NONBLOCK);
    __android_log_write(ANDROID_LOG_DEBUG, "tag", "readfromptty {INSIDE}");
    ssize_t r = read(ref->fd_master, ch, 1);
    fcntl(ref->fd_master, F_SETFL, o);
    __android_log_write(ANDROID_LOG_DEBUG, "tag", "readfromptty {OUTAGAIN}");
    char e = -1;
    if (r == 0)
        return &e;
    else if (r < 0)
        return NULL;
    return ch;
}


JNIEXPORT jobject JNICALL
Java_low_minicode_NTermEmulator_makeNewPtty(JNIEnv *env, jclass thisObject, jint cols, jint lines)
{
    jclass jpty = (*env)->FindClass(env, "low/minicode/Ptty");
    jmethodID constructor = (*env)->GetMethodID(env, jpty, "<init>", "(IIII)V");
    ptty *tty = create_new_ptty(cols, lines);
    if (tty == NULL)
        return (*env)->NewGlobalRef(env, NULL);
    jobject object = (*env)->NewObject(env, jpty, constructor, tty->cols, tty->lines,
                                       tty->fd_master, tty->fd_slave);
    return object;
}

JNIEXPORT jint JNICALL
Java_low_minicode_NTermEmulator_runProg(JNIEnv *env, jclass thisObject, jobjectArray args,
                                        jobject tty)
{
    long alloc = 1;
    const char **al = malloc(sizeof(char **));
    int size = (*env)->GetArrayLength(env, args);
    for (int i = 0; i < size; ++i)
    {
        al = realloc(al, alloc * sizeof(char **));
        if (al == NULL)
            return (jint) (*env)->NewGlobalRef(env, NULL);
        al[alloc - 1] = (*env)->GetStringUTFChars(env, (*env)->GetObjectArrayElement(env, args, i),
                                                  0);
        ++alloc;
    }
    ++alloc;
    al = realloc(al, alloc * sizeof(char **));
    if (al == NULL)
        return (jint) (*env)->NewGlobalRef(env, NULL);
    al[alloc - 1] = NULL;
    jfieldID id = (*env)->GetFieldID(env, (*env)->FindClass(env, "low/minicode/Ptty"), "fd_slave",
                                     "I");
    int jfd = (*env)->GetIntField(env, tty, id);
    ptty *pty = malloc(sizeof(ptty));
    pty->fd_slave = jfd;
    __android_log_write(ANDROID_LOG_DEBUG, "runProg(...)<jniNative>",
                        num_to_char_array((unsigned) pty->fd_slave));
    __android_log_write(ANDROID_LOG_DEBUG, "information", al[0]);
    return create_process_on_slave(pty, al, al[0]);
}

JNIEXPORT jint JNICALL
Java_low_minicode_NTermEmulator_killProg(JNIEnv *env, jclass type, jint pid, jint signal)
{
    kill(pid, signal);
    return errno;
}

JNIEXPORT jchar JNICALL
Java_low_minicode_NTermEmulator_readFromPtty(JNIEnv *env, jclass type, jobject pttya)
{
    jfieldID id = (*env)->GetFieldID(env, (*env)->FindClass(env, "low/minicode/Ptty"), "fd_master",
                                     "I");
    int jfd = (*env)->GetIntField(env, pttya, id);
    ptty *pty = malloc(sizeof(ptty));
    pty->fd_master = jfd;
    __android_log_write(ANDROID_LOG_DEBUG, "tag", "readfromptty called");
    char *result = ptymread(pty);
    __android_log_write(ANDROID_LOG_DEBUG, "tag", "readfromptty returned");
    if (result == NULL)
        return (jchar) -1;
    else if (result == -1)
        return (jchar) -2;
    return (jchar) (*result);
}
