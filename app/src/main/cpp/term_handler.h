#ifndef MINICODE_TERM_HANDLER_H
#define MINICODE_TERM_HANDLER_H

typedef struct
{
    int cols;
    int lines;
    int fd_master;
    int fd_slave;
    char *slave_path;
} ptty;


#endif //MINICODE_TERM_HANDLER_H
